#-------------------------------------------------
#
# Project created by QtCreator 2015-05-02T07:54:51
#
#-------------------------------------------------

QT       += core gui widgets network concurrent xml

greaterThan(QT_MAJOR_VERSION, 4): 

TARGET = cmdrevian
TEMPLATE = app

# Windows requires major, minor, patch level, and build number
VERSION = 0.9.4.0

# Define the preprocessor macro to get the application version in our application.
DEFINES += EVIAN_APP_VERSION=\\\"$$VERSION\\\"


SOURCES +=  3rdparty\format.cpp \
            3rdparty\jsmn.c \
            src\DatabaseQuery.cpp \
            src\ImportEDDB.cpp \
            src\aboutdialog.cpp \
            src\database.cpp \
            src\eddn.cpp \
            src\ednetlogparserqt.cpp \
            src\eddnondynamodb.cpp \
            src\evian.cpp \
            src\eviandatabaseexplorerqt.cpp \
            src\evianqueryqt.cpp \
            src\eviansettingsqt.cpp \
            src\evianutil.cpp \
            src\evianqt.cpp \
            src\flowlayout.cpp \
            src\main.cpp \
            src\mainwindow.cpp \
            src\model.cpp \
            src\querywidget.cpp \
            src\splashscreen.cpp \
            src\tradedetailwidget.cpp \
            src\evianeddnticker.cpp

HEADERS  += 3rdparty\format.h \
            3rdparty\jsmn.h \
            src\DatabaseQuery.hpp \
            src\ImportEDDB.hpp \
            src\aboutdialog.h \
            src\database.hpp \
            src\eddn.hpp \
            src\ednetlogparserqt.hpp \
            src\eddnondynamodb.hpp \
            src\evian.hpp \
            src\eviandatabaseexplorerqt.hpp \
            src\evianqueryqt.hpp \
            src\eviansettingsqt.hpp \
            src\evianutil.hpp \
            src\evianqt.hpp \
            src\flowlayout.hpp \
            src\mainwindow.hpp \
            src\model.hpp \
            src\querywidget.hpp \
            src\splashscreen.hpp \
            src\stdafx.h \
            src\targetver.h \
            src\tradedetailwidget.hpp \
            src\evianeddnticker.hpp


INCLUDEPATH += src \
               3rdparty \
               "C:\Program Files\ZeroMQ 4.0.4\include" \
               "c:\opt\winsparkle-0.4\include" \
               "c:\projects\libzmq\include"


win32-msvc2012:LIBS        += -L"C:\Program Files\ZeroMQ 4.0.4\lib"
win32-msvc2013:LIBS        += -L"C:\Program Files\ZeroMQ 4.0.4\lib"

CONFIG(debug, debug|release) {
	win32-msvc2015:LIBS += -L"C:\projects\libzmq\bin\x64\Debug\v140\dynamic"
    win32-msvc2012:LIBS += libzmq-v110-mt-gd-4_0_4.lib \
                           3rdparty\lib\Debug_110\zlib.lib
    win32-msvc2013:LIBS += libzmq-v120-mt-gd-4_0_4.lib \
                           3rdparty\lib\Debug_120\zlib.lib
    win32-msvc2015:LIBS += libzmq.lib \
                           3rdparty\lib\Debug_140\zlibstaticd.lib
                           
    #CONFIG += console    
    win32-msvc2012:EVIAN_INSTALL_DLLS.files += "C:\Program Files\ZeroMQ 4.0.4\bin\libzmq-v110-mt-gd-4_0_4.dll"
    win32-msvc2013:EVIAN_INSTALL_DLLS.files += "C:\Program Files\ZeroMQ 4.0.4\bin\libzmq-v120-mt-gd-4_0_4.dll"
    win32-msvc2015:EVIAN_INSTALL_DLLS.files += "C:\projects\libzmq\bin\x64\Debug\v140\dynamic\libzmq.dll"
    DESTDIR = debug
    
} else {
	win32-msvc2015:LIBS                     += -L"C:\projects\libzmq\bin\x64\Release\v140\dynamic"
    win32-msvc2012:LIBS                     += libzmq-v110-mt-4_0_4.lib \
                                               3rdparty\lib\Release_110\zlib.lib
    win32-msvc2013:LIBS                     += libzmq-v120-mt-4_0_4.lib \
                                               3rdparty\lib\Release_120\zlib.lib
	win32-msvc2015:LIBS                     += libzmq.lib \
                                               3rdparty\lib\Release_140\zlibstatic.lib
    CONFIG								    += suppress_vcproj_warnings # Do not warn about the below VS options.
	win32-msvc2013:QMAKE_CXXFLAGS_RELEASE   += /arch:AVX
	win32-msvc2015:QMAKE_CXXFLAGS_RELEASE   += /arch:AVX2
    QMAKE_CXXFLAGS_RELEASE                  += /Qvec-report:1
	win32-msvc2015:QMAKE_CXXFLAGS_RELEASE   += /Qpar-report:1
    QMAKE_CXXFLAGS_RELEASE                  += /Zi
    QMAKE_LFLAGS                            += /DEBUG
    
    win32-msvc2012:EVIAN_INSTALL_DLLS.files += "C:\Program Files\ZeroMQ 4.0.4\bin\libzmq-v110-mt-4_0_4.dll"
    win32-msvc2013:EVIAN_INSTALL_DLLS.files += "C:\Program Files\ZeroMQ 4.0.4\bin\libzmq-v120-mt-4_0_4.dll"
    win32-msvc2015:EVIAN_INSTALL_DLLS.files += "C:\projects\libzmq\bin\x64\Release\v140\dynamic\libzmq.dll"

    DESTDIR = release
}
LIBS += c:\opt\winsparkle-0.4\x64\release\WinSparkle.lib
EVIAN_INSTALL_DLLS.files += c:\opt\winsparkle-0.4\x64\release\WinSparkle.dll


# Copies the given files to the destination directory
defineTest(copyToDestdir) {
    files = $$1

    for(FILE, files) {
        DDIR = $$DESTDIR

        # Replace slashes in paths with backslashes for Windows
        win32:FILE ~= s,/,\\,g
        win32:DDIR ~= s,/,\\,g

        QMAKE_POST_LINK += $$QMAKE_COPY $$shell_quote($$FILE) $$shell_quote($$DDIR) $$escape_expand(\\n\\t)
    }

    export(QMAKE_POST_LINK)
}

package {
    DESTDIR = installer\packages\com.johanseland.cmdrevian\data
#QMAKE_POST_LINK += C:\Qt\QtIFW2.0.1\bin\binarycreator.exe -c installer\config\config.xml -p installer\packages $${TARGET}-$${VERSION}.exe
}

DEPLOY_TARGET = $$shell_quote($$shell_path($${DESTDIR}/$${TARGET}.exe))
copyToDestdir($$EVIAN_INSTALL_DLLS.files)
QMAKE_POST_LINK += $$shell_path($$[QT_INSTALL_BINS]\windeployqt.exe) $$DEPLOY_TARGET 

PRECOMPILED_HEADER = src\stdafx.h

DEFINES += EVIAN_HAVE_QT _CRT_SECURE_NO_WARNINGS _SCL_SECURE_NO_WARNINGS EVIAN_HAVE_ZMQ EVIAN_HAVE_WINSPARKLE

# Installs target is deprecated after starting to use QMAKE_POST_LINK. The latter also triggers in Visual Studio.

EVIAN_INSTALL_DLLS.path = $$DESTDIR
INSTALLS += EVIAN_INSTALL_DLLS

FORMS    += forms\aboutdialog.ui \
            forms\eviandatabaseexplorerqt.ui \
            forms\evianqueryqt.ui \
            forms\eviansettingsqt.ui \
            forms\mainwindow.ui \
            forms\querywidget.ui \
            forms\splashscreen.ui \
            forms\tradedetailwidget.ui \
            forms\evianeddnticker.ui

RESOURCES += \
            resources.qrc

# Hack to make the files visible in Visual Studio
GENERATED_FILES +=\
           cmdrevian.pro \
           installer\config\config.xml \
           installer\packages\com.johanseland.cmdrevian\meta\license.txt \
           installer\packages\com.johanseland.cmdrevian\meta\package.xml

#testlib {
#    # In order to be able to run the tests from VS test explorer it is best to just
#    # copy the required dlls to the output directory.
#    TEMPLATE = lib
#    SOURCES -= main.cpp
#    SOURCES += EvianQTTest\unittest1.cpp
#    TARGET = eviantestlib
#    INCLUDEPATH += "$(VCInstallDir)UnitTest\include"
#    LIBS += "-L$(VCInstallDir)UnitTest\lib"
#}
