#include "stdafx.h"
#include "CppUnitTest.h"
#include "evian.hpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

using namespace evian;
namespace EvianTest
{		
	TEST_CLASS(GridTest)
	{
	public:
		
		TEST_METHOD(TestMethod1)
		{
			Grid_c<System_c>
        cGrid;

      cGrid.SetBoundingBox({{0.0, 0.0, 0.0}, {10.0, 10.0, 10.0}});
      cGrid.SetCellSpacing(1);

      auto
        cGridCell = cGrid.IJKFromPosition({0.5, 0.5, 0.5});

      Int3 
        cGridCellExpected = {0,0,0}; 

      Assert::AreEqual(cGridCellExpected, cGridCell);
      
      Vec3f
        cPosition = {3.4f, 7.0f, 2.f};

      cGridCell = cGrid.IJKFromPosition(cPosition);
      
      auto
        cGridIndex = cGrid.IndexFromPosition(cPosition);
      
      auto
        cGridIndex2 = cGrid.IndexFromIJK(cGridCell);
         
      Assert::AreEqual(cGridIndex, cGridIndex2);

      auto
        cGridCell2 = cGrid.IJKFromIndex(cGridIndex);

      Assert::AreEqual(cGridCell, cGridCell2);


         
		}

	};
}