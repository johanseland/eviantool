![splash.png](https://bitbucket.org/repo/jrXAad/images/3159097211-splash.png)

CMDR Evians tool is a new desktop based trade tool that relies on data from EDDN.
It is almost ready for public release, but some late minute issues are still being worked on.

**The lack of a public release is due to a bug with Qt on Windows 10!**

See the [wiki](https://bitbucket.org/johanseland/eviantool/wiki/Home) for more details and download links.

It is designed to be very effective to use, both with regards to the computational time it takes to find trade routes, and to have a very effective GUI.
The tool is designed for experienced traders that known that Cr/Hr/Tn is the only metric that really matters, and CMDRs are expected to be able to fill their cargo hold with the most lucrative commodity for any trade route. (No attempt is made to fill your hold if you do not have enough CRs for the most lucrative commodity.)

Major Features:

* High-performance trade-engine. Matching all stations to every other station in the galaxy takes less than 10 seconds on a quad-core machine. Most “sane”-queries complete in much less than a second, and the query engine scales very effectively with the number of cores.
* Quickly find trades routes from subsets of all the stations. You can quickly find trade routes that stay Empire space at all costs, or find routes from Zemina Torval Control Systems to system exploited by Archon Delaine.
* Trade-routes “cards” track prices and profits on trade routes in real-time.
* Minimal but effective GUI, which allows for keeping unlimited number of windows open and arrange them freely.
* EDDN is tracked live, initial data import is from EDDB.io.