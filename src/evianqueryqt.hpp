//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef EVIANQUERYQT_HPP
#define EVIANQUERYQT_HPP

#include <QDockWidget>
#include "evian.hpp"

namespace Ui {
class EvianQueryQT;
}

class MainWindow;

class EvianQueryQT : public QWidget
{
    Q_OBJECT

public:
    explicit EvianQueryQT(MainWindow *parent = 0);
    ~EvianQueryQT();

signals:
    void ClearAllRestrictions();
    void ExcludeCommodity(QString);
    void ExcludeStation(QString, QString);
    void ExcludeSystem(QString);

private slots:
    void on_pushButton_clicked();
    void on_comboBoxQueryType_activated(const QString &arg1);
    void ContextMenuRequested(QPoint);

private:
    Ui::EvianQueryQT 
                *ui;
    MainWindow  *_pcMainWindow;
    
    QSortFilterProxyModel*
                _pcProxyModel;

    void HideQuerySpecificFrames();
};

EVIAN_BEGIN_NAMESPACE(evian)

class TradeDatabaseEngine_c;

class QueryExclusionDatabase_c : public QObject
{
  Q_OBJECT
public:
  QueryExclusionDatabase_c(TradeDatabaseEngine_c * pcTradeDatabaseEngine, QObject * pcQObject);

public slots:
  void AddExcludedStation(QString, QString); // System/Station
  void AddExcludedSystem(QString);
  void AddExcludedCommodity(QString);
  void AddExcludedFaction(QString);

  void AddIncludedCommodity(QString);
  void AddIncludedSystem(QString);
  void AddIncludedStation(QString, QString);
  void AddIncludedFaction(QString);

  void ClearAllExclusions();

signals:
  void StationExcluded(QString, QString); //System/Station
  void SystemExcluded(QString);
  void CommodityExcluded(QString);
  void FactionExcluded(QString);

public:
  TradeDatabaseEngine_c
                *_pcTradeDatabaseEngine;
  std::vector<int64_t>
                _lnExcludedSystems,
                _lnExcludedStations;
  std::array<char, Station_c::MAX_COMMODITIES>
                _lisCommodityExcluded;
  QStringList   _lsExcludedFactions;
};

EVIAN_END_NAMESPACE(evian)

#endif // EVIANQUERYQT_HPP
