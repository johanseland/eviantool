//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#pragma once

#include <string>
#include <atomic>
#include "evian.hpp"


namespace evian {
class TradeDatabaseStorage_c;

class DataBaseQueryResult_c 
{
public:
  virtual ~DataBaseQueryResult_c();
};

/// To be used as part of concurrency::combinable to get thread-local-storage.
class AlignedBuffer 
{
public:
  std::shared_ptr<int> 
                _pInt;

  AlignedBuffer() :
    _pInt(static_cast<int*>(_aligned_malloc(sizeof(int) * Station_c::MAX_COMMODITIES, Station_c::BLOCK_ALIGNMENT)), evian::AlignedDeleter_c<int>())
  {}
};


struct QueryRestriction_c
{
  float         _rMaxDistanceToStar,
                _rMaxDistance;

  int           _nMinSupply,
                _rMinProfit,
                _nMaxResults;

  bool          _isIgnoreStationsWithoutDistance,
                _isIntraSystemTradingIgnored,
                _isIgnorePlanetaryOutposts;

  Station_c::LandingPadSize
                _eMinLandingPadSize,
                _eMaxLandingPadSize;

  std::array<char, Station_c::MAX_COMMODITIES>
                _isCommodityExcluded;

  std::vector<int64_t>
                _lnExcludedStations,
                _lnExcludedSystems;

  QStringList   _lsExcludedFactions;

  QueryRestriction_c() :
    _rMaxDistanceToStar(std::numeric_limits<float>::max()),
    _rMaxDistance(30),
    _nMinSupply(250),
    _rMinProfit(100),
    _isIgnoreStationsWithoutDistance(true),
    _isIntraSystemTradingIgnored(true),
    _isIgnorePlanetaryOutposts(true),
    _eMinLandingPadSize(Station_c::LANDINGPAD_SMALL),
    _eMaxLandingPadSize(Station_c::LANDINGPAD_LARGE),
    _nMaxResults(25)
  {
    std::fill(begin(_isCommodityExcluded), end(_isCommodityExcluded), false);
  }

  bool IsSystemExcluded(const System_c& cSystem) const;
  bool IsStationExcluded(const Station_c& cStation) const;
  bool IsSystemTupleExcluded(const evian::System_c& cSystem0, const evian::System_c& cSystem1);
  bool IsStationTupleExcluded(const  Station_c& pcStation0, const  Station_c& pcStation1);
};

class BestTrade_c : public DataBaseQueryResult_c
{
public:
  int64_t     _nCommodityID,
              _nFromStationID,
              _nToStationID,
              _nFromSystemID,
              _nToSystemID,
              _nProfit;

  bool operator<(const BestTrade_c& cBestTrade1) const;
  bool operator>(const BestTrade_c& cBestTrade1) const;

  static BestTrade_c FindBestTradeBetweenStations(const Station_c& cStation0, const Station_c& cStation1);
  static BestTrade_c FindBestTradeBetweenStationsFast(const Station_c& cStation0, const Station_c& cStation1, QueryRestriction_c cQueryRestriction = QueryRestriction_c());
  static QList<BestTrade_c> FindSmugglingRouteBetweenStations(const Station_c& cStation0, const Station_c& cStation1, const TradeDatabaseStorage_c& cTradeDatabase, const QueryRestriction_c cQueryRestriction);

  static concurrency::combinable<AlignedBuffer>
                _cAlignedBuffer;

  std::string PrettyPrint(const TradeDatabaseStorage_c& cTradeDatabase) const;

  BestTrade_c(const int64_t& nCommodityID,
              const int64_t& nFromStationID,
              const int64_t& nToStationID,
              const int64_t& nFromSystemID,
              const int64_t& nToSystemID,
              const int64_t& nProfit) :
    _nCommodityID  (nCommodityID  ),
    _nFromStationID(nFromStationID),
    _nToStationID  (nToStationID  ),
    _nFromSystemID (nFromSystemID ),
    _nToSystemID   (nToSystemID   ),
    _nProfit       (nProfit       )
  {}

  BestTrade_c() :
    _nCommodityID(-1),
    _nFromStationID(-1),
    _nToStationID(-1),
    _nFromSystemID(-1),
    _nToSystemID(-1),
    _nProfit(0)
  {

  }
};

class SymmetricTrade_c : public DataBaseQueryResult_c
{
public:
  BestTrade_c   _cBestTrade0,
    _cBestTrade1;

  SymmetricTrade_c(const BestTrade_c& cBestTrade0, const BestTrade_c& cBestTrade1) :
    _cBestTrade0(cBestTrade0),
    _cBestTrade1(cBestTrade1)
  {}

  SymmetricTrade_c()
  {}

  bool operator<(const SymmetricTrade_c& cSymmetricTrade) const;
  bool operator>(const SymmetricTrade_c& cSymmetricTrade) const;

  int Profit() const;

  std::string PrettyPrint(const TradeDatabaseStorage_c& cTradeDatabase) const;
  std::string PrettyPrint2(const TradeDatabaseStorage_c& cTradeDatabase) const;
};


class DatabaseQuery_c
{
public:
  enum QueryStatus { QUERY_CREATED, QUERY_QUEUED, QUERY_PROCESSING, QUERY_COMPLETED, QUERY_CANCELLED };
  enum QueryType { QUERYTYPE_SYSTEM = 1, QUERYTYPE_SYSTEM_TO_SYSTEM = 2, QUERYTYPE_SYSTEM_WITH_STATION_TO_SYSTEM_WITH_STATION_SYMMETRIC = 4,
                   QUERYTYPE_INTERNAL_PROCESSING = 8};

  std::atomic<QueryStatus>
                _eQueryStatus;
  
  const QueryType
                _eQueryType;

  virtual std::string Description() const;
  
  DatabaseQuery_c(QueryType eQueryType = QUERYTYPE_INTERNAL_PROCESSING) :
    _eQueryStatus(QUERY_CREATED),
    _eQueryType(eQueryType)
  {}

  virtual ~DatabaseQuery_c();

  virtual void Combine();
  virtual int  GetNumberOfResults() const;

  virtual void operator()(const TradeDatabaseStorage_c& cTradeDatabase);
  virtual void operator()(const System_c& cSystem);
  virtual void operator()(const System_c& cSystem0, const System_c& cSystem1);
  
  virtual void SetQueryRestriction(const QueryRestriction_c& cQueryRestriction);

  QueryRestriction_c
                _cQueryRestriction;
};

class CommodityPriceAnalysisResult_c : public DataBaseQueryResult_c
{
public:
  int           _nCommodityID,
                _nPriceMax,
                _nPriceMin,
                _nPriceMedian,
                _nSamples,
                _nStationIDMin,
                _nStationIDMax;
  
  int64_t       _nPriceAverage;

  float         _rVariance,
                _rSigma;

  void AddPrice(int nPrice, int nStationID);
  void ComputeAverage();
  void PrepareFrequencyTable();
  void ComputeSigma();

  struct FrequencyTable_c {
    float 
      _rH,
      _rMin;

    std::vector<int>
      _lnFrequencyTable;

    void AddPrice(int nPrice);
  };

  FrequencyTable_c
                _cFrequencyTable;

  CommodityPriceAnalysisResult_c() :
    _nCommodityID(-1),
    _nPriceMax(std::numeric_limits<int>::min()),
    _nPriceMin(std::numeric_limits<int>::max()),
    _nPriceAverage(0),
    _nPriceMedian(0),
    _nSamples(0),
    _nStationIDMax(-1),
    _nStationIDMin(-1),
    _rVariance(0),
    _rSigma(0)
  {}
};

class CommodityPriceAnalysisQuery_c : public DatabaseQuery_c
{
public:
  CommodityPriceAnalysisQuery_c() :
    DatabaseQuery_c(QUERYTYPE_INTERNAL_PROCESSING)
    {}

  virtual void operator()(const TradeDatabaseStorage_c& cTradeDatabase);

  std::array<CommodityPriceAnalysisResult_c, Station_c::MAX_COMMODITIES>
                _lcCommodityPriceAnalysisBuy,
                _lcCommodityPriceAnalysisSell;
};

class CommodityMinMaxAnalysisQuery_c : public DatabaseQuery_c
{
public:
  CommodityMinMaxAnalysisQuery_c(int nCommodityID, int nSystemID) :
    DatabaseQuery_c(QUERYTYPE_INTERNAL_PROCESSING),
    _nCommodityID(nCommodityID),
    _nSystemID(nSystemID)
  {}
  
  virtual void operator()(const TradeDatabaseStorage_c& cTradeDatabase);

  QList<std::reference_wrapper<const evian::Listing_c>>
                _lcListingMinBuy,
                _lcListingMaxSell;
  
  int GetSystemID() const;

private:
  int           _nCommodityID,
                _nSystemID;
};


class CountSystemsWithStationsQuery : public DatabaseQuery_c
{
public:
  CountSystemsWithStationsQuery() :
    DatabaseQuery_c(DatabaseQuery_c::QUERYTYPE_SYSTEM),
    _nSystemsWithStations(0)
  {
  }

  std::string Description() const;
  void operator()(const System_c& cSystem) override;

private:
  int64_t       _nSystemsWithStations;
  BoundingBox_c _cBoundingBoxWithStations,
                _cBoundingBox;
};

class DirectionalTradeQuery_c : public DatabaseQuery_c
{
protected:
  DirectionalTradeQuery_c() :
    DatabaseQuery_c(DatabaseQuery_c::QUERYTYPE_INTERNAL_PROCESSING)
  {}
  
  int GetNumberOfResults() const
  {
    return static_cast<int>(_lcBestTrade.size());
  }

public:
  std::vector<BestTrade_c>
    _lcBestTrade;
};

class SymmetricTradeQuery_c : public DatabaseQuery_c
{
protected:
  SymmetricTradeQuery_c() :
    DatabaseQuery_c(DatabaseQuery_c::QUERYTYPE_INTERNAL_PROCESSING)
  {}

  int GetNumberOfResults() const
  {
    return static_cast<int>(_lcSymmetricTrade.size());
  }

public:
  std::vector<SymmetricTrade_c>
    _lcSymmetricTrade;
};

class DirectionalTradeQueryImpl_c : public DirectionalTradeQuery_c
{
public:
  void ComputeTrade(const evian::Station_c& cStation0, const evian::Station_c& cStation1, const QueryRestriction_c& cQueryRestriction, const TradeDatabaseStorage_c& cTradeDatabase);
  void Combine() override;

private:
  concurrency::combinable<std::vector<BestTrade_c>>
                _cCombineableBestTrade;
};

class SymmetricTradeQueryImpl_c : public SymmetricTradeQuery_c
{
public:
  void ComputeTrade(const evian::Station_c& cStation0, const evian::Station_c& cStation1, const QueryRestriction_c& cQueryRestriction, const TradeDatabaseStorage_c& cTradeDatabase);
  void Combine() override;

private:
  concurrency::combinable<std::vector<SymmetricTrade_c>>
                _cCombineableSymmetricTrade;
};

class SymmetricSmugglingQuery_c : public SymmetricTradeQuery_c
{
public:
  void ComputeTrade(const evian::Station_c& cStation0, const evian::Station_c& cStation1, const QueryRestriction_c& cQueryRestriction, const TradeDatabaseStorage_c& cTradeDatabase);
  void Combine() override;

private:
  concurrency::combinable<std::vector<SymmetricTrade_c>>
    _cCombineableSymmetricTrade;
};

class DirectionalSmugglingQuery_c : public DirectionalTradeQuery_c
{
public:
  void ComputeTrade(const evian::Station_c& cStation0, const evian::Station_c& cStation1, const QueryRestriction_c& cQueryRestriction, const TradeDatabaseStorage_c& cTradeDatabase);
  void Combine() override;

private:
  concurrency::combinable<std::vector<BestTrade_c>>
    _cCombineableBestTrade;
};

template<class T>
class GroupToGroupQuery_c : public T
{
public:
  typedef T super_type;

  GroupToGroupQuery_c(const QList<std::reference_wrapper<const evian::System_c>>& lcSystemFrom, const QList<std::reference_wrapper<const evian::System_c>>& lcSystemTo) :
    _lcSystemFrom(lcSystemFrom),
    _lcSystemTo(lcSystemTo)
  {
    qDebug() << "Setting up GroupToGroupQuery lcSystemFrom.size() " << lcSystemFrom.size() << ", lcSystemTo.size() " << lcSystemTo.size();
  }

  void operator()(const TradeDatabaseStorage_c& cTradeDatabase) override
  {
    auto
      cGrid = cTradeDatabase.CreateGrid(_lcSystemFrom, _lcSystemTo, _cQueryRestriction._rMaxDistance);

    concurrency::parallel_for_each(_lcSystemFrom.begin(), _lcSystemFrom.end(), [&](const evian::System_c& cSystem0)
    {
      if (_cQueryRestriction.IsSystemExcluded(cSystem0))
      {
        return;
      }

      auto
        cIJK = cGrid.IJKFromPosition(cSystem0._cPosition);

      for (int i = -1; i <= 1; ++i)
        for (int j = -1; j <= 1; ++j)
          for (int k = -1; k <= 1; ++k)
          {
            Int3
              cOffset(i, j, k),
              cActiveIJK = cIJK + cOffset;

            if (!cGrid.IsInsideGridIJK(cActiveIJK))
            {
              continue;
            }

            const auto&
              cGridCell = cGrid.GetGridCell(cActiveIJK);

            for (const System_c& cSystem1: cGridCell._lCellElement)
            {
              if (_cQueryRestriction.IsSystemTupleExcluded(cSystem0, cSystem1))
              {
                continue;
              }

              for(const auto pcStation0 : cSystem0._lpcStation)
              {
                for (const auto pcStation1: cSystem1._lpcStation)
                {
                  if (_cQueryRestriction.IsStationTupleExcluded(*pcStation0, *pcStation1))
                  {
                    continue;;
                  }
                  super_type::ComputeTrade(*pcStation0, *pcStation1, _cQueryRestriction, cTradeDatabase);
                }
              }
            }
          }
    });
  }

private:
  QList<std::reference_wrapper<const evian::System_c>>
    _lcSystemFrom,
    _lcSystemTo;
};

void BestTradeFromSystem(const System_c& cSystem0, const Grid_c<System_c>& cGrid, int nCellIndex, const Int3& cIJK, const QueryRestriction_c& cQueryRestriction, std::vector<SymmetricTrade_c>& lcSymmetricTrade, bool isIntraCellCullingPerfomed);

class BestSymmetricTrade_c : public SymmetricTradeQuery_c
{
public:
  BestSymmetricTrade_c(float rMaxDistance, bool bUseConcurrencyRuntime) :
    _bUseConcurrencyRuntime(bUseConcurrencyRuntime)
  {
    _cQueryRestriction._rMaxDistance = rMaxDistance;
    EVIAN_ASSERT(rMaxDistance > 0.0f);
  }

  void operator()(const TradeDatabaseStorage_c& cTradeDatabase) override;
  void Combine() override;

private:
  concurrency::combinable<AlignedBuffer>     /// To be used as TLS-scratch arrays and not to be combined.
                _cCombineableAlignedBuffer;

  bool          _bUseConcurrencyRuntime;

  concurrency::combinable<std::vector<SymmetricTrade_c>>
                _cCombineableSymmetricTrade;
};

} // namespace Evian