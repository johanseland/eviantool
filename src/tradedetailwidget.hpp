//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef TRADEDETAILWIDGET_HPP
#define TRADEDETAILWIDGET_HPP

#include <QWidget>
#include "DatabaseQuery.hpp"

class MainWindow;

namespace Ui {
class TradeDetailWidget_c;
}

QString FormatDurationSinceNow(QDateTime);
int ComputeNextUIRedraw(QList<QDateTime>);

class TradeDetailWidget_c : public QWidget
{
  Q_OBJECT

public:
  explicit TradeDetailWidget_c(MainWindow * pcMainWindow, QWidget *parent = 0);
  ~TradeDetailWidget_c();

  void SetDirectionalTrade(evian::BestTrade_c cDirectionalTrade);
  void SetSymmetricTrade(evian::SymmetricTrade_c cSymmetricTrade);

public slots:
    void UpdateListing(evian::Listing_c, int iRowHint);
    void UpdateCurrentSystem(QString cCurrentSystemName);

private slots:
    void UpdateUI();

protected:
  void mouseDoubleClickEvent(QMouseEvent *);
  void mousePressEvent(QMouseEvent * pcQMouseEvent);
  void mouseMoveEvent(QMouseEvent * pcQMouseEvent);

private:
  void UpdateUIImpl(const  evian::TradeDatabaseStorage_c &cTradeDatabase);

  QString FormatAllegianceString(const evian::System_c& cSystem) const;

  evian::Listing_c
                _cListing0Buy ,
                _cListing0Sell,
                _cListing1Buy ,
                _cListing1Sell;

  evian::System_c
                _cSystem0,
                _cSystem1;

  const evian::Station_c
                *_pcStation0,
                *_pcStation1;

  Ui::TradeDetailWidget_c *ui;
  QPoint        _cQPointOldPos;
  MainWindow    *_pcMainWindow;
  QPalette      _cQPalette;
  QTimer        _cQTimer;
};

#endif // TRADEDETAILWIDGET_HPP
