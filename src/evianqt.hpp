#pragma once

#include "evian.hpp"
#include <QApplication>

EVIAN_BEGIN_NAMESPACE(evian)

void            QtMessageHandler(QtMsgType eQtMsgType, const QMessageLogContext &QMessageLogContext, const QString &sMessage);
QVariant        InitAndSaveSettingsHelper(const QString& key, const QVariant& defaultValue);
QString         EvianUserAgentString();
void            AddDropShadowEffect(QWidget *pcQWidget);

EVIAN_END_NAMESPACE(evian)