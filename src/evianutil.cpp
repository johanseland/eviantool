//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#include "stdafx.h"
#include "evianutil.hpp"

bool 
evian::FileExists(std::string filename)
{
  struct stat   buffer;
  return (stat(filename.c_str(), &buffer) == 0);  
}

std::string
evian::GetCurrentTimeAsString()
{
  time_t
    cTime;

  tm *
    pcTm;

  time(&cTime);

  pcTm = localtime(&cTime);

  std::stringstream
    cStringStream;

  cStringStream.fill('0');

  cStringStream << std::setw(2) << pcTm->tm_hour << ":" << std::setw(2) << pcTm->tm_min << ":" << std::setw(2) << pcTm->tm_sec;

  return cStringStream.str();
}

std::vector<char> 
evian::ReadFileIntoBuffer(std::string cFileName)
{
  if (!FileExists(cFileName))
  {
    throw std::runtime_error("Could not open " + cFileName);
  }

  FILE *
    pFile = fopen(cFileName.c_str(), "r");
  
  fseek(pFile, 0, SEEK_END);
  auto
    nFileSize = ftell(pFile);
  fseek(pFile, 0, SEEK_SET);

  std::vector<char>
    cBuffer(nFileSize);

  fread(cBuffer.data(), 1, nFileSize, pFile);

  fclose(pFile);

  return cBuffer;
}
