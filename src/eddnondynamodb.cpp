#include "stdafx.h"
#include "evianqt.hpp"
#include "eddnondynamodb.hpp"

EVIAN_BEGIN_NAMESPACE(evian)

EDDNOnDynamoDB_c::EDDNOnDynamoDB_c(QNetworkAccessManager* pcQNetworkAccessManager,QObject *parent) :
  QObject(parent),
  _pcQNetworkAccessManager(pcQNetworkAccessManager),
  _nDownloadInterval(3000),
  _nMaxDownloadErrors(10),
  _nMinDownloadInterval(3000)
{
}

void
EDDNOnDynamoDB_c::DownloadDate(QDateTime cQDateTime, qint64 iFromOffset)
{
  // https://forums.frontier.co.uk/showthread.php?t=57986&page=48&p=2667390&viewfull=1#post2667390

  // Beta public API access can be found at: https://43h3di62h7.execute-api.eu-we...o=4567&limit=1 (and since it's in beta, URL is messy, there are no nice API docs etc. as yet)
  // 
  // Day should be in YYYY-MM-DD (all times & days reflect original EDDN gatewayTimestamp)
  // from (optional) should be epoch time in microseconds (inclusive)
  // to (optional) should be epoch time in microseconds (inclusive)
  // limit (optional) should be an integer
  // 
  // Assuming a 200 OK is returned, returned data is a JSON object with three properties:
  // Count - number of items returned
  // Items - array of raw EDDN JSON strings
  // LastEvaluatedTimestamp - optional timestamp indicating the last key returned if more data is available

  auto
    sURL        = InitAndSaveSettingsHelper(key_eddn_on_dynamodb_url, "https://43h3di62h7.execute-api.eu-west-1.amazonaws.com/beta/commodities/").toString(),
    sDate       = cQDateTime.toString("yyyy-MM-dd"), // Do not mix with "mm" which is minutes ;)
    sFrom       = QString("?from=%1").arg(iFromOffset),
    sRequest    = sURL + sDate + sFrom,
    sFormattedDate = cQDateTime.toString("yyyy-MMM-dd:hh.mm");
 
  QSettings
    qSettings;

  qSettings.setValue(key_eddn_on_dynamodb_last_update, cQDateTime);

  auto
    sMessage = QString("Requesting EDDN data for %1").arg(sFormattedDate);
  
  qDebug() << sMessage;
  emit UIMessage(sMessage);

  DownloadURL(sRequest);
}

QDateTime
QDateTimeFromMicroseconds(double rMicroseconds)
{
  qint64
    iLastEvaluatedTimestampMilliseconds = rMicroseconds / 1000.0;

  EVIAN_ASSERT(iLastEvaluatedTimestampMilliseconds > 0);

  return QDateTime::fromMSecsSinceEpoch(iLastEvaluatedTimestampMilliseconds);
}

void
EDDNOnDynamoDB_c::QNetworkReplyFinished()
{
  auto
    pcQNetworkReply = dynamic_cast<QNetworkReply*>(QObject::sender());

  EVIAN_ASSERT(pcQNetworkReply->isFinished());

  if (pcQNetworkReply->error() != QNetworkReply::NoError)
  {
    qDebug() << "Download error on url " << pcQNetworkReply->url().toString();
    emit UIMessage("Download error on url " +  pcQNetworkReply->url().toString());
  }
  else
  {
    _nDownloadErrors = 0;

    auto
      cQJsonDocument = QJsonDocument::fromJson(pcQNetworkReply->readAll());

    auto
      cQJsonObject = cQJsonDocument.object();

    if (cQJsonObject.contains("Count") && cQJsonObject.contains("Items") && cQJsonObject["Items"].isArray())
    {
      QString
        sMessage = QString("Got %1 EDDN messages").arg(cQJsonObject["Count"].toInt());

      bool
        isOutstandingMessage = cQJsonObject.contains("LastEvaluatedTimestamp");
      
      if (isOutstandingMessage)
      {
        sMessage += ", there are more messages on server.";
      }
      
      qDebug() << sMessage;
      emit UIMessage(sMessage);

      auto
        cQJsonArray = cQJsonObject["Items"].toArray();

      for (auto cQJsonValue: cQJsonArray)
      {
        emit EDDNMessage(QJsonDocument(cQJsonValue.toObject()));
      }

      if (isOutstandingMessage)
      {
        auto
          rLastEvaluatedTimeStampMicroSeconds = cQJsonObject["LastEvaluatedTimestamp"].toDouble();

        auto
          cQDateTime = QDateTimeFromMicroseconds(rLastEvaluatedTimeStampMicroSeconds);

        QTimer::singleShot(_nDownloadInterval, [this, cQDateTime, rLastEvaluatedTimeStampMicroSeconds]()
                {
                  qDebug() << "Triggering DownloadDate from QTimer.";
                  DownloadDate(cQDateTime, rLastEvaluatedTimeStampMicroSeconds + 1);
                });
      }
      else
      {
        emit UIMessage("Update complete");
        qDebug() << "DynamoDB download is finished.";
      }
      
    }
    else
    {
      qDebug() << "Does not seem like a valid response from EDDN on DynamoDB.";
    }
  }

  pcQNetworkReply->close();
  pcQNetworkReply->deleteLater();
}

void
EDDNOnDynamoDB_c::QNetworkReplyError(QNetworkReply::NetworkError eNetworkError)
{
  qDebug() << "Got network error when requesting data from EDDN on DynamoDB.";
  qDebug() << "Network error: " << eNetworkError;

  auto
    pcQNetworkReply = dynamic_cast<QNetworkReply*>(QObject::sender());
  
  if (pcQNetworkReply)
  {
    qDebug() << "Network error (errorString): " << pcQNetworkReply->errorString();
    emit UIMessage("Download error on url " +  pcQNetworkReply->url().toString());
    
    auto
      iHTTPStatusCode = pcQNetworkReply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    auto
      cByteArrayPhraseAttribue = pcQNetworkReply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toByteArray();
    auto
      cQUrl = pcQNetworkReply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl();

    qDebug() << "Status: " << iHTTPStatusCode << ", PhraseAttribute: " << cByteArrayPhraseAttribue << " RedirectURL: " << cQUrl.toString();

    for (const auto& cQPair : pcQNetworkReply->rawHeaderPairs())
    {
      qDebug() << cQPair.first << ": " << cQPair.second;
    }
    
    if (iHTTPStatusCode == 500 || iHTTPStatusCode == 429)
    {
      RetryDownloadURL(pcQNetworkReply->url().toString());
    }

    pcQNetworkReply->close();
    pcQNetworkReply->deleteLater();
  }
  else
  {
    EVIAN_ASSERT(false && "Should have a sender");
    qDebug() << "Network error without QNetworkReply";
  }
}

void
evian::EDDNOnDynamoDB_c::RetryDownloadURL(QString sUrl)
{
  qDebug() << "Retrying download of URL " << sUrl;
  _nDownloadInterval += 1000;
  _nDownloadErrors++;
  
  qDebug() << "Download interval increased to: " << _nDownloadInterval;

  if (_nDownloadErrors < _nMaxDownloadErrors)
  {
    QTimer::singleShot(_nDownloadInterval, [this,sUrl]()
    {
      qDebug() << "Triggering DownloadURL from QTimer.";
      DownloadURL(sUrl);
    });
  }
}

void
evian::EDDNOnDynamoDB_c::DownloadURL(QString sRequest)
{
  qDebug() << "Performing EDDN on DynamoDB-request for " << sRequest;

  QUrl
    cQUrl(sRequest);

  QNetworkRequest
    cQNetworkRequest(cQUrl);

  cQNetworkRequest.setHeader(QNetworkRequest::UserAgentHeader, evian::EvianUserAgentString());

  auto
    pcQNetworkReply = _pcQNetworkAccessManager->get(cQNetworkRequest);

  connect(pcQNetworkReply, SIGNAL(finished()),
          this,            SLOT(QNetworkReplyFinished()));

  connect(pcQNetworkReply, static_cast<void(QNetworkReply::*)(QNetworkReply::NetworkError)>(&QNetworkReply::error),
          this,             &EDDNOnDynamoDB_c::QNetworkReplyError);
}


EVIAN_END_NAMESPACE(evian)
