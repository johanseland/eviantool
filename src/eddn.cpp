//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "stdafx.h"
#include "eddn.hpp"
#include "evianqt.hpp"
#include "mainwindow.hpp"


EVIAN_BEGIN_NAMESPACE(eddn)

const char SchemaVersion::SchemaV1[] = "http://schemas.elite-markets.net/eddn/commodity/1";
const char SchemaVersion::SchemaV2[] = "http://schemas.elite-markets.net/eddn/commodity/2";


const char* pazMonitorAddress = "inproc://eddn-monitor.rep";
 
// Avoid zmq leaking out into public header.
struct ZMQResources
{
  zmq::context_t
                _cContext;
  zmq::socket_t
                _cSocket;

  ZMQResources() :
    _cContext(1),
    _cSocket(_cContext, ZMQ_SUB)
  {}

  ~ZMQResources()
  {
    qDebug() << __FUNCTION__;
  }
};

/// ZMQMonitor_c forwards messages back into Monitor_c
struct ZMQMonitor_c : public zmq::monitor_t
{
public:
  ZMQMonitor_c(Monitor_c& cMonitor) :
    _cMonitor(cMonitor)
  {}

  virtual void on_monitor_started() {}
  virtual void on_event_connected      (const zmq_event_t &/*event_*/, const char* /*addr_*/      ) { qDebug() << __FUNCTION__; }
  virtual void on_event_connect_delayed(const zmq_event_t &/*event_*/, const char* /*addr_*/      ) { qDebug() << __FUNCTION__; }
  virtual void on_event_connect_retried(const zmq_event_t &/*event_*/, const char* /*addr_*/      ) { qDebug() << __FUNCTION__; }
  virtual void on_event_listening      (const zmq_event_t &/*event_*/, const char* /*addr_*/      ) { qDebug() << __FUNCTION__; }
  virtual void on_event_bind_failed    (const zmq_event_t &/*event_*/, const char* /*addr_*/      ) { qDebug() << __FUNCTION__; }
  virtual void on_event_accepted       (const zmq_event_t &/*event_*/, const char* /*addr_*/      ) { qDebug() << __FUNCTION__; }
  virtual void on_event_accept_failed  (const zmq_event_t &/*event_*/, const char* /*addr_*/      ) { qDebug() << __FUNCTION__; }
  virtual void on_event_closed         (const zmq_event_t &/*event_*/, const char* /*addr_*/      ) { qDebug() << __FUNCTION__; }
  virtual void on_event_close_failed   (const zmq_event_t &/*event_*/, const char* /*addr_*/      ) { qDebug() << __FUNCTION__; }
  virtual void on_event_disconnected   (const zmq_event_t &/*event_*/, const char* /*addr_*/      ) { qDebug() << __FUNCTION__; }
  virtual void on_event_unknown        (const zmq_event_t &/*event_*/, const char* /*addr_*/      ) { qDebug() << __FUNCTION__; }
private:
  Monitor_c&    _cMonitor;
};


Monitor_c::Monitor_c(std::shared_ptr<ZMQResources> pcZMQResources, QObject *parent /*= 0*/) :
  QObject(parent),
  _pcZMQResources(pcZMQResources)
{
  
}

Monitor_c::~Monitor_c()
{
  qDebug() << __FUNCTION__;
}

void Monitor_c::Process()
{
  qDebug() << "Evian EDDN Subscriber Monitor running in thread: " << QThread::currentThreadId();

  ZMQMonitor_c
    cMonitor(*this);
  
  cMonitor.monitor(_pcZMQResources->_cSocket, pazMonitorAddress, ZMQ_EVENT_ALL);

  emit Finished();
}

eddn::SchemaVersion::Type 
eddn::SchemaVersion::GetSchemaVersion(const QJsonObject& cQJsonObject)
{
  const QString
    cSchemaRef = cQJsonObject["$schemaRef"].toString();

  if (cSchemaRef == SchemaVersion::SchemaV1)
  {
    return SchemaVersion::Commodity_V1;
  }
  else if (cSchemaRef == SchemaVersion::SchemaV2)
  {
    return SchemaVersion::Commodity_V2;
  }
  return SchemaVersion::Invalid;
}

const QStringList CommodityV1_c::Header_c::GetRequiredKeys()
{
  static QStringList
    cQStringList;

  if (cQStringList.isEmpty())
  {
    cQStringList << key::uploaderID
                 << key::softwareName
                 << key::softwareVersion
                 << key::gatewayTimestamp;
  }

  return cQStringList;
}

eddn::CommodityV1_c::Header_c 
CommodityV1_c::Header_c::ReadFromJSON(const QJsonObject& cQJsonObject)
{
  for (const auto& cKey : GetRequiredKeys())
  {
    if (!cQJsonObject.contains(cKey))
    {
      qDebug() << "Invalid EDDN Header object, missing "  << cKey;
    }
  }

  Header_c
    cHeader;

  cHeader._cUploaderID         = cQJsonObject[key::uploaderID].toString();
  cHeader._cSoftwareName       = cQJsonObject[key::softwareName].toString();
  cHeader._cSoftwareVersion    = cQJsonObject[key::softwareVersion].toString();  
  cHeader._cQDateTimeTimestamp = cQJsonObject[key::gatewayTimestamp].toVariant().toDateTime();
  cHeader._cQDateTimeTimestamp.setTimeSpec(Qt::UTC);
  
  //qDebug() << "Timestampstring: " << cQJsonObject[key::gatewayTimestamp].toString();
  //qDebug() << "setTimespec(Qt::UTC): " << cHeader._cQDateTimeTimestamp.toString();

  return cHeader;
}

void 
CommodityV1_c::Header_c::WriteToJSON(const Header_c& cHeader, QJsonObject& cQJsonObject)
{
  cQJsonObject[key::uploaderID]       = cHeader._cUploaderID;
  cQJsonObject[key::softwareName]     = cHeader._cSoftwareName;
  cQJsonObject[key::softwareVersion]  = cHeader._cSoftwareVersion;
  cQJsonObject[key::gatewayTimestamp] = cHeader._cQDateTimeTimestamp.toString(Qt::ISODate);
}

eddn::Subscriber::Subscriber(MainWindow * pcMainWindow, QObject *parent) :
  QObject(parent),
  _pcMainWindow(pcMainWindow),
  _bTerminate(false)
{
}

eddn::Subscriber::~Subscriber()
{
  qDebug() << __FUNCTION__;
}

void 
eddn::Subscriber::Terminate()
{
  // No mutex needed since we just read from the bool.
  _bTerminate = true;
}

QString
PrettyPrintZlibErrno(int nErrno)
{
  switch (nErrno)
  {
  case Z_OK:
    return "Z_OK";
  case Z_STREAM_END:
    return "Z_STREAM_END";
  case Z_NEED_DICT:
    return "Z_NEED_DICT";
  case Z_ERRNO:
    return "Z_ERRNO";
  case Z_STREAM_ERROR:
    return "Z_STREAM_ERROR";
  case Z_DATA_ERROR:
    return "Z_DATA_ERROR";
  case Z_MEM_ERROR:
    return "Z_MEM_ERROR";
  case Z_BUF_ERROR:
    return "Z_BUF_ERROR";
  case Z_VERSION_ERROR:
    return "Z_VERSION_ERROR";
  default:
    return "UNKNOWN_ERROR";
  }
}
void 
eddn::Subscriber::Process()
{
  qDebug() << "Evian EDDN Subscriber running in thread: " << QThread::currentThreadId();

  InitializeZeroMQ();
  //InjectEDDNTestdata();
  /*
  auto
    pcQThreadMonitor = new QThread();
  pcQThreadMonitor->setObjectName(QStringLiteral("Evian Subscriber Monitor Thread"));
  
  auto
    pcMonitor = new Monitor_c(_pcZMQResources);
  pcMonitor->moveToThread(pcQThreadMonitor);

  connect(pcQThreadMonitor, &QThread::started,
          pcMonitor, &eddn::Monitor_c::Process);
  
  connect(pcMonitor, &eddn::Monitor_c::Finished,
          pcMonitor, &eddn::Monitor_c::deleteLater);

  connect(pcMonitor, &eddn::Monitor_c::destroyed,
          pcQThreadMonitor, &QThread::quit);

  connect(pcQThreadMonitor, &QThread::finished,
          pcQThreadMonitor, &QThread::deleteLater);

  pcQThreadMonitor->start();
  */

  std::vector<unsigned char>
    cBuffer(1024 * 1024);

  auto
    cQDateTimeLastUpdate = QDateTime::currentDateTime();

  while (true) 
  {
    QApplication::processEvents();

    if (_bTerminate)
    {
      break;
    }

    zmq::message_t
      cMessage;

    try {
      if (!_pcZMQResources->_cSocket.recv(&cMessage)) { // Means no message was recieved in timeout;
        int
          nReconnectSeconds = evian::InitAndSaveSettingsHelper(key_eddn_reconnect_s, 60).toInt();

        if (cQDateTimeLastUpdate.secsTo(QDateTime::currentDateTime()) > nReconnectSeconds)
        {
          qDebug() << "Reconnecting to EDDN due to timeout.";
          InitializeZeroMQ();
          cQDateTimeLastUpdate = QDateTime::currentDateTime();
        }
        continue;
      }
    }
    catch (zmq::error_t& err)
    {
      qDebug() << "Error when receiving ZMQ-message " << err.what();
      emit Error(QString("Error when receiving ZMQ-message:") + err.what());
      continue;
    }

    unsigned long
      nReturnSize = static_cast<unsigned long>(cBuffer.size());

    auto
      nZlibErrno = uncompress(&cBuffer[0], &nReturnSize, static_cast<unsigned char*>(cMessage.data()), static_cast<unsigned long>(cMessage.size()));

    if (nZlibErrno != Z_OK)
    {
      auto
        sError = PrettyPrintZlibErrno(nZlibErrno);

      qDebug() << "Error when uncompressing ZMQ data: " << sError;
      emit Error(QString("Error when uncompressing ZMQ data: %1").arg(sError));
      continue;
    }

    cQDateTimeLastUpdate = QDateTime::currentDateTime();

    QByteArray
      cQByteArray(reinterpret_cast<char*>(&cBuffer[0]), nReturnSize);

    QJsonDocument
      cQJsonDocument = QJsonDocument::fromJson(cQByteArray);

    emit EDDNMessage(cQJsonDocument);
  }
  
  _pcZMQResources.reset(); // Make sure we clear the ZMQ resources in the same thread that created them.

  emit Finished();
}

void eddn::Subscriber::InjectEDDNTestdata()
{
  {
    QFile
      cQFile(":/assets/testdata/commodity-v2.0-draft.json");

    EVIAN_ASSERT(cQFile.open(QIODevice::ReadOnly));

    auto
      cQByteArray = cQFile.readAll();

    auto
      cQJsonDocument = QJsonDocument::fromJson(cQByteArray);

    emit EDDNMessage(cQJsonDocument);
  }
  {
    QFile
      cQFile(":/assets/testdata/commodity-v0.1.json");

    EVIAN_ASSERT(cQFile.open(QIODevice::ReadOnly));

    auto
      cQByteArray = cQFile.readAll();

    auto
      cQJsonDocument = QJsonDocument::fromJson(cQByteArray);

    emit EDDNMessage(cQJsonDocument);
  }
}

void 
eddn::Subscriber::InitializeZeroMQ()
{
  _pcZMQResources.reset(new ZMQResources());

  _cEDDNRelay = evian::InitAndSaveSettingsHelper(key_eddn_relay, "tcp://eddn-relay.elite-markets.net:9500").toString();
  _cQByteArray = _cEDDNRelay.toUtf8();
  
  int
    nZMQTimeout = evian::InitAndSaveSettingsHelper(key_eddn_timeout_ms, "1000").toInt();

  try 
  {
    _pcZMQResources->_cSocket.setsockopt(ZMQ_SUBSCRIBE, 0, 0);
    _pcZMQResources->_cSocket.setsockopt(ZMQ_RCVTIMEO, &nZMQTimeout, sizeof(int));
    _pcZMQResources->_cSocket.connect(_cQByteArray.constData());
    emit EDDNConnectionReady();

  } catch (zmq::error_t &cError)
  {
    qDebug() << "Could not connect to EDDN. " << cError.what();
  }
}

const QStringList 
CommodityV1_c::Message_c::GetRequiredKeys()
{
  QStringList
    cQStringList;

  cQStringList << key::systemName
               << key::stationName
               << key::itemName
               << key::stationStock
               << key::sellPrice
               << key::demand
               << key::timestamp;

  return cQStringList;
}

eddn::CommodityV1_c::Message_c 
CommodityV1_c::Message_c::ReadFromJSON(const QJsonObject& cQJsonObject)
{
  for (const auto& cKey : GetRequiredKeys())
  {
    if (!cQJsonObject.contains(cKey))
    {
      qDebug() << "Invalid EDDN Commodity Message V1 object, missing: " << cKey;
      qDebug() << QJsonDocument(cQJsonObject).toJson(QJsonDocument::Indented);
    }
  }
  
  Message_c
    cCommodityMessageV1;

  // Required fields
  cCommodityMessageV1._cSystemName         = cQJsonObject[key::systemName].toString();
  cCommodityMessageV1._cStationName        = cQJsonObject[key::stationName].toString();
  cCommodityMessageV1._cItemName           = cQJsonObject[key::itemName].toString();
  cCommodityMessageV1._nStationStock       = cQJsonObject[key::stationStock].toInt();
  cCommodityMessageV1._nSellPrice          = cQJsonObject[key::sellPrice].toInt();
  cCommodityMessageV1._nDemand             = cQJsonObject[key::demand].toInt();
  cCommodityMessageV1._cQDateTimeTimestamp = cQJsonObject[key::timestamp].toVariant().toDateTime();

  cCommodityMessageV1._nBuyPrice           = cQJsonObject[key::buyPrice].toInt();
  cCommodityMessageV1._eSupplyLevel        = SupplyLevelFromString(cQJsonObject[key::supplyLevel].toString());
  cCommodityMessageV1._eDemandLevel        = SupplyLevelFromString(cQJsonObject[key::demandLevel].toString());

  return cCommodityMessageV1;
}

CommodityV1_c::Message_c::SupplyLevel 
CommodityV1_c::Message_c::SupplyLevelFromString(const QString& cQString)
{
  if (cQString == QLatin1Literal("Low"))
  {
    return SupplyLevel_Low;
  }
  else if (cQString == QLatin1Literal("Med"))
  {
    return SupplyLevel_Medium;
  }
  else if (cQString == QLatin1Literal("High"))
  {
    return SupplyLevel_High;
  }
  else
  {
    return SupplyLevel_Invalid;
  }
}

QString
CommodityV1_c::Message_c::SupplyLevelToString(SupplyLevel eSupplyLevel)
{
  switch (eSupplyLevel)
  {
    case SupplyLevel_Low:
      return QLatin1Literal("Low");
    case SupplyLevel_Medium:
      return QLatin1Literal("Med");
    case SupplyLevel_High:
      return QLatin1Literal("High");
    default:
    case SupplyLevel_Invalid:
      return QLatin1Literal("Invalid");
  }
}

CommodityV2_c::Message_c
CommodityV2_c::Message_c::ReadFromJSON(const QJsonObject& cQJsonObject)
{
  Message_c
    cMessage;

  cMessage._cProperties._cSystemName = cQJsonObject["systemName"].toString();
  cMessage._cProperties._cStationName = cQJsonObject["stationName"].toString();
  cMessage._cProperties._cQDateTimeTimestamp = QDateTime::fromString(cQJsonObject["timestamp"].toString(), Qt::ISODate);

  const auto
    cQJsonArray = cQJsonObject["commodities"].toArray();

  for (const auto& cQJsonObject : cQJsonArray)
  {
    Item_c
      cItem;
    cItem._cName      = cQJsonObject.toObject()["name"].toString();
    cItem._nBuyPrice  = cQJsonObject.toObject()["buyPrice"].toInt();
    cItem._nSupply    = cQJsonObject.toObject()["supply"].toInt();
    cItem._nSellPrice = cQJsonObject.toObject()["sellPrice"].toInt();
    cItem._nDemand    = cQJsonObject.toObject()["demand"].toInt();

    cItem._eDemandLevel = CommodityV1_c::Message_c::SupplyLevelFromString(cQJsonObject.toObject()["demandLevel"].toString());
    cItem._eSupplyLevel = CommodityV1_c::Message_c::SupplyLevelFromString(cQJsonObject.toObject()["supplyLevel"].toString());
    
    cMessage._lcItem.push_back(std::move(cItem));
  }

  return cMessage;
}

eddn::CommodityV2_c CommodityV2_c::ReadFromJSON(const QJsonObject& cQJsonObject)
{
  CommodityV2_c
    cCommodityV2;
  
  auto
    cSchemaRef = cQJsonObject["$schemaRef"].toString();

  if (cSchemaRef != SchemaVersion::SchemaV2)
  {
    qDebug() << "Unknown schemaRef: " << cSchemaRef;
  }

  cCommodityV2._cHeader = Header_c::ReadFromJSON(cQJsonObject["header"].toObject());
  cCommodityV2._cMessage = Message_c::ReadFromJSON(cQJsonObject["message"].toObject());

  return cCommodityV2;
}

SchemaVersion::Type CommodityV2_c::GetSchemaVersion() const 
{
  return SchemaVersion::Commodity_V2;
}

SchemaVersion::Type CommodityV1_c::GetSchemaVersion() const 
{
  return SchemaVersion::Commodity_V1;
}

EVIAN_END_NAMESPACE(eddn)
