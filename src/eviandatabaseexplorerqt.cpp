//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "stdafx.h"
#include "eviandatabaseexplorerqt.hpp"
#include "ui_eviandatabaseexplorerqt.h"
#include "mainwindow.hpp"
#include "ednetlogparserqt.hpp"
#include "model.hpp"
#include "DatabaseQuery.hpp"
#include "flowlayout.hpp"
#include "querywidget.hpp"
#include "evianqt.hpp"

EvianDatabaseExplorerQt::EvianDatabaseExplorerQt(MainWindow *parent) :
    QWidget(parent),
    ui(new Ui::EvianDatabaseExplorerQt),
    _pcMainWindow(parent),
    _pcAbstractItemModel(0),
    _pcQSortFilterProxyModel(0),
    _eWidgetMode(WidgetMode_e::Table),
    _pcQCompleter(NULL),
    _pcCommodityMaxSellPriceModel(NULL),
    _pcCommodityMinBuyPriceModel(NULL)
{
    ui->setupUi(this);
    ui->groupBoxStationPricelist->hide();
    ui->groupBoxCommodityAnalysis->hide();
    ui->groupBoxProhibitedCommodities->hide();
    ui->groupBoxSystemProperties->hide();
    ui->groupBoxStationProperties->hide();
    ui->groupBoxCommodity->hide();

    ui->groupBoxProhibitedCommodities->setLayout(new FlowLayout(ui->groupBoxProhibitedCommodities));
    ui->groupBoxStationProperties->setLayout(new FlowLayout(ui->groupBoxStationProperties));
    ui->groupBoxSystemProperties->setLayout(new FlowLayout(ui->groupBoxSystemProperties));

    InitCompleter();

    connect(_pcMainWindow, &MainWindow::CurrentSystemChanged,
            ui->lineEditCommoditySystem, &QLineEdit::setPlaceholderText);

    connect(ui->lineEditFilter, &QLineEdit::textChanged,
            this, &EvianDatabaseExplorerQt::ConsiderUpdateFilterRegexp);
   
    ui->comboBox->setCurrentIndex(1);

    on_comboBox_activated(ui->comboBox->currentText());
}

EvianDatabaseExplorerQt::~EvianDatabaseExplorerQt()
{
    delete ui;
}

void EvianDatabaseExplorerQt::on_comboBox_activated(const QString &arg1)
{
  if (_pcAbstractItemModel)
  {
    delete _pcAbstractItemModel;
    _pcAbstractItemModel = 0;
  }

  if (_pcQSortFilterProxyModel)
  {
    delete _pcQSortFilterProxyModel;
    _pcQSortFilterProxyModel = 0;
  }

  disconnect(ui->lineEditFilter, &QLineEdit::textChanged,
             0, 0);

  ui->lineEditFilter->setCompleter(0);
  ui->lineEditFilter->clear();

  // All but min/max commodity use the filter through this signal.
  connect(ui->lineEditFilter, &QLineEdit::textChanged,
          this, &EvianDatabaseExplorerQt::ConsiderUpdateFilterRegexp);

  ui->groupBoxCommodityAnalysis->hide();
  ui->groupBoxStationPricelist->hide();
  ui->groupBoxProhibitedCommodities->hide();
  ui->groupBoxSystemProperties->hide();
  ui->groupBoxStationProperties->hide();
  ui->groupBoxCommodity->hide();
  ui->groupBoxSystemSubsetSelector->hide();

  auto
    lpcQPushButton = ui->groupBoxProhibitedCommodities->findChildren<QPushButton*>();

  for (auto pcQPushButton : lpcQPushButton)
  {
    pcQPushButton->deleteLater();
  }

  _eWidgetMode = WidgetMode_e::Table;

  if (arg1 == "Systems")
  {
    _pcAbstractItemModel = new model::SystemTableModel_c(model::SystemTableModel_c::Table_All_Systems, _pcMainWindow, _pcMainWindow);
    _pcQSortFilterProxyModel = new QSortFilterProxyModel(this);
    _pcQSortFilterProxyModel->setFilterKeyColumn(-1);
    setWindowTitle(arg1);
  }
  else if (arg1 == "Systems with Stations")
  {
    _pcAbstractItemModel = new model::SystemTableModel_c(model::SystemTableModel_c::Table_Systems_with_Stations, _pcMainWindow, _pcMainWindow);
    _pcQSortFilterProxyModel = new QSortFilterProxyModel(this);
    _pcQSortFilterProxyModel->setFilterKeyColumn(-1);
    setWindowTitle(arg1);
  }
  else if (arg1 == "System Subset")
  {
    ui->groupBoxSystemSubsetSelector->show();
    
    PopulateSystemSubsetSelector();

    auto
      pcSystemTableModel = new model::SystemTableModel_c(model::SystemTableModel_c::Table_Faction, _pcMainWindow, _pcMainWindow);

    _pcAbstractItemModel = pcSystemTableModel;
    _pcQSortFilterProxyModel = new QSortFilterProxyModel(this); // Revise this?
    _pcQSortFilterProxyModel->setFilterKeyColumn(-1);

    on_comboBoxSystemSubsetSelector_currentIndexChanged(ui->comboBoxSystemSubsetSelector->currentText());

  }
  else if (arg1 == "ED Logfile")
  {
    auto 
      cLogDirectory = evian::InitAndSaveSettingsHelper(key_fd_log_directory, "D:/Google Drive/Privat/EvianTool/EvianTool/Logs");

    auto
      cMostRecentLogFile = EDNetLogParserQT::FindMostRecentLogfile(cLogDirectory.toString());

    auto 
      lcEvianLogDatabaseItem = EDNetLogParserQT::ParseEDLogfile(cMostRecentLogFile);

    auto
      pEvianLogTableModel = new EvianLogTableModel_c(lcEvianLogDatabaseItem, this);
    
    _pcQSortFilterProxyModel = new QSortFilterProxyModel(this);

    connect(_pcMainWindow->_pcEDNetLogParserQT, &EDNetLogParserQT::DatabaseUpdated,
            pEvianLogTableModel, &EvianLogTableModel_c::DatabaseUpdated);

    connect(pEvianLogTableModel, &EvianLogTableModel_c::modelReset,
            _pcQSortFilterProxyModel, &QSortFilterProxyModel::invalidate);

    _pcAbstractItemModel = pEvianLogTableModel;
    _pcQSortFilterProxyModel->setFilterKeyColumn(EvianLogTableModel_c::Field_SystemName);
    setWindowTitle(arg1);
  }
  else if (arg1 == "Stations")
  {
    auto 
      _pcStationsTableModel = new StationsTableModel_c(_pcMainWindow, _pcMainWindow);

    _pcQSortFilterProxyModel = new QSortFilterProxyModel(this);;//new AcceleratedFilterProxyModel(_pcMainWindow, this);
    _pcQSortFilterProxyModel->setFilterKeyColumn(StationsTableModel_c::Field_Name);

    _pcAbstractItemModel = _pcStationsTableModel;

    //_pcQSortFilterProxyModel->setFilterKeyColumn(-1); // Sort on all columns to allow for searching on both station and system. Slow.
    setWindowTitle(arg1);
  }
  else if (arg1 == "Station Pricelist")
  {
    ui->groupBoxStationPricelist->show();
    ui->groupBoxStationProperties->show();
    ui->groupBoxProhibitedCommodities->show();
    ui->groupBoxSystemProperties->show();
    
    auto
      pcListingTableModel = new ListingTableModel_c(_pcMainWindow, _pcMainWindow);
    
    _pcAbstractItemModel = pcListingTableModel;

    connect(pcListingTableModel, &ListingTableModel_c::ListingUpdated,
            _pcMainWindow->_pcTradeDatabaseEngine, &evian::TradeDatabaseEngine_c::UpdateListing);

    connect(_pcMainWindow->_pcTradeDatabaseEngine, &evian::TradeDatabaseEngine_c::ListingUpdated,
            pcListingTableModel, &ListingTableModel_c::UpdateListing);

    connect(_pcMainWindow->_pcTradeDatabaseEngine, &evian::TradeDatabaseEngine_c::ListingAdded,
            pcListingTableModel, &ListingTableModel_c::AddListing);
    
    _pcQSortFilterProxyModel = new QSortFilterProxyModel(this);
    _pcQSortFilterProxyModel->setFilterKeyColumn(ListingTableModel_c::Field_Name);

    PriceListStationUpdatedLineEdit();
  }
  else if (arg1 == "EDDN")
  {
    auto
      pcEDDNItemModel = new model::EDDNItemModel_c(*_pcMainWindow->_pcTradeDatabaseEngine, _pcMainWindow);

    connect(_pcMainWindow->_pcTradeDatabaseEngine, &evian::TradeDatabaseEngine_c::EDDNV1CommodityMessagesAdded,
      pcEDDNItemModel, &model::EDDNItemModel_c::MessagesAdded);
    _pcQSortFilterProxyModel = new QSortFilterProxyModel(this);
    _pcQSortFilterProxyModel->setFilterKeyColumn(-1);
    _eWidgetMode = WidgetMode_e::Tree;
    _pcAbstractItemModel = pcEDDNItemModel;
    setWindowTitle(arg1);
  }
  else if (arg1 == "EDDN Hotspot")
  {
    auto
      pcEDDNHotspotModel = new model::EDDNHotspotModel_c(*_pcMainWindow->_pcTradeDatabaseEngine, _pcMainWindow);

    connect(_pcMainWindow->_pcTradeDatabaseEngine, &evian::TradeDatabaseEngine_c::EDDNV1CommodityMessagesAdded,
            pcEDDNHotspotModel, &model::EDDNHotspotModel_c::MessagesAdded);
    _pcQSortFilterProxyModel = new QSortFilterProxyModel(this);
    _pcQSortFilterProxyModel->setFilterKeyColumn(2);
    _eWidgetMode = WidgetMode_e::Table;
    _pcAbstractItemModel = pcEDDNHotspotModel;
    setWindowTitle(arg1);
  }
  else if (arg1 == "Commodities")
  {
    auto
      pcCommodityAnalysisModel = new model::CommodityAnalysisModel_c(_pcMainWindow);
    _pcQSortFilterProxyModel = new QSortFilterProxyModel(this);
    _pcQSortFilterProxyModel->setFilterKeyColumn(-1);
    _pcAbstractItemModel = pcCommodityAnalysisModel;
    ui->groupBoxCommodityAnalysis->show();
    setWindowTitle(arg1);
  }
  else if (arg1 == "Commodity")
  {
    ui->groupBoxCommodity->show();

    ui->lineEditCommoditySystem->setPlaceholderText(_pcMainWindow->GetCurrentSystem());

    QStringList
      cQStringList,
      cQStringListSystem;

    for (const auto& cCommodity : _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase()._lcCommodity)
    {
      cQStringList << cCommodity._cName;
    }
    
    auto
      pcQCompleter = new QCompleter(cQStringList, this);

    for (const auto& cSystem : _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase()._lcSystem)
    {
      cQStringListSystem << cSystem._cName;
    }
    
    // Existing _pcCommodityMaxSellPriceModel will have been deleted when _pcAbstracModel is deleted above.

    if (_pcCommodityMaxSellPriceModel == NULL && _pcCommodityMinBuyPriceModel == NULL) 
    {
      _pcCommodityMaxSellPriceModel = new model::CommodityMaxSellPriceModel_c(_pcMainWindow, this);
      _pcCommodityMinBuyPriceModel = new model::CommodityMinBuyPriceModel_c(_pcMainWindow, this);
      SetHeaderParameters(ui->tableView, _pcCommodityMinBuyPriceModel->columnCount(QModelIndex()));
      SetHeaderParameters(ui->tableView, _pcCommodityMaxSellPriceModel->columnCount(QModelIndex()));
    }

    on_comboBoxCommodityBuyOrSell_activated(ui->comboBoxCommodityBuyOrSell->currentText());

    EVIAN_ASSERT(_pcAbstractItemModel == NULL && "Commodity-mode bypasses _pcAbstractItemModel");

    _cCompleterListCache.SetQStringList(cQStringListSystem);

    disconnect(ui->lineEditFilter, &QLineEdit::textChanged,
               this, &EvianDatabaseExplorerQt::ConsiderUpdateFilterRegexp);

    connect(ui->lineEditFilter, &QLineEdit::editingFinished,
            this, &EvianDatabaseExplorerQt::UpdateActiveCommodity);

    connect(ui->lineEditCommoditySystem, &QLineEdit::textChanged,
            [this](const QString cQString)
            {
              CompleterListCache_c::ConsiderModifyCompleter(cQString, ui->lineEditCommoditySystem->completer(), ui->lineEditCommoditySystem, &_cCompleterListCache);
            });

    CompleterListCache_c::SetupQCompleter(pcQCompleter);

    ui->lineEditFilter->setCompleter(pcQCompleter);

    if (!cQStringListSystem.isEmpty())
    {
      ui->lineEditFilter->setText(cQStringList[0]);
    }

    ui->treeView->hide();
    ui->tableView->show();
    UpdateActiveCommodity();
  }
  else
  {
    qDebug() << "Unhandled argument " << arg1;
    return; // Avoid crashing.
  }

  if (_pcQSortFilterProxyModel != 0)
  {
    _pcQSortFilterProxyModel->setDynamicSortFilter(true);
    _pcQSortFilterProxyModel->setSourceModel(_pcAbstractItemModel);
    _pcQSortFilterProxyModel->setFilterCaseSensitivity(Qt::CaseInsensitive);
 
    switch (_eWidgetMode)
    {
      case WidgetMode_e::Table:
        ui->tableView->setModel(_pcQSortFilterProxyModel);
        SetHeaderParameters(ui->tableView, _pcQSortFilterProxyModel->columnCount());
        ui->treeView->hide();
        ui->tableView->show();
        break;
      case WidgetMode_e::Tree:
        ui->treeView->setModel(_pcQSortFilterProxyModel);
        SetHeaderParameters(ui->treeView, _pcQSortFilterProxyModel->columnCount());
        ui->treeView->show();
        ui->tableView->hide();
    }
  }
  else if (_pcAbstractItemModel != 0)
  {
    SetHeaderParameters(ui->tableView, _pcAbstractItemModel->columnCount());
    ui->tableView->setModel(_pcAbstractItemModel);
  }
}

void EvianDatabaseExplorerQt::ConsiderUpdateFilterRegexp(const QString & cQString)
{
  AcceleratedFilterProxyModel
    *pAcceleratedFilterProxyModel = dynamic_cast<AcceleratedFilterProxyModel*>(_pcQSortFilterProxyModel);

  if (pAcceleratedFilterProxyModel)
  {
    pAcceleratedFilterProxyModel->SetFilterString(cQString);
  }
  else
  {
    _pcQSortFilterProxyModel->setFilterWildcard(cQString);
  }
}

void EvianDatabaseExplorerQt::UpdateActiveCommodity()
{
  auto
    sCommodity = CompleterListCache_c::DeduceLocationFromCompleter(ui->lineEditFilter);

  const auto&
    cTradeDatabase = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase();

  const auto&
    nCommodityID = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetCommodityID(sCommodity);

  if (nCommodityID == -1)
  {
    setWindowTitle("Commodity unknown");
    return;
  }

  const auto
    sCommodityName = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetCommodityName(nCommodityID);

  auto
    nMinBuy = cTradeDatabase._lcCommodityPriceAnalysisBuy[nCommodityID]._nPriceMin,
    nMaxSell = cTradeDatabase._lcCommodityPriceAnalysisSell[nCommodityID]._nPriceMax,
    nMaxProfit = nMaxSell - nMinBuy,
    nStationMaxSell = cTradeDatabase._lcCommodityPriceAnalysisSell[nCommodityID]._nStationIDMax,
    nStationMinBuy  = cTradeDatabase._lcCommodityPriceAnalysisBuy[nCommodityID]._nStationIDMin;

  const auto
    &cStationMaxSell = cTradeDatabase.GetStation(nStationMaxSell),
    &cStationMinBuy  = cTradeDatabase.GetStation(nStationMinBuy);

  const auto&
    cSystemMaxSell = cTradeDatabase.GetSystem(cStationMaxSell._nSystemID);

  const auto&
    cSystemMinBuy = cTradeDatabase.GetSystem(cStationMinBuy._nSystemID);

  QLocale
    cQLocale(QLocale::English);

  ui->labelCommodityAverage->setText(  QString("%1 Cr").arg(cQLocale.toString(cTradeDatabase._lcCommodityPriceAnalysisBuy[nCommodityID]._nPriceAverage)));
  ui->labelCommodityMin->setText(      QString("%1 Cr (%2/%3)").arg(cQLocale.toString(nMinBuy)).arg(cStationMinBuy._cName).arg(cSystemMinBuy._cName));
  ui->labelCommodityMax->setText(      QString("%1 Cr (%2/%3)").arg(cQLocale.toString(nMaxSell)).arg(cStationMaxSell._cName).arg(cSystemMaxSell._cName));
  ui->labelCommodityMaxProfit->setText(QString("%1 Cr").arg(cQLocale.toString(nMaxProfit)));

  using namespace evian;
  
  auto
    sSystem = DeduceActiveCommoditySystem();
  
  auto
    nSystemID = cTradeDatabase.GetSystemID(sSystem);
  
  auto
    nRange    = ui->spinBoxCommodityRange->value();
  
  auto
    pcCommodityMinMaxAnalysisQuery = QSharedPointer<evian::CommodityMinMaxAnalysisQuery_c>(new evian::CommodityMinMaxAnalysisQuery_c(nCommodityID, nSystemID));

  pcCommodityMinMaxAnalysisQuery->_cQueryRestriction._rMaxDistance = nRange;
  pcCommodityMinMaxAnalysisQuery->_cQueryRestriction._eMinLandingPadSize = static_cast<Station_c::LandingPadSize>(ui->comboBoxCommodityMinLandingPad->currentIndex());

  _pcCommodityMaxSellPriceModel->SetDatabaseQuery(pcCommodityMinMaxAnalysisQuery);
  _pcCommodityMinBuyPriceModel->SetDatabaseQuery(pcCommodityMinMaxAnalysisQuery);

  auto
    pcQuery = new ExecuteQuery(this, pcCommodityMinMaxAnalysisQuery, *_pcMainWindow->_pcTradeDatabaseEngine);

  connect(pcQuery,                       &ExecuteQuery::QueryCompleted,
          _pcCommodityMaxSellPriceModel, &model::CommodityMaxSellPriceModel_c::ResultsReady);

  connect(pcQuery,                       &ExecuteQuery::QueryCompleted,
          _pcCommodityMinBuyPriceModel,  &model::CommodityMinBuyPriceModel_c::ResultsReady);
  
  connect(pcQuery,                       &ExecuteQuery::finished,
          pcQuery,                       &ExecuteQuery::deleteLater);

  pcQuery->start();
  ui->groupBoxCommodity->setTitle(sCommodityName);
  setWindowTitle(sCommodityName);
}

void
EvianDatabaseExplorerQt::PriceListStationUpdatedLineEdit()
{
  auto
    cQStringList = ui->lineEditStationSelector->text().split(QueryWidget::SPLIT);

  if (cQStringList.size() == 2)
  {
    auto
      nStationID = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetStationID(cQStringList[0].trimmed(), cQStringList[1].trimmed());

    if (nStationID != -1)
    {
      PriceListStationUpdated(nStationID);
      setWindowTitle(QString("Pricelist %1/%2").arg(cQStringList[0]).arg(cQStringList[1]));

    }
    else
    {
      _pcMainWindow->ShowStatusMessage(QString("Could not find station/system-combo"), MainWindow::Message_Warning);
      setWindowTitle("Pricelist unknown station/system");
    }
  }
}

void
EvianDatabaseExplorerQt::PriceListStationUpdated(int nStationID)
{
  ListingTableModel_c *
    pListingTableModel = dynamic_cast<ListingTableModel_c*>(_pcAbstractItemModel);

  auto
    lpcQPushButton = ui->groupBoxProhibitedCommodities->findChildren<QPushButton*>();

  for (auto pcQPushButton : lpcQPushButton)
  {
    pcQPushButton->deleteLater();
  }

  lpcQPushButton = ui->groupBoxStationProperties->findChildren<QPushButton*>();

  for (auto pcQPushButton : lpcQPushButton)
  {
    pcQPushButton->deleteLater();
  }

  lpcQPushButton = ui->groupBoxSystemProperties->findChildren<QPushButton*>();
  
  for (auto pcQPushButton : lpcQPushButton)
  {
    pcQPushButton->deleteLater();
  }

  if (pListingTableModel) 
  {
    pListingTableModel->SetStationID(nStationID);
    
    const auto&
      cStation = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetStation(nStationID);

    const auto&
      cSystem = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetSystem(cStation._nSystemID);
    
    CreateSystemTags(cSystem, ui->groupBoxSystemProperties);
    CrateProhibitedCommoditiesTags(cStation);
    CreateStationTags(cStation, ui->groupBoxStationProperties);
  }
}

void
EvianDatabaseExplorerQt::CrateProhibitedCommoditiesTags(const evian::Station_c &cStation)
{
  QIcon cQIcon(":/assets/icons/boxes6.svg");

  for (auto cCommodity : cStation._lcProhibitedCommodities)
  {
    auto
      pcQPushButton = new QPushButton(cQIcon, cCommodity, ui->groupBoxProhibitedCommodities);

    pcQPushButton->setProperty("evian_commodity", true);
    pcQPushButton->setProperty("evian_tag", true);

    ui->groupBoxProhibitedCommodities->layout()->addWidget(pcQPushButton);
  }
}

void
EvianDatabaseExplorerQt::CreateStationTags(const evian::Station_c &cStation, QGroupBox * pcQGroupBox)
{
  CreateTag(model::GetFactionIcon(cStation._sAllegiance), cStation._sAllegiance, "evian_allegiance", pcQGroupBox);
  CreateTag(cStation._sFaction, "evian_faction", pcQGroupBox);

  if (!cStation._sEconomicState.isEmpty() && cStation._sEconomicState != "None")
  {
    CreateTag(cStation._sEconomicState, "evian_economicstate", pcQGroupBox);
  }
  
  CreateTag(cStation._sGovernment, "evian_government", pcQGroupBox);

  for (auto sEconomy : cStation._lcEconomies)
  {
    CreateTag(sEconomy, "evian_economy", pcQGroupBox);
  }

  if (cStation._isBlackMarket)
  {
    CreateTag("Black Market", "evian_blackmarket", pcQGroupBox);
  }

  if (cStation._isOutfitting)
  {
    CreateTag("Outfitting", "evian_outfitting", pcQGroupBox);
  }

  if (cStation._isRearm)
  {
    CreateTag("Rearm", "evian_rearm", pcQGroupBox);
  }

  if (cStation._isRefuel)
  {
    CreateTag("Refuel", "evian_refuel", pcQGroupBox);
  }

  if (cStation._isRepair)
  {
    CreateTag("Repair", "evian_repair", pcQGroupBox);
  }

  if (cStation._isShipyard)
  {
    CreateTag("Shipyard", "evian_shipyard", pcQGroupBox);
  }
}

bool AcceleratedFilterProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex & /* sourceParent*/) const
{
  // This bypasses going through QVariant...

  const auto&
    cStation = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase()._lcStation[sourceRow];

  const auto&
    cSystem = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetSystem(cStation._nSystemID);

  const evian::EvianFilterHash
    &cStationNameHash = cStation._cNameHash,
    &cSystemNameHash = cSystem._cNameHash;
  
  if (_cEvianFilterHash.any() && 
      ((cStationNameHash & _cEvianFilterHash).none() && (cSystemNameHash & _cEvianFilterHash).none()))
  {
    return false;
  }

  return cStation._cName.contains(filterRegExp()) || cSystem._cName.contains(filterRegExp());
}

void AcceleratedFilterProxyModel::SetFilterHash(const evian::EvianFilterHash& cEvianFilterHash)
{
  _cEvianFilterHash = cEvianFilterHash;
}

void
AcceleratedFilterProxyModel::SetFilterString(const QString& cQString)
{
  int 
    nMinFilterStringLength = evian::InitAndSaveSettingsHelper(key_dbexplorer_min_filter_length, 3).toInt();

  if (_cQStringFilter.length() < nMinFilterStringLength &&
      cQString.length()        < nMinFilterStringLength)
  {
    // Do nothing
  } 
  else if (cQString.length() < nMinFilterStringLength &&
           _cQStringFilter.length() >= nMinFilterStringLength)
  {
    //invalidateFilter();
    setFilterWildcard(QString());
  } 
  else
  {
    SetFilterHash(ComputeStringHash(cQString));
    setFilterWildcard(cQString);
  }

  _cQStringFilter = cQString;
}

QString AcceleratedFilterProxyModel::GetFilterString() const
{
  return _cQStringFilter;
}

void EvianDatabaseExplorerQt::on_toolButtonCurrent_clicked()
{
  auto
    nSystemID = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetSystemID(_pcMainWindow->GetCurrentSystem());

  if (nSystemID == -1)
  {
    _pcMainWindow->statusBar()->showMessage("Current system does not exist in trade-database");
    return;
  }
  
  const auto&
    cSystem = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetSystem(nSystemID);

  if (cSystem._lpcStation.size() == 0)
  {
    _pcMainWindow->statusBar()->showMessage("Current system does have any stations");
    return;
  }
 
  const auto&
    lcSystemWithStation = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase()._lcSystemWithStation;

  auto
    it = std::lower_bound(lcSystemWithStation.begin(), lcSystemWithStation.end(), cSystem);

  EVIAN_ASSERT(it != lcSystemWithStation.end() && "Since the system has station(s) it should be in lcSystemWithStation");
  EVIAN_ASSERT(it->get()._cName == cSystem._cName);

 /* auto
    nPos = std::distance(lcSystemWithStation.begin(), it);*/
  // We cannot deduce the station from the system, so we just pick the first one.
  ui->lineEditStationSelector->setText(it->get()._cName + "/" + cSystem._cName);
}

void
EvianDatabaseExplorerQt::SetHeaderParameters(QTableView* pcQTableView, int /*columnCount*/)
{
  pcQTableView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
  //pcQTableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Interactive);
  pcQTableView->horizontalHeader()->setSectionsMovable(true);
  //pcQTableView->horizontalHeader()->setStretchLastSection(false);
  //pcQTableView->horizontalHeader()->setDefaultSectionSize(pcQTableView->width() / columnCount);
  
  /*for (int i = 0; i < columnCount; ++i)
  {
    pcQTableView->horizontalHeader()->resizeSection(i, pcQTableView->width()/columnCount);
  }*/
  pcQTableView->horizontalHeader()->setStretchLastSection(true);
  //pcQTableView->horizontalHeader()->setCascadingSectionResizes(true);
  //qDebug() << "pcQTableView->width() " << pcQTableView->width();
}

void
EvianDatabaseExplorerQt::SetHeaderParameters(QTreeView* pcQTreeView, int columnCount)
{
  pcQTreeView->header()->setSectionResizeMode(QHeaderView::ResizeToContents);
  pcQTreeView->header()->setSectionsMovable(true);
  pcQTreeView->header()->setStretchLastSection(true);
  pcQTreeView->header()->setCascadingSectionResizes(true);
  pcQTreeView->header()->setDefaultSectionSize(pcQTreeView->width() / columnCount);
}

void
EvianDatabaseExplorerQt::on_pushButton_clicked()
{
  auto
    pcCommodityPriceAnalysis = QSharedPointer<evian::CommodityPriceAnalysisQuery_c>(new evian::CommodityPriceAnalysisQuery_c());

  _pcMainWindow->_lcDatabaseQuery.append(pcCommodityPriceAnalysis);
  
  auto
    pcQuery = new ExecuteQuery(this, pcCommodityPriceAnalysis, *_pcMainWindow->_pcTradeDatabaseEngine);

  connect(pcQuery, &ExecuteQuery::ElapsedTimeMessage, _pcMainWindow->statusBar(), &QStatusBar::showMessage);

  auto
    pcCommodityAnalysisModel = dynamic_cast<model::CommodityAnalysisModel_c*>(_pcAbstractItemModel);

  EVIAN_ASSERT(pcCommodityPriceAnalysis);

  pcCommodityAnalysisModel->SetDatabaseQuery(pcCommodityPriceAnalysis);

  connect(pcQuery,                  &ExecuteQuery::QueryCompleted, 
          pcCommodityAnalysisModel, &model::CommodityAnalysisModel_c::ResultsReady);

  connect(pcQuery,                  &ExecuteQuery::ElapsedTimeMessage, 
          _pcMainWindow->statusBar(), &QStatusBar::showMessage);

  pcQuery->start();
}

void
EvianDatabaseExplorerQt::InitCompleter()
{
  _lcQStringSystemsAndStations.clear();
  
  if (_pcQCompleter != NULL)
  {
    _pcQCompleter->deleteLater();
  }

  _pcQCompleter = new QCompleter(this);

  for(const auto& cSystem: _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase()._lcSystemWithStation)
  {
    for (const auto cStation : cSystem.get()._lpcStation)
    {
      _lcQStringSystemsAndStations << QString("%1 / %2").arg(cStation->_cName).arg(cSystem.get()._cName);
    }
  }

   _pcQCompleter = new QCompleter(_lcQStringSystemsAndStations, this);
   CompleterListCache_c::SetupQCompleter(_pcQCompleter);

   ui->lineEditStationSelector->setCompleter(_pcQCompleter);
   ui->lineEditStationSelector->setText("Lave Station/Lave");
}

QString
EvianDatabaseExplorerQt::DeduceActiveCommoditySystem()
{
  auto 
    sSystem = ui->lineEditCommoditySystem->text();
  if (sSystem.isEmpty())
  {
    sSystem = ui->lineEditCommoditySystem->placeholderText();
  }

  return sSystem;
}

void
EvianDatabaseExplorerQt::CreateSystemTags(const evian::System_c& cSystem, QGroupBox * pcQGroupBox)
{
  QLocale
    cQLocale(QLocale::English);

  CreateTag(model::GetFactionIcon(cSystem._sAllegiance), cSystem._sAllegiance, "evian_allegiance", pcQGroupBox);
  
  if (!cSystem._sPowerControlFaction.isEmpty())
  {
    CreateTag(model::GetPowerIcon(cSystem), cSystem._sPowerControlFaction, "evian_powercontrol", pcQGroupBox);
  }

  for (const auto& cExploitedBy : cSystem._lcExploitedBy)
  {
    CreateTag(model::GetFactionIcon(cExploitedBy._sPowerControlFaction), QString("Exploited: %1 (%2)").arg(cExploitedBy._sPowerControlFaction).arg(cExploitedBy._sControlSystem),
              "evian_powercontrol", pcQGroupBox);
  }
  
  CreateTag(cSystem._sMinorFaction, "evian_minorfaction", pcQGroupBox);
  CreateTag(cSystem._sGovernment,   "evian_government", pcQGroupBox);
  
  if (cSystem._nPopulation > 0)
  {
    CreateTag(QString("Pop: %1").arg(cQLocale.toString(cSystem._nPopulation)), "evian_population", pcQGroupBox);
  }

  CreateTag(cSystem._sPrimaryEconomy,                              "evian_economy",    pcQGroupBox);
  CreateTag(QString("Security: %1").arg(cSystem._sSecurity),       "evian_security",   pcQGroupBox);
  
  if(cSystem._sEconomicState != "None")
  {
    CreateTag(QString("Economic State: %1").arg(cSystem._sEconomicState),          "evian_state",      pcQGroupBox);
  }
}

void
EvianDatabaseExplorerQt::CreateTag(QString sDisplay, QString sTagName, QGroupBox * pcQGroupBox)
{
  if (sDisplay.isEmpty())
  {
    return;
  }

  auto
    pcQPushButton = new QPushButton(sDisplay, pcQGroupBox);

  pcQPushButton->setProperty(sTagName.toLatin1().data(), true);
  pcQPushButton->setProperty("evian_tag", true);

  pcQGroupBox->layout()->addWidget(pcQPushButton);
}

void
EvianDatabaseExplorerQt::CreateTag(QIcon cQIcon, QString sDisplay, QString sTagName, QGroupBox* pcQGroupBox)
{
  if (sDisplay.isEmpty())
  {
    return;
  }

  auto
    pcQPushButton = new QPushButton(cQIcon, sDisplay, pcQGroupBox);

  pcQPushButton->setProperty(sTagName.toLatin1().data(), true);
  pcQPushButton->setProperty("evian_tag", true);

  pcQGroupBox->layout()->addWidget(pcQPushButton);
}

void
EvianDatabaseExplorerQt::PopulateSystemSubsetSelector()
{
  ui->comboBoxSystemSubsetSelector->blockSignals(true);

  while (ui->comboBoxSystemSubsetSelector->count() > 0)
  {
    ui->comboBoxSystemSubsetSelector->removeItem(0);
  }

  for(const auto& sFaction : _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase()._cFactionToSystemMap.keys())
  {
    ui->comboBoxSystemSubsetSelector->addItem(sFaction);
  }

  ui->comboBoxSystemSubsetSelector->blockSignals(false);
}

void
EvianDatabaseExplorerQt::on_lineEditStationSelector_editingFinished()
{
  PriceListStationUpdatedLineEdit();
}

void
EvianDatabaseExplorerQt::on_spinBoxCommodityRange_editingFinished()
{
  UpdateActiveCommodity();
}

void
EvianDatabaseExplorerQt::on_lineEditCommoditySystem_editingFinished()
{
  UpdateActiveCommodity();
}

void EvianDatabaseExplorerQt::on_comboBoxCommodityBuyOrSell_activated(const QString &arg1)
{
  if (arg1 == "Buy")
  {
   ui->tableView->setModel(_pcCommodityMinBuyPriceModel);
  }
  else
  {
    EVIAN_ASSERT(arg1 == "Sell");
    ui->tableView->setModel(_pcCommodityMaxSellPriceModel);
  }
}

void
EvianDatabaseExplorerQt::on_comboBoxCommodityMinLandingPad_currentIndexChanged(int /*index*/)
{
  UpdateActiveCommodity();
}

void
EvianDatabaseExplorerQt::on_comboBoxSystemSubsetSelector_currentIndexChanged(const QString &arg1)
{
  auto
    pcSystemTableModel = dynamic_cast<model::SystemTableModel_c*>(_pcAbstractItemModel);

  if (pcSystemTableModel)
  {
    pcSystemTableModel->SetFaction(arg1);
  }
  else
  {
    EVIAN_ASSERT(false && "This slot should not be triggered unless a systemTableModel is active.");
  }
}
