//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#pragma once

#include <string>
#include <sys/stat.h>
#include <vector>

#define EVIANLOG(msg) \
  std::cout << GetCurrentTimeAsString() << GetCurrentTimeAsString() << " -- " <<  __FUNCTION__ << ":" << __LINE__  << " -- " << msg << std::endl; \
  std::cout.flush();

#define EVIAN_BREAKPOINT \
  do                     \
  {                      \
    __debugbreak();      \
  } while(0)             \

#define EVIAN_ASSERT(isExpression)                                           \
  do                                                                         \
  {                                                                          \
    if (!(isExpression))                                                     \
    {                                                                        \
      EVIAN_BREAKPOINT;                                                      \
      std::cerr << "Assertion failed " << __FILE__ << __LINE__ << std::endl; \
      assert(isExpression);                                                  \
    }                                                                        \
  } while(0)                                                                 \

namespace evian {

bool
FileExists(std::string filename);

std::string
GetCurrentTimeAsString();

template<class T>
struct AlignedDeleter_c
{
  void operator()(T* pT)
  {
    _aligned_free(pT);
  }
};

std::vector<char>
ReadFileIntoBuffer(std::string cFileName);

// Calls the provided work function and returns the number of milliseconds  
// that it takes to call that function. 
template <class Function>
int64_t time_call(Function&& f)
{
  auto 
    nBegin = GetTickCount64();
  
  f();
  
  return GetTickCount64() - nBegin;
}

template<class Function>
int64_t TimeCall(Function&& f, std::string cDescription, std::ostream& cOstream)
{
  auto
    nElapsed = time_call(f);
  cOstream << cDescription << " completed in " << nElapsed << " ms.\n";
}
} // namespace evian