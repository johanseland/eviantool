//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "database.hpp"
#include "DatabaseQuery.hpp"
#include "tradedetailwidget.hpp"
#include "mainwindow.hpp"
#include "evianqt.hpp"

EVIAN_BEGIN_NAMESPACE(evian)

void
evian::TradeDatabaseStorage_c::CreateNameToSystemID()
{
  for(const auto& cSystem : _lcSystem) 
  {
    QString
      cLowercaseName = cSystem._cName.toLower();

    //EVIAN_ASSERT(_cNameToSystemID.find(cLowercaseName) == _cNameToSystemID.end() && "Duplicate system names are not allowed");
    if (_cNameToSystemID.find(cLowercaseName) != _cNameToSystemID.end())
    {
      qDebug() << "Duplicate system name found, ignoring " << cLowercaseName;
      return;
    }
    
    _cNameToSystemID[cLowercaseName] = cSystem._nID;
  }
}

evian::TradeDatabaseStorage_c::TradeDatabaseStorage_c() :
  _bIsDatabaseReady(false)
{}

bool
evian::TradeDatabaseStorage_c::IsDatabaseReady() const
{
  return _bIsDatabaseReady;
}

void
evian::TradeDatabaseStorage_c::ExecuteQuery(DatabaseQuery_c& cDataBaseQuery)
{
  std::thread
    cThread([&]()
  {
    cDataBaseQuery._eQueryStatus = DatabaseQuery_c::QUERY_PROCESSING;

    if (cDataBaseQuery._eQueryType & DatabaseQuery_c::QUERYTYPE_SYSTEM)
    { 
      for (const auto& cSystem : _lcSystem)
      {
        cDataBaseQuery(cSystem);
      }
    }

    if (cDataBaseQuery._eQueryType & DatabaseQuery_c::QUERYTYPE_SYSTEM_TO_SYSTEM)
    {
      for (const auto& cSystem0 : _lcSystem)
      {
        for (const auto& cSystem1 : _lcSystem)
        {
          cDataBaseQuery(cSystem0, cSystem1);
        }
      }
    }

    if (cDataBaseQuery._eQueryType & DatabaseQuery_c::QUERYTYPE_SYSTEM_WITH_STATION_TO_SYSTEM_WITH_STATION_SYMMETRIC)
    {
      concurrency::parallel_for(0, _lcSystemWithStation.size(), [&](int i)
      {
        for (auto j = i; j < _lcSystemWithStation.size(); ++j)
        {
          cDataBaseQuery(_lcSystemWithStation[i], _lcSystemWithStation[j]);
        }
      });
    }

    if (cDataBaseQuery._eQueryType & DatabaseQuery_c::QUERYTYPE_INTERNAL_PROCESSING)
    {
      cDataBaseQuery(*this);
    }

    cDataBaseQuery.Combine();
    cDataBaseQuery._eQueryStatus = DatabaseQuery_c::QUERY_COMPLETED;
  });

  cThread.detach();
}

void
evian::TradeDatabaseStorage_c::PostInit()
{
  qDebug() << "Entering PostInit";
  if (!isDatabaseValid()) 
  {
    qWarning() << "Database is invalid";
    _bIsDatabaseReady = false;
    return;
  }
  
  SetupBackLinks();

  QList<QFuture<void>>
    lcQFuture;

  auto
    cQFutureSystemsWithStations = QtConcurrent::run(this, &evian::TradeDatabaseStorage_c::CreateSystemsWithStations);

  lcQFuture.push_back(QtConcurrent::run(this, &evian::TradeDatabaseStorage_c::ComputeNameHashes));
  lcQFuture.push_back(QtConcurrent::run(this, &evian::TradeDatabaseStorage_c::CreateNameToSystemMap));
  lcQFuture.push_back(QtConcurrent::run(this, &evian::TradeDatabaseStorage_c::CreateNameToSystemID));
  lcQFuture.push_back(QtConcurrent::run(this, &evian::TradeDatabaseStorage_c::CreateNameToCommodityID));
  
  cQFutureSystemsWithStations.waitForFinished();
  
  lcQFuture.push_back(QtConcurrent::run(this, &evian::TradeDatabaseStorage_c::CreatePowerplayFactionMap));
  lcQFuture.push_back(QtConcurrent::run(this, &evian::TradeDatabaseStorage_c::CreateCommodityAnalysis));

  for (auto& cQFuture : lcQFuture)
  {
    cQFuture.waitForFinished();
  }

  CreateExploitedBy();
  UpdateEarliestCommodityDateTime();

  qDebug() << "Leaving PostInit";

  _bIsDatabaseReady = true;
}

evian::TradeDatabaseEngine_c::TradeDatabaseEngine_c() :
  _isTerminate(false)
{
  connect(&_cTradeDatabaseStorage, &TradeDatabaseStorage_c::Message,
          this,                    &TradeDatabaseEngine_c::Message);
}

evian::TradeDatabaseEngine_c::~TradeDatabaseEngine_c()
{
  qDebug() << __FUNCTION__;
}

evian::EvianReadWriteLock_c
  evian::TradeDatabaseEngine_c::_cEvianReadWriteLock("TradeDatabaseEngine::_cEvianReadWriteLock");

bool
evian::TradeDatabaseEngine_c::IsDatabaseReady() const
{
  return _cTradeDatabaseStorage.IsDatabaseReady();
}

evian::EvianReadWriteLock_c& 
  evian::TradeDatabaseEngine_c::GetEvianReadWriteLock()
{
  return _cEvianReadWriteLock;
}

const evian::TradeDatabaseStorage_c& 
evian::TradeDatabaseEngine_c::GetTradeDatabase() const
{  
  EVIAN_ASSERT(_cEvianReadWriteLock.isReadLockedByCurrentThread());
  return _cTradeDatabaseStorage;
}

void
evian::TradeDatabaseEngine_c::ResetDatabaseStorageImpl(TradeDatabaseStorage_c& cTradeDatabaseStorage)
{
  EVIAN_ASSERT(!_cEvianReadWriteLock.IsWriteLockedByCurrentThread() && "Database cannot be locked before reset, as we are dependent on model event-loops to run.");
  //qDebug() << "Emitting DatabaseAboutToBeReset()";
  emit DatabaseAboutToBeReset(); // Should be a blocked connection!
  //qDebug() << "Waiting for writelock";
  _cEvianReadWriteLock.LockForWrite();
  //qDebug() << "Got writelock";

  _cTradeDatabaseStorage.Clear();

  //qDebug() << "Clear complete. Starting to move data.";

  _cTradeDatabaseStorage._lcStation           = std::move(cTradeDatabaseStorage._lcStation);
  _cTradeDatabaseStorage._lcSystem            = std::move(cTradeDatabaseStorage._lcSystem);
  _cTradeDatabaseStorage._lcCommodity         = std::move(cTradeDatabaseStorage._lcCommodity);
  _cTradeDatabaseStorage._lcCommodityCategory = std::move(cTradeDatabaseStorage._lcCommodityCategory);

  _cTradeDatabaseStorage.PostInit();

  _cEvianReadWriteLock.Unlock();
  //qDebug() << "Releasing writelock";
  //qDebug() << "Emitting DatabaseResetComplete";
  emit DatabaseResetComplete(); // Not important if it is blocked or not.

  if (_cTradeDatabaseStorage.IsDatabaseReady())
  {
    qDebug() << "New database is ready!";
    emit DatabaseReady();
  }
  else
  {
    qWarning() << "Database not ready after reset.";
  }
  
}

bool
evian::TradeDatabaseEngine_c::ParseEDDNJSon(const QJsonDocument& cQJsonDocument, Listing_c& cListing)
{
  if (!cQJsonDocument.isObject()) {
    qDebug() << "Got JSON that was not an object";
    return false;
  }

  auto
    cQJsonObject = cQJsonDocument.object();

  auto
    cSchemaRef = cQJsonObject["$schemaRef"].toString();

  auto
    cQJsonObjectMessage = cQJsonObject["message"].toObject();

  auto
    cStationName = cQJsonObjectMessage["stationName"].toString(),
    cSystemName  = cQJsonObjectMessage["systemName"].toString(),
    cItemName    = cQJsonObjectMessage["itemName"].toString();

  if (cStationName.isEmpty() || cSystemName.isEmpty() || cItemName.isEmpty())
  {
    qDebug() << "Could not parse JSON: " << cSchemaRef;
    //qDebug() << cQJsonDocument.toJson(QJsonDocument::Indented);
    return false;
  }

  _cEvianReadWriteLock.LockForRead();

  auto
    nCommodityID = _cTradeDatabaseStorage.GetCommodityID(cItemName),
    nStationID   = _cTradeDatabaseStorage.GetStationID(cStationName, cSystemName);

  _cEvianReadWriteLock.Unlock();

  if (nCommodityID == -1)
  {
    qDebug() << "Unknown commodity: " << cItemName;
    return false;
  }

  if (nStationID == -1)
  {
    qDebug() << "Unknown system/station: " << cSystemName << "/" << cStationName;
    return false;
  }

  cListing._nCommodityID = nCommodityID;
  cListing._nStationID = nStationID;
  cListing._nBuyPrice = cQJsonObjectMessage["buyPrice"].toInt();
  cListing._nSellPrice = cQJsonObjectMessage["sellPrice"].toInt();
  cListing._nSupply = cQJsonObjectMessage["stationStock"].toInt();
  cListing._nDemand = cQJsonObjectMessage["demand"].toInt();

  QDateTime
    cQDateTime = QDateTime::fromString(cQJsonObjectMessage["timestamp"].toString(), Qt::ISODate);

  cListing._nUpdated = cQDateTime.toTime_t();

  return true;
}

void evian::TradeDatabaseStorage_c::ComputeNameHashes()
{
  for (auto & cSystem : _lcSystem)
  {
    cSystem._cNameHash = ComputeStringHash(cSystem._cName);
  }

  for (auto & cStation : _lcStation)
  {
    cStation._cNameHash = ComputeStringHash(cStation._cName);
  }
}

void evian::TradeDatabaseStorage_c::CreateNameToCommodityID()
{
  for (const auto& cCommodity : _lcCommodity)
  {
    EVIAN_ASSERT(_cQHashLowerCaseCommodityToCommodityID.find(cCommodity._cName.toLower()) == _cQHashLowerCaseCommodityToCommodityID.end());
    _cQHashLowerCaseCommodityToCommodityID[cCommodity._cName.toLower()] = cCommodity._nID; 
  }
}

void
  evian::TradeDatabaseStorage_c::SetupBackLinks()
{
  for (auto& cStation : _lcStation)
  {
    System_c
      cSystem;

    cSystem._nID = cStation._nSystemID;

    auto
      it = std::lower_bound(_lcSystem.begin(), _lcSystem.end(), cSystem);

    assert(it != _lcSystem.end());
    assert(it->_nID == cStation._nSystemID);

    it->_lpcStation.push_back(&cStation);
    cStation._pcSystem = &(*it);

    cStation.CreateScratchArrays();
    for (auto& cListing : cStation._lcListing)
    {
      EVIAN_ASSERT(cListing._nCommodityID < Station_c::MAX_COMMODITIES);
      if (cListing._isEnabled)
      {
        cStation.EnableListing(cListing);
      }
      else
      {
        cStation.DisableListing(cListing);
      }
      
      // This also clears away data are definitively bogus, which would introduce arbitrage.
      /*cStation._paBuyPrice[cListing._nCommodityID] = cListing._nBuyPrice <= 0 ? Station_c::BUY_DEFAULT : cListing._nBuyPrice;
      cStation._paSellPrice[cListing._nCommodityID] = cListing._nSellPrice <= 0 ? Station_c::SELL_DEFAULT : cListing._nSellPrice;
      cStation._paSupply[cListing._nCommodityID] = cListing._nSupply <= 0 ? 0 : cListing._nSupply;*/
    }
  }
}

void evian::TradeDatabaseStorage_c::CreateSystemsWithStations()
{
  for (auto& cSystem : _lcSystem)
  {
    if (!cSystem._lpcStation.empty())
    {
      _lcSystemWithStation.push_back(cSystem);
    }
  }
}

void evian::TradeDatabaseStorage_c::CreateNameToSystemMap()
{
  for (const System_c& cSystem : _lcSystem)
  {
    //EVIAN_ASSERT(_cNameToSystemMap.find(cSystem._cName) == _cNameToSystemMap.end());
    if (_cNameToSystemMap.find(cSystem._cName) != _cNameToSystemMap.end())    
    {
      qDebug() << "Ignoring duplicate name " << cSystem._cName;

    }
    else
    {
      _cNameToSystemMap.emplace(cSystem._cName, cSystem);
    }
  }
}

bool
evian::TradeDatabaseStorage_c::isDatabaseValid()
{
  bool 
    isValid = true;
  isValid = isValid && std::is_sorted(_lcCommodityCategory.begin(), _lcCommodityCategory.end());
  isValid = isValid && std::is_sorted(_lcCommodity.begin(), _lcCommodity.end());
  isValid = isValid && std::is_sorted(_lcStation.begin(), _lcStation.end());
  isValid = isValid && std::is_sorted(_lcSystem.begin(), _lcSystem.end());

  auto 
    it = std::find_if(_lcStation.begin(), _lcStation.end(), 
    [](Station_c& cStation)
  {
    return cStation._cName == "Abraham Lincoln";
  });

  isValid = isValid && (it != _lcStation.end());
  
  return isValid;
}

void evian::TradeDatabaseStorage_c::Clear()
{
  _bIsDatabaseReady = false;
  //qDebug() << "_lcCommodity";
  _lcCommodity.clear();
  //qDebug() << "_lcCommodityCategory";
  _lcCommodityCategory.clear();
  //qDebug() << "_lcSystemWithStation";
  _lcSystemWithStation.clear();
  //qDebug() << "_cNameToSystemMap";
  _cNameToSystemMap.clear();
  //qDebug() << "_cNameToSystemId";
  _cNameToSystemID.clear();
  //qDebug() << "_lcSystem";
  _lcSystem.clear();
  //qDebug() << "_lcStation";
  _lcStation.clear();
  //qDebug() << "_cQHashLowerCaseCommodityToCommodityID";
  _cQHashLowerCaseCommodityToCommodityID.clear();
  //qDebug() << "_lpcEDDNMessage";
  _lpcEDDNMessage.clear();
  //qDebug() << "_cFactionToSystemMap";
  _cFactionToSystemMap.clear();

  //qDebug() << "_lsPowerControlFaction";
  _lsPowerControlFaction.clear();
}

int64_t evian::TradeDatabaseStorage_c::GetCommodityID(QString cCommodityName) const
{
  auto 
    it = _cQHashLowerCaseCommodityToCommodityID.find(cCommodityName.toLower());

  return it != _cQHashLowerCaseCommodityToCommodityID.end() ? it.value() : -1;
}

int64_t evian::TradeDatabaseStorage_c::GetSystemID(QString cSystemName) const
{

  auto
    it = _cNameToSystemID.find(cSystemName.toLower());

  return it != _cNameToSystemID.end() ? it.value() : -1;
}

int64_t evian::TradeDatabaseStorage_c::GetStationID(QString cStationName, QString cSystemName) const
{
  auto
    nSystemID = GetSystemID(cSystemName);

  if (nSystemID == -1)
  {
    return -1;
  }

  const auto&
    cSystem = GetSystem(nSystemID);

  for(const auto& cStation : cSystem._lpcStation)
  {
    if (cStation->_cName.toLower() == cStationName.toLower()) 
    {
      return cStation->_nID;
    }
  }

  return -1;
}

const evian::System_c & evian::TradeDatabaseStorage_c::GetSystem(int64_t nSystemID) const
{
  System_c
    cSystem;

  cSystem._nID = nSystemID;

  auto
    it = std::lower_bound(_lcSystem.begin(), _lcSystem.end(), cSystem);

  assert(it != _lcSystem.end());

  return *it;
}

const evian::System_c & 
  evian::TradeDatabaseStorage_c::GetSystem(QString cSystemName) const
{
  auto
    it = std::find_if(_lcSystem.begin(), _lcSystem.end(), 
    [&cSystemName](const System_c& cSystem)
  {
    return cSystem._cName == cSystemName;
  });

  if (it == _lcSystem.end())
  {
    throw std::runtime_error("Could not find system");
  }

  return *it;
}

const evian::Station_c & 
  evian::TradeDatabaseStorage_c::GetStation(int64_t nStationID) const
{
  Station_c
    cStation;

  cStation._nID = nStationID;

  auto
    it = std::lower_bound(_lcStation.begin(), _lcStation.end(), cStation);

  assert(it != _lcStation.end());

  return *it;
}

evian::Station_c & 
evian::TradeDatabaseStorage_c::GetStation(int64_t nStationID)
{
  Station_c
    cStation;

  cStation._nID = nStationID;

  auto
    it = std::lower_bound(_lcStation.begin(), _lcStation.end(), cStation);

  assert(it != _lcStation.end());

  return *it;
}


const Station_c&
evian::TradeDatabaseStorage_c::GetStation(QString cStationName, QString cSystemName) const
{
  return GetStation(GetStationID(cStationName, cSystemName));
}



QString evian::TradeDatabaseStorage_c::GetStationName(int64_t nStationID) const
{
  Station_c
    cStation;

  cStation._nID = nStationID;

  auto 
    it = std::lower_bound(_lcStation.begin(), _lcStation.end(), cStation);

  assert(it != _lcStation.end());

  return it->_cName;

}

QString evian::TradeDatabaseStorage_c::GetSystemName(int64_t nSystemID) const
{
  System_c
    cSystem;

  cSystem._nID = nSystemID;

  auto
    it = std::lower_bound(_lcSystem.begin(), _lcSystem.end(), cSystem);

  assert(it != _lcSystem.end());

  return it->_cName;

}

QString evian::TradeDatabaseStorage_c::GetCommodityName(int64_t nCommodityID) const
{
  Commodity_c
    cCommodity;

  cCommodity._nID = nCommodityID;

  if (nCommodityID == -1)
  {
    return QString("None");
  }

  auto 
    it = std::lower_bound(_lcCommodity.begin(), _lcCommodity.end(), cCommodity);

  //assert(it != _lcCommodity.end());
  if (it != _lcCommodity.end() && it->_nID == nCommodityID)
  {
    return it->_cName;
  }
  else
  {
    return QString("Unknown commodity") + QString::number(nCommodityID);
  }
  
}

evian::Grid_c<evian::System_c>
evian::TradeDatabaseStorage_c::CreateGrid(float rGridSpacing) const
{
  Grid_c<System_c>
    cGrid;

  cGrid.SetCellSpacing(rGridSpacing);

  EVIAN_ASSERT(!_lcSystemWithStation.empty());

  BoundingBox_c 
    cBoundingBox;

  for (const System_c& cSystem : _lcSystemWithStation)
  {
    cBoundingBox.Expand(cSystem._cPosition);
  }

  auto
    cPadding = Vec3f(5,5,5);

  cBoundingBox.Expand(cBoundingBox._cMin - cPadding);
  cBoundingBox.Expand(cBoundingBox._cMax + cPadding);

  cGrid.SetBoundingBox(cBoundingBox);
  cGrid.ComputeGridDimension();

  for (const System_c& cSystem : _lcSystemWithStation)
  {
    cGrid.AddToGrid(cSystem);
  }

  return cGrid;
}

Grid_c<System_c>
evian::TradeDatabaseStorage_c::CreateGrid(const QList<std::reference_wrapper<const evian::System_c>>& lcSystemFrom, QList<std::reference_wrapper<const evian::System_c>>& lcSystemTo, float rGridSpacing) const
{
  Grid_c<System_c>
    cGrid;

  cGrid.SetCellSpacing(rGridSpacing);

  EVIAN_ASSERT(!_lcSystemWithStation.empty());

  BoundingBox_c 
    cBoundingBox;

  for (const System_c& cSystem : lcSystemFrom)
  {
    cBoundingBox.Expand(cSystem._cPosition);
  }

  for (const System_c& cSystem : lcSystemTo)
  {
    cBoundingBox.Expand(cSystem._cPosition);
  }

  auto
    cPadding = Vec3f(5,5,5);

  cBoundingBox.Expand(cBoundingBox._cMin - cPadding);
  cBoundingBox.Expand(cBoundingBox._cMax + cPadding);

  cGrid.SetBoundingBox(cBoundingBox);
  cGrid.ComputeGridDimension();

  for (const System_c& cSystem : lcSystemTo)
  {
    cGrid.AddToGrid(cSystem);
  }

  return cGrid;
}


void
evian::TradeDatabaseEngine_c::ResetDatabaseStorage(QSharedPointer<evian::TradeDatabaseStorage_c> cQSharedPointerTradeDatabaseStorage)
{
  ResetDatabaseStorageImpl(*cQSharedPointerTradeDatabaseStorage);
}

void
evian::TradeDatabaseEngine_c::UpdateListingEDDN(QJsonDocument cQJsonDocument)
{
  // qDebug() << __FUNCTION__ << "Handling:";
  //qDebug() << cQJsonDocument.toJson(QJsonDocument::Indented);

  HandleEDDNMessage(cQJsonDocument);

  //Listing_c
  //  cListing;

  //if (ParseEDDNJSon(cQJsonDocument, cListing))
  //{
  //  UpdateListing(cListing, -1);
  //}
}

void evian::TradeDatabaseEngine_c::UpdateListing(Listing_c cListing, int iRowHint)
{
  if (_cEvianReadWriteLock.TryLockForWrite())
  {
    ProcessListingUpdate(cListing, iRowHint);
    _cEvianReadWriteLock.Unlock();
  }
  else
  {
    QMutexLocker
      cQMutexLocker(&_cQMutex);
    _lcQListOutstandingListingUpdates.append(qMakePair(cListing, iRowHint));
  }

  EVIAN_ASSERT(!_cEvianReadWriteLock.IsWriteLockedByCurrentThread());
}

void evian::TradeDatabaseEngine_c::Process()
{
  qDebug() << "Evian Database Engine running in thread: " << QThread::currentThreadId();

  while (true)
  {
    if (_isTerminate)
    {
      break;
    }

    QCoreApplication::processEvents();

    {
      QMutexLocker
        cQMutexLocker(&_cQMutex);

      bool
        isAnyRequestOutstanding = !_lcQListOutstandingListingUpdates.isEmpty() || !_lpcQListOutstandingEDDNUpdates.isEmpty();
      
      if (isAnyRequestOutstanding && _cEvianReadWriteLock.TryLockForWrite())
      {
        EVIAN_ASSERT(_cEvianReadWriteLock.IsWriteLockedByCurrentThread());

        for (const auto& cQPair : _lcQListOutstandingListingUpdates)
        {
          ProcessListingUpdate(cQPair.first, cQPair.second);
        }
        _lcQListOutstandingListingUpdates.clear();

        ProcessEDDNMessageQueue();

        _cEvianReadWriteLock.Unlock();
      }
      EVIAN_ASSERT(!_cEvianReadWriteLock.IsWriteLockedByCurrentThread());
    }

    QThread::msleep(InitAndSaveSettingsHelper(key_database_sleeptime_ms, 1000).toInt()); // No need to hog the CPU.
  }

  emit Finished();
}

void evian::TradeDatabaseEngine_c::Terminate()
{
  QMutexLocker
    cQMutexLocker(&_cQMutex);

  _isTerminate = true;
}

void evian::TradeDatabaseEngine_c::ProcessListingUpdate(const Listing_c& cListing, int iRowHint)
{
  EVIAN_ASSERT(_cEvianReadWriteLock.IsWriteLockedByCurrentThread());

  if (!IsListingValid(cListing))
  {
    return;
  }

  auto
    &cStation = _cTradeDatabaseStorage.GetStation(cListing._nStationID);

  EVIAN_ASSERT(iRowHint < static_cast<int>(cStation._lcListing.size()));

  if (iRowHint != -1)
  {
    EVIAN_ASSERT(cListing._nCommodityID == cStation._lcListing[iRowHint]._nCommodityID);

    if (cStation._lcListing[iRowHint]._nUpdated < cListing._nUpdated)
    {
      cStation._lcListing[iRowHint] = cListing;
      emit ListingUpdated(cListing, iRowHint);
    }
  }
  else
  {
    auto
      it = std::lower_bound(cStation._lcListing.begin(), cStation._lcListing.end(), cListing);

    iRowHint = std::distance(cStation._lcListing.begin(), it);

    if (it != cStation._lcListing.end() && it->_nCommodityID == cListing._nCommodityID)
    {
      if (it->_nUpdated < cListing._nUpdated)
      {
        *it = cListing;
        emit ListingUpdated(cListing, iRowHint);
      }
    }
    else // Listing does not exist for station
    {
      cStation._lcListing.insert(it, cListing);
      emit ListingAdded(cListing, iRowHint);
    }
  }

  if (cListing._isEnabled)
  {
    cStation.EnableListing(cListing);
  }
  else
  {
    cStation.DisableListing(cListing);
  }
}


void TradeDatabaseEngine_c::HandleEDDNMessage(const QJsonDocument& cQJsonDocument)
{
  auto
    cQJsonObject = cQJsonDocument.object();

  auto
    eSchemaVersion = eddn::SchemaVersion::GetSchemaVersion(cQJsonObject);

  switch (eSchemaVersion)
  {
    case (eddn::SchemaVersion::Commodity_V1):
      HandleEDDNV1Message(cQJsonObject);
      break;
    case (eddn::SchemaVersion::Commodity_V2):
      HandleEDDNV2Message(cQJsonObject);
      break;
    case (eddn::SchemaVersion::Invalid):
    default:
      break;
      //qDebug() << "Unhandled EDDN message: " << cQJsonObject;
      //qDebug() << cQJsonObject;
  }
}

bool
evian::TradeDatabaseEngine_c::IsListingValid(const Listing_c& cListing)
{
  
  auto
    nCommodityID = cListing._nCommodityID;

  if (!_cTradeDatabaseStorage.IsCommodityIDValid((cListing._nCommodityID)))
  {
    qDebug() << "Listing invalid due to unknown commodityID: " << nCommodityID;
    return false;
  }

  auto
    cCommodityName = _cTradeDatabaseStorage.GetCommodityName(nCommodityID);

  if (cListing._nBuyPrice != 0 && cListing._nSellPrice != 0 && cListing._nSellPrice > cListing._nBuyPrice) // Arbitrage!
  {
   qDebug() << "Listing invalid due to arbitrage, " << cCommodityName << "buy: " << cListing._nBuyPrice << " sell: " << cListing._nSellPrice;
    return false;
  }

  if (cListing._nBuyPrice != 0 &&
      (cListing._nBuyPrice > 1.10 * _cTradeDatabaseStorage._lcCommodityPriceAnalysisBuy[nCommodityID]._nPriceMax ||
       cListing._nBuyPrice < 0.90 * _cTradeDatabaseStorage._lcCommodityPriceAnalysisBuy[nCommodityID]._nPriceMin))
  {
    qDebug() << "Listing invalid since the buy price is outside the known buy-bracket:" << cCommodityName << "buy: " << cListing._nBuyPrice << " sell: " << cListing._nSellPrice
             << "Buy bracket: [" << 0.90 * _cTradeDatabaseStorage._lcCommodityPriceAnalysisBuy[nCommodityID]._nPriceMin << ", " 
                                 << 1.10 * _cTradeDatabaseStorage._lcCommodityPriceAnalysisBuy[nCommodityID]._nPriceMax << "]";
    return false;
  }

  if (cListing._nSellPrice != 0 &&
      (cListing._nSellPrice > 1.10 * _cTradeDatabaseStorage._lcCommodityPriceAnalysisSell[nCommodityID]._nPriceMax ||
       cListing._nSellPrice < 0.90 * _cTradeDatabaseStorage._lcCommodityPriceAnalysisSell[nCommodityID]._nPriceMin))
  {
    qDebug() << "Listing invalid since the sell price is outside the known sell-bracket:" << cCommodityName << "buy: " << cListing._nBuyPrice << " sell: " << cListing._nSellPrice
             << "Sell bracket: [" << 0.90 * _cTradeDatabaseStorage._lcCommodityPriceAnalysisSell[nCommodityID]._nPriceMin << ", " 
                                  << 1.10 * _cTradeDatabaseStorage._lcCommodityPriceAnalysisSell[nCommodityID]._nPriceMax << "]";
    return false;
  }
  
  return true;
}

void
evian::TradeDatabaseEngine_c::UpdateEarliestCommodityTime(int nSecs)
{
  _cEvianReadWriteLock.LockForWrite();

  _cTradeDatabaseStorage.UpdateEarliestCommodityDateTime(nSecs);

  _cEvianReadWriteLock.Unlock();
}

#define SERIALIZE_KEYS \
  X(commodities) \
  X(stations)    \
  X(systems)     \
  X(commodityCategories) \
  X(listings)

namespace TradeDatabaseEngine
{
#define X(A) static const char * A  = #A;
  SERIALIZE_KEYS
#undef X
}

template<class T>
void
SerializeVectorAsArray(const std::vector<T>& lcT, const char* paKey, QJsonObject& cQJsonObjectParent)
{
  QJsonArray
    qJsonArray;

  for (const auto& cT : lcT)
  {
    qJsonArray.append(T::WriteToJsonAsArray(cT));
  }

  cQJsonObjectParent[paKey] = qJsonArray;
}

template<class T>
void 
SerializeVector(const std::vector<T>& lcT, const char* paKey, QJsonObject& cQJsonObjectParent, std::function<void(const T&, QJsonObject&)> tFunction = T::WriteToJson)
{
  QJsonArray
    cQJsonArray;

  for (const auto& cT: lcT)
  {
    QJsonObject
      cQJsonObject;
    tFunction(cT, cQJsonObject);
    cQJsonArray.append(cQJsonObject);
  }

  cQJsonObjectParent[paKey] = cQJsonArray;
}

template<class T>
void
SerializeVectorPointer(const std::vector<T>* pcVector, const char* paKey, QJsonObject * pcQJsonObjectParent)
{
  SerializeVector(*pcVector, paKey, *pcQJsonObjectParent);
}

template<class T>
void
SerializeVectorPointerAsArray(const std::vector<T>* pcVector, const char* paKey, QJsonObject * pcQJsonObjectParent)
{
  SerializeVectorAsArray(*pcVector, paKey, *pcQJsonObjectParent);
}

template<class T>
void 
DeserializeVector(std::vector<T>& lcT, const char* paKey, const QJsonObject& cQJsonObjectParent)
{
  EVIAN_ASSERT(cQJsonObjectParent[paKey].isArray());

  const QJsonArray
    cQJsonArray = cQJsonObjectParent[paKey].toArray();
  
  lcT.reserve(cQJsonArray.size());

  for (const auto& cQJsonObject: cQJsonArray)
  {
    lcT.push_back(T::ReadFromJson(cQJsonObject.toObject()));
  }
}

template<class T>
void
DeserializeVectorPointer(std::vector<T>* pcVector, const char* paKey, const QJsonObject& cQJsonObjectParent)
{
  DeserializeVector(*pcVector, paKey, cQJsonObjectParent);
}


bool
evian::TradeDatabaseEngine_c::Serialize(QString cFilename, QString sListingsFilename, SerializationMode eSerializationMode)
{
  EVIAN_ASSERT(_cEvianReadWriteLock.isReadLockedByCurrentThread());

  QJsonObject
    cQJsonObject,
    qJsonObjectListings;
  
  std::function<void(const Station_c&, QJsonObject&)>
    tFunction = &Station_c::WriteListingsJson;

  SerializeVector(_cTradeDatabaseStorage._lcCommodity,          TradeDatabaseEngine::commodities,         cQJsonObject);
  SerializeVector(_cTradeDatabaseStorage._lcCommodityCategory,  TradeDatabaseEngine::commodityCategories, cQJsonObject);
  SerializeVector(_cTradeDatabaseStorage._lcSystem,             TradeDatabaseEngine::systems,             cQJsonObject);
  SerializeVector(_cTradeDatabaseStorage._lcStation,            TradeDatabaseEngine::stations,            cQJsonObject);
  SerializeVector(_cTradeDatabaseStorage._lcStation,            TradeDatabaseEngine::listings,            qJsonObjectListings, tFunction);

  QJsonDocument
    cQJsonDocument(cQJsonObject),
    qJsonDocumentListings(qJsonObjectListings);

  QFile
    qFileserializeDatabase(cFilename),
    qFileSerializedCommodities(sListingsFilename);

  if (!qFileserializeDatabase.open(QIODevice::WriteOnly) || !qFileSerializedCommodities.open(QIODevice::WriteOnly)) 
  {
    qDebug() << "Could not open file for writing";
    return false;
  }

  switch (eSerializationMode)
  {
    case SerializationMode_Binary:
      qFileserializeDatabase.write(cQJsonDocument.toBinaryData());
      qFileSerializedCommodities.write(qJsonDocumentListings.toBinaryData());
      break;
    case SerializationMode_JSON:
      qFileserializeDatabase.write(cQJsonDocument.toJson());
      qFileSerializedCommodities.write(qJsonDocumentListings.toJson());
      break;
  }

  return true;
}

QSharedPointer<TradeDatabaseStorage_c> 
evian::TradeDatabaseEngine_c::Deserialize(QString sDatabaseFilename, QString sListingsFilename)
{
  QFile
    qFileDeserialize(sDatabaseFilename),
    qFileListings(sListingsFilename);

  if (!qFileDeserialize.open(QIODevice::ReadOnly)) 
  {
    qDebug() << "Could not open" << sDatabaseFilename <<  " for reading";
    return QSharedPointer<TradeDatabaseStorage_c>(0);
  }

  if (!qFileListings.open(QIODevice::ReadOnly))
  {
    qDebug() << "Could not open" << sListingsFilename << " for reading";
    return QSharedPointer<TradeDatabaseStorage_c>(0);
  }

  QByteArray 
    qByteArrayDatabase = qFileDeserialize.readAll(),
    qByteArrayListings = qFileListings.readAll();

  auto
    uElapsedTime = GetTickCount64();
  
  QSettings
    qSettings;

  QJsonDocument::DataValidation
    eDataValidation = static_cast<QJsonDocument::DataValidation>(qSettings.value(key_database_data_validation_mode, QJsonDocument::Validate).toInt());

  qDebug() << "Reading bytearray using DataValidation-mode: " << (eDataValidation == QJsonDocument::Validate ? "Validate" : "BypassValidation");

  QJsonDocument
    qJsonDocumentDatabase = QJsonDocument::fromBinaryData(qByteArrayDatabase, eDataValidation),
    qJsonDocumentListings = QJsonDocument::fromBinaryData(qByteArrayListings, eDataValidation);
  
  uElapsedTime = GetTickCount64() - uElapsedTime;
  qDebug() << "Read database from binary data in " << uElapsedTime << " ms.";

  auto
    pcTradeDatabaseStorage = QSharedPointer<TradeDatabaseStorage_c>::create();
  
  TradeDatabaseStorage_c&
    cTradeDatabaseStorage = *pcTradeDatabaseStorage;

  const auto
    qJsonObject = qJsonDocumentDatabase.object();

  uElapsedTime = GetTickCount64();

 /* auto
    qFuture = QtConcurrent::run(DeserializeVectorPointer<evian::Station_c>, &cTradeDatabaseStorage._lcStation, TradeDatabaseEngine::stations, qJsonObject[TradeDatabaseEngine::stations].toObject());
*/
  DeserializeVector(cTradeDatabaseStorage._lcStation,           TradeDatabaseEngine::stations,            qJsonObject);
  DeserializeVector(cTradeDatabaseStorage._lcCommodity,         TradeDatabaseEngine::commodities,         qJsonObject);
  DeserializeVector(cTradeDatabaseStorage._lcCommodityCategory, TradeDatabaseEngine::commodityCategories, qJsonObject);
  DeserializeVector(cTradeDatabaseStorage._lcSystem,            TradeDatabaseEngine::systems,             qJsonObject);
  
  Station_c::DeserializeListings(cTradeDatabaseStorage._lcStation, TradeDatabaseEngine::listings, qJsonDocumentListings.object());

  //qFuture.waitForFinished();

  uElapsedTime = GetTickCount64() - uElapsedTime;
  
  qDebug() << "Deserialized database in" << uElapsedTime << "ms.";
  return pcTradeDatabaseStorage;
}


void 
evian::TradeDatabaseEngine_c::ProcessEDDNMessageUpdate(QSharedPointer<eddn::EDDNMessage_c> pcCommodityV1)
{
  EVIAN_ASSERT(_cEvianReadWriteLock.IsWriteLockedByCurrentThread());
  _cTradeDatabaseStorage._lpcEDDNMessage.push_back(pcCommodityV1);
}

void
evian::TradeDatabaseEngine_c::ProcessEDDNMessageQueue()
{
  if (!_lpcQListOutstandingEDDNUpdates.isEmpty())
  {
    auto
      iRowHint = static_cast<int>(_cTradeDatabaseStorage._lpcEDDNMessage.size()),
      nUpdates = _lpcQListOutstandingEDDNUpdates.size();

    for(const auto& pcMessage: _lpcQListOutstandingEDDNUpdates)
    {
      ProcessEDDNMessageUpdate(pcMessage);
    }

    _lpcQListOutstandingEDDNUpdates.clear();

    // nUpdates is okay since it is the number of messages and not the number of listings that is relevant for
    // EDDNItemModel_c.
    emit EDDNV1CommodityMessagesAdded(iRowHint, nUpdates);
  }
  
}

void 
evian::TradeDatabaseEngine_c::HandleEDDNV1Message(const QJsonObject& cQJsonObject)
{
  auto
    pcCommodityV1 = QSharedPointer<eddn::CommodityV1_c>::create();

  pcCommodityV1->_cHeader  = eddn::CommodityV1_c::Header_c::ReadFromJSON(cQJsonObject["header"].toObject());
  pcCommodityV1->_cMessage = eddn::CommodityV1_c::Message_c::ReadFromJSON(cQJsonObject["message"].toObject());

  // Messages will normally come in bursts, so we queue them up and update all in one go.
  _lpcQListOutstandingEDDNUpdates.append(pcCommodityV1);

  auto
    cStationName = pcCommodityV1->_cMessage._cStationName,
    cSystemName  = pcCommodityV1->_cMessage._cSystemName;

  qDebug() << "Got EDDN-V1 message: " << cStationName << "/" << cSystemName;
  
  _cEvianReadWriteLock.LockForRead();

  int
    nStationID   = _cTradeDatabaseStorage.GetStationID(cStationName, cSystemName),
    nCommodityID = _cTradeDatabaseStorage.GetCommodityID(pcCommodityV1->_cMessage._cItemName);

  _cEvianReadWriteLock.Unlock();
  
  if (nStationID == -1)
  {
    qDebug() << "Unknown station " << cStationName << "/" << cSystemName;
    return;
  }

  if (nCommodityID == -1)
  {
    qDebug() << "Unknown commodity " << pcCommodityV1->_cMessage._cItemName;
    return;
  }

  Listing_c
    cListing;

  cListing._nStationID = nStationID;
  cListing._nCommodityID = nCommodityID;

  cListing._nUpdated = pcCommodityV1->_cHeader._cQDateTimeTimestamp.toTime_t();
  cListing._nBuyPrice  = pcCommodityV1->_cMessage._nBuyPrice;
  cListing._nSellPrice = pcCommodityV1->_cMessage._nSellPrice;
  cListing._nDemand    = pcCommodityV1->_cMessage._nDemand;
  cListing._nSupply    = pcCommodityV1->_cMessage._nStationStock;

  UpdateListing(cListing, -1);
}

void
evian::TradeDatabaseEngine_c::HandleEDDNV2Message(QJsonObject cQJsonObject)
{
  auto
    pcCommodityV2 = QSharedPointer<eddn::CommodityV2_c>::create();

  pcCommodityV2->_cHeader  = eddn::CommodityV2_c::Header_c::ReadFromJSON(cQJsonObject["header"].toObject());
  pcCommodityV2->_cMessage = eddn::CommodityV2_c::Message_c::ReadFromJSON(cQJsonObject["message"].toObject());

  _lpcQListOutstandingEDDNUpdates.append(pcCommodityV2);

  auto
    cStationName = pcCommodityV2->_cMessage._cProperties._cStationName,
    cSystemName  = pcCommodityV2->_cMessage._cProperties._cSystemName;

  _cEvianReadWriteLock.LockForRead();
  auto
    nStationID = _cTradeDatabaseStorage.GetStationID(cStationName, cSystemName);
  _cEvianReadWriteLock.Unlock();

  //qDebug() << "Got EDDN-V2 message: " << cStationName << "/" << cSystemName;
  if (nStationID == -1)
  {
    qDebug() << "Unknown station " << cStationName << "/" << cSystemName;
    return;
  }

  Listing_c
    cListing;

  cListing._nStationID = nStationID;
  cListing._nUpdated = pcCommodityV2->_cHeader._cQDateTimeTimestamp.toTime_t();
  
  for (const auto& cItem : pcCommodityV2->_cMessage._lcItem)
  {
    _cEvianReadWriteLock.LockForRead();
    cListing._nCommodityID = _cTradeDatabaseStorage.GetCommodityID(cItem._cName);
    _cEvianReadWriteLock.Unlock();

    if (cListing._nCommodityID == -1)
    {
      qDebug() << "Unknown commodity " << cItem._cName;
      continue;
    }

    cListing._nBuyPrice  = cItem._nBuyPrice;
    cListing._nSellPrice = cItem._nSellPrice;
    cListing._nDemand    = cItem._nDemand;
    cListing._nSupply    = cItem._nSupply;

    UpdateListing(cListing, -1);
  }
}

void
OldCommodityDisabler_c::Process(evian::Station_c& cStation)
{
  for (auto& cListing : cStation._lcListing)
  {
    QDateTime
      cCommodityTime = QDateTime::fromTime_t(cListing._nUpdated);

    if (cCommodityTime < _cQDateTime)
    {
      cListing._isEnabled = false;
      cStation.DisableListing(cListing);
    }
    else
    {
      cListing._isEnabled = true;
      cStation.EnableListing(cListing);
    }
  }
}

void
OldCommodityDisabler_c::SetEarliestDate(QDateTime cQDateTime)
{
  _cQDateTime = cQDateTime;
}

bool evian::TradeDatabaseStorage_c::IsCommodityIDValid(int64_t nCommodityID)
{
  Commodity_c
    cCommodity;

  cCommodity._nID = nCommodityID;

  auto 
    it = std::lower_bound(_lcCommodity.begin(), _lcCommodity.end(), cCommodity);
  
  return it != _lcCommodity.end();
}

void evian::TradeDatabaseStorage_c::CreatePowerplayFactionMap()
{
  for (const System_c& cSystem: _lcSystemWithStation)
  {
    const auto&
      sPowerControlFaction = cSystem._sPowerControlFaction;

    if (!sPowerControlFaction.isEmpty())
    {
      if (!_cFactionToSystemMap.contains(sPowerControlFaction))
      {
        _lsPowerControlFaction << sPowerControlFaction;
      }
      _cFactionToSystemMap[sPowerControlFaction].append(cSystem);
    }
    
    const auto&
      sAllegiance = cSystem._sAllegiance;

    if (!sAllegiance.isEmpty() && sAllegiance != QLatin1Literal("None"))
    {
      _cFactionToSystemMap[sAllegiance].append(cSystem);
    }
  }
}

void
evian::TradeDatabaseStorage_c::UpdateEarliestCommodityDateTime()
{
  int
    nSecs = InitAndSaveSettingsHelper(key_database_filter_older_than_s, 14 * 24 * 3600).toInt();

  UpdateEarliestCommodityDateTime(nSecs);
}

void
evian::TradeDatabaseStorage_c::UpdateEarliestCommodityDateTime(int nSecs)
{
  EVIAN_ASSERT(nSecs > 0);

  QDateTime 
    cQDateTime  = QDateTime::currentDateTime().addSecs(-nSecs);

  OldCommodityDisabler_c
    cOldCommodityDisabler;

  cOldCommodityDisabler.SetEarliestDate(cQDateTime);

  auto
    nElapsedTime = GetTickCount64();

  // 2015-12-22: std::for_each is faster than concurrency::parallel_for_each.
  std::for_each(_lcSystemWithStation.begin(), _lcSystemWithStation.end(), [&cOldCommodityDisabler](const System_c& cSystem)
  {
      for(auto& pcStation: cSystem._lpcStation)
      {
        cOldCommodityDisabler.Process(*pcStation);
    }
  });

  nElapsedTime = GetTickCount64() - nElapsedTime;

  QString
    sMessage = QString("Disabled listings older than ") + FormatDurationSinceNow(cQDateTime) + " (" + QString::number(nElapsedTime) + " ms).";

  emit Message(sMessage, 2500);

  qDebug() << sMessage;
}

void
evian::TradeDatabaseStorage_c::CreateExploitedBy()
{
  const float
    rExploitRadius = 15.0f;

  auto
    cGrid = CreateGrid(rExploitRadius);

  for (const auto& sPowerControlFaction : _lsPowerControlFaction)
  {
    for (const System_c& cSystem : _cFactionToSystemMap[sPowerControlFaction])
    {
      auto
        cIJK = cGrid.IJKFromPosition(cSystem._cPosition);
      
      for (int i = -1; i <= 1; ++i)
        for (int j = -1; j <= 1; ++j)
          for (int k = -1; k <= 1; ++k)
          {
            Int3
              cOffset(i, j, k),
              cActiveIJK = cIJK + cOffset;
            
            if (!cGrid.IsInsideGridIJK(cActiveIJK))
            {
              continue;
            }

            const auto&
              cGridCell1 = cGrid.GetGridCell(cActiveIJK);
             
            for (const System_c& cSystem1 : cGridCell1._lCellElement)
            {
              auto
                rDist = Dist(cSystem._cPosition, cSystem1._cPosition);

              if (cSystem._nID == cSystem1._nID ||
                  !cSystem1._sPowerControlFaction.isEmpty() ||
                  rDist > rExploitRadius)
              {
                continue;
              }

              System_c::ExploitedBy_c
                cExploitedBy;

              cExploitedBy._sPowerControlFaction = sPowerControlFaction;
              cExploitedBy._rDistToControlSystem = rDist;
              cExploitedBy._sControlSystem = cSystem._cName;
              cExploitedBy._nControlSystemID = cSystem._nID;

              const_cast<System_c&>(cSystem1)._lcExploitedBy.append(cExploitedBy);

              auto 
                sExploitedKey = sPowerControlFaction + " (Exploited)";

              if (!_cFactionToSystemMap[sExploitedKey].contains(cSystem1))
              {
                _cFactionToSystemMap[sExploitedKey].append(cSystem1);
              }
            }
          }
    }
  }

  for (const auto& sPowerControlFaction : _lsPowerControlFaction)
  {
    auto 
      sExploitedKey = sPowerControlFaction + " (Exploited)",
      sControlAndExploitedKey = sPowerControlFaction + " (Control+Exploited)";

    EVIAN_ASSERT(_cFactionToSystemMap[sControlAndExploitedKey].isEmpty());

    _cFactionToSystemMap[sControlAndExploitedKey].append(_cFactionToSystemMap[sPowerControlFaction]);
    _cFactionToSystemMap[sControlAndExploitedKey].append(_cFactionToSystemMap[sExploitedKey]);
  }
}

const
Commodity_c& TradeDatabaseStorage_c::GetCommodity(int64_t nCommodityID)
{
  Commodity_c
    cCommodity;

  cCommodity._nID = nCommodityID;

  auto 
    it = std::lower_bound(_lcCommodity.begin(), _lcCommodity.end(), cCommodity);
  
  assert(it != _lcCommodity.end());

  return *it;
}

void
evian::TradeDatabaseStorage_c::CreateCommodityAnalysis()
{
  CommodityPriceAnalysisQuery_c
    cCommodityPriceAnalysisQuery;

  cCommodityPriceAnalysisQuery(*this);

  _lcCommodityPriceAnalysisBuy = cCommodityPriceAnalysisQuery._lcCommodityPriceAnalysisBuy;
  _lcCommodityPriceAnalysisSell = cCommodityPriceAnalysisQuery._lcCommodityPriceAnalysisSell;
}

EVIAN_END_NAMESPACE(evian)
