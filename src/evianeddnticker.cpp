#include "stdafx.h"
#include "evianeddnticker.hpp"
#include "ui_evianeddnticker.h"
#include "model.hpp"

EvianEDDNTicker_c::EvianEDDNTicker_c(const evian::TradeDatabaseEngine_c& cTradeDatabaseEngine, QWidget *parent) :
  QDockWidget(parent),
  ui(new Ui::EvianEDDNTicker_c),
  _cTradeDatabaseEngine(cTradeDatabaseEngine),
  _nPixelsPerSecond(100)
{
  ui->setupUi(this);
  _pqGraphicsScene = new QGraphicsScene(this);
  ui->graphicsView->setViewport(new QOpenGLWidget());
  ui->graphicsView->setScene(_pqGraphicsScene);
  ui->graphicsView->setRenderHints(QPainter::SmoothPixmapTransform | QPainter::HighQualityAntialiasing);

  SetSceneRect(ui->graphicsView->size());
}

EvianEDDNTicker_c::~EvianEDDNTicker_c()
{
  delete ui;
}

void
EvianEDDNTicker_c::EDDNMessagesAdded(int iRowHint, int nMessages)
{
  const auto&
    lpcEDDNMessage = _cTradeDatabaseEngine.GetTradeDatabase()._lpcEDDNMessage;

  for (auto i = iRowHint; i < iRowHint + nMessages; ++i)
  {
    CreateEDDNMessage(lpcEDDNMessage[i]);
  }
}

void
CenterAlignGraphicsItemText(QGraphicsTextItem *pqGraphicTextItem)
{
  pqGraphicTextItem->setTextWidth(pqGraphicTextItem->boundingRect().width());

  QTextBlockFormat 
    format;
  format.setAlignment(Qt::AlignCenter);
  
  QTextCursor 
    cursor = pqGraphicTextItem->textCursor();
  cursor.select(QTextCursor::Document);
  cursor.mergeBlockFormat(format);
  cursor.clearSelection();
  pqGraphicTextItem->setTextCursor(cursor);
}

void
EvianEDDNTicker_c::CreateEDDNMessage(QSharedPointer<eddn::EDDNMessage_c> pcEDDNMessage)
{
  auto 
    pcCommodityV2 = pcEDDNMessage.dynamicCast<eddn::CommodityV2_c>();

  QDateTime
    qDateTime = QDateTime::currentDateTimeUtc();

  qDateTime = qDateTime.addSecs(-180);

  // Return EDDN which are more than three minutes old.
  if (pcCommodityV2->_cHeader._cQDateTimeTimestamp < qDateTime)
  {
    //qDebug() << "Ignoring EDDN message with date " << pcCommodityV2->_cHeader._cQDateTimeTimestamp.toString() << " since it is older than " << qDateTime.toString();
    return;
  }

  if (!pcCommodityV2)
    return;
  
  auto
    s = QString("%1/%2\nCMDR %3").arg(pcCommodityV2->_cMessage._cProperties._cStationName).arg(pcCommodityV2->_cMessage._cProperties._cSystemName).arg(pcCommodityV2->_cHeader._cUploaderID);

  auto
    pqText = _pqGraphicsScene->addText(s, QFont("Euro Caps", 11));

  auto
    sHtml = QString("<p align=center>%1/%2<br>CMDR %3</p>").arg(pcCommodityV2->_cMessage._cProperties._cStationName).arg(pcCommodityV2->_cMessage._cProperties._cSystemName).arg(pcCommodityV2->_cHeader._cUploaderID);
  
  pqText->setTextWidth(pqText->boundingRect().width());
  pqText->setHtml(sHtml);
  

  auto
    qBoundingRect = pqText->boundingRect();
  
  auto 
    nPadding = (ui->graphicsView->height() - qBoundingRect.height()) / 2;
  
  QPointF
    qPointFBegin(ui->graphicsView->width(), nPadding),
    qPointFEnd(-qBoundingRect.width(), nPadding);

  QPropertyAnimation
    *pqPropertyAnimation = new QPropertyAnimation(pqText, "pos");

  QGraphicsItem
    *pqGraphicsItem = ui->graphicsView->itemAt((int)qPointFBegin.x(), (int)qPointFBegin.y());
  
  while(pqGraphicsItem != 0)
  {
    auto
      qBoundingRect = pqGraphicsItem->boundingRect();
    
    //qDebug() << "Old QPoints " << qPointFBegin << ", " << qPointFEnd;

    qPointFBegin += QPointF(qBoundingRect.width(), 0);

    //qDebug() << "New QPoints " << qPointFBegin << ", " << qPointFEnd;

    pqGraphicsItem = ui->graphicsView->itemAt((int)qPointFBegin.x(), (int)qPointFBegin.y());
  }

  pqPropertyAnimation->setStartValue(qPointFBegin);
  pqPropertyAnimation->setEndValue(qPointFEnd);

  auto
    nPixels = qPointFBegin.x() - qPointFEnd.x();

  auto
    rSeconds = 1.0 / (_nPixelsPerSecond / nPixels);

  pqPropertyAnimation->setDuration(rSeconds * 1000);

  pqPropertyAnimation->start(QAbstractAnimation::DeleteWhenStopped);
  connect(pqPropertyAnimation, &QPropertyAnimation::finished,
          pqText,              &QGraphicsTextItem::deleteLater);
}

void
EvianEDDNTicker_c::resizeEvent(QResizeEvent * pqResizeEvent)
{
  QDockWidget::resizeEvent(pqResizeEvent);

  SetSceneRect(ui->graphicsView->size());
}

void
EvianEDDNTicker_c::moveEvent(QMoveEvent * pqMoveEvent)
{
  QDockWidget::moveEvent(pqMoveEvent);
  SetSceneRect(ui->graphicsView->size());
}

void
EvianEDDNTicker_c::SetSceneRect(QSize qSize)
{
  _pqGraphicsScene->setSceneRect(0, 0, qSize.width(), qSize.height());
}
