//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#include <stdafx.h>
#include "ednetlogparserqt.hpp"

EDNetLogParserQT::EDNetLogParserQT(QObject *parent /*= 0*/) :
  QObject(parent),
  _pcQFileSystemWatcher(0)
{
  InitFileSystemWatcher();
  
  QSettings
    cQSettings;

  SetLogDirectory(cQSettings.value(key_fd_log_directory).toString());

  if (!_cCurrentLogFile.isEmpty())
  {
    try 
    {
      auto
       lcEvianDataBaseItem = ParseEDLogfile(_cCurrentLogFile);

      if (!lcEvianDataBaseItem.empty())
      {
       _cCurrentSystem = lcEvianDataBaseItem.back()._cSystemName;
      }
    }
    catch (std::runtime_error& cRuntime_Error)
    {
      qDebug() << "Could not parse " << _cCurrentLogFile << " e.what(): " << cRuntime_Error.what();
    }
  }

  _pcQTimer = new QTimer(this);
  connect(_pcQTimer, &QTimer::timeout,
          this,      &EDNetLogParserQT::CheckCurrentLogFile);

  _pcQTimer->start(10000);
}

bool EDNetLogParserQT::IsValidEDLogDirectory(QString cPath)
{
  return !FindMostRecentLogfile(cPath).isEmpty();
}

QString EDNetLogParserQT::FindMostRecentLogfile(QString cPath)
{
  QDir 
    cQDir(cPath);
    cQDir.setSorting(QDir::Time | QDir::DirsLast);

  QString
    cMostRecentLogFile;

  for(auto& cQFileInfo : cQDir.entryInfoList())
  {
    if (cQFileInfo.baseName().startsWith("netLog"))
    {
      cMostRecentLogFile = cQFileInfo.absoluteFilePath();
      break;
    }
  }

  return cMostRecentLogFile;
}

QString
EDNetLogParserQT::FindEDFile(QString sBasename)
{
  // Based on https://support.elitedangerous.com/kb/faq.php?id=108
  QStringList
    lsProductFolder,
    lsDriveLetter,
    lsGameFolder;
  
  lsProductFolder << "elite-dangerous-64"
                  << "FORC-FDEV-D-1010"  // Elite Dangerous
                  << "FORC-FDEV-D-1008"  // Elite Dangerous - Gamma
                  << "FORC-FDEV-D-1003"  // Elite Dangerous - Mercenary Edition
                  << "FORC-FDEV-D-1002"  // Elite Dangerous - Beta
                  << "FORC-FDEV-D-1001"  // Elite Dangerous - Premium Beta
                  << "FORC-FDEV-D-1000"; // Elite Dangerous - Alpha
  
  for (char c = 'c'; c <= 'z'; ++c)
    lsDriveLetter << QString(c);
  
  lsGameFolder << "Program Files (x86)/Frontier/Products"
               << "Program Files/Frontier/EDLaunch/Products" // Possible 64 bit version location?
               << "Program Files (x86)/Frontier/EDLaunch/Products"
               << "Program Files (x86)/Steam/steamapps/common/Elite Dangerous";

  for (auto qString : QStandardPaths::standardLocations(QStandardPaths::GenericDataLocation))
  {
    // Strip leading drive letter from qString
    if (qString.size() > 3 && qString[1] == ':' && qString[2] == '/')
      qString.remove(0, 3);

    lsGameFolder << qString + "/Frontier Developments/Products";
    lsGameFolder << qString + "/Frontier_Developments/Products";
  }

  QList<QFileInfo>
    lQFileInfo;

  for (QString sDriveLetter : lsDriveLetter)
    for (QString sGameFolder : lsGameFolder)
      for (QString sProductFolder : lsProductFolder)
      {
        QString
          s = sDriveLetter + ":/" + sGameFolder + "/" + sProductFolder + "/" + sBasename;
        
        QFileInfo
          qFileInfo(s);

        if (qFileInfo.exists()) 
        {
          lQFileInfo.append(qFileInfo);
          qDebug() << "Found " << qFileInfo.absoluteFilePath();
        }
      }
  
  if (lQFileInfo.isEmpty())
    return QString();

  std::sort(std::begin(lQFileInfo), std::end(lQFileInfo), 
    [](const QFileInfo& qFileInfo0, const QFileInfo& qFileInfo1)
    {
      return qFileInfo0.lastModified() < qFileInfo1.lastModified();
    });
  
  return lQFileInfo.last().absoluteFilePath();
}

QList<EvianLogDatabaseItem_c>
EDNetLogParserQT::ParseEDLogfile(QString cFilename)
{
  QFile
    qFile(cFilename);

  if (!qFile.open(QIODevice::ReadOnly | QIODevice::Text))
  {
    throw std::runtime_error(QString("Could not open file " + cFilename).toStdString());
  }

  QList<EvianLogDatabaseItem_c> 
    lcEvianLogDatabaseItem;
  
  QTextStream
    cQTextStream(&qFile);

  // 15-05-17-21:11 W. Europe Daylight Time  (19:11 GMT) - part 1
  // {21:11:59} GetPortMapping(57363) failed with code 714 (NoSuchEntryInArray)
  // {21:11:59} GetPortMapping(57363) External 80.212.204.200:57363 -> internal 10.0.0.26:57363 (0)
  // ...
  // {21:12:20} System:0(GCRV 2334) Body:1 Pos:(5.37825e+011,-4.50284e+010,1.34488e+011) 


  QDateTime
    cQDateTimeSessionStarted;
  // Try to read the timestamp at the top
  {
    QString
      cLine = cQTextStream.readLine();

    QString
      format = "yy-MM-dd-HH:mm";

    QStringList
      cQStringList = cLine.split(" ");

    if (cQStringList.size() > 0)
    {
      QDateTime
        cQDateTime = QDateTime::fromString(cQStringList[0], format);

      cQDateTime = cQDateTime.addYears(100); // yy is parsed as 19xx.
      cQDateTimeSessionStarted = cQDateTime;
      //qDebug() << "Parsed date: " << cQDateTime.toString();
    }
  }

  while (!cQTextStream.atEnd())
  {
    auto
      cLine = cQTextStream.readLine();

    // {21:12:20} System:0(GCRV 2334) Body:1 Pos:(5.37825e+011,-4.50284e+010,1.34488e+011) 

    QRegExp // Use negated class http://www.rexegg.com/regex-quantifiers.html#negclass_solution to avoid greedy matching.
      rx("\\{(\\d\\d:\\d\\d:\\d\\d)\\} System:\\d+\\(([^)]*)\\) Body:\\d+ Pos:\\(([^)]*)\\).*");
    rx.setPatternSyntax(QRegExp::RegExp2);

    if (!rx.exactMatch(cLine))
    {
      continue;
    }

    EvianLogDatabaseItem_c
      cEvianLogDatabaseItem;

    QStringList 
      cQStringList = rx.capturedTexts();
    // ["<full match>", "hh:mm:ss", "System Name", "nnnn, nnnn, nnn"]

    QString
      cFormat = "HH:mm:ss";

    auto
      cQDateTime = QDateTime::fromString(cQStringList[1], cFormat);

    if (!cQDateTime.isValid())
    {
      qDebug() << "Invalid date when parsing logfile!";
      continue;
    }

    cQDateTime.setDate(cQDateTimeSessionStarted.date());

    // Playing over midnight...
    while (cQDateTimeSessionStarted.isValid() && cQDateTime < cQDateTimeSessionStarted)
    {
      //qDebug() << "Adding one more day to " << cQDateTime.toString();
      cQDateTime = cQDateTime.addDays(1);
    }

    cEvianLogDatabaseItem._cQDateTime = cQDateTime;
    cEvianLogDatabaseItem._cSystemName = cQStringList[2];

    auto
      lcPositionString = cQStringList[3].split(",");

    if (lcPositionString.size() != 3)
    {
      qDebug() << "Trouble parsing position for system when parsing logfile";
      continue;
    }

    for (auto i = 0; i < 3; ++i)
    {
      cEvianLogDatabaseItem._cPosition.data[i] = lcPositionString[i].toFloat();
    }

    // Skip duplicates if the logfile say we are in the same system. (In and out of supercruise?)
    if (lcEvianLogDatabaseItem.empty() ||
        lcEvianLogDatabaseItem.back()._cSystemName != cEvianLogDatabaseItem._cSystemName)
    {
      lcEvianLogDatabaseItem.push_back(cEvianLogDatabaseItem);
    }
  }

  return lcEvianLogDatabaseItem;
}

void EDNetLogParserQT::BuildEvianLogDatabase()
{
  QDir 
    cQDir(_cEDLogDir);
  cQDir.setSorting(QDir::Time | QDir::DirsLast);

  bool
    isFirstLogFile = true;

  _lcEvianLogDatabaseItemPreviousLogFiles.clear();

  for(auto& cQFileInfo : cQDir.entryInfoList())
  {
    if (cQFileInfo.baseName().startsWith("netLog"))
    {
      auto
        cLogFile = cQFileInfo.absoluteFilePath();
      if (isFirstLogFile)
      {
        _lcEvianLogDatabaseItemActiveLogFile = ParseEDLogfile(cLogFile);
        isFirstLogFile = false;
      }
      else
      {
        _lcEvianLogDatabaseItemPreviousLogFiles += ParseEDLogfile(cLogFile);
      }
    }
  }
}

QString EDNetLogParserQT::GetCurrentSystem() const
{
  return _cCurrentSystem;
}

void EDNetLogParserQT::DirectoryChanged(QString cDirectory)
{
  auto 
    cPossibleNewLogFile = FindMostRecentLogfile(cDirectory);

  // Elite is probably deleting the file and then rewriting it fully for every new system, causing QFileSystemWatcher to 
  // lose it from the list.

  qDebug() << _pcQFileSystemWatcher->files();
  
  if (cPossibleNewLogFile != _cCurrentLogFile) 
  {
    qDebug() << "Starting to watch new logfile " << cPossibleNewLogFile;
    _pcQFileSystemWatcher->removePath(_cCurrentLogFile);
    _cCurrentLogFile = cPossibleNewLogFile;
    _pcQFileSystemWatcher->addPath(_cCurrentLogFile);
  }
}


void EDNetLogParserQT::FileChanged(QString cEDLogFile)
{
  //qDebug() << "Got filechanged on" << cEDLogFile;
  
  try {
    auto 
      lcEvianLogDatabaseItem = ParseEDLogfile(cEDLogFile);

    if (!lcEvianLogDatabaseItem.empty() && 
      lcEvianLogDatabaseItem.back()._cSystemName != _cCurrentSystem)
    {
      _cCurrentSystem = lcEvianLogDatabaseItem.back()._cSystemName;
      qDebug() << "Got new current system: " << _cCurrentSystem;
      _lcEvianLogDatabaseItemActiveLogFile = lcEvianLogDatabaseItem;

      emit CurrentSystemChanged(_cCurrentSystem);
      emit DatabaseUpdated(_lcEvianLogDatabaseItemActiveLogFile);
    }
  } 
  catch (std::runtime_error& err)
  {
    qDebug() << "Failed to parsed EDLogFile: " << cEDLogFile << " " << err.what();
    return;
  }
}

void EDNetLogParserQT::CheckCurrentLogFile()
{
  if (!_cCurrentLogFile.isEmpty())
    FileChanged(_cCurrentLogFile);
}

void EDNetLogParserQT::SetLogDirectory(QString cEDLogDir)
{
  _cEDLogDir = cEDLogDir;

  // Naively trust the directory that is set. Setter is expected to use the static query functions 
  // to ensure that it is a valid path.

  if (!_pcQFileSystemWatcher->directories().isEmpty())
  {
    qDebug() << "Removing watch on " << _pcQFileSystemWatcher->directories();
    _pcQFileSystemWatcher->removePaths(_pcQFileSystemWatcher->directories());
  }
  
  if (!_pcQFileSystemWatcher->files().isEmpty())
  {
    qDebug() << "Removing watch on " << _pcQFileSystemWatcher->files();
    _pcQFileSystemWatcher->removePaths(_pcQFileSystemWatcher->files());
  }
  
  if (QFile::exists(_cEDLogDir))
  {
    qDebug() << "Started watching directory: " << _cEDLogDir;
    
    _pcQFileSystemWatcher->addPath(_cEDLogDir);
    
    _cCurrentLogFile = FindMostRecentLogfile(_cEDLogDir);
    
    if (!_cCurrentLogFile.isEmpty())
    {
      _pcQFileSystemWatcher->addPath(_cCurrentLogFile);
      qDebug() << "Started watching " << _cCurrentLogFile;
    }
  }
}

void EDNetLogParserQT::SetMaxEDLogfiles(int)
{

}

void EDNetLogParserQT::SetWatching(bool isWatching)
{
  if (!isWatching)
  {
    if (_pcQFileSystemWatcher)
    {
      delete _pcQFileSystemWatcher;
      _pcQFileSystemWatcher = 0;
    }

    if (_pcQTimer  != 0)
    {
      _pcQTimer->stop();
      delete _pcQTimer;
      _pcQTimer = 0;
    }
  }
  else
  {
    InitFileSystemWatcher();
  }
}

void EDNetLogParserQT::InitFileSystemWatcher()
{
  if (!_pcQFileSystemWatcher)
  {
    _pcQFileSystemWatcher = new QFileSystemWatcher(this);
  }

  connect(_pcQFileSystemWatcher, &QFileSystemWatcher::directoryChanged,
    this, &EDNetLogParserQT::DirectoryChanged);

  connect(_pcQFileSystemWatcher, &QFileSystemWatcher::fileChanged,
    this, &EDNetLogParserQT::FileChanged);
}

EvianLogTableModel_c::EvianLogTableModel_c(const QList<EvianLogDatabaseItem_c>& lcEvianLogDatabaseItem, QObject * parent) :
  QAbstractTableModel(parent),
  _lcEvianLogDatabaseItem(lcEvianLogDatabaseItem)
{

}

int EvianLogTableModel_c::rowCount(const QModelIndex & /*parent*/) const 
{
  return static_cast<int>(_lcEvianLogDatabaseItem.size());
}

int EvianLogTableModel_c::columnCount(const QModelIndex & /*parent*/) const 
{
  return Field_Last;
}

QVariant EvianLogTableModel_c::data(const QModelIndex &index, int role /* = Qt::DisplayRole */) const 
{
  auto
    nRow = index.row(),
    nColumn = index.column();

  const auto&
    cEvianLogDatabaseItem = _lcEvianLogDatabaseItem[nRow];

  if (role == Qt::DisplayRole)
  {
    switch(nColumn)
    {
    case Field_Timestamp:
      return cEvianLogDatabaseItem._cQDateTime.time().toString();
    case Field_Position:
      return '(' + QString::number(cEvianLogDatabaseItem._cPosition.x, 'f', 2) + ", "
                 + QString::number(cEvianLogDatabaseItem._cPosition.y, 'f', 2) + ", "
                 + QString::number(cEvianLogDatabaseItem._cPosition.z, 'f', 2) + ")";
    case Field_SystemName:
      return cEvianLogDatabaseItem._cSystemName;
    case Field_Duration:
      if (_lcEvianLogDatabaseItem.size() > nRow + 1)
      {
        auto 
          nDifference = cEvianLogDatabaseItem._cQDateTime.secsTo(_lcEvianLogDatabaseItem[nRow+1]._cQDateTime);
        return QDateTime::fromTime_t(nDifference).toUTC().toString("mm:ss");
      }
   }

  }

  return QVariant();
}

QVariant EvianLogTableModel_c::headerData(int section, Qt::Orientation orientation, int role /* = Qt::DisplayRole */) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case Field_SystemName:
        return QString("System Name");
      case Field_Position:
        return QString("Position");
      case Field_Timestamp:
        return QString("Timestamp");
      case Field_Duration:
        return QString("Time spent in system");
      }
    }
  }
  return QVariant();
}

void EvianLogTableModel_c::DatabaseUpdated(QList<EvianLogDatabaseItem_c> lcEvianLogDatabaseItem)
{
  qDebug() << "Got new database. Resetting model";
  beginResetModel();
  _lcEvianLogDatabaseItem = lcEvianLogDatabaseItem;
  endResetModel();
  qDebug() << "Model reset";
}
