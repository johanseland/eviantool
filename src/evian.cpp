//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#include "stdafx.h"
#include "evian.hpp"
#include "evianutil.hpp"
#include "DatabaseQuery.hpp"
#include "eddn.hpp"
#include "database.hpp"

#include "jsmn.h"

//struct CommodityCatergory_c
//{
//  int           _nID;
//  std::string   _cName;
//
//  CommodityCatergory_c() :
//  _nID(-1)
//  {
//  }
//};

//struct Commodity_c
//{
//  int           _nID,
//                _nCategoryID,
//                _nAveragePrice;
//  std::string   _cName;  
//
//  Commodity_c() :
//    _nID(-1),
//    _nCategoryID(-1),
//    _nAveragePrice(-1)
//  {
//  }
//};


#define COMMODITY_KEYS \
  X(ID)           \
  X(CategoryID)   \
  X(AveragePrice) \
  X(Name)

namespace Commodity {
#define X(A) static const char A [] = #A;
COMMODITY_KEYS
#undef X
}

void evian::Commodity_c::WriteToJson(const evian::Commodity_c& cCommodity, QJsonObject& cQJsonObject)
{
  cQJsonObject[Commodity::AveragePrice] = cCommodity._nAveragePrice;
  cQJsonObject[Commodity::CategoryID]   = cCommodity._nCategoryID;
  cQJsonObject[Commodity::ID]           = cCommodity._nID;
  cQJsonObject[Commodity::Name]         = cCommodity._cName;
}

evian::Commodity_c evian::Commodity_c::ReadFromJson(const QJsonObject& cQJsonObject)
{
  Commodity_c
    cCommodity;

  cCommodity._nAveragePrice = cQJsonObject[Commodity::AveragePrice].toInt();
  cCommodity._nCategoryID   = cQJsonObject[Commodity::CategoryID].toInt();
  cCommodity._nID           = cQJsonObject[Commodity::ID].toInt();
  cCommodity._cName         = cQJsonObject[Commodity::Name].toString();

  return cCommodity;
}

bool json_token_streq(const char *js, jsmntok_t *t, const char *s)
{
  return (strncmp(js + t->start, s, t->end - t->start) == 0
    && strlen(s) == (size_t)(t->end - t->start));
}

char * json_token_tostr(char *js, jsmntok_t *t)
{
  js[t->end] = '\0';
  return js + t->start;
}

void
ParseCommodities(std::string filename)
{
  if (!evian::FileExists(filename))
  {
    throw std::runtime_error("Could not open " + filename);
  }

  FILE *
    pFile = fopen(filename.c_str(), "r");

  fseek(pFile, 0, SEEK_END);
  auto 
    nFileSize = ftell(pFile);
  fseek(pFile, 0, SEEK_SET);

  std::vector<char>
    lBuffer(nFileSize);

  fread(lBuffer.data(), 1, nFileSize, pFile);

  fclose(pFile);

  jsmn_parser
    cJsmn_Parser;

  jsmn_init(&cJsmn_Parser);

  auto 
    nTokens = jsmn_parse(&cJsmn_Parser, lBuffer.data(), lBuffer.size(), 0, 0);

  std::vector<jsmntok_t>
    lJsmntok(nTokens);

  jsmn_init(&cJsmn_Parser);
  
  assert(lJsmntok.size() <= std::numeric_limits<unsigned int>::max());
  
  auto
    nParsedTokens = jsmn_parse(&cJsmn_Parser, lBuffer.data(), lBuffer.size(), lJsmntok.data(), static_cast<unsigned int>(lJsmntok.size()));
  
  Q_UNUSED(nParsedTokens);

  assert(nParsedTokens >= 0);
  assert(nParsedTokens == nTokens);
  
  jsmntok_t*
    pToken = &lJsmntok[0];
  assert(pToken->type == JSMN_ARRAY);

  auto
    nCommodity = pToken->size;
  
  std::vector<evian::Commodity_c>
    lcCommodity(nCommodity);

  std::vector<evian::CommodityCatergory_c>
    lcCommodityCategories;

  for (auto iCommodity = 0; iCommodity < nCommodity; ++iCommodity)
  {
    // {"id":1,"name":"Explosives","category_id":1,"average_price":262,"category":{"id":1,"name":"Chemicals"}}
    // {"id":94,"name":"Wuthielo Ku Froth","category_id":15,"average_price":null,"category":{"id":15,"name":"Unknown"}}
    evian::Commodity_c& 
      cCommodity = lcCommodity[iCommodity];

    pToken++; 
    assert(pToken->type == JSMN_OBJECT);
    
    pToken++; 
    assert(pToken->type == JSMN_STRING && json_token_streq(lBuffer.data(), pToken, "id"));

    pToken++; 
    assert(pToken->type == JSMN_PRIMITIVE);    
    cCommodity._nID = atoi(json_token_tostr(lBuffer.data(), pToken));
    assert(cCommodity._nID = iCommodity + 1);

    pToken++; 
    assert(pToken->type == JSMN_STRING && json_token_streq(lBuffer.data(), pToken, "name"));
    pToken++; assert(pToken->type == JSMN_STRING);
    cCommodity._cName = json_token_tostr(lBuffer.data(), pToken);

    pToken++; 
    assert(pToken->type == JSMN_STRING && json_token_streq(lBuffer.data(), pToken, "category_id"));
    pToken++; assert(pToken->type == JSMN_PRIMITIVE);
    cCommodity._nCategoryID = atoi(json_token_tostr(lBuffer.data(), pToken));

    pToken++;
    assert(pToken->type == JSMN_STRING && json_token_streq(lBuffer.data(), pToken, "average_price"));
    pToken++; assert(pToken->type == JSMN_PRIMITIVE);
    cCommodity._nAveragePrice = atoi(json_token_tostr(lBuffer.data(), pToken));

    pToken++;
    assert(pToken->type == JSMN_STRING && json_token_streq(lBuffer.data(), pToken, "category"));

    pToken++;
    assert(pToken->type == JSMN_OBJECT);

    if (static_cast<int64_t>(lcCommodityCategories.size()) >= cCommodity._nCategoryID)
    {
      pToken += 4;
    }
    else {
      lcCommodityCategories.resize(cCommodity._nCategoryID);

      evian::CommodityCatergory_c&
        cCommodityCategory = lcCommodityCategories[cCommodity._nCategoryID-1];
      
      pToken++;
      assert(pToken->type == JSMN_STRING && json_token_streq(lBuffer.data(), pToken, "id"));

      pToken++;
      assert(pToken->type == JSMN_PRIMITIVE);
      cCommodityCategory._nID = atoi(json_token_tostr(lBuffer.data(), pToken));
      assert(cCommodity._nCategoryID = cCommodityCategory._nID);

      pToken++;
      assert(pToken->type == JSMN_STRING && json_token_streq(lBuffer.data(), pToken, "name"));

      pToken++; 
      assert(pToken->type == JSMN_STRING);
      cCommodityCategory._cName = json_token_tostr(lBuffer.data(), pToken);
    }
  }
}

template<>
double Dist(const Vec3<float>& v0, const Vec3<float>& v1)
{
  __m128
    v = _mm_setr_ps(v0.x-v1.x, v0.y-v1.y, v0.z - v1.z, 0.0f);

  return _mm_cvtss_f32(_mm_sqrt_ss(_mm_dp_ps(v, v, 0x71)));
}

std::bitset<37> ComputeStringHash(const QString& cQString)
{
  std::bitset<37>
    cBitset;
  
  const QByteArray
    str = cQString.toLower().toLatin1();
  
  for (const char& c : str)
  {
    if (c >= 97 && c <= 122) // IsLetter
    {
      cBitset[c-97] = 1;
    }
    else if (c >= 48 && c <= 57)
    {
      cBitset[c-22] = 1;
    }
  }

  return cBitset;
}

evian::Station_c::LandingPadSize 
evian::Station_c::LandingPadSizeFromString(const std::string cString)
{
  if (cString == "L")
  {
    return LANDINGPAD_LARGE;
  }
  else if (cString == "M")
  {
    return LANDINGPAD_MEDIUM;
  }
  else if (cString == "S")
  {
    return LANDINGPAD_SMALL;
  }
  return LANDINGPAD_UNKNOWN;
}

bool evian::Station_c::IsPlanetaryOutpostFromString(const QString& qString)
{
  return qString == QLatin1Literal("Planetary Outpost");
}

evian::Station_c::Station_c() :
  _paBuyPrice(0),
  _paSellPrice(0),
  _paSupply(0),
  _isBlackMarket(false),
  _isRefuel(false),
  _isRepair(false),
  _isRearm(false),
  _isOutfitting(false),
  _isShipyard(false),
  _isPlanetaryOutpost(false)
{
  _nSystemID = -1;
  _pcSystem = nullptr;
  _nUpdated = -1;
  _nShipyardUpdated = -1;
  _nDistanceToStar = -1;
  _eMaxLandingPadSize = LANDINGPAD_UNKNOWN;
}

evian::Station_c::~Station_c()
{
  if (_paBuyPrice)
  {
    _aligned_free(_paBuyPrice);
  }

  if (_paSellPrice)
  {
    _aligned_free(_paSellPrice);
  }

  if (_paSupply)
  {
    _aligned_free(_paSupply);
  }
}

void evian::Station_c::CreateScratchArrays()
{
  EVIAN_ASSERT(_paBuyPrice == 0 && _paSellPrice == 0 && "No need to create scratch arrays more than once");

  _paBuyPrice  = static_cast<int*>(_aligned_malloc(sizeof(int) * MAX_COMMODITIES, BLOCK_ALIGNMENT));
  _paSellPrice = static_cast<int*>(_aligned_malloc(sizeof(int) * MAX_COMMODITIES, BLOCK_ALIGNMENT));
  _paSupply    = static_cast<int*>(_aligned_malloc(sizeof(int) * MAX_COMMODITIES, BLOCK_ALIGNMENT));

  std::fill(_paBuyPrice, _paBuyPrice + MAX_COMMODITIES, BUY_DEFAULT);
  std::fill(_paSellPrice, _paSellPrice + MAX_COMMODITIES, SELL_DEFAULT);
  std::fill(_paSupply, _paSupply + MAX_COMMODITIES, 0);
}

void
evian::Station_c::DisableListing(const Listing_c& cListing)
{
  EVIAN_ASSERT(cListing._nCommodityID >= 0);
  EVIAN_ASSERT(!cListing._isEnabled);

  _paBuyPrice [cListing._nCommodityID] = BUY_DEFAULT;
  _paSellPrice[cListing._nCommodityID] = SELL_DEFAULT;
  _paSupply   [cListing._nCommodityID] = SUPPLY_DEFAULT;
}

void
evian::Station_c::EnableListing(const Listing_c& cListing)
{
  EVIAN_ASSERT(cListing._nCommodityID >= 0);
  EVIAN_ASSERT(cListing._isEnabled);

  _paBuyPrice [cListing._nCommodityID] = cListing._nBuyPrice  <= 0 ? Station_c::BUY_DEFAULT  : cListing._nBuyPrice;
  _paSellPrice[cListing._nCommodityID] = cListing._nSellPrice <= 0 ? Station_c::SELL_DEFAULT : cListing._nSellPrice;
  _paSupply   [cListing._nCommodityID] = cListing._nSupply    <= 0 ? SUPPLY_DEFAULT          : cListing._nSupply;
}

int
evian::Station_c::GetSupplyForCommodity(const int nID) const
{
  for (int i = 0; i < _lcListing.size(); ++i)
  {
    if (_lcListing[i]._nCommodityID == nID)
    {
      return _lcListing[i]._nSupply;
    }
  }
  return 0;
/*
  Listing_c
    cListing;

  cListing._nCommodityID = nID;


  auto
    it = std::lower_bound(_lcListing.begin(), _lcListing.end(), cListing);
         

  if (it == _lcListing.end())
  {
    return 0;
  } 
  else
  {
    return it->_nSupply;
  }*/
}

const evian::Listing_c& evian::Station_c::GetListingForCommodity(int nCommodityID) const
{
  auto 
    it = std::find_if(_lcListing.begin(), _lcListing.end(), 
      [nCommodityID](const Listing_c& cListing){ return cListing._nCommodityID == nCommodityID; });

  //EVIAN_ASSERT(it != _lcListing.end());
  
  if (it == _lcListing.end())
  {
    return Listing_c::DummyListing;
  }

  return *it;
}

std::string 
evian::Station_c::PrettyPrintListings(const TradeDatabaseStorage_c& cTradeDatabase) const
{
  std::stringstream
    ss;

  ss << "Station: " << _cName.toStdString() << std::endl;
  for (const Listing_c& l : _lcListing) 
  {
    ss << cTradeDatabase.GetCommodityName(l._nCommodityID).toStdString()
       << " sell: " << l._nSellPrice 
       << " buy:" << l._nBuyPrice 
       << " supply: " << l._nSupply
       << " demand: " << l._nDemand
       << std::endl;
  }

  return ss.str();
}

#define STATION_KEYS       \
  X(ID)                    \
  X(SystemID)              \
  X(DistanceToStar)        \
  X(Updated)               \
  X(Name)                  \
  X(StarportType)          \
  X(MaxLandingPadSize)     \
  X(ProhibitedCommodities) \
  X(BlackMarket)           \
  X(Refuel)                \
  X(Repair)                \
  X(Rearm)                 \
  X(Outfitting)            \
  X(Shipyard)              \
  X(Economies)             \
  X(Faction)               \
  X(Allegiance)            \
  X(Government)            \
  X(State)                 \
  X(Listings)

namespace Station
{
#define X(A) static const char A [] = #A;
  STATION_KEYS
#undef X
}

QJsonArray
evian::Station_c::WriteToJsonAsArray(const Station_c& cStation)
{
  QJsonArray
    qJsonArray;

  qJsonArray.append(cStation._nID);
  qJsonArray.append(cStation._cName);
  qJsonArray.append(cStation._nSystemID);
  qJsonArray.append(cStation._nDistanceToStar);
  qJsonArray.append(cStation._nUpdated);
  qJsonArray.append(cStation._cStarportType);
  qJsonArray.append(cStation._eMaxLandingPadSize);
  qJsonArray.append(cStation._isBlackMarket);
  qJsonArray.append(cStation._isRefuel);
  qJsonArray.append(cStation._isRepair);
  qJsonArray.append(cStation._isRearm);
  qJsonArray.append(cStation._isOutfitting);
  qJsonArray.append(cStation._isShipyard);
  qJsonArray.append(cStation._sAllegiance);
  qJsonArray.append(cStation._sGovernment);
  qJsonArray.append(cStation._sFaction);
  qJsonArray.append(cStation._sEconomicState);
  
  QJsonArray
    qJsonArrayListing;

  for (const auto& cListing : cStation._lcListing)
  {
    QJsonArray
      qJsonArrayCommodity;

    qJsonArrayCommodity.append(cListing._nCommodityID);
    qJsonArrayCommodity.append(cListing._nBuyPrice);
    qJsonArrayCommodity.append(cListing._nSellPrice);
    qJsonArrayCommodity.append(cListing._nSupply);
    qJsonArrayCommodity.append(cListing._nDemand);
    qJsonArrayCommodity.append(cListing._nUpdated);
    qJsonArrayCommodity.append(cListing._isEnabled);

    qJsonArrayListing.append(qJsonArrayCommodity);
  }

  qJsonArray.append(qJsonArrayListing);

  QJsonArray
    qJsonArrayProhibitedCommodities;
  
  for (const auto& cCommodity : cStation._lcProhibitedCommodities)
  {
    qJsonArrayProhibitedCommodities.append(cCommodity);
  }

  qJsonArray.append(qJsonArrayProhibitedCommodities);

  QJsonArray
    qJsonArrayEconomies;

  for (const auto& sEconomy : cStation._lcEconomies)
  {
    qJsonArrayEconomies.append(sEconomy);
  }

  qJsonArray.append(qJsonArrayEconomies);

  return qJsonArray;
}

void
evian::Station_c::WriteListingsJson(const Station_c & cStation, QJsonObject & qJsonObject)
{
  qJsonObject[Station::ID] = cStation._nID;

  QJsonArray
    qJsonArray;

  for (const auto& cListing : cStation._lcListing)
  {
    QJsonArray
      cQJsonArrayListing;
    cQJsonArrayListing.append(cListing._nCommodityID);
    cQJsonArrayListing.append(cListing._nBuyPrice);
    cQJsonArrayListing.append(cListing._nSellPrice);
    cQJsonArrayListing.append(cListing._nSupply);
    cQJsonArrayListing.append(cListing._nDemand);
    cQJsonArrayListing.append(cListing._nUpdated);
    cQJsonArrayListing.append(cListing._isEnabled);

    qJsonArray.append(cQJsonArrayListing);
  }

  qJsonObject[Station::Listings] = qJsonArray;
}

void
evian::Station_c::DeserializeListings(std::vector<Station_c>& lcStation, const char* paKey, const QJsonObject& qJsonObjectParent)
{
  EVIAN_ASSERT(qJsonObjectParent[paKey].isArray());

  const QJsonArray
    qJsonArray = qJsonObjectParent[paKey].toArray();

  EVIAN_ASSERT(lcStation.size() == qJsonArray.size());

  // lcStation is assumed to be sorted
  for (auto iStation = 0; iStation < lcStation.size(); ++iStation)
  {
    Station_c&
      cStation = lcStation[iStation];

    QJsonObject
      qObject = qJsonArray[iStation].toObject();

    QJsonArray
      qJsonArray = qObject[Station::Listings].toArray();
    EVIAN_ASSERT(cStation._nID == qObject[Station::ID].toInt());

    cStation._lcListing.resize(qJsonArray.size());

    ReadListingsJson(cStation, qJsonArray);
  }
}


void
evian::Station_c::ReadListingsJson(Station_c& cStation, QJsonArray& qJsonArray)
{
  for (auto i = 0; i < qJsonArray.size(); ++i)
  {
    QJsonArray
      qJsonArrayListing = qJsonArray[i].toArray();

    auto &
      cListing = cStation._lcListing[i];

    cListing._nCommodityID = qJsonArrayListing[0].toInt();
    cListing._nBuyPrice    = qJsonArrayListing[1].toInt();
    cListing._nSellPrice   = qJsonArrayListing[2].toInt();
    cListing._nSupply      = qJsonArrayListing[3].toInt();
    cListing._nDemand      = qJsonArrayListing[4].toInt();
    cListing._nUpdated     = qJsonArrayListing[5].toDouble();
    cListing._isEnabled    = qJsonArrayListing[6].toBool();
    cListing._nStationID = cStation._nID;
  }
}

void
evian::Station_c::WriteToJson(const Station_c& cStation, QJsonObject& cQJsonObject)
{
  cQJsonObject[Station::ID]                = cStation._nID;
  cQJsonObject[Station::Name]              = cStation._cName;
  cQJsonObject[Station::SystemID]          = cStation._nSystemID;
  cQJsonObject[Station::DistanceToStar]    = cStation._nDistanceToStar;
  cQJsonObject[Station::Updated]           = cStation._nUpdated;
  cQJsonObject[Station::StarportType]      = cStation._cStarportType;
  cQJsonObject[Station::MaxLandingPadSize] = cStation._eMaxLandingPadSize;
  cQJsonObject[Station::BlackMarket]       = cStation._isBlackMarket;
  cQJsonObject[Station::Refuel]            = cStation._isRefuel;
  cQJsonObject[Station::Repair]            = cStation._isRepair;
  cQJsonObject[Station::Rearm]             = cStation._isRearm;
  cQJsonObject[Station::Outfitting]        = cStation._isOutfitting;
  cQJsonObject[Station::Shipyard]          = cStation._isShipyard;
  cQJsonObject[Station::Allegiance]        = cStation._sAllegiance;
  cQJsonObject[Station::Government]        = cStation._sGovernment;
  cQJsonObject[Station::Faction]           = cStation._sFaction;
  cQJsonObject[Station::State]             = cStation._sEconomicState;

  QJsonArray
    cQJsonArray;

  //for (const auto& cListing: cStation._lcListing)
  //{
  //  QJsonArray
  //    cQJsonArrayListing;
  //  cQJsonArrayListing.append(cListing._nCommodityID);
  //  cQJsonArrayListing.append(cListing._nBuyPrice);
  //  cQJsonArrayListing.append(cListing._nSellPrice);
  //  cQJsonArrayListing.append(cListing._nSupply);
  //  cQJsonArrayListing.append(cListing._nDemand);
  //  cQJsonArrayListing.append(cListing._nUpdated);
  //  cQJsonArrayListing.append(cListing._isEnabled);

  //  cQJsonArray.append(cQJsonArrayListing);
  //}
  //
  //cQJsonObject[Station::Listings] = cQJsonArray;

  //cQJsonArray = QJsonArray(); // No clear function.
  //EVIAN_ASSERT(cQJsonArray.isEmpty());

  for (const auto& cCommodity: cStation._lcProhibitedCommodities)
  {
    cQJsonArray.append(cCommodity);
  }

  cQJsonObject[Station::ProhibitedCommodities] = cQJsonArray;

  cQJsonArray = QJsonArray(); // No clear function.
  
  for (const auto& sEconomy: cStation._lcEconomies)
  {
    cQJsonArray.append(sEconomy);
  }

  cQJsonObject[Station::Economies] = cQJsonArray;
}

evian::Station_c 
evian::Station_c::ReadFromJson(const QJsonObject& cQJsonObject)
{
  Station_c
    cStation;

  cStation._nID                = cQJsonObject[Station::ID].toDouble();
  cStation._cName              = cQJsonObject[Station::Name].toString();
  cStation._nSystemID          = cQJsonObject[Station::SystemID].toDouble();
  cStation._nDistanceToStar    = cQJsonObject[Station::DistanceToStar].toDouble();
  cStation._nUpdated           = cQJsonObject[Station::Updated].toDouble();
  cStation._cStarportType      = cQJsonObject[Station::StarportType].toString();
  cStation._eMaxLandingPadSize = static_cast<LandingPadSize>(cQJsonObject[Station::MaxLandingPadSize].toInt());
  cStation._isBlackMarket      = cQJsonObject[Station::BlackMarket].toBool();
  cStation._isRefuel           = cQJsonObject[Station::Refuel].toBool();
  cStation._isRepair           = cQJsonObject[Station::Repair].toBool();
  cStation._isRearm            = cQJsonObject[Station::Rearm].toBool();
  cStation._isOutfitting       = cQJsonObject[Station::Outfitting].toBool();
  cStation._isShipyard         = cQJsonObject[Station::Shipyard].toBool();

  cStation._sAllegiance        = cQJsonObject[Station::Allegiance].toString();
  cStation._sGovernment        = cQJsonObject[Station::Government].toString();
  cStation._sFaction           = cQJsonObject[Station::Faction].toString();
  cStation._sEconomicState     = cQJsonObject[Station::State].toString();
  cStation._isPlanetaryOutpost = IsPlanetaryOutpostFromString(cStation._cStarportType);

  QJsonArray
    cQJsonArray = cQJsonObject[Station::Listings].toArray();

  cStation._lcListing.resize(cQJsonArray.size());

  for (auto i = 0; i < cQJsonArray.size(); ++i)
  {
    QJsonArray
      cQJsonArrayListing = cQJsonArray[i].toArray();

    auto &
      cListing = cStation._lcListing[i];

    cListing._nCommodityID = cQJsonArrayListing[0].toInt();
    cListing._nBuyPrice    = cQJsonArrayListing[1].toInt();
    cListing._nSellPrice   = cQJsonArrayListing[2].toInt();
    cListing._nSupply      = cQJsonArrayListing[3].toInt();
    cListing._nDemand      = cQJsonArrayListing[4].toInt();
    cListing._nUpdated     = cQJsonArrayListing[5].toDouble();
    
    if (cQJsonArrayListing.size() == 7)
    {
      cListing._isEnabled = cQJsonArrayListing[6].toBool();
    }

    cListing._nStationID   = cStation._nID;
  }

  cQJsonArray = cQJsonObject[Station::ProhibitedCommodities].toArray();
  
  for (auto i = 0; i < cQJsonArray.size(); ++i)
  {
    QString
      cQString = cQJsonArray[i].toString();

    cStation._lcProhibitedCommodities.push_back(cQString);
  }

  cQJsonArray = cQJsonObject[Station::Economies].toArray();
  
  for (auto i = 0; i < cQJsonArray.size(); ++i)
  {
    QString
      cQString = cQJsonArray[i].toString();

    cStation._lcEconomies.push_back(cQString);
  }

  return cStation;
}

bool evian::Listing_c::operator<(const Listing_c & cListing) const
{
  return _nCommodityID < cListing._nCommodityID;
}

bool evian::Listing_c::IsSameStationAndCommodiy(const Listing_c& cListing) const
{
  return _nCommodityID == cListing._nCommodityID && _nStationID == cListing._nStationID;
}

bool evian::Listing_c::IsValid() const
{
  return _nCommodityID != -1;
}

#define LISTING_KEYS \
  X(CommodityID) \
  X(BuyPrice)    \
  X(SellPrice)   \
  X(Supply)      \
  X(Demand)      \
  X(Updated)

//  X(StationID)   \
//  X(UpdateCount) \

namespace Listing {
namespace key {
#define X(A) key##A,
enum JSON {LISTING_KEYS };
#undef X

#define X(A) #A,
static const char *Name[] = { LISTING_KEYS };
#undef X

#define X(A) static const char A [] = #A;
LISTING_KEYS
#undef X
}
}

const evian::Listing_c
evian::Listing_c::DummyListing = evian::Listing_c();

void evian::Listing_c::WriteToJson(const Listing_c& cListing, QJsonObject& cQJsonObject)
{
  using namespace Listing;

#define X(A) cQJsonObject[key::##A] = cListing._n##A;
  LISTING_KEYS
#undef X
}

void evian::CommodityCatergory_c::WriteToJson(const CommodityCatergory_c& cCommodityCategory, QJsonObject& cQJsonObject)
{
  cQJsonObject["ID"]   = cCommodityCategory._nID;
  cQJsonObject["Name"] = cCommodityCategory._cName;
}

evian::CommodityCatergory_c evian::CommodityCatergory_c::ReadFromJson(const QJsonObject& cQJsonObject)
{
  CommodityCatergory_c
    cCommodityCategory;

  cCommodityCategory._nID   = cQJsonObject["ID"].toDouble();
  cCommodityCategory._cName = cQJsonObject["Name"].toString();

  return cCommodityCategory;
}

#define SYSTEM_KEYS      \
  X(ID)                  \
  X(Position)            \
  X(Updated)             \
  X(Name)                \
  X(NeedPermit)          \
  X(PowerControlFaction) \
  X(Population)          \
  X(Faction)             \
  X(State)               \
  X(Government)          \
  X(Security)            \
  X(PrimaryEconomy)      \
  X(Allegiance)          \
  X(PowerState)

namespace System
{
#define X(A) static const char A [] = #A;
SYSTEM_KEYS
#undef X
}

void
evian::System_c::WriteToJson(const System_c& cSystem, QJsonObject& cQJsonObject)
{
  cQJsonObject[System::ID]                  = cSystem._nID;
  cQJsonObject[System::Name]                = cSystem._cName;
  cQJsonObject[System::Updated]             = cSystem._nUpdated;
  cQJsonObject[System::NeedPermit]          = cSystem._bNeedPermit;
  cQJsonObject[System::PowerControlFaction] = cSystem._sPowerControlFaction;
  cQJsonObject[System::PowerState]          = cSystem._sPowerState;
  cQJsonObject[System::Allegiance]          = cSystem._sAllegiance;
  cQJsonObject[System::Population]          = cSystem._nPopulation;
  cQJsonObject[System::Faction]             = cSystem._sMinorFaction;
  cQJsonObject[System::Government]          = cSystem._sGovernment;
  cQJsonObject[System::State]               = cSystem._sEconomicState;
  cQJsonObject[System::Security]            = cSystem._sSecurity;
  cQJsonObject[System::PrimaryEconomy]      = cSystem._sPrimaryEconomy;

  QJsonArray
    cQJSonArray;

  cQJSonArray.append(cSystem._cPosition.x);
  cQJSonArray.append(cSystem._cPosition.y);
  cQJSonArray.append(cSystem._cPosition.z);

  cQJsonObject[System::Position] = cQJSonArray;
}

evian::System_c
evian::System_c::ReadFromJson(const QJsonObject& cQJsonObject)
{
  System_c
    cSystem;

  cSystem._nID                  = cQJsonObject[System::ID].toDouble();
  cSystem._cName                = cQJsonObject[System::Name].toString();
  cSystem._nUpdated             = cQJsonObject[System::Updated].toDouble();
  cSystem._bNeedPermit          = cQJsonObject[System::NeedPermit].toBool();
  cSystem._sPowerControlFaction = cQJsonObject[System::PowerControlFaction].toString();
  cSystem._sPowerState          = cQJsonObject[System::PowerState].toString();
  cSystem._sAllegiance          = cQJsonObject[System::Allegiance].toString();
  cSystem._nPopulation          = cQJsonObject[System::Population].toInt();
  cSystem._sMinorFaction        = cQJsonObject[System::Faction].toString();
  cSystem._sGovernment          = cQJsonObject[System::Government].toString();
  cSystem._sEconomicState       = cQJsonObject[System::State].toString();
  cSystem._sSecurity            = cQJsonObject[System::Security].toString();
  cSystem._sPrimaryEconomy      = cQJsonObject[System::PrimaryEconomy].toString();

  auto 
    cQJsonArray = cQJsonObject[System::Position].toArray();

  cSystem._cPosition.x = cQJsonArray[0].toDouble();
  cSystem._cPosition.y = cQJsonArray[1].toDouble();
  cSystem._cPosition.z = cQJsonArray[2].toDouble();

  return cSystem;
}
