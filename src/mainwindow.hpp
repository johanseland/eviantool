//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QAbstractTableModel>
#include <QSettings>
#include <QFileSystemWatcher>

#include "evian.hpp"
#include "database.hpp"
#include "ImportEDDB.hpp"

namespace evian {
  class BestSymmetricTrade_c;
  struct Station_c;
  struct System_c;
  class QueryExclusionDatabase_c;
}

namespace Ui {
class MainWindow;
}

namespace eddn {
  class Subscriber;
}

class AboutDialog_c;
class eviansettingsqt;
class EDNetLogParserQT;
class QComboBox;
class EvianApplication;
class EvianEDDNTicker_c;

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(EvianApplication * pEvianApplication, QWidget *parent = 0);

  ~MainWindow();

  static void MessageHandler(QtMsgType eQtMsgType, const QMessageLogContext &context, const QString &cQString);

  evian::TradeDatabaseEngine_c
                  *_pcTradeDatabaseEngine;
  
  QList<QSharedPointer<evian::DatabaseQuery_c>>
                _lcDatabaseQuery;

  QList<QAbstractTableModel*>
                _lpcQAbstractTableModel;

  Ui::MainWindow
                *ui;

  QLabel        *_pcQLabelCurrentSystem;

  EDNetLogParserQT
                *_pcEDNetLogParserQT;

  static QString LandingPadToString(evian::Station_c::LandingPadSize);

  QString GetCurrentSystem() const;

  void ShowStatusMessage(QString);

  Q_ENUMS(Message_Class);
  enum Message_Class { Message_Information, Message_Warning, Message_Error };

  QList<QDockWidget*> GetDockWidgetsForArea(Qt::DockWidgetArea);

  QDockWidget* DecorateAsDockWidget(QString cTitle, QWidget* pcQWidget);
  void AddToTopArea(QDockWidget* pcQDockWidget);

public slots:
  void DeleteDatabaseExplorerQtChildren();
  void ShowStatusMessage(QString, Message_Class);
  void SetCurrentSystem(QString);
  void QuitApplication();

protected:
  void paintEvent(QPaintEvent *);

private slots:
  void on_actionQuit_triggered();
  void on_actionNew_Query_triggered();
  void on_actionQuery_2_triggered();
  void on_actionDatabase_triggered();
  void on_actionSettings_triggered();

  void CreateEvianSettings();
  void CreateEvianEDDNTicker();

  void on_actionAbout_triggered();
  void on_tabWidget_tabCloseRequested(int index);
  
  void StartEDDNSubscriber();
  void RebuildEDDBDatabase();

signals:
  void DatabaseStorageCreated(QSharedPointer<evian::TradeDatabaseStorage_c>);
  void DatabaseStatusUpdated(QString);
  void CurrentSystemChanged(QString);
  void StatusMessageUpdated(QString, int);

private:
  void ImportDatabase();
  void ImportDatabaseFromEDDB();

  void InitCurrentSystem();
  void InitLogFileWatcher();

  void InitDatabaseEngine();
  void TerminateDatabaseEngine();

  void InitEDDNSubscriber();
  void TerminateEDDNSubscriber();
  
  void InitUpdater();
  void TerminateUpdater();

  eviansettingsqt
                *_pcEvianSettingsQt;
  
  AboutDialog_c
                *_pcAboutDialog;
  
  EvianEDDNTicker_c
                *_pcEvianEDDNTicker;

  eddn::Subscriber
                *_pcEDDNSubscriber;

  QThread       *_pcQThreadEDDNSubscriber,
                *_pcQThreadDatabaseEngine;

  EvianApplication 
                *_pcEvianApplication;

  QPoint        _cQPointOldPos;

  QStringList   _cQStringList;

  static MainWindow 
                *_pcMainWindowInstance;
};

/// The sole responsibilty of EvianApplication is to hook into
/// the QT-GUI event loop and make sure we have a database read-lock before GUI-events are processed.
class EvianApplication : public QApplication
{
public:
  EvianApplication(int &argc, char* argv[]);
  bool notify(QObject *, QEvent *) override;

  void SetMainWindow(MainWindow * pcMainWindow);

private:
  MainWindow    *_pcMainWindow;
};

class ExecuteQuery : public QThread
{
  Q_OBJECT

  QSharedPointer<evian::DatabaseQuery_c>
                _pcDatabaseQuery;

  evian::TradeDatabaseEngine_c&
                _cTradeDatabaseEngine;

  int64_t       _nElapsedTime,
                _nResult;
                
public:
  ExecuteQuery(QObject *parent, QSharedPointer<evian::DatabaseQuery_c> pcDatabaseQuery, evian::TradeDatabaseEngine_c& cTradeDatabase);
  ~ExecuteQuery();

  void run();

signals:
  void QueryCompleted();
  void ElapsedTimeMessage(QString, int);
};

class ImportEDDBFutureHelper_c : public QObject
{
  Q_OBJECT
public:
  ImportEDDBFutureHelper_c(QObject * pcQObject = 0);
  ~ImportEDDBFutureHelper_c();

  void Start();
signals:
  void DatabaseStorageCreated(QSharedPointer<evian::TradeDatabaseStorage_c>);
  void DatabaseStatusUpdated();

private slots:
  void FutureFinished();
  void FutureCanceled();
private:
  //typedef QSharedPointer<evian::TradeDatabaseStorage_c> Result_Type;
  typedef evian::ImportEDDB_c::CreateTradeDatabaseResult_c Result_Type;

  QFuture<Result_Type> 
                _cQFuture;

  QFutureWatcher<Result_Type>
                _cQFutureWatcher;
};


class StationsTableModel_c : public QAbstractTableModel
{
  Q_OBJECT
public:
  StationsTableModel_c(MainWindow *pMainWindow, QObject * parent = 0);

  int rowCount(const QModelIndex & parent) const override;
  int columnCount(const QModelIndex & parent) const override;
  QVariant data(const QModelIndex &index, int role /* = Qt::DisplayRole */) const override;
  QVariant headerData(int section, Qt::Orientation orientation, int role /* = Qt::DisplayRole */) const;

  enum StationField
  {
    Field_Name, Field_System, Field_DistanceToStar, Field_MaxLandingPadSize, Field_StarportType, Field_Updated, StationsField_Last
  };

private:
  MainWindow   *_pcMainWindow;
  std::vector<evian::Station_c>
                _lcStation;
  std::vector<evian::System_c>
                _lcSystem;
};

class ListingTableModel_c : public QAbstractTableModel
{
  Q_OBJECT
public:
  ListingTableModel_c(MainWindow * pMainWindow, QObject * parent);
  int rowCount(const QModelIndex & parent) const override;
  int columnCount(const QModelIndex & parent) const override;
  QVariant data(const QModelIndex &index, int role /* = Qt::DisplayRole */) const override;
  QVariant headerData(int section, Qt::Orientation orientation, int role /* = Qt::DisplayRole */) const;

  Qt::ItemFlags flags(const QModelIndex & index) const override;

  bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole) override;

  void SetStationID(int nStationID);

  enum ListingField
  {
      Field_Name, Field_BuyPrice, Field_SellPrice, Field_Supply, Field_Demand, Field_UpdateCount, Field_Updated, Field_ListingStatus, ListingField_Last
  };

public slots:
  void UpdateListing(evian::Listing_c, int iRowHint);
  void AddListing(evian::Listing_c, int iRowHint);

signals:
  void ListingUpdated(evian::Listing_c, int iRowHint);

private:
  MainWindow   *_pcMainWindow;
  int           _nStationID;
};

#endif // MAINWINDOW_HPP
