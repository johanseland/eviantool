#include "stdafx.h"
#include "evianqt.hpp"

void 
evian::QtMessageHandler(QtMsgType eQtMsgType, const QMessageLogContext &QMessageLogContext, const QString &sMessage)
{
  QSettings
    cQSettings;

  auto
    sLogFile = cQSettings.value(key_logfile).toString();

  QFile
    cQFile(sLogFile);

  bool
    isLogToFile = cQFile.open(QIODevice::Append | QIODevice::Text);

  EVIAN_ASSERT(isLogToFile && "Cannot create logfile");

  auto
    sFormattedMessage = qFormatLogMessage(eQtMsgType, QMessageLogContext, sMessage);
    
  if (isLogToFile)
  {
    QTextStream 
      out(&cQFile);
    
    out << sFormattedMessage << endl;
  }

  OutputDebugString(reinterpret_cast<const wchar_t*>(sFormattedMessage.utf16()));
}

QVariant
evian::InitAndSaveSettingsHelper(const QString& key, const QVariant& defaultValue)
{
  QSettings
    cQSettings;

  auto
    cValue = cQSettings.value(key, defaultValue);

  if (!cQSettings.contains(key))
  {
    cQSettings.setValue(key, cValue);
  }

  return cValue;
}

QString
evian::EvianUserAgentString()
{
  auto
    sUserAgent = QString("CMDR Evians Tool v%1").arg(QCoreApplication::applicationVersion());
  sUserAgent.remove('"');

  return sUserAgent;
}

void
evian::AddDropShadowEffect(QWidget *pcQWidget)
{
  QGraphicsDropShadowEffect * dse = new QGraphicsDropShadowEffect();
  dse->setBlurRadius(16);
  dse->setOffset(0);
  dse->setColor(QColor(255, 255, 255));
  pcQWidget->setGraphicsEffect(dse);
}
