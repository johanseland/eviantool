//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#include "tradedetailwidget.hpp"
#include "ui_tradedetailwidget.h"
#include "mainwindow.hpp"
#include "database.hpp"
#include "evianqt.hpp"

TradeDetailWidget_c::TradeDetailWidget_c(MainWindow * pcMainWindow, QWidget *parent) :
  QWidget(parent),
  ui(new Ui::TradeDetailWidget_c),
  _pcMainWindow(pcMainWindow),
  _pcStation0(0),
  _pcStation1(0)
{
  _cQPalette = QPalette(QColor(210, 20, 50));
  setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
  //setAttribute(Qt::WA_TranslucentBackground); // Only needed if you want dropshadow, but this overrides stylesheet background-color.
  ui->setupUi(this);

  connect(&_cQTimer, &QTimer::timeout,
          this,     &TradeDetailWidget_c::UpdateUI);

  _cQTimer.setSingleShot(false);
  _cQTimer.start();
}

TradeDetailWidget_c::~TradeDetailWidget_c()
{
  delete ui;
}

void
TradeDetailWidget_c::SetDirectionalTrade(evian::BestTrade_c cDirectionalTrade)
{
  const auto&
    cTradeDatabase = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase();

  _pcStation0 = &cTradeDatabase.GetStation(cDirectionalTrade._nFromStationID),
  _pcStation1 = &cTradeDatabase.GetStation(cDirectionalTrade._nToStationID);

  _cSystem0 = cTradeDatabase.GetSystem(cDirectionalTrade._nFromSystemID);
  _cSystem1 = cTradeDatabase.GetSystem(cDirectionalTrade._nToSystemID);

  _cListing0Buy  = _pcStation0->GetListingForCommodity(cDirectionalTrade._nCommodityID);
  _cListing0Sell = _pcStation1->GetListingForCommodity(cDirectionalTrade._nCommodityID);

  _cListing1Buy = evian::Listing_c();
  _cListing1Sell = evian::Listing_c();

  UpdateUIImpl(cTradeDatabase);
  UpdateCurrentSystem(_pcMainWindow->GetCurrentSystem());
}

void
TradeDetailWidget_c::SetSymmetricTrade(evian::SymmetricTrade_c cSymmetricTrade)
{
  const auto&
    cTradeDatabase = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase();

  _pcStation0 = &cTradeDatabase.GetStation(cSymmetricTrade._cBestTrade0._nFromStationID),
  _pcStation1 = &cTradeDatabase.GetStation(cSymmetricTrade._cBestTrade0._nToStationID);
  
  _cSystem0 = cTradeDatabase.GetSystem(cSymmetricTrade._cBestTrade0._nFromSystemID),
  _cSystem1 = cTradeDatabase.GetSystem(cSymmetricTrade._cBestTrade0._nToSystemID);

  if (cSymmetricTrade._cBestTrade0._nCommodityID != -1)
  {
    _cListing0Buy  = _pcStation0->GetListingForCommodity(cSymmetricTrade._cBestTrade0._nCommodityID);
    _cListing0Sell = _pcStation1->GetListingForCommodity(cSymmetricTrade._cBestTrade0._nCommodityID);
  }
  else
  {
    _cListing0Buy  = evian::Listing_c();
    _cListing0Sell = evian::Listing_c();
  }
  
  if (cSymmetricTrade._cBestTrade1._nCommodityID != -1)
  {
    _cListing1Buy  = _pcStation1->GetListingForCommodity(cSymmetricTrade._cBestTrade1._nCommodityID);
    _cListing1Sell = _pcStation0->GetListingForCommodity(cSymmetricTrade._cBestTrade1._nCommodityID);
  }
  else
  {
    _cListing1Buy  = evian::Listing_c();
    _cListing1Sell = evian::Listing_c();
  }

  UpdateUIImpl(cTradeDatabase);

  UpdateCurrentSystem(_pcMainWindow->GetCurrentSystem());
}

void TradeDetailWidget_c::UpdateCurrentSystem(QString cCurrentSystemName)
{
  auto
    nCurrentSystemID = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetSystemID(cCurrentSystemName);

  if (nCurrentSystemID != -1) // Compute distance from current system
  {
    const auto
      &cCurrentSystem = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetSystem(nCurrentSystemID);

    auto
      rDistSystem0 = Dist(cCurrentSystem._cPosition, _cSystem0._cPosition),
      rDistSystem1 = Dist(cCurrentSystem._cPosition, _cSystem1._cPosition);

    if (rDistSystem0 < rDistSystem1) 
    {
      ui->labelFooter->setText(QString("%1 to %2: %3 LY").arg(cCurrentSystem._cName).arg(_cSystem0._cName).arg(QString::number(rDistSystem0)));
    }
    else
    {
      ui->labelFooter->setText(QString("%1 to %2: %3 LY").arg(cCurrentSystem._cName).arg(_cSystem1._cName).arg(QString::number(rDistSystem1)));
    }
  }
}

void
TradeDetailWidget_c::UpdateUI()
{
  UpdateUIImpl(_pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase());
}

void TradeDetailWidget_c::UpdateListing(const evian::Listing_c cListing, int /*iRowHint*/)
{
  if (cListing._nStationID != _cListing0Buy._nStationID &&
      cListing._nStationID != _cListing1Buy._nStationID)
  {
    return;
  }
  
  if (_cListing0Buy.IsSameStationAndCommodiy(cListing))
  {
    _cListing0Buy = cListing;
  }
  else if (_cListing0Sell.IsSameStationAndCommodiy(cListing))
  {
    _cListing0Sell = cListing;
  }
  else if (_cListing1Buy.IsSameStationAndCommodiy(cListing))
  {
    _cListing1Buy = cListing;
  }
  else if (_cListing1Sell.IsSameStationAndCommodiy(cListing))
  {
    _cListing1Sell = cListing;
  }
  else
  {
    return;
  }

  UpdateUIImpl(_pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase());

}

void
TradeDetailWidget_c::UpdateUIImpl(const evian::TradeDatabaseStorage_c &cTradeDatabase)
{
  QLocale
    cQLocale(QLocale::English);

  ui->labelBuyPrice0->setText(  cQLocale.toString(_cListing0Buy._nBuyPrice) + " Cr");
  ui->labelSellPrice0->setText( cQLocale.toString(_cListing0Sell._nSellPrice) + " Cr");

  ui->labelBuyPrice1->setText(  cQLocale.toString(_cListing1Buy._nBuyPrice) + " Cr");
  ui->labelSellPrice1->setText( cQLocale.toString(_cListing1Sell._nSellPrice) + " Cr");

  QString
    cStation0Text = QString("%1/%4\n%2 ls, Pad: %3").arg(_pcStation0->_cName).arg(_pcStation0->_nDistanceToStar).arg(MainWindow::LandingPadToString(_pcStation0->_eMaxLandingPadSize)).arg(_cSystem0._cName),
    cStation1Text = QString("%1/%4\n%2 ls, Pad: %3").arg(_pcStation1->_cName).arg(_pcStation1->_nDistanceToStar).arg(MainWindow::LandingPadToString(_pcStation1->_eMaxLandingPadSize)).arg(_cSystem1._cName);

  QString
    cAllegiance0Text = FormatAllegianceString(_cSystem0),
    cAllegiance1Text = FormatAllegianceString(_cSystem1);

  ui->labelSystem0->setText(cStation0Text);
  ui->labelSystem1->setText(cStation1Text);

  ui->labelStation0->setText(cAllegiance0Text);
  ui->labelStation1->setText(cAllegiance1Text);

  ui->labelCommodity0->setText(cTradeDatabase.GetCommodityName(_cListing0Buy._nCommodityID));
  ui->labelCommodity1->setText(cTradeDatabase.GetCommodityName(_cListing1Buy._nCommodityID));

  auto
    nProfit0 = _cListing0Sell._nSellPrice - _cListing0Buy._nBuyPrice,
    nProfit1 = _cListing1Sell._nSellPrice - _cListing1Buy._nBuyPrice;

  ui->labelProfit0->setText(cQLocale.toString(nProfit0) + " Cr");
  ui->labelProfit1->setText(cQLocale.toString(nProfit1) + " Cr");

  ui->labelBuySupply0->setText(QString("Supply: ") + cQLocale.toString(_cListing0Buy._nSupply));
  ui->labelBuySupply1->setText(QString("Supply: ") + cQLocale.toString(_cListing1Buy._nSupply));
  ui->labelDurationBuyPrice0->setText (FormatDurationSinceNow(QDateTime::fromTime_t(_cListing0Buy._nUpdated)));
  ui->labelDurationSellPrice0->setText(FormatDurationSinceNow(QDateTime::fromTime_t(_cListing0Sell._nUpdated)));
  ui->labelDurationBuyPrice1->setText (FormatDurationSinceNow(QDateTime::fromTime_t(_cListing1Buy._nUpdated)));
  ui->labelDurationSellPrice1->setText(FormatDurationSinceNow(QDateTime::fromTime_t(_cListing1Sell._nUpdated)));

  if (std::find(_pcStation0->_lcProhibitedCommodities.begin(), _pcStation0->_lcProhibitedCommodities.end(), ui->labelCommodity0->text()) != 
      _pcStation0->_lcProhibitedCommodities.end())
  {
    evian::AddDropShadowEffect(ui->labelIllicitBuy0);
    ui->labelIllicitBuy0->setText("Illicit Cargo");
    ui->labelIllicitBuy0->show();
  }
  else
  {
    ui->labelIllicitBuy0->hide();
  }

  if (std::find(_pcStation1->_lcProhibitedCommodities.begin(), _pcStation1->_lcProhibitedCommodities.end(), ui->labelCommodity1->text()) != 
                _pcStation1->_lcProhibitedCommodities.end())
  {
    ui->labelIllicitBuy1->setText("Illicit Cargo");
    QGraphicsDropShadowEffect * dse = new QGraphicsDropShadowEffect();
    dse->setBlurRadius(16);
    dse->setOffset(0);
    dse->setColor(QColor(255, 255, 255));
    ui->labelIllicitBuy1->setGraphicsEffect(dse);
    ui->labelIllicitBuy1->show();
  }
  else
  {
    ui->labelIllicitBuy1->hide();
  }

  auto 
    cDistance = QString::number( Dist(_cSystem0._cPosition, _cSystem1._cPosition), 'f', 2) + " LY";

  ui->labelHeader->setText(QString("Profit: %1 CR, Distance: %2").arg(cQLocale.toString(nProfit0 + nProfit1)).arg(cDistance));
  
  
  // Hide or show GUI depending on if we are directional or symmetric.
  ui->widgetSell1->setVisible(_cListing1Buy.IsValid());
  ui->widgetBuy1->setVisible(_cListing1Buy.IsValid());
  ui->widgetTrade1->setVisible(_cListing1Buy.IsValid());
  ui->labelLoopIcon->setVisible(_cListing1Buy.IsValid());
  ui->labelDirectionalIcon->setVisible(!_cListing1Buy.IsValid());

  QList<QDateTime>
    lcQDateTime;

  lcQDateTime << QDateTime::fromTime_t(_cListing0Buy._nUpdated)
              << QDateTime::fromTime_t(_cListing0Sell._nUpdated)
              << QDateTime::fromTime_t(_cListing1Buy._nUpdated)
              << QDateTime::fromTime_t(_cListing1Sell._nUpdated);
  
  auto
    nNextUpdate = ComputeNextUIRedraw(lcQDateTime);

  _cQTimer.setInterval(nNextUpdate);
}

QString
TradeDetailWidget_c::FormatAllegianceString(const evian::System_c& cSystem) const
{
  QString
    s;

  if (!cSystem._sPowerControlFaction.isEmpty())
  {
    s += cSystem._sPowerControlFaction;
  }
  else if (!cSystem._lcExploitedBy.isEmpty())
  {
    s += cSystem._lcExploitedBy[0]._sPowerControlFaction + "(Exploited)";
  }

  if (!cSystem._sAllegiance.isEmpty())
  {
    if (!s.isEmpty())
    {
      s += "/";
    }

    s += cSystem._sAllegiance;
  }
  
  return s;
}

void
TradeDetailWidget_c::mouseDoubleClickEvent(QMouseEvent *)
{
  hide();
  deleteLater();
}

void 
TradeDetailWidget_c::mousePressEvent(QMouseEvent * pcQMouseEvent)
{
  _cQPointOldPos = pcQMouseEvent->globalPos();
}

void
TradeDetailWidget_c::mouseMoveEvent(QMouseEvent * pcQMouseEvent)
{
  const QPoint 
    delta = pcQMouseEvent->globalPos() - _cQPointOldPos;

  move(x()+delta.x(), y()+delta.y());
  _cQPointOldPos = pcQMouseEvent->globalPos();
}

QString
FormatDurationSinceNow(QDateTime cQDateTime)
{
  auto
    nSecs = cQDateTime.secsTo(QDateTime::currentDateTime());


  if (nSecs < 60) // Seconds
  {
    return QString::number(nSecs) + " sec";
  } 
  else if (nSecs < 60*60) // Minutes
  {
    return QString::number(nSecs/60) + " min";
  }
  else if (nSecs < 60*60*24) // Hours
  {
    return QString::number(nSecs/(60*60)) + " + hours";
  }
  else
  {
    return QString::number(cQDateTime.daysTo(QDateTime::currentDateTime())) + " days";
  }
}

int
ComputeNextUIRedraw(QList<QDateTime> lcQDateTime)
{
  auto
    cQDateTime = *std::max_element(lcQDateTime.begin(), lcQDateTime.end());

  auto
    nSecs = cQDateTime.secsTo(QDateTime::currentDateTime());

  if (nSecs < 60)
  {
    return 1000;
  }
  else if (nSecs < 60*60)
  {
    return 60 * 1000;
  }
  else if (nSecs < 60*60*24)
  {
    return 60 * 60 * 1000;
  }
  else
  {
    return 60 * 60 * 24 * 1000;
  }
}
