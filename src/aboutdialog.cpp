//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "aboutdialog.h"
#include "ui_aboutdialog.h"

AboutDialog_c::AboutDialog_c(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AboutDialog_c)
{
  setWindowFlags(Qt::Window | Qt::FramelessWindowHint | Qt::Dialog);
  setAttribute(Qt::WA_TranslucentBackground);
  
  ui->setupUi(this);
  auto
    s = QString("Version: %1").arg(QCoreApplication::applicationVersion());

  ui->labelVersion->setText(s);
  
  dynamic_cast<QBoxLayout*>(ui->widgetSizeGrip->layout())->addWidget(new QSizeGrip(this), 0, Qt::AlignBottom | Qt::AlignRight);

  QFont
    cQFont = ui->label_2->property("font").value<QFont>();

  QFontMetrics
    cQFontMetrics(cQFont);

  int
    nWidth = cQFontMetrics.width("Faster than light trading");
 
  resize(1.2 * nWidth, 1.2*nWidth);

}

AboutDialog_c::~AboutDialog_c()
{
    delete ui;
}

void 
AboutDialog_c::mousePressEvent(QMouseEvent * pcQMouseEvent)
{
  _cQPointOldPos = pcQMouseEvent->globalPos();
}

void
AboutDialog_c::mouseMoveEvent(QMouseEvent * pcQMouseEvent)
{
  const QPoint 
    delta = pcQMouseEvent->globalPos() - _cQPointOldPos;

  move(x()+delta.x(), y()+delta.y());
  _cQPointOldPos = pcQMouseEvent->globalPos();
}
