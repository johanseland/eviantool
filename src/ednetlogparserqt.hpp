//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef EDNETLOGPARSERQT_HPP
#define EDNETLOGPARSERQT_HPP

#include "evian.hpp"

class EvianLogDatabaseItem_c
{
public:
  QDateTime     _cQDateTime;
  QString       _cSystemName;
  Vec3f         _cPosition;
};

class EvianLogTableModel_c : public QAbstractTableModel
{
  Q_OBJECT

public:
  EvianLogTableModel_c(const QList<EvianLogDatabaseItem_c>& lcEvianLogDatabaseItem, QObject* parent = 0);
  int rowCount(const QModelIndex & parent) const override;
  int columnCount(const QModelIndex & parent) const override;
  QVariant data(const QModelIndex &index, int role /* = Qt::DisplayRole */) const override;
  QVariant headerData(int section, Qt::Orientation orientation, int role /* = Qt::DisplayRole */) const;

  enum Field
  {
    Field_Timestamp, Field_SystemName, Field_Position, Field_Duration, Field_Last
  };

public slots:
  void DatabaseUpdated(QList<EvianLogDatabaseItem_c>);

private:
  QList<EvianLogDatabaseItem_c>
                _lcEvianLogDatabaseItem;
};

class EDNetLogParserQT : public QObject
{
  Q_OBJECT
public:
  //EDNetLogParserQT(QString cFilename, QObject * parent = 0);
  EDNetLogParserQT(QObject * parent = 0);

  static bool     IsValidEDLogDirectory(QString cPath);
  static QString 
		              FindMostRecentLogfile(QString cPath);
  static QString 
		              FindEDFile(QString sBasename);
  static QList<EvianLogDatabaseItem_c> 
		              ParseEDLogfile(QString cFilename);

  void BuildEvianLogDatabase();

 /* QList<EvianLogDatabaseItem_c>
                _lcEvianLogDatabaseItem;
 */ 
  QString GetCurrentSystem() const;

public slots:
  void SetLogDirectory(QString);
  void SetMaxEDLogfiles(int);
  void SetWatching(bool);

private slots:
  void DirectoryChanged(QString);
  void FileChanged(QString);
  void CheckCurrentLogFile();

signals:  
  void CurrentSystemChanged(QString);
  void DatabaseUpdated(QList<EvianLogDatabaseItem_c>);

private:
  QFileSystemWatcher
                *_pcQFileSystemWatcher;

  QTimer
                *_pcQTimer;

  void InitFileSystemWatcher();

  QString       _cEDLogDir,
                _cCurrentLogFile,
                _cCurrentSystem;

  QList<EvianLogDatabaseItem_c>
                _lcEvianLogDatabaseItemActiveLogFile,
                _lcEvianLogDatabaseItemPreviousLogFiles;
};

#endif // EDNETLOGPARSERQT_HPP
