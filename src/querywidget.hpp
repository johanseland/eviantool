//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef QUERYWIDGET_HPP
#define QUERYWIDGET_HPP

#include <QWidget>
#include "DatabaseQuery.hpp"
#include "evianqueryqt.hpp"

namespace Ui {
class QueryWidget_c;
}

class MainWindow;

namespace QueryWidget {
  static const char ANY_LOCATION[] = "-- ANY --";
  static const char FACTION_PREFIX[] = "Faction ";
  static const char SPLIT = '/';

  QString RemoveFactionPrefix(const QString& cQString);
}

class RestrictionDatabaseView_c : public QObject
{
Q_OBJECT

public:
  RestrictionDatabaseView_c(MainWindow * pcMainWindow, QWidget * parent);

  void SetLayout(QLayout * pcQLayout);

public slots:
  void AddExludedCommodity(QString);
  void AddExcludedSystem(QString);
  void AddExcludedStation(QString, QString);
  void AddExcludedFaction(QString);

  void ClearAllExclusions();

signals:
  void IncludeFaction(QString);
  void IncludeCommodity(QString);
  void IncludeSystem(QString);
  void IncludeStation(QString, QString);

private:
  MainWindow *  _pcMainWindow;
  QLayout *     _pcQLayout;
  QWidget *     _pcQWidgetParent;
  void SetupClickedConnections(QPushButton * pcQPushButton);
};

class CompleterListCache_c
{
public:
  void SetQStringList(const QStringList& cQStringList);
  QStringList GetStringList(const QString& sKey);

  static void ConsiderModifyCompleter(const QString & cQString, QCompleter * pcQCompleter, QLineEdit* pcQLineEdit, CompleterListCache_c * pcCompleterListCache);
  static void SetupQCompleter(QCompleter * pcQCompleterLocationFrom);
  static QString DeduceLocationFromCompleter(QLineEdit * pcQLineEdit);
private:
  QStringList
                _cQStringList;

  QHash<QString, QStringList>
                _cQHash;
};

class QueryWidget_c : public QWidget
{
  Q_OBJECT

public:
  explicit QueryWidget_c(MainWindow * pcMainWindow, QWidget *parent = 0);
  ~QueryWidget_c();

  Q_ENUMS(Query_Type);

  enum Query_Type { Query_Hop, Query_Loop };

  Q_PROPERTY(Query_Type queryType READ GetQueryType WRITE SetQueryType NOTIFY QueryTypeChanged);

  Query_Type GetQueryType() const;
  void SetQueryType(Query_Type eQueryType);
private slots:
  void on_pushButtonExecuteQuery_clicked();

  void HeaderContextMenuRequested(QPoint);
  void SymmetricTradeModelContextMenuRequested(QPoint);
  void DirectionalTradeModelContextMenuRequested(QPoint);
  void RestrictionContextMenuRequested(QPoint);

  void TradeDetailsRequested(QModelIndex);

  void on_pushButtonSwapLocations_clicked();
  void on_checkBoxAdvancedSettings_clicked(bool checked);
  void SliderMoved(int);

  void on_pushButtonSetCurrentSystem_clicked();

  void on_lineEditStartLocation_textEdited(const QString &arg1);
  void on_lineEditEndLocation_textEdited(const QString &arg1);

  void on_doubleSpinBoxMaxLY_valueChanged(double arg1);

  void on_doubleSpinBoxMaxLS_valueChanged(double arg1);

  void on_comboBoxMinLandingPad_activated(int index);

  void on_comboBoxMaxLandingPad_activated(int index);

  void on_spinBoxMinProfit_valueChanged(int arg1);

  void on_spinBoxMinimumSupply_valueChanged(int arg1);

  void on_checkBoxIgnorePlanetaryOutposts_toggled(bool checked);

signals:
  void ClearAllRestrictions();
  void QueryTypeChanged();

  void ExcludeCommodity(QString);
  void ExcludeStation(QString, QString);
  void ExcludeSystem(QString);
  void ExcludeFaction(QString);

private:
  void SetWindowAndTabTitle(const QString& cQString);
  void SetDefaultValuesFromSettings();

  QAction* CreateClearAllAction(QMenu* pcQMenu);

  QString DeduceSystemNameBegin() const;
  QString DeduceSystemNameEnd() const;
  QString DeduceSystemName(const QString& cQString) const;

  QString FormatLocation(const QString& cQString) const;
  QString FormatSystemDistance() const;

  void Message(QString cQString) const;

  void InitCompleters();
  void InitTableViewResults();
  
  void ExecuteSymmetricAnyToAnyQuery();
  void ExecuteSymmetricQueryGroupToGroup(  const QList<std::reference_wrapper<const evian::System_c>>& lcSystemFrom, const QList<std::reference_wrapper<const evian::System_c>>& lcSystemTo);
  void ExecuteDirectionalQueryGroupToGroup(const QList<std::reference_wrapper<const evian::System_c>>& lcSystemFrom, const QList<std::reference_wrapper<const evian::System_c>>& lcSystemTo);

  void CreateSymmetricModelAndStartQuery(QSharedPointer<evian::SymmetricTradeQuery_c> pcSymmetricTradeQuery);
  void CreateDirectionalModelAndStartQuery(QSharedPointer<evian::DirectionalTradeQuery_c> pcDirectionalTradeQuery);

  Query_Type    _eQueryType;

  evian::ConcurrencyMode GetConcurrencyMode() const;
  evian::QueryRestriction_c CreateQueryRestriction() const;

  void SetupAndPerformDirectionalQuery();
  void SetupAndPerformSymmetricQuery();
  
  void AddOtherStationsInSystemToIgnoreList(evian::QueryRestriction_c& cQueryRestriction, QString cStationName, QString cSystemName) const;
  
  bool IsAnyLocation(const QString& cQString) const;

  void DisableBackgroundImage();
  void EnableBackgroundImage();

  QList<std::reference_wrapper<const evian::System_c>> GetFromGroup();
  QList<std::reference_wrapper<const evian::System_c>> GetToGroup();
  QList<std::reference_wrapper<const evian::System_c>> GetSystemGroup(const QString& cQString);

  MainWindow    *_pcMainWindow;
  Ui::QueryWidget_c 
                *ui;

  QSortFilterProxyModel
                *_pcProxyModel;

  QStringList   _lcQStringSystemsAndStations;

  RestrictionDatabaseView_c
                _cRestrictionDatabaseView;

  CompleterListCache_c
                _cCompleterListCache;

 evian::QueryExclusionDatabase_c
                _cQueryRestrictionDatabase;
};

#endif // QUERYWIDGET_HPP
