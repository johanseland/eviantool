//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#include "stdafx.h"
#include "evian.hpp"
#include "ImportEDDB.hpp"
#include "jsmn.h"
#include "evianutil.hpp"
#include "database.hpp"

using namespace evian;

class JSONParser
{
public:
  JSONParser(std::string cFileName)
  {
    _cBuffer = ReadFileIntoBuffer(cFileName);

    jsmn_init(&_cJsmn_Parser);

    auto  
      nTokens = jsmn_parse(&_cJsmn_Parser, _cBuffer.data(), _cBuffer.size(), 0, 0);

    jsmn_init(&_cJsmn_Parser);

    _lcJsmntok.resize(nTokens);

    assert(_lcJsmntok.size() <= std::numeric_limits<unsigned int>::max());

    auto 
      nParsedTokens = jsmn_parse(&_cJsmn_Parser, _cBuffer.data(), _cBuffer.size(), _lcJsmntok.data(), static_cast<unsigned int>(_lcJsmntok.size()));
    
    Q_UNUSED(nParsedTokens);
    if (nParsedTokens < 0)
    {
      std::stringstream 
        ss;
      ss << "Error when parsing " << cFileName << " jsmn_parse returned " << nParsedTokens;
      throw JSONParserException_c(ss.str());
    }

    EVIAN_ASSERT(nParsedTokens == nTokens);
  }

  class JSONParserException_c : public std::runtime_error
  {
  public:
    JSONParserException_c(const std::string& what) :
      std::runtime_error(what)
    {}
  };

  QString     GetTokenAsQString(jsmntok_t * pJsmntok)
  {
    return QString::fromLatin1(json_token_tostr(_cBuffer.data(), pJsmntok));
  }

  std::string GetTokenAsString(jsmntok_t* pJsmntok) 
  {
    return json_token_tostr(_cBuffer.data(), pJsmntok);
  }

  int GetTokenAsInt(jsmntok_t* pJsmntok)
  {
    return atoi(json_token_tostr(_cBuffer.data(), pJsmntok));
  }

  float GetTokenAsFloat(jsmntok_t* pJsmntok)
  {
    return static_cast<float>(atof(json_token_tostr(_cBuffer.data(), pJsmntok)));
  }

  bool IsTokenStringEqual(jsmntok_t* pJsmntok, const char * s) const
  {
    return json_token_streq(_cBuffer.data(), pJsmntok, s);
  }

  jsmntok_t* GetStartingToken()
  {
    assert(!_lcJsmntok.empty());
    return _lcJsmntok.data();
  }

  size_t GetNumberOfTokens() const
  {
    return _lcJsmntok.size();
  }

  template<class T> jsmntok_t* ParseInto(T&, const char* key, jsmntok_t* pJsmntok)
  {
    std::static_assert(false);
    return pJsmntok;
  }

  template<> jsmntok_t* ParseInto(int& nInt, const char* key, jsmntok_t* pToken)
  {
    Q_UNUSED(key);

    assert(pToken->type == JSMN_STRING && IsTokenStringEqual(pToken, key));
    pToken++;
    assert(pToken->type == JSMN_PRIMITIVE || pToken->type == JSMN_STRING);
    // Had a case where the token type was JSMN_STRING, but value was still 2.
    nInt = GetTokenAsInt(pToken);
    pToken++;

    return pToken;
  }

  template<> jsmntok_t* ParseInto(int64_t& nInt, const char* key, jsmntok_t* pToken)
  {
    Q_UNUSED(key);

    assert(pToken->type == JSMN_STRING && IsTokenStringEqual(pToken, key));
    pToken++;
    assert(pToken->type == JSMN_PRIMITIVE);
    nInt = GetTokenAsInt(pToken);
    pToken++;

    return pToken;
  }

  template<> jsmntok_t* ParseInto(std::string& cString, const char* key, jsmntok_t* pToken)
  {
    Q_UNUSED(key);

    assert(pToken->type == JSMN_STRING && IsTokenStringEqual(pToken, key));
    pToken++;

    assert(pToken->type == JSMN_STRING || pToken->type == JSMN_PRIMITIVE);
    cString = GetTokenAsString(pToken);

    if (cString == "null")
    {
      cString.clear();
    }
    
    pToken++;

    return pToken;
  }

  template<> jsmntok_t* ParseInto(QString& cString, const char* key, jsmntok_t* pToken)
  {
    Q_UNUSED(key);

    assert(pToken->type == JSMN_STRING && IsTokenStringEqual(pToken, key));
    pToken++;

    assert(pToken->type == JSMN_STRING || pToken->type == JSMN_PRIMITIVE);
    cString = GetTokenAsQString(pToken);

    if (cString == "null")
    {
      cString.clear();
    }

    pToken++;

    return pToken;
  }

  template<> jsmntok_t* ParseInto(bool& isTrue, const char* key, jsmntok_t* pToken)
  {
    Q_UNUSED(key);

    assert(pToken->type == JSMN_STRING && IsTokenStringEqual(pToken, key));
    pToken++;

    assert(pToken->type == JSMN_STRING || pToken->type == JSMN_PRIMITIVE);
    std::string
      sString = GetTokenAsString(pToken);

    if (sString == "0" || sString == "null")
    {
      isTrue = false;
    }
    else
    {
      EVIAN_ASSERT(sString == "1");
      isTrue = true;
    }

    pToken++;

    return pToken;
  }

  template<> jsmntok_t* ParseInto(float& rFloat, const char* key, jsmntok_t* pToken)
  {
    Q_UNUSED(key);

    assert(pToken->type == JSMN_STRING && IsTokenStringEqual(pToken, key));
    pToken++;
    assert(pToken->type == JSMN_PRIMITIVE);
    rFloat = GetTokenAsFloat(pToken);
    pToken++;

    return pToken;
  }

  jsmntok_t* SkipArray(const char* key, jsmntok_t* pToken)
  {
    Q_UNUSED(key);
    assert(pToken->type == JSMN_STRING && IsTokenStringEqual(pToken, key));
    pToken++;
    assert(pToken->type == JSMN_ARRAY);
    pToken += pToken->size; // Only for arrays without objects!
    
    pToken++;
    return pToken;
  }

  template<> jsmntok_t * ParseInto(std::vector<std::string>& lcString, const char* key, jsmntok_t* pToken)
  {
    Q_UNUSED(key);
    assert(pToken->type == JSMN_STRING && IsTokenStringEqual(pToken, key));
    pToken++;
    assert(pToken->type == JSMN_ARRAY);
    const auto 
      nSize = pToken->size;
    
    for (auto i = 0; i < nSize; ++i)
    {
      pToken++;
      assert(pToken->type == JSMN_STRING);
      lcString.emplace_back(GetTokenAsString(pToken));
    }

    pToken++;

    return pToken;
  }

  template<> jsmntok_t * ParseInto(std::vector<QString>& lcString, const char* key, jsmntok_t* pToken)
  {
    Q_UNUSED(key);
    assert(pToken->type == JSMN_STRING && IsTokenStringEqual(pToken, key));
    pToken++;
    assert(pToken->type == JSMN_ARRAY);
    const auto 
      nSize = pToken->size;

    for (auto i = 0; i < nSize; ++i)
    {
      pToken++;
      assert(pToken->type == JSMN_STRING);
      lcString.emplace_back(GetTokenAsQString(pToken));
    }

    pToken++;

    return pToken;
  }

private:
  jsmn_parser
    _cJsmn_Parser;

  std::vector<char>
    _cBuffer;

  std::vector<jsmntok_t>
    _lcJsmntok;
    
  bool json_token_streq(const char *js, jsmntok_t *t, const char *s) const
  {
    return (strncmp(js + t->start, s, t->end - t->start) == 0
      && strlen(s) == (size_t)(t->end - t->start));
  }

  char * json_token_tostr(char *js, jsmntok_t *t)
  {
    js[t->end] = '\0';
    return js + t->start;
  }
};

void
ImportEDDB_c::ParseCommodities(std::string cFileName)
{
  JSONParser
    cJSONParser(cFileName);

  auto
    pToken = cJSONParser.GetStartingToken();

  auto
    nCommodity = pToken->size;

  _lcCommodity.reserve(nCommodity);

  for (auto iCommodity = 0; iCommodity < nCommodity; ++iCommodity)
  {
    // {"id":1,"name":"Explosives","category_id":1,"average_price":262,"category":{"id":1,"name":"Chemicals"}}
    // {"id":94,"name":"Wuthielo Ku Froth","category_id":15,"average_price":null,"category":{"id":15,"name":"Unknown"}}
    Commodity_c
      cCommodity;// = _lcCommodity[iCommodity];

    pToken++;
    assert(pToken->type == JSMN_OBJECT);

    pToken++;
    assert(pToken->type == JSMN_STRING && cJSONParser.IsTokenStringEqual(pToken, "id"));

    pToken++;
    assert(pToken->type == JSMN_PRIMITIVE);
    cCommodity._nID = cJSONParser.GetTokenAsInt(pToken);
    //assert(cCommodity._nID == iCommodity + 1); // Not true! There are holes in the commodity-list!

    pToken++;
    assert(pToken->type == JSMN_STRING && cJSONParser.IsTokenStringEqual(pToken, "name"));
    pToken++; assert(pToken->type == JSMN_STRING);
    cCommodity._cName = cJSONParser.GetTokenAsQString(pToken);

    pToken++;
    assert(pToken->type == JSMN_STRING && cJSONParser.IsTokenStringEqual(pToken, "category_id"));
    pToken++; assert(pToken->type == JSMN_PRIMITIVE);
    cCommodity._nCategoryID = cJSONParser.GetTokenAsInt(pToken);

    pToken++;
    assert(pToken->type == JSMN_STRING && cJSONParser.IsTokenStringEqual(pToken, "average_price"));
    pToken++; assert(pToken->type == JSMN_PRIMITIVE);
    cCommodity._nAveragePrice = cJSONParser.GetTokenAsInt(pToken);

    pToken++;
    assert(pToken->type == JSMN_STRING && cJSONParser.IsTokenStringEqual(pToken, "category"));

    pToken++;
    assert(pToken->type == JSMN_OBJECT);

    if (_lcCommodityCategory.size() >= static_cast<size_t>(cCommodity._nCategoryID))
    {
      pToken += 4;
    }
    else {
      _lcCommodityCategory.resize(cCommodity._nCategoryID);

      CommodityCatergory_c&
        cCommodityCategory = _lcCommodityCategory[cCommodity._nCategoryID - 1];

      pToken++;
      assert(pToken->type == JSMN_STRING && cJSONParser.IsTokenStringEqual(pToken, "id"));

      pToken++;
      assert(pToken->type == JSMN_PRIMITIVE);
      cCommodityCategory._nID = cJSONParser.GetTokenAsInt(pToken);
      assert(cCommodity._nCategoryID = cCommodityCategory._nID);

      pToken++;
      assert(pToken->type == JSMN_STRING && cJSONParser.IsTokenStringEqual(pToken, "name"));

      pToken++;
      assert(pToken->type == JSMN_STRING);
      cCommodityCategory._cName = cJSONParser.GetTokenAsQString(pToken);
    }

    _lcCommodity.push_back(cCommodity);
  }
}

void
ImportEDDB_c::ParseSystems(std::string cFileName)
{
  JSONParser
    cJSONParser(cFileName);

  auto
    pToken = cJSONParser.GetStartingToken();

  assert(pToken->type == JSMN_ARRAY);

  auto
    nSystem = pToken->size;

  // {"id":1,"name":"1 G. Caeli","x":80.90625,"y":-83.53125,"z":-30.8125,"faction":"Empire League","population":6544826,"government":"Patronage","allegiance":"Empire","state":"None","security":"Medium",
  //  "primary_economy":"Industrial","needs_permit":0,"updated_at":1442257023,"power_control_faction":null,"simbad_ref":""},

  
  _lcSystem.resize(nSystem);

  std::string
    cStringTmp;
  pToken++; // Move to first object.
  assert(pToken->type == JSMN_OBJECT);
  for (auto iSystem = 0; iSystem < nSystem; ++iSystem)
  {
    System_c&
      cSystem = _lcSystem[iSystem];

    assert(pToken->type == JSMN_OBJECT);

    pToken++;
    assert(pToken->type == JSMN_STRING && cJSONParser.IsTokenStringEqual(pToken, "id"));

    pToken++;
    assert(pToken->type == JSMN_PRIMITIVE);
    cSystem._nID = cJSONParser.GetTokenAsInt(pToken);
    
    /*if (cSystem._nID == 17072)
    {
      std::cout << "Parsing SOL\n";
    }*/

    pToken++;
    assert(pToken->type == JSMN_STRING && cJSONParser.IsTokenStringEqual(pToken, "name"));
    pToken++; 
    assert(pToken->type == JSMN_STRING);
    cSystem._cName = cJSONParser.GetTokenAsQString(pToken);
    
    pToken++;
    assert(pToken->type == JSMN_STRING && cJSONParser.IsTokenStringEqual(pToken, "x"));
    pToken++;
    assert(pToken->type == JSMN_PRIMITIVE);
    cSystem._cPosition.x = cJSONParser.GetTokenAsFloat(pToken);

    pToken++;
    assert(pToken->type == JSMN_STRING && cJSONParser.IsTokenStringEqual(pToken, "y"));
    pToken++;
    assert(pToken->type == JSMN_PRIMITIVE);
    cSystem._cPosition.y = cJSONParser.GetTokenAsFloat(pToken);

    pToken++;
    assert(pToken->type == JSMN_STRING && cJSONParser.IsTokenStringEqual(pToken, "z"));
    pToken++;
    assert(pToken->type == JSMN_PRIMITIVE);
    cSystem._cPosition.z = cJSONParser.GetTokenAsFloat(pToken);
    pToken++;

    pToken = cJSONParser.ParseInto(cSystem._sMinorFaction,   "faction",         pToken);
    pToken = cJSONParser.ParseInto(cSystem._nPopulation,     "population",      pToken);
    pToken = cJSONParser.ParseInto(cSystem._sGovernment,     "government",      pToken);
    pToken = cJSONParser.ParseInto(cSystem._sAllegiance,     "allegiance",      pToken);
    pToken = cJSONParser.ParseInto(cSystem._sEconomicState,  "state",           pToken);
    pToken = cJSONParser.ParseInto(cSystem._sSecurity,       "security",        pToken);
    pToken = cJSONParser.ParseInto(cSystem._sPrimaryEconomy, "primary_economy", pToken);

    assert(pToken->type == JSMN_STRING && cJSONParser.IsTokenStringEqual(pToken, "needs_permit"));
    pToken++;
    cSystem._bNeedPermit = cJSONParser.GetTokenAsInt(pToken) == 1;

    pToken++;
    assert(pToken->type == JSMN_STRING && cJSONParser.IsTokenStringEqual(pToken, "updated_at"));
    pToken++;
    assert(pToken->type == JSMN_PRIMITIVE);
    cSystem._nUpdated = cJSONParser.GetTokenAsInt(pToken);
    pToken++;
    
    pToken = cJSONParser.ParseInto(cSystem._sPowerControlFaction, "power_control_faction", pToken);
    pToken = cJSONParser.ParseInto(cSystem._sSimbad_ref, "simbad_ref", pToken);
  }
}

void
ImportEDDB_c::ParseStations(std::string cFileName)
{
  JSONParser
    cJSONParser(cFileName);

  auto
    pToken = cJSONParser.GetStartingToken();

  assert(pToken->type == JSMN_ARRAY);

  auto
    nStations = pToken->size;

  _lcStation.resize(nStations);
  

  // {"id":1,"name":"Bain Colony","system_id":18370,"max_landing_pad_size":"L","distance_to_star":16253,"faction":"","government":null,
  // "allegiance":null,"state":null,"type":"Unknown Starport","has_blackmarket":0,"has_commodities":1,"has_refuel":null,"has_repair":null,
  // "has_rearm":null,"has_outfitting":null,"has_shipyard":null,"import_commodities":[],"export_commodities":[],"prohibited_commodities":[],"economies":[],
  // "updated_at":1429408824,
  //"listings":[{"id":1,"station_id":1,"commodity_id":5,"supply":0,"buy_price":0,"sell_price":378,"demand":1137,"collected_at":1429359631,"update_count":2}]}
  pToken++;
  for (auto iStation = 0; iStation < nStations; ++iStation)
  {
    assert(pToken->type == JSMN_OBJECT);
    pToken++;

    Station_c&
      cStation = _lcStation[iStation];
    
    pToken = cJSONParser.ParseInto(cStation._nID, "id", pToken);
    pToken = cJSONParser.ParseInto(cStation._cName, "name", pToken);
    pToken = cJSONParser.ParseInto(cStation._nSystemID, "system_id", pToken);
    
    std::string
      cStringTmp;

    pToken = cJSONParser.ParseInto(cStringTmp, "max_landing_pad_size", pToken);
    cStation._eMaxLandingPadSize = Station_c::LandingPadSizeFromString(cStringTmp);

    pToken = cJSONParser.ParseInto(cStation._nDistanceToStar, "distance_to_star", pToken);

    pToken = cJSONParser.ParseInto(cStation._sFaction, "faction", pToken);
    pToken = cJSONParser.ParseInto(cStation._sGovernment, "government", pToken);
    pToken = cJSONParser.ParseInto(cStation._sAllegiance, "allegiance", pToken);
    pToken = cJSONParser.ParseInto(cStation._sEconomicState, "state", pToken);
    pToken = cJSONParser.ParseInto(cStation._cStarportType, "type", pToken);
    pToken = cJSONParser.ParseInto(cStation._isBlackMarket, "has_blackmarket", pToken);
    pToken = cJSONParser.ParseInto(cStringTmp, "has_commodities", pToken);
    pToken = cJSONParser.ParseInto(cStation._isRefuel, "has_refuel", pToken);
    pToken = cJSONParser.ParseInto(cStation._isRepair, "has_repair", pToken);
    pToken = cJSONParser.ParseInto(cStation._isRearm, "has_rearm", pToken);
    pToken = cJSONParser.ParseInto(cStation._isOutfitting, "has_outfitting", pToken);
    pToken = cJSONParser.ParseInto(cStation._isShipyard, "has_shipyard", pToken);
    pToken = cJSONParser.SkipArray("import_commodities", pToken);
    pToken = cJSONParser.SkipArray("export_commodities", pToken);
    pToken = cJSONParser.ParseInto(cStation._lcProhibitedCommodities, "prohibited_commodities", pToken);
    pToken = cJSONParser.ParseInto(cStation._lcEconomies, "economies", pToken);
    pToken = cJSONParser.ParseInto(cStation._nUpdated, "updated_at", pToken);

    assert(pToken->type == JSMN_STRING && cJSONParser.IsTokenStringEqual(pToken, "listings"));
    pToken++;
    assert(pToken->type == JSMN_ARRAY);
    
    cStation._lcListing.resize(pToken->size);
    pToken++;
    //"listings":[{"id":1,"station_id":1,"commodity_id":5,"supply":0,"buy_price":0,"sell_price":378,"demand":1137,"collected_at":1429359631,"update_count":2}]}
    for (auto i = 0; i < cStation._lcListing.size(); ++i)
    {
      assert(pToken->type == JSMN_OBJECT);
      pToken++;

      Listing_c&
        cListing = cStation._lcListing[i];
      
      pToken = cJSONParser.ParseInto(cListing._nID, "id", pToken);
      pToken = cJSONParser.ParseInto(cListing._nStationID, "station_id", pToken);
      pToken = cJSONParser.ParseInto(cListing._nCommodityID, "commodity_id", pToken);
      pToken = cJSONParser.ParseInto(cListing._nSupply, "supply", pToken);
      pToken = cJSONParser.ParseInto(cListing._nBuyPrice, "buy_price", pToken);
      pToken = cJSONParser.ParseInto(cListing._nSellPrice, "sell_price", pToken);
      pToken = cJSONParser.ParseInto(cListing._nDemand, "demand", pToken);
      pToken = cJSONParser.ParseInto(cListing._nUpdated, "collected_at", pToken);
      pToken = cJSONParser.ParseInto(cListing._nUpdateCount, "update_count", pToken);
    }

    std::sort(cStation._lcListing.begin(), cStation._lcListing.end(), [](const Listing_c& l0, const Listing_c& l1) {
      return l0._nCommodityID < l1._nCommodityID;
    });

  }

}

evian::ImportEDDB_c::ImportEDDB_c(TradeDatabaseStorage_c& cTradeDataBase) :
  _lcStation(cTradeDataBase._lcStation),
  _lcSystem(cTradeDataBase._lcSystem),
  _lcCommodityCategory(cTradeDataBase._lcCommodityCategory),
  _lcCommodity(cTradeDataBase._lcCommodity)
{

}

//QSharedPointer<evian::TradeDatabaseStorage_c> 
evian::ImportEDDB_c::CreateTradeDatabaseResult_c
evian::ImportEDDB_c::CreateTradeDatabaseFromEDDB(QString cCommoditiesFilename, QString cStationsFilename, QString cSystemsFilename)
{
  CreateTradeDatabaseResult_c
    cCreateTradeDatabaseResult;

  cCreateTradeDatabaseResult._pcTradeDatabaseStorage = QSharedPointer<evian::TradeDatabaseStorage_c>::create();

  ImportEDDB_c
    cImportEDDB(*cCreateTradeDatabaseResult._pcTradeDatabaseStorage);

  try 
  {
    cImportEDDB.ParseCommodities(cCommoditiesFilename.toStdString());
    cImportEDDB.ParseStations(cStationsFilename.toStdString());
    cImportEDDB.ParseSystems(cSystemsFilename.toStdString());

    QSettings
      cQSettings;

    cCreateTradeDatabaseResult._eResultStatus = CreateTradeDatabaseResult_c::Result_Success;
    cQSettings.setValue(key_eddb_last_update, QDateTime::currentDateTime());
  }
  catch (std::runtime_error& cRuntimeError)
  {
    qWarning() << "Could not parse EDDB-data: " << cRuntimeError.what();

    cCreateTradeDatabaseResult._eResultStatus = CreateTradeDatabaseResult_c::Result_Failure;
    cCreateTradeDatabaseResult._sErrorMessage = cRuntimeError.what();
    cCreateTradeDatabaseResult._pcTradeDatabaseStorage.reset();
  }

  return cCreateTradeDatabaseResult;
}

template<typename Container, typename Generator>
void
ParseJSON(QString sFilename, Container& lContainer, Generator generator)
{
  QElapsedTimer
    qElapsedTimer;

  qElapsedTimer.start();

  QFile
    qFile(sFilename);

  if (!qFile.open(QIODevice::ReadOnly)) {
    qDebug() << "Could not open " << sFilename << " for reading.";
    throw std::runtime_error("Could not open input file");
  }

  QByteArray
    cQByteArray = qFile.readAll();

  auto
    cQJsonDocument = QJsonDocument::fromJson(cQByteArray);

  EVIAN_ASSERT(cQJsonDocument.isArray());

  auto
    qJsonArray = cQJsonDocument.array();

  lContainer.resize(qJsonArray.size());

  for (auto iJsonObject = 0; iJsonObject < qJsonArray.size(); ++iJsonObject)
  {
    if (qJsonArray[iJsonObject].isObject())
    {
      lContainer[iJsonObject] = generator(qJsonArray[iJsonObject].toObject());
    }
    else
    {
      qWarning() << "Could not parse object from JSON.";
    }
  }
  qDebug() << "Parsed " << sFilename << " in " << qElapsedTimer.elapsed() << " nanoseconds.";
  
  qElapsedTimer.restart();

  std::sort(lContainer.begin(), lContainer.end());
  qDebug() << "Sorted " << sFilename << " in " << qElapsedTimer.elapsed() << " nanoseconds.";
}

int fast_atoi( const char * str )
{
    int val = 0;
    while( *str ) {
        val = val*10 + (*str++ - '0');
    }
    return val;
}

long fast_atol( const char * str ) 
{
    long val = 0;
    while( *str ) {
        val = val*10 + (*str++ - '0');
    }
    return val;
}

qint64
ParseCSV(QString sFilename, std::vector<Station_c>& lcStation)
{
  QElapsedTimer
    qElapsedTimer;

  qElapsedTimer.start();

  QFile
    qFile(sFilename);
    
  if (!qFile.open(QIODevice::ReadOnly|QIODevice::Text)) 
  {
    qDebug() << "Could not open " << sFilename << " for reading.";
    throw std::runtime_error("Could not open input file");
  }

  // First validate the first line
  auto 
    qByteArray = qFile.readLine();

  QString
    qString(qByteArray),
    qStringExpected("id,station_id,commodity_id,supply,buy_price,sell_price,demand,collected_at,update_count");

  if (qString.simplified() != qStringExpected)
  {
    qWarning() << "Unknown header for " << sFilename;
    qDebug()   << qString;

    throw std::runtime_error("Invalid input file");
  }

  qByteArray = qFile.readAll();
  
  int
    iStartOffset = -1;

  // Find end of first line.
  for (auto i = 0; i < qByteArray.size(); ++i)
  {
    if (qByteArray[i] == '\n')
    {
      iStartOffset = i+1;
      break;
    }
  }

  if (iStartOffset == -1)
  {
    qWarning() << "Could not find any numeric data in " << sFilename;
    throw std::runtime_error("Invalid input file");
  }

  int
    nTokensParsed = 0,
    iTokenStart = iStartOffset,
    iTokenEnd   = iStartOffset;

  qint64
    nLastUpdated = 0;


  Station_c*
    pcStation = nullptr;

  Listing_c
    cListing;

  for (auto i = iStartOffset; i < qByteArray.size(); ++i)
  {
    if (qByteArray[i] == ',' || qByteArray[i] == '\n')
    {
      qByteArray[i] = '\0';
      iTokenStart = iTokenEnd + 1;
      iTokenEnd = i;

      const char*
        cToken = qByteArray.data() + iTokenStart;

      switch (nTokensParsed % 9) 
      {
      case 0:
        cListing._nID = fast_atol(cToken);
        break;
      case 1:
        cListing._nStationID = fast_atoi(cToken);
        break;
      case 2:
        cListing._nCommodityID = fast_atoi(cToken);
        break;
      case 3:
        cListing._nSupply = fast_atoi(cToken);
        break;
      case 4:
        cListing._nBuyPrice = fast_atoi(cToken);
        break;
      case 5:
        cListing._nSellPrice = fast_atoi(cToken);
        break;
      case 6:
        cListing._nDemand = fast_atoi(cToken);
        break;
      case 7:
        cListing._nUpdated = fast_atol(cToken);
        nLastUpdated = std::max(nLastUpdated, cListing._nUpdated);
        break;
      case 8:
        cListing._nUpdateCount = fast_atoi(cToken);

        if (!pcStation || pcStation->_nID != cListing._nStationID)
        {
          Station_c
            cStation;
    
          cStation._nID = cListing._nStationID;
          auto
            it = std::lower_bound(lcStation.begin(), lcStation.end(), cStation);
          
          if (it == lcStation.end())
          {
            qDebug() << "Could not find station id " << cListing._nID;
            pcStation = nullptr;
            continue;
          }

          pcStation = &(*it);
        }

        pcStation->_lcListing.push_back(cListing);
      }

      nTokensParsed++;
    }
  }

  for (auto& cStation: lcStation)
  {
    std::sort(cStation._lcListing.begin(), cStation._lcListing.end(), [](const Listing_c& l0, const Listing_c& l1) {
      return l0._nCommodityID < l1._nCommodityID;
    });
  }

  qDebug() << "Parsed " << sFilename << " in " << qElapsedTimer.elapsed() << " nanoseconds.";
  return nLastUpdated;
}

namespace System_ns 
{
  static QLatin1String 
                ID   =           QLatin1String("id"),
                Name =           QLatin1String("name"),
                X    =           QLatin1String("x"),
                Y    =           QLatin1String("y"),
                Z    =           QLatin1String("z"),
                Faction =        QLatin1String("faction"),
                Population =     QLatin1String("population"),
                Government =     QLatin1String("government"),
                Allegiance =     QLatin1String("allegiance"),
                State      =     QLatin1String("state"),
                Security   =     QLatin1String("security"),
                PrimaryEconomy = QLatin1String("primary_economy"),
                Power        =   QLatin1String("power"),
                PowerState   =   QLatin1String("power_state"),
                NeedPermit   =   QLatin1String("needs_permit"),
                UpdatedAt    =   QLatin1String("updated_at"),
                SimbadRef    =   QLatin1String("simnbad_ref");

  evian::System_c
  ReadFromJson(const QJsonObject& cQJsonObject)
  {

    //  {"id":1162,"name":"Apurui","x":96.21875,"y":-75.3125,"z":81.4375,
    //  "faction":"Protectores Zemina Nostri",
    //  "population":1222781896,
    //  "government":"Dictatorship",
    //  "allegiance":"Empire",
    //  "state":"None","security":"High","primary_economy":"Industrial","power":"Zemina Torval",
    //  */"power_state":"Control","needs_permit":0,"updated_at":1447969505,"simbad_ref":""}


    evian::System_c
      cSystem;

    cSystem._nID                  = cQJsonObject[ID].toDouble();
    cSystem._cName                = cQJsonObject[Name].toString();
    cSystem._nUpdated             = cQJsonObject[UpdatedAt].toDouble();
    cSystem._bNeedPermit          = cQJsonObject[NeedPermit].toBool();  
    cSystem._sAllegiance          = cQJsonObject[Allegiance].toString();
    cSystem._nPopulation          = cQJsonObject[Population].toInt();
    cSystem._sMinorFaction        = cQJsonObject[Faction].toString();
    cSystem._sGovernment          = cQJsonObject[Government].toString();
    cSystem._sEconomicState       = cQJsonObject[State].toString();
    cSystem._sSecurity            = cQJsonObject[Security].toString();
    cSystem._sPrimaryEconomy      = cQJsonObject[PrimaryEconomy].toString();
    cSystem._sPowerControlFaction = cQJsonObject[Power].toString();

    cSystem._cPosition.x          = cQJsonObject[X].toDouble();
    cSystem._cPosition.y          = cQJsonObject[Y].toDouble();
    cSystem._cPosition.z          = cQJsonObject[Z].toDouble();

    return cSystem;
  } 
} // namesapce System

// {
//   "id": 1,
//   "name": "Bain Colony",
//   "system_id": 18370,
//   "max_landing_pad_size": "L",
//   "distance_to_star": 16253,
//   "faction": "V492 Lyrae Co",
//   "government": "Corporate",
//   "allegiance": "Federation",
//   "state": null,
//   "type": "Unknown Starport",
//   "has_blackmarket": 0,
//   "has_market": 1,
//   "has_refuel": 1,
//   "has_repair": 1,
//   "has_rearm": 1,
//   "has_outfitting": 1,
//   "has_shipyard": 1,
//   "has_commodities": 1,
//   "import_commodities": [
// 
//   ],
//     "export_commodities": [
// 
//     ],
//       "prohibited_commodities": [
// 
//       ],
//         "economies": [
//           "Tourism"
//         ],
//         "updated_at": 1444494520,
//         "shipyard_updated_at": 1448161536,
//         "outfitting_updated_at": 1448161530,
//         "selling_ships": [
//           "Asp Explorer",
//             "Sidewinder Mk. I"
//         ],
//         "selling_modules": [
//             1306
//         ]
// }

namespace Station_ns
{
  static QLatin1String
                ID =                    QLatin1String("id"),
                Name =                  QLatin1String("name"),
                SystemId =              QLatin1String("system_id"),
                MaxLandingPadSize =     QLatin1String("max_landing_pad_size"),
                DistanceToStar =        QLatin1String("distance_to_star"),
                Faction =               QLatin1String("faction"),
                Government =            QLatin1String("government"),
                Allegiance =            QLatin1String("Allegiance"),
                State =                 QLatin1String("state"),
                StarportType =          QLatin1String("type"),

                HasBlackmarket =        QLatin1String("has_blackmarket"),
                HasRefuel =             QLatin1String("has_refuel"),
                HasRepair =             QLatin1String("has_repair"),
                HasRearm =              QLatin1String("has_rearm"),
                HasOutfitting =         QLatin1String("has_outfitting"),
                HasShipyard =           QLatin1String("has_shipyard"),
                HasCommodities =        QLatin1String("has_commodities"),

                ImportCommodities =     QLatin1String("import_commodities"),
                ExportCommodities =     QLatin1String("export_commodities"),
                ProhibitedCommodities = QLatin1String("prohibites_commodities"),
                Economies =             QLatin1String("economies"),
                UpdatedAt =             QLatin1String("updated_at"),
                ShipyardUpdatedAt =     QLatin1String("shipyard_updated_at"),
                SellingShips =          QLatin1String("selling_ships"),
                SellingModules =        QLatin1String("selling_modules");
  
  evian::Station_c
  ReadFromJson(const QJsonObject& cQJsonObject) 
  {
    evian::Station_c
      cStation;

    cStation._nID             = cQJsonObject[ID].toInt();
    cStation._cName           = cQJsonObject[Name].toString();
    cStation._nSystemID       = cQJsonObject[SystemId].toInt();
    cStation._nUpdated        = cQJsonObject[UpdatedAt].toInt();

    cStation._nDistanceToStar = cQJsonObject[DistanceToStar].toInt();
    cStation._sFaction        = cQJsonObject[Faction].toString();
    cStation._sAllegiance     = cQJsonObject[Allegiance].toString();
    cStation._cStarportType   = cQJsonObject[StarportType].toString();
                              
    cStation._isBlackMarket   = cQJsonObject[HasBlackmarket].toBool();
    cStation._isOutfitting    = cQJsonObject[HasOutfitting].toBool();
    cStation._isRearm         = cQJsonObject[HasRearm].toBool();
    cStation._isRepair        = cQJsonObject[HasRepair].toBool();
    cStation._isShipyard      = cQJsonObject[HasShipyard].toBool();
    cStation._isRefuel        = cQJsonObject[HasRefuel].toBool();

    cStation._eMaxLandingPadSize = Station_c::LandingPadSizeFromString(cQJsonObject[MaxLandingPadSize].toString().toStdString());
    cStation._isPlanetaryOutpost = Station_c::IsPlanetaryOutpostFromString(cStation._cStarportType);
    
    for (const auto& cCommodity: cQJsonObject[ProhibitedCommodities].toArray())
      cStation._lcProhibitedCommodities.push_back(cCommodity.toString());
    
    for (const auto& cEconomy: cQJsonObject[Economies].toArray())
      cStation._lcEconomies.push_back(cEconomy.toString());

    for (const auto& cShip: cQJsonObject[SellingShips].toArray())
      cStation._lcShips.push_back(cShip.toString());

    for (const auto& cModule: cQJsonObject[SellingModules].toArray())
      cStation._lnModules.push_back(cModule.toInt());

    return cStation;
  }

  
} // Station_ns

namespace Commodity_ns 
{
  //  {"id":1,"name":"Explosives","category_id":1,"average_price":271,"category":{"id":1,"name":"Chemicals"}}
  static QLatin1String
    ID           = QLatin1String("id"),
    Name         = QLatin1String("name"),
    CategoryID   = QLatin1String("category_id"),
    AveragePrice = QLatin1String("average_price"),
    Category     = QLatin1String("category");

  evian::Commodity_c
  ReadFromJson(const QJsonObject& cQJsonObject, std::vector<evian::CommodityCatergory_c>& lcCommodityCategory)
  {
    evian::Commodity_c
      cCommodity;

    cCommodity._nID           = cQJsonObject[ID].toInt();
    cCommodity._cName         = cQJsonObject[Name].toString();
    cCommodity._nAveragePrice = cQJsonObject[AveragePrice].toInt();
    cCommodity._nCategoryID   = cQJsonObject[CategoryID].toInt();

    if ((size_t)cCommodity._nCategoryID > lcCommodityCategory.size())
    {
      lcCommodityCategory.resize(cCommodity._nCategoryID);
      
      CommodityCatergory_c&
        cCommodityCategory = lcCommodityCategory[cCommodity._nCategoryID - 1];

      QJsonObject
        qJsonObjectCategory = cQJsonObject[Category].toObject();

      cCommodityCategory._nID = qJsonObjectCategory[ID].toInt();
      EVIAN_ASSERT(cCommodityCategory._nID == cCommodity._nCategoryID);

      cCommodityCategory._cName = qJsonObjectCategory[Name].toString();
    }

    return cCommodity;
  }
}
void
evian::ImportEDDB_c::ParseSystemsV4(QString sSystems)
{
  QFile
    cQFile(sSystems);

  if (!cQFile.open(QIODevice::ReadOnly)) {
    qDebug() << "Could not open " << sSystems << " for reading.";
    throw std::runtime_error("Could not open systems.json");
  }

  QByteArray
    cQByteArray = cQFile.readAll();

  auto
    cQJsonDocument = QJsonDocument::fromJson(cQByteArray);

  EVIAN_ASSERT(cQJsonDocument.isArray());

  for (const auto& cQJsonObject: cQJsonDocument.array())
  {
    if (cQJsonObject.isObject())
    {
      _lcSystem.push_back(System_ns::ReadFromJson(cQJsonObject.toObject()));
    }
    else
    {
      qWarning() << "Could not parse object from JSON.";
    }
  }
}

evian::ImportEDDB_c::CreateTradeDatabaseResult_c
evian::ImportEDDB_c::CreateTradeDatabaseFromEDDBV4(EDDBV4Context_c cEDDBV4Context)
{
  CreateTradeDatabaseResult_c
    cCreateTradeDatabaseResult;

  cCreateTradeDatabaseResult._pcTradeDatabaseStorage = QSharedPointer<evian::TradeDatabaseStorage_c>::create();

  ImportEDDB_c
    cImportEDDB(*cCreateTradeDatabaseResult._pcTradeDatabaseStorage);

  try {
    QElapsedTimer
      qElapsedTimer;

    qElapsedTimer.start();

    ParseJSON(cEDDBV4Context._sStations,    cImportEDDB._lcStation,   Station_ns::ReadFromJson);
    EVIAN_ASSERT(std::is_sorted(cImportEDDB._lcStation.begin(), cImportEDDB._lcStation.end()));
    
    auto 
      nLastUpdated = ParseCSV(cEDDBV4Context._sListings, cImportEDDB._lcStation);

    ParseJSON(cEDDBV4Context._sSystems,     cImportEDDB._lcSystem,    System_ns::ReadFromJson);
    ParseJSON(cEDDBV4Context._sCommodities, cImportEDDB._lcCommodity, std::bind(Commodity_ns::ReadFromJson, std::placeholders::_1, std::ref(cImportEDDB._lcCommodityCategory)));

    qDebug() << "Parsed EDDBv4 input data in " << qElapsedTimer.elapsed() << " nanoseconds.";
    
    QSettings
      cQSettings;

    cCreateTradeDatabaseResult._eResultStatus = CreateTradeDatabaseResult_c::Result_Success;

    QDateTime
      qDateTime = QDateTime::fromTime_t(nLastUpdated);
    qDebug() << "Last updated " << qDateTime;

    cQSettings.setValue(key_eddb_last_update, qDateTime);
    cQSettings.setValue(key_eddn_on_dynamodb_last_update, qDateTime);
  }
  catch (std::runtime_error& cRuntimeError)
  {
    qWarning() << "Could not parse EDDBV4-data: " << cRuntimeError.what();

    cCreateTradeDatabaseResult._eResultStatus = CreateTradeDatabaseResult_c::Result_Failure;
    cCreateTradeDatabaseResult._sErrorMessage = cRuntimeError.what();
    cCreateTradeDatabaseResult._pcTradeDatabaseStorage.reset();
  }

  return cCreateTradeDatabaseResult;
}

