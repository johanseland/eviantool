//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#include "stdafx.h"

#include "evianqueryqt.hpp"
#include "ui_evianqueryqt.h"
#include "DatabaseQuery.hpp"
#include "mainwindow.hpp"
#include "model.hpp"
//#include "ui_mainwindow.h"

EvianQueryQT::EvianQueryQT(MainWindow *parent) :
    QWidget(parent),
    _pcMainWindow(parent),
    ui(new Ui::EvianQueryQT),
    _pcProxyModel(new QSortFilterProxyModel(this))
{
    ui->setupUi(this);
    ui->groupBoxAdvancedOptions->hide();
    ui->progressBar->hide();
    HideQuerySpecificFrames();

    ui->tableViewResults->setContextMenuPolicy(Qt::CustomContextMenu);
    
    connect(ui->tableViewResults, &QTableView::customContextMenuRequested,
            this, &EvianQueryQT::ContextMenuRequested);
}

EvianQueryQT::~EvianQueryQT()
{
    delete ui;
}

void
EvianQueryQT::on_pushButton_clicked()
{
  QSettings
    cQSettings;

  evian::ConcurrencyMode
    eConcurrencyMode = static_cast<evian::ConcurrencyMode>(cQSettings.value(key_concurrency_mode).toInt());

  float
    rMaxDistance = ui->doubleSpinBoxMaxLY->value() != 0 ? ui->doubleSpinBoxMaxLY->value() : std::numeric_limits<float>::max();

  bool 
    bUseConcurrencyRuntime = false;

  switch (eConcurrencyMode)
  {
    case evian::Concurrency_Automatic:
      bUseConcurrencyRuntime = rMaxDistance > 30;
      break;
    case evian::Concurrency_ForceMultiThreading:
      bUseConcurrencyRuntime = true;
      break;
    case evian::Concurrency_ForceSingleThreading:
      bUseConcurrencyRuntime = false;
      break;
  }

  auto
    pcBestSymmetricTrade = QSharedPointer<evian::BestSymmetricTrade_c>(new evian::BestSymmetricTrade_c(30, bUseConcurrencyRuntime));

  _pcMainWindow->_lcDatabaseQuery.append(pcBestSymmetricTrade);

  evian::BestSymmetricTrade_c &
    cBestSymmetricTrade = *pcBestSymmetricTrade;

  cBestSymmetricTrade._cQueryRestriction._nMinSupply = ui->spinBoxMinimumSupply->value();
  cBestSymmetricTrade._cQueryRestriction._rMaxDistanceToStar = ui->doubleSpinBoxMaxLS->value() != 0 ? ui->doubleSpinBoxMaxLS->value() : std::numeric_limits<float>::max();
  cBestSymmetricTrade._cQueryRestriction._rMaxDistance = rMaxDistance;
  cBestSymmetricTrade._cQueryRestriction._isIntraSystemTradingIgnored = ui->checkBoxSkipIntrasystemTrading->isChecked();
  cBestSymmetricTrade._cQueryRestriction._isIgnoreStationsWithoutDistance = ui->checkBoxIgnoreStationsWithoutDistance->isChecked();

  cBestSymmetricTrade._cQueryRestriction._nMaxResults = ui->spinBoxMaxResults->value();

  /*cBestSymmetricTrade._cQueryRestriction._isCommodityExcluded = _pcMainWindow->_pcQueryRestrictionDatabase->_lisCommodityExcluded;
  cBestSymmetricTrade._cQueryRestriction._lnExcludedStations  = _pcMainWindow->_pcQueryRestrictionDatabase->_lnExcludedStations;
  cBestSymmetricTrade._cQueryRestriction._lnExcludedSystems   = _pcMainWindow->_pcQueryRestrictionDatabase->_lnExcludedSystems;
*/
  auto
    pcSymmetricTradeModel = new model::SymmetricTradeModel_c(_pcMainWindow, pcBestSymmetricTrade);

  _pcProxyModel->setDynamicSortFilter(true);
  _pcProxyModel->setSourceModel(pcSymmetricTradeModel);
  _pcProxyModel->setSortRole(model::SymmetricTradeModel_c::SortRole);

  //ui->tableViewResults->setModel(pcSymmetricTradeModel);  
  ui->tableViewResults->setModel(_pcProxyModel);
  ui->tableViewResults->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

  _pcMainWindow->_lpcQAbstractTableModel.append(pcSymmetricTradeModel);

  auto
    pcQuery = new ExecuteQuery(this, pcBestSymmetricTrade, *_pcMainWindow->_pcTradeDatabaseEngine);

  connect(pcQuery, &ExecuteQuery::QueryCompleted, pcSymmetricTradeModel, &model::SymmetricTradeModel_c::ResultsReady);
  connect(pcQuery, &ExecuteQuery::QueryCompleted, ui->progressBar, &QProgressBar::hide);
  connect(pcQuery, &ExecuteQuery::ElapsedTimeMessage, _pcMainWindow->statusBar(), &QStatusBar::showMessage);

  QString
    cNewWindowTitle = ui->comboBoxQueryType->currentText() + " (" + QString::number(ui->doubleSpinBoxMaxLY->value()) + " LY, " + QString::number(ui->doubleSpinBoxMaxLS->value()) + " ls)";

  ui->progressBar->show();

  setWindowTitle(cNewWindowTitle);

  pcQuery->start();
}

void
EvianQueryQT::on_comboBoxQueryType_activated(const QString &arg1)
{
  HideQuerySpecificFrames();

  if (arg1 == QStringLiteral("System -> Any"))
  {
    //ui->frameSystemToAny->show();
    
    model::SetupSystemToStationComboboxes(ui->comboBoxFromSystem, ui->comboBoxFromStation, _pcMainWindow, this);
  }
}

void
EvianQueryQT::ContextMenuRequested(QPoint pos)
{
  QModelIndex 
    index  = _pcProxyModel->mapToSource(ui->tableViewResults->indexAt(pos));

  auto
    nRow = index.row();

  //qDebug() << ui->tableViewResults->indexAt(pos);
  //qDebug() << index;
  
  using namespace model::SymmetricTradeModel;
  auto
    pSourceModel = _pcProxyModel->sourceModel();

    QString
      cCommodity  = _pcProxyModel->sourceModel()->data(index).toString(),
      cSystem1    = _pcProxyModel->sourceModel()->data(index.sibling(nRow, Column_System1)).toString();

    auto
      pcQMenu = new QMenu(this);

    QList<Column>
      leColumn;

    leColumn << Column_System1 << Column_Station1 << Column_System2 << Column_Station2 << Column_Commodity1 << Column_Commodity2;
    
    QMap<Column, QAction*>
      cQMap;

    for (auto eColumn: leColumn)
    {
      QString 
        s = pSourceModel->data(index.sibling(nRow, eColumn)).toString();

      auto
        pcQAction = new QAction( QString("Exclude %1").arg(s), pcQMenu);
      
      cQMap[eColumn] = pcQAction;
      switch (eColumn) 
      {
        case Column_System1:
        case Column_System2:
          connect(pcQAction, &QAction::triggered,
          [s, this]() 
          {
            emit ExcludeSystem(s);
          });
          break;
        case Column_Station1:
        case Column_Station2:
          pcQAction->setText(QString("Exclude %1/%2").arg(pSourceModel->data(index.sibling(nRow, eColumn-1)).toString()).arg(s));

          connect(pcQAction, &QAction::triggered,
            [s, this, eColumn, pSourceModel, index, nRow]() 
          {
            QString
              cSystem = pSourceModel->data(index.sibling(nRow, eColumn-1)).toString();
            emit ExcludeStation(cSystem, s);
          });
          break;
        case Column_Commodity1:
        case Column_Commodity2:
          connect(pcQAction, &QAction::triggered,
            [s, this]() 
          {
            emit ExcludeCommodity(s);
          });
          break;
        default:
          EVIAN_ASSERT("Unhandled column in action create list");
      }
    }
      
    auto
      pcActionClearAll = new QAction("Clear all restrictions", pcQMenu);
    
    connect(pcActionClearAll, &QAction::triggered,
            this,             &EvianQueryQT::ClearAllRestrictions);

    for (auto pcAction: cQMap.values())
    {
      pcQMenu->addAction(pcAction);
    }
    pcQMenu->addSeparator();

    pcQMenu->addAction(pcActionClearAll);
    pcQMenu->popup(ui->tableViewResults->viewport()->mapToGlobal(pos));
  
    connect(pcQMenu, &QMenu::aboutToHide,
            pcQMenu, &QMenu::deleteLater);
}

void
EvianQueryQT::HideQuerySpecificFrames()
{
  //ui->frameSystemToAny->hide();
}

void
evian::QueryExclusionDatabase_c::AddExcludedStation(QString cSystemName, QString cStationName)
{
  auto // NB argumnet order is switched...
    nID = _pcTradeDatabaseEngine->GetTradeDatabase().GetStationID(cStationName, cSystemName);
  
  if (nID != -1)
  {
    auto 
      it = std::lower_bound(_lnExcludedStations.begin(), _lnExcludedStations.end(), nID);

    if (it == _lnExcludedStations.end() || *it != nID)
    {
      _lnExcludedStations.insert(it, nID);
      emit StationExcluded(cSystemName, cStationName);
    }
  }
  else
  {
    qDebug() << "Trying to exclude station that is not in database " << cSystemName << "/" << cStationName;
  }
}

void evian::QueryExclusionDatabase_c::AddExcludedSystem(QString cSystemName)
{
  auto
    nID = _pcTradeDatabaseEngine->GetTradeDatabase().GetSystemID(cSystemName);

  if (nID != -1)
  {
    auto 
      it = std::lower_bound(_lnExcludedSystems.begin(), _lnExcludedSystems.end(), nID);
    
    if (it == _lnExcludedSystems.end() || *it != nID)
    {
      _lnExcludedSystems.insert(it, nID);
      emit SystemExcluded(cSystemName);
    }
  }
  else
  {
    qDebug() << "Trying to exclude system that is not in database " << cSystemName;
  }
}

void evian::QueryExclusionDatabase_c::AddExcludedCommodity(QString cCommodityName)
{
  auto
    nID = _pcTradeDatabaseEngine->GetTradeDatabase().GetCommodityID(cCommodityName);
  
  if (nID != -1)
  {
    if (!_lisCommodityExcluded[nID])
    {
      emit CommodityExcluded(cCommodityName);
    }
    _lisCommodityExcluded[nID] = true;
  }
  else
  {
    qDebug() << "Trying to exclude commodity that is not in database " << cCommodityName;
  }
}

void
evian::QueryExclusionDatabase_c::AddExcludedFaction(QString sFaction)
{
  if (!_lsExcludedFactions.contains(sFaction))
  {
    _lsExcludedFactions.append(sFaction);
  }

  emit FactionExcluded(sFaction);
}


void
evian::QueryExclusionDatabase_c::ClearAllExclusions()
{
  //_lisCommodityExcluded.reset();
  fill(_lisCommodityExcluded.begin(), _lisCommodityExcluded.end(), false);

  _lnExcludedStations.clear();
  _lnExcludedSystems.clear();
  _lsExcludedFactions.clear();
}

evian::QueryExclusionDatabase_c::QueryExclusionDatabase_c(TradeDatabaseEngine_c * pcTradeDatabaseEngine, QObject * pcQObject) :
  QObject(pcQObject),
  _pcTradeDatabaseEngine(pcTradeDatabaseEngine)
{
  ClearAllExclusions();
}

void
evian::QueryExclusionDatabase_c::AddIncludedCommodity(QString cCommodityName)
{
  auto
    nID = _pcTradeDatabaseEngine->GetTradeDatabase().GetCommodityID(cCommodityName);

  if (nID != -1)
  {
    _lisCommodityExcluded[nID] = false;
  }
  else
  {
    qDebug() << "Trying to include commodity that is not in database " << cCommodityName;
  }
}

void
evian::QueryExclusionDatabase_c::AddIncludedSystem(QString cSystemName)
{
  auto
    nID = _pcTradeDatabaseEngine->GetTradeDatabase().GetSystemID(cSystemName);

  _lnExcludedSystems.erase(std::remove(_lnExcludedSystems.begin(), _lnExcludedSystems.end(), nID));
}

void
evian::QueryExclusionDatabase_c::AddIncludedStation(QString cSystemName, QString cStationName)
{
  auto // NB argument order is switched...
    nID = _pcTradeDatabaseEngine->GetTradeDatabase().GetStationID(cStationName, cSystemName);

  _lnExcludedStations.erase(std::remove(_lnExcludedStations.begin(), _lnExcludedStations.end(), nID), _lnExcludedStations.end());
}

void
evian::QueryExclusionDatabase_c::AddIncludedFaction(QString sFaction)
{
  _lsExcludedFactions.removeAll(sFaction);
}