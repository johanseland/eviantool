#ifndef EVIANEDDNTICKER_HPP
#define EVIANEDDNTICKER_HPP

#include <QDockWidget>
#include <QPointF>
#include "database.hpp"

namespace Ui {
class EvianEDDNTicker_c;
}

class QGraphicsScene;

class EvianEDDNTicker_c : public QDockWidget
{
  Q_OBJECT

public:
  explicit EvianEDDNTicker_c(const evian::TradeDatabaseEngine_c& cTradeDatabaseEngine, QWidget *parent = 0);
  ~EvianEDDNTicker_c();

public slots:
  void          EDDNMessagesAdded(int iRowHint, int nMessages);
  void          CreateEDDNMessage(QSharedPointer<eddn::EDDNMessage_c> pcEDDNMessage);

private:
  void resizeEvent(QResizeEvent * pqResizeEvent) override;
  void moveEvent(QMoveEvent * pqMoveEvent) override;
  void SetSceneRect(QSize qSize);

  Ui::EvianEDDNTicker_c
                *ui;
  
  QGraphicsScene
                *_pqGraphicsScene;
  
  const evian::TradeDatabaseEngine_c
                &_cTradeDatabaseEngine;
  int           _nPixelsPerSecond;
};

#endif // EVIANEDDNTICKER_HPP
