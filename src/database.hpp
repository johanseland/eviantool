//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#pragma once

#include "evian.hpp"
#include "eddn.hpp"
#include "DatabaseQuery.hpp"

EVIAN_BEGIN_NAMESPACE(evian)

  /// TradeDatabaseStorage holds the concrete bits and bytes of the trade data.
  /// The class will be used by clients through TradeDatabaseEngine_c.GetTradeDatabase.
class TradeDatabaseStorage_c : public QObject
{
  Q_OBJECT
private:
  bool          _bIsDatabaseReady;

  QHash<QString, int64_t>
                _cNameToSystemID,
                _cQHashLowerCaseCommodityToCommodityID;

  void ComputeNameHashes();
  void CreateNameToCommodityID();
  void CreateNameToSystemID();
  void CreateNameToSystemMap();
  void CreateSystemsWithStations();
  void SetupBackLinks();
  bool isDatabaseValid();
  void CreateCommodityAnalysis();
  void CreatePowerplayFactionMap();
  void CreateExploitedBy();
  void UpdateEarliestCommodityDateTime();

public:
  TradeDatabaseStorage_c();

  bool IsDatabaseReady() const;
  void ExecuteQuery(DatabaseQuery_c& cDataBaseQuery);
  void PostInit();

  std::vector<Station_c>
                _lcStation;

  std::vector<System_c>
                _lcSystem;

  QList<std::reference_wrapper<const System_c>>
                _lcSystemWithStation;

  QHash<QString, QList<std::reference_wrapper<const System_c>>>
                _cFactionToSystemMap;

  std::vector<CommodityCatergory_c>
                _lcCommodityCategory;

  std::vector<Commodity_c>
                _lcCommodity;

  //std::map<std::string, std::reference_wrapper<System_c>>
  std::map<QString, std::reference_wrapper<const System_c>>
                _cNameToSystemMap;

  std::vector<QSharedPointer<eddn::EDDNMessage_c>>
                _lpcEDDNMessage;

  std::array<evian::CommodityPriceAnalysisResult_c, evian::Station_c::MAX_COMMODITIES>
                _lcCommodityPriceAnalysisBuy,
                _lcCommodityPriceAnalysisSell;

  QStringList   _lsPowerControlFaction; // Filled by CreatePowerplayFactionMap.

  void Clear();

  int64_t GetCommodityID(QString cCommoditName) const;
  int64_t GetSystemID(QString cSystemName) const;
  int64_t GetStationID(QString cStationName, QString cSystemName) const;

  const System_c&  GetSystem(int64_t nSystemID) const;
  const System_c&  GetSystem(QString cSystemName) const;

  const Station_c& GetStation(int64_t nStationID) const;
  Station_c& GetStation(int64_t nStationID);
  const Station_c& GetStation(QString cStationName, QString cSystemName) const;

  bool IsCommodityIDValid(int64_t nCommodityID);

  const Commodity_c& GetCommodity(int64_t nCommodityID);

  QString GetStationName(int64_t nStationID) const;
  QString GetSystemName(int64_t nSystemID) const;
  QString GetCommodityName(int64_t nCommodityID) const;
  Grid_c<System_c> CreateGrid(float rGridSpacing) const;
  
  /// Create a grid with systems in lcSystemTo, but based the grid coordinate system on the union of systems in lcSystemFrom and lcSystemTo.
  Grid_c<System_c> CreateGrid(const QList<std::reference_wrapper<const evian::System_c>>& lcSystemFrom, QList<std::reference_wrapper<const evian::System_c>>& lcSystemTo, float rGridSpacing) const;

  void UpdateEarliestCommodityDateTime(int nSecs);
signals:
  void Message(const QString&, int);

};

Q_DECLARE_METATYPE(QSharedPointer<evian::TradeDatabaseStorage_c>);

/// TradeDatabaseEnginne_c runs in it's own thread and protects access to the database.
/// Is receives updates to the database, and signals what they are through QT signals/slots.
class TradeDatabaseEngine_c : public QObject
{
  Q_OBJECT
public:
  TradeDatabaseEngine_c();
  ~TradeDatabaseEngine_c();

  bool IsDatabaseReady() const;
  static EvianReadWriteLock_c& GetEvianReadWriteLock();
  const TradeDatabaseStorage_c& GetTradeDatabase() const;

  bool ParseEDDNJSon(const QJsonDocument& cQJsonDocument, Listing_c& cListing);
  
  void HandleEDDNMessage(const QJsonDocument& cQJsonDocument);
  
  enum SerializationMode
  {
    SerializationMode_Binary, SerializationMode_JSON
  };

  bool IsListingValid(const Listing_c& cListing);

  bool Serialize(QString sFilename, QString sListingsFilename, SerializationMode eSerializationMode);
  static QSharedPointer<TradeDatabaseStorage_c> Deserialize(QString qDatabaseFilename, QString sListingsFilename);

public slots:
    void ResetDatabaseStorage(QSharedPointer<evian::TradeDatabaseStorage_c>);
    void UpdateListingEDDN(QJsonDocument);
    void UpdateListing(evian::Listing_c, int iRowHint); /// Handle update to the listing, possibly from the GUI. Will eventually update the listing in the database.
    void Process();
    void Terminate();
    void UpdateEarliestCommodityTime(int);

signals:
    void DatabaseReady();
    void DatabaseAboutToBeReset(); // Use with a blocking queued connection to allow for MVC-models to emit the correct reset signals.
    void DatabaseResetComplete();
    void ListingUpdated(const evian::Listing_c&, int iRowHint);
    void ListingAdded(const evian::Listing_c&, int iRowHint);
    void EDDNV1CommodityMessagesAdded(int iRowHint, int nMessages);
    void Finished();
    void Error(const QString&);
    void Message(QString, int);

private:
  static EvianReadWriteLock_c
    _cEvianReadWriteLock;

  TradeDatabaseStorage_c
    _cTradeDatabaseStorage;

  QMutex        _cQMutex; // For thread control, not database access.
  bool          _isTerminate;

  QList<QPair<Listing_c, int>>
    _lcQListOutstandingListingUpdates;
  
  QList<QSharedPointer<eddn::EDDNMessage_c>>
    _lpcQListOutstandingEDDNUpdates;

  void ProcessListingUpdate(const Listing_c& cListing, int iRowHint);
  void ResetDatabaseStorageImpl(TradeDatabaseStorage_c& cTradeDatabaseStorage);
  void HandleEDDNV1Message(const QJsonObject& cQJsonObject); // Create the eddn::EDDNMessage instance from the JSON and put in the EDDN-queue
  void HandleEDDNV2Message(QJsonObject cQJsonObject);        // These two methods does not need the database-write-lock.
  
  void ProcessEDDNMessageQueue(); // Empty the EDDN-queue and add to database. Requires the database-write-lock. 
  void evian::TradeDatabaseEngine_c::ProcessEDDNMessageUpdate(QSharedPointer<eddn::EDDNMessage_c> pcEDDNMessage);
};

class CommodityDisabler_c
{
public:
  virtual void Process(evian::Station_c& cStation) = 0;
};

class OldCommodityDisabler_c : public CommodityDisabler_c
{
public:
  void SetEarliestDate(QDateTime cQDateTime);
  void Process(evian::Station_c& cStation);
private:
  QDateTime     _cQDateTime;
};

EVIAN_END_NAMESPACE(evian)

