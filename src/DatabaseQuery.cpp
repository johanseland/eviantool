//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "stdafx.h"
#include "DatabaseQuery.hpp"
#include "evianutil.hpp"
#include "database.hpp"
#include "3rdparty/format.h"

evian::DataBaseQueryResult_c::~DataBaseQueryResult_c()
{
}

std::string evian::DatabaseQuery_c::Description() const
{
  return typeid(*this).name();
}

evian::DatabaseQuery_c::~DatabaseQuery_c()
{
}

void evian::DatabaseQuery_c::Combine()
{
}

int evian::DatabaseQuery_c::GetNumberOfResults() const
{
  return -1;
}

void evian::DatabaseQuery_c::operator()(const TradeDatabaseStorage_c & )
{
}

void evian::DatabaseQuery_c::SetQueryRestriction(const QueryRestriction_c& cQueryRestriction)
{
  _cQueryRestriction = cQueryRestriction;
}

void evian::DatabaseQuery_c::operator()(const System_c & )
{
}

void evian::DatabaseQuery_c::operator()(const System_c &, const System_c &)
{
}

std::string evian::CountSystemsWithStationsQuery::Description() const
{
  return "CountSystemsWithStationQuery";
}

void evian::CountSystemsWithStationsQuery::operator()(const System_c & cSystem)
{
  if (!cSystem._lpcStation.empty())
  {
    _nSystemsWithStations++;
    _cBoundingBoxWithStations.Expand(cSystem._cPosition);
  }

  _cBoundingBox.Expand(cSystem._cPosition);
}

concurrency::combinable<evian::AlignedBuffer>
evian::BestTrade_c::_cAlignedBuffer;

evian::BestTrade_c
evian::BestTrade_c::FindBestTradeBetweenStations(const Station_c & cFromStation, const Station_c & cToStation)
{
  BestTrade_c
    cBestTrade;

  const auto
    &lcListingFrom = cFromStation._lcListing,
    &lcListingTo = cToStation._lcListing;

  if (lcListingFrom.empty() || lcListingTo.empty())
  {
    return cBestTrade;
  }

  for (auto iListingFrom = 0, iListingTo = 0; iListingFrom < lcListingFrom.size(); )
  {
    while (iListingFrom < lcListingFrom.size() && lcListingFrom[iListingFrom]._nCommodityID < lcListingTo[iListingTo]._nCommodityID)
    {
      ++iListingFrom;
    }

    if (iListingFrom >= lcListingFrom.size())
    {
      break;
    }

    while (iListingTo < lcListingTo.size() && lcListingFrom[iListingFrom]._nCommodityID > lcListingTo[iListingTo]._nCommodityID)
    {
      ++iListingTo;
    }

    if (iListingTo >= lcListingTo.size())
    {
      break;
    }

    bool
      bIsSameCommodity = lcListingFrom[iListingFrom]._nCommodityID == lcListingTo[iListingTo]._nCommodityID,
      bCanBuyFromStation = lcListingFrom[iListingFrom]._nBuyPrice > 0;

    if (bIsSameCommodity && bCanBuyFromStation)
    {
      auto
        nProfit = lcListingTo[iListingTo]._nSellPrice - lcListingFrom[iListingFrom]._nBuyPrice;

      if (nProfit > cBestTrade._nProfit)
      {
        cBestTrade._nProfit = nProfit;
        cBestTrade._nCommodityID = lcListingFrom[iListingFrom]._nCommodityID;
        cBestTrade._nFromStationID = cFromStation._nID;
        cBestTrade._nToStationID = cToStation._nID;
        cBestTrade._nFromSystemID = cFromStation._nSystemID;
        cBestTrade._nToSystemID = cToStation._nSystemID;
      }
    }

    ++iListingFrom;
  }

  cBestTrade._nProfit = std::max(static_cast<int64_t>(0), cBestTrade._nProfit);
  return cBestTrade;
}

evian::BestTrade_c
evian::BestTrade_c::FindBestTradeBetweenStationsFast(const Station_c & cStationFrom, const Station_c & cStationTo, const QueryRestriction_c cQueryRestriction)
{
  int * __restrict
    paProfit = _cAlignedBuffer.local()._pInt.get();

  // Loop should be auto-vectorized
  for (int i = 0; i < Station_c::MAX_COMMODITIES; ++i)
  {
    paProfit[i] = cStationTo._paSellPrice[i] - cStationFrom._paBuyPrice[i];
  }

  auto
    nBestProfit = 0,
    nCommodityID = -1;

  for (int i = 0; i < Station_c::MAX_COMMODITIES; ++i)
  {
    if (paProfit[i] > nBestProfit &&
        !cQueryRestriction._isCommodityExcluded[i] &&
        cStationFrom._paSupply[i] > cQueryRestriction._nMinSupply)
    {
      nBestProfit = paProfit[i];
      nCommodityID = i;
    }
  }

  EVIAN_ASSERT(nCommodityID <= 1 || cStationTo._paBuyPrice[nCommodityID] > 0);

  BestTrade_c
    cBestTrade(nCommodityID, 
               cStationFrom._nID, 
               cStationTo._nID, 
               cStationFrom._nSystemID,
               cStationTo._nSystemID,
               std::max(0, nBestProfit));

  return cBestTrade;
}

QList<evian::BestTrade_c>
evian::BestTrade_c::FindSmugglingRouteBetweenStations(const evian::Station_c& cStationFrom, const evian::Station_c& cStationTo, const TradeDatabaseStorage_c& cTradeDatabase, const QueryRestriction_c cQueryRestriction)
{
  
  QList<evian::BestTrade_c>
    lcBestTrade;

  if (!cStationTo._isBlackMarket)
  {
    return lcBestTrade; // Smuggling not possible
  }

  for (const auto& sCommodity : cStationTo._lcProhibitedCommodities)
  {
    auto
      nCommodityID = cTradeDatabase.GetCommodityID(sCommodity);
    
    if (cStationFrom._paBuyPrice[nCommodityID] != Station_c::BUY_DEFAULT)
    {
      lcBestTrade.append(BestTrade_c(nCommodityID, 
                                     cStationFrom._nID, 
                                     cStationTo._nID, 
                                     cStationFrom._nSystemID,
                                     cStationTo._nSystemID,
                                     9999)); // TODO: Estimate smuggling profit!
    }
  }

  return lcBestTrade;
}



std::string evian::BestTrade_c::PrettyPrint(const evian::TradeDatabaseStorage_c& cTradeDatabase) const
{
  std::stringstream
    cStringStream;

  cStringStream << cTradeDatabase.GetCommodityName(_nCommodityID).toStdString() << " "
    << cTradeDatabase.GetSystemName(_nFromSystemID).toStdString()
    << " (" << cTradeDatabase.GetStationName(_nFromStationID).toStdString() << ")"
    << " --> "
    << cTradeDatabase.GetSystemName(_nToSystemID).toStdString()
    << " (" << cTradeDatabase.GetStationName(_nToStationID).toStdString() << ")"
    << " for a profit of " << _nProfit << "pr tonne.";


  return cStringStream.str();
}

bool
evian::BestTrade_c::operator<(const BestTrade_c & cBestTrade) const
{
  return _nProfit < cBestTrade._nProfit;
}

bool
evian::BestTrade_c::operator>(const BestTrade_c & cBestTrade) const
{
  return _nProfit > cBestTrade._nProfit;
}

bool
evian::SymmetricTrade_c::operator<(const SymmetricTrade_c & cSymmetricTrade) const
{
  return _cBestTrade0._nProfit + _cBestTrade1._nProfit < cSymmetricTrade._cBestTrade0._nProfit + cSymmetricTrade._cBestTrade1._nProfit;
}

bool
evian::SymmetricTrade_c::operator>(const SymmetricTrade_c & cSymmetricTrade) const
{
  return _cBestTrade0._nProfit + _cBestTrade1._nProfit > cSymmetricTrade._cBestTrade0._nProfit + cSymmetricTrade._cBestTrade1._nProfit;
}

int evian::SymmetricTrade_c::Profit() const
{
  return _cBestTrade0._nProfit + _cBestTrade1._nProfit;
}

std::string
evian::SymmetricTrade_c::PrettyPrint2(const TradeDatabaseStorage_c & cTradeDatabase) const
{
  using namespace fmt;

  const auto
    & cStation0 = cTradeDatabase.GetStation(_cBestTrade0._nFromStationID),
    & cStation1 = cTradeDatabase.GetStation(_cBestTrade0._nToStationID);

  const System_c
    & cSystem0 = cTradeDatabase.GetSystem(_cBestTrade0._nFromSystemID),
    & cSystem1 = cTradeDatabase.GetSystem(_cBestTrade0._nToSystemID);

  std::stringstream
    ss;
  ss << format("{} ({}, {} ls) <--> {} ({}, {} ls)\n",
    cTradeDatabase.GetSystemName(_cBestTrade0._nFromSystemID).toStdString(),
    cStation0._cName.toStdString(), cStation0._nDistanceToStar,
    cTradeDatabase.GetSystemName(_cBestTrade1._nFromSystemID).toStdString(),
    cStation1._cName.toStdString(), cStation1._nDistanceToStar);

  ss << format("{} ({} cr) <--> {} ({} cr)\n",
    cTradeDatabase.GetCommodityName(_cBestTrade0._nCommodityID).toStdString(),
    _cBestTrade0._nProfit,
    cTradeDatabase.GetCommodityName(_cBestTrade1._nCommodityID).toStdString(),
    _cBestTrade1._nProfit);

  ss << format("Profit: {} cr, Distance: {} Ly.\n",
    _cBestTrade0._nProfit + _cBestTrade1._nProfit,
    Dist(cSystem0._cPosition, cSystem1._cPosition));

  return ss.str();
}

std::string evian::SymmetricTrade_c::PrettyPrint(const TradeDatabaseStorage_c & cTradeDatabase) const
{
  std::stringstream
    ss;

  ss << _cBestTrade0.PrettyPrint(cTradeDatabase) << std::endl;
  ss << _cBestTrade1.PrettyPrint(cTradeDatabase) << std::endl;
  ss << "Total profit " << _cBestTrade0._nProfit + _cBestTrade1._nProfit << std::endl;

  const System_c &
    cSystem0 = cTradeDatabase.GetSystem(_cBestTrade0._nFromSystemID),
    cSystem1 = cTradeDatabase.GetSystem(_cBestTrade0._nToSystemID);

  ss << "Distance: " << Dist(cSystem0._cPosition, cSystem1._cPosition) << " LY.\n";

  return ss.str();
}

void evian::BestSymmetricTrade_c::operator()(const TradeDatabaseStorage_c & cTradeDatabase)
{
  auto
    cGrid = cTradeDatabase.CreateGrid(_cQueryRestriction._rMaxDistance);

  const int
    nResultSortThreshHold = 10 * _cQueryRestriction._nMaxResults;

  for (const auto& cPair : cGrid._lcGridCell)
  {
    auto
      nCellIndex = cPair.first;

    auto
      cIJK = cGrid.IJKFromIndex(nCellIndex);

    if (_bUseConcurrencyRuntime)
    {
      concurrency::parallel_for(static_cast<size_t>(0), cPair.second._lCellElement.size(), [&](size_t iSystem)
      {
        
        _cCombineableSymmetricTrade.local().reserve(nResultSortThreshHold);

        const System_c&
          cSystem0 = cPair.second._lCellElement[iSystem];

        BestTradeFromSystem(cSystem0, cGrid, nCellIndex, cIJK, _cQueryRestriction, _cCombineableSymmetricTrade.local(), true);

        if (_cCombineableSymmetricTrade.local().size() > 0.90 * nResultSortThreshHold) 
        {
          std::sort(_cCombineableSymmetricTrade.local().begin(), _cCombineableSymmetricTrade.local().end(), std::greater<SymmetricTrade_c>());
          _cCombineableSymmetricTrade.local().resize(_cQueryRestriction._nMaxResults);
        }
      });
    }
    else
    {
      for (const System_c& cSystem0 : cPair.second._lCellElement)
      {
        BestTradeFromSystem(cSystem0, cGrid, nCellIndex, cIJK, _cQueryRestriction, _cCombineableSymmetricTrade.local(), true);
      }

      auto& // Combine will do nothing if we have been running in single-thread mode.
        lcSymmetricTrade = _cCombineableSymmetricTrade.local();

      std::sort(lcSymmetricTrade.begin(), lcSymmetricTrade.end(), [](const SymmetricTrade_c& cSymmetricTrade0, const SymmetricTrade_c& cSymmetricTrade1)
      {
        return cSymmetricTrade0 > cSymmetricTrade1;
      });

      if (_cQueryRestriction._nMaxResults > 0 &&
          lcSymmetricTrade.size() > _cQueryRestriction._nMaxResults)
      {
        lcSymmetricTrade.resize(_cQueryRestriction._nMaxResults);
      }
    }
  }
}

void
evian::BestSymmetricTrade_c::Combine()
{
  _lcSymmetricTrade = _cCombineableSymmetricTrade.combine([this](const std::vector<SymmetricTrade_c>& s0, const std::vector<SymmetricTrade_c>& s1)
  {
    //qDebug() << "Sizes are " << s0.size() << ", " << s1.size();

    std::vector<SymmetricTrade_c>
      lcSymmetricTrade(s0.size() + s1.size());

    auto
      nByteCount = sizeof(SymmetricTrade_c) * s0.size();

    // Bypass ctors.
    memcpy(lcSymmetricTrade.data(), s0.data(), nByteCount);
    memcpy(&lcSymmetricTrade[s0.size()], s1.data(), sizeof(SymmetricTrade_c) * s1.size());

    if (lcSymmetricTrade.size() > 50000)
    {
      qDebug() << "Performing parallel sort";
      concurrency::parallel_sort(lcSymmetricTrade.begin(), lcSymmetricTrade.end(), std::greater<SymmetricTrade_c>());
    }
    else
    {
      std::sort(lcSymmetricTrade.begin(), lcSymmetricTrade.end(), std::greater<SymmetricTrade_c>());
    }

    if (_cQueryRestriction._nMaxResults > 0 &&
      lcSymmetricTrade.size() > _cQueryRestriction._nMaxResults)
    {
      lcSymmetricTrade.resize(_cQueryRestriction._nMaxResults);
    }

    return lcSymmetricTrade;
  });
}

void 
evian::BestTradeFromSystem(const System_c& cSystem0, const Grid_c<System_c>& cGrid, int nCellIndex, const Int3& cIJK, const QueryRestriction_c& cQueryRestriction, std::vector<SymmetricTrade_c>& lcSymmetricTrade, bool isIntraCellCullingPerfomed /*= true*/)
{
  if (cQueryRestriction.IsSystemExcluded(cSystem0))
  {
    return;
  }

  for (int i = -1; i <= 1; ++i)
    for (int j = -1; j <= 1; ++j)
      for (int k = -1; k <= 1; ++k)
      {
        Int3
          cOffset(i, j, k),
          cActiveIJK = cIJK + cOffset;

        // Initially it was assumed that all grid cells were looped over on the outside,
        // so this test excludes doing double work. Pass nCellIndex as MAX_INT to disable test.
        if (!cGrid.IsInsideGridIJK(cActiveIJK) ||
           (nCellIndex < cGrid.IndexFromIJK(cActiveIJK)))
        {
          continue;
        }

        const auto&
          cGridCell1 = cGrid.GetGridCell(cActiveIJK);

        for (const System_c& cSystem1 : cGridCell1._lCellElement)
        {
          if (cSystem0._nID == cSystem1._nID &&
              cQueryRestriction._isIntraSystemTradingIgnored)
          {
            continue;
          }

          if (cQueryRestriction.IsSystemExcluded(cSystem1))
          {
            continue;
          }

          if (Dist(cSystem0._cPosition, cSystem1._cPosition) > cQueryRestriction._rMaxDistance)
          {
            continue;
          }

          for (const auto pcStation0 : cSystem0._lpcStation)
          {

            if (cQueryRestriction.IsStationExcluded(*pcStation0))
            {
              continue;;
            }

            for (const auto pcStation1 : cSystem1._lpcStation)
            {

              if (cQueryRestriction.IsStationExcluded(*pcStation1))
              {
                continue;
              }

              // Since we cull on the cell-level, we cannot also cull on the station level except in the same cell
              if (isIntraCellCullingPerfomed &&
                  cIJK == cActiveIJK &&
                  pcStation0->_nID < pcStation1->_nID)
              {
                continue;
              }

              SymmetricTrade_c
                cSymmetricTrade(BestTrade_c::FindBestTradeBetweenStationsFast(*pcStation0, *pcStation1, cQueryRestriction),
                                BestTrade_c::FindBestTradeBetweenStationsFast(*pcStation1, *pcStation0, cQueryRestriction));

              if (cSymmetricTrade.Profit() >= cQueryRestriction._rMinProfit)
              { 
                lcSymmetricTrade.push_back(cSymmetricTrade);
              }
            }
          }
        }
      }
}

bool
evian::QueryRestriction_c::IsSystemExcluded(const System_c& cSystem) const
{
  if (evian::find_sorted(_lnExcludedSystems.begin(), _lnExcludedSystems.end(), cSystem._nID) != _lnExcludedSystems.end())
  {
    return true;
  }
  else if (_lsExcludedFactions.contains(cSystem._sAllegiance) || _lsExcludedFactions.contains(cSystem._sPowerControlFaction))
  {
    return true;
  }
  
  return false;
}

bool
evian::QueryRestriction_c::IsStationExcluded(const Station_c & cStation) const{
  if ((cStation._nDistanceToStar    > _rMaxDistanceToStar)                 ||
      (cStation._nDistanceToStar == 0 && _isIgnoreStationsWithoutDistance) ||
      (cStation._eMaxLandingPadSize < _eMinLandingPadSize)                 ||
      (cStation._eMaxLandingPadSize > _eMaxLandingPadSize)                 ||
      (cStation._isPlanetaryOutpost && _isIgnorePlanetaryOutposts)         ||
       evian::find_sorted(_lnExcludedStations.begin(), _lnExcludedStations.end(), cStation._nID) != _lnExcludedStations.end())
  {
    return true;
  }
  
  return false;
}

bool
evian::QueryRestriction_c::IsSystemTupleExcluded(const evian::System_c& cSystem0, const evian::System_c& cSystem1)
{
  if (IsSystemExcluded(cSystem0))
  {
    return true;
  }

  if (IsSystemExcluded(cSystem1))
  {
    return true;
  }

  if (Dist(cSystem0._cPosition, cSystem1._cPosition) > _rMaxDistance)
  {
    return true;
  }

  if (cSystem0._nID == cSystem1._nID &&
      _isIntraSystemTradingIgnored)
  {
    return true;
  }

  return false;
}

bool evian::QueryRestriction_c::IsStationTupleExcluded(const Station_c& pcStation0, const Station_c& pcStation1)
{
  if (IsStationExcluded(pcStation0) || IsStationExcluded(pcStation1))
  {
    return true;
  }

  return false;
}

void evian::CommodityPriceAnalysisQuery_c::operator()(const TradeDatabaseStorage_c& cTradeDatabase)
{
  concurrency::parallel_for(static_cast<size_t>(0), cTradeDatabase._lcCommodity.size(), [&](size_t iCommodityID)
  //for(const auto& cCommodity : cTradeDatabase._lcCommodity)
  {
    const int
      nCommodityID = cTradeDatabase._lcCommodity[iCommodityID]._nID;

    _lcCommodityPriceAnalysisBuy[nCommodityID]._nCommodityID = nCommodityID;
    _lcCommodityPriceAnalysisSell[nCommodityID]._nCommodityID = nCommodityID;

    EVIAN_ASSERT(nCommodityID > 0);

    for (const auto& cSystem : cTradeDatabase._lcSystemWithStation)
    {
      for(const auto pcStation : cSystem.get()._lpcStation) 
      {
        if (pcStation->_paBuyPrice[nCommodityID] != Station_c::BUY_DEFAULT)
        {
          const auto
            nPrice = pcStation->_paBuyPrice[nCommodityID];
          _lcCommodityPriceAnalysisBuy[nCommodityID].AddPrice(nPrice, pcStation->_nID);
        }

        if (pcStation->_paSellPrice[nCommodityID] != Station_c::SELL_DEFAULT)
        {
          const auto
            nPrice = pcStation->_paSellPrice[nCommodityID];
          _lcCommodityPriceAnalysisSell[nCommodityID].AddPrice(nPrice, pcStation->_nID);
        }
      }
    }

    _lcCommodityPriceAnalysisBuy[nCommodityID].ComputeAverage();
    _lcCommodityPriceAnalysisBuy[nCommodityID].PrepareFrequencyTable();
    _lcCommodityPriceAnalysisSell[nCommodityID].ComputeAverage();
    _lcCommodityPriceAnalysisSell[nCommodityID].PrepareFrequencyTable();

    for (const auto& cSystem : cTradeDatabase._lcSystemWithStation)
    {
      for(const auto pcStation : cSystem.get()._lpcStation) 
      {
        if (pcStation->_paBuyPrice[nCommodityID] != Station_c::BUY_DEFAULT)
        {
          const auto
            nPrice = pcStation->_paBuyPrice[nCommodityID];
          _lcCommodityPriceAnalysisBuy[nCommodityID]._cFrequencyTable.AddPrice(nPrice);
        }

        if (pcStation->_paSellPrice[nCommodityID] != Station_c::SELL_DEFAULT)
        {
          const auto
            nPrice = pcStation->_paSellPrice[nCommodityID];
          _lcCommodityPriceAnalysisSell[nCommodityID]._cFrequencyTable.AddPrice(nPrice);
        }
      }
    }
    _lcCommodityPriceAnalysisBuy[nCommodityID].ComputeSigma();
    _lcCommodityPriceAnalysisSell[nCommodityID].ComputeSigma();
  });
}

void
evian::CommodityPriceAnalysisResult_c::AddPrice(int nPrice, int nStationID)
{
  EVIAN_ASSERT(nPrice >= 0);

  _nSamples++;
  _nPriceAverage += nPrice;
  
  if (nPrice < _nPriceMin)
  {
    _nStationIDMin = nStationID;
    _nPriceMin = nPrice;
  }

  if (nPrice > _nPriceMax)
  {
    _nStationIDMax = nStationID;
    _nPriceMax = nPrice;
  }
}

void evian::CommodityPriceAnalysisResult_c::ComputeAverage()
{
  if (_nSamples != 0)
  {
    _nPriceAverage /= _nSamples;
  }
}

void
evian::CommodityPriceAnalysisResult_c::PrepareFrequencyTable()
{
  if (_nSamples == 0)
  {
    return;
  }
  
  EVIAN_ASSERT(_nPriceMax >= _nPriceMin);

  auto
    nBuckets = static_cast<int>(ceil(1 + 3.3 * logf(_nSamples)));

  _cFrequencyTable._lnFrequencyTable.resize(nBuckets);
  _cFrequencyTable._rH = static_cast<float>(_nPriceMax - _nPriceMin + 1) / nBuckets;
  _cFrequencyTable._rMin = _nPriceMin;
}

void
evian::CommodityPriceAnalysisResult_c::ComputeSigma()
{
  if (_nSamples == 0)
  {
    return;
  }

  double
    rVariance = 0;

  for (auto i = 0; i < _cFrequencyTable._lnFrequencyTable.size(); ++i)
  {
    double
      rDeviation = (_nPriceMin + i * _cFrequencyTable._rH) - _nPriceAverage;

    rVariance += _cFrequencyTable._lnFrequencyTable[i] * (rDeviation*rDeviation);
  }

  EVIAN_ASSERT(rVariance >= 0);

  rVariance /= _nPriceAverage;

  _rVariance = rVariance;
  
  _rSigma = sqrt(rVariance);
}

void
evian::CommodityPriceAnalysisResult_c::FrequencyTable_c::AddPrice(int nPrice)
{
  auto 
    iBucket = (nPrice - _rMin) / _rH;
  EVIAN_ASSERT(iBucket >= 0);
  EVIAN_ASSERT(iBucket < _lnFrequencyTable.size());

  _lnFrequencyTable[iBucket]++;
}

void
evian::DirectionalTradeQueryImpl_c::ComputeTrade(const evian::Station_c& cStation0, const evian::Station_c& cStation1, const QueryRestriction_c& cQueryRestriction, const TradeDatabaseStorage_c& /*cTradeDatabase*/)
{
  auto
    cBestTradeStation = BestTrade_c::FindBestTradeBetweenStationsFast(cStation0, cStation1, cQueryRestriction);

  if (cBestTradeStation._nProfit > cQueryRestriction._rMinProfit)
  {
    _cCombineableBestTrade.local().push_back(cBestTradeStation);
  }

  if (_cCombineableBestTrade.local().size() > 10 * _cQueryRestriction._nMaxResults)
  {
    std::sort(_cCombineableBestTrade.local().begin(), _cCombineableBestTrade.local().end(), std::greater<BestTrade_c>());
    _cCombineableBestTrade.local().resize(_cQueryRestriction._nMaxResults);
  }
}

void
evian::DirectionalSmugglingQuery_c::ComputeTrade(const evian::Station_c& cStation0, const evian::Station_c& cStation1, const QueryRestriction_c& cQueryRestriction, const TradeDatabaseStorage_c& cTradeDatabase)
{
  auto
    lcBestTrade = BestTrade_c::FindSmugglingRouteBetweenStations(cStation0, cStation1, cTradeDatabase, cQueryRestriction);

  for (const auto& cBestTrade : lcBestTrade)
  {
    _cCombineableBestTrade.local().push_back(cBestTrade);
  }
}

void
evian::DirectionalSmugglingQuery_c::Combine()
{
  _lcBestTrade = _cCombineableBestTrade.combine([this](const std::vector<BestTrade_c>& s0, const std::vector<BestTrade_c>& s1)
  {
    //qDebug() << "Sizes are " << s0.size() << ", " << s1.size();

    std::vector<BestTrade_c>
      lcBestTrade(s0.size() + s1.size());

    auto
      nByteCount = sizeof(BestTrade_c) * s0.size();

    // Bypass ctors.
    if (!s0.empty())
    {
      memcpy(lcBestTrade.data(), s0.data(), nByteCount);
    }

    if (!s1.empty())
    {
      memcpy(&lcBestTrade[s0.size()], s1.data(), sizeof(BestTrade_c) * s1.size());
    }

    return lcBestTrade;
  });
}

void
evian::DirectionalTradeQueryImpl_c::Combine()
{
  bool
    isSorted = false;

  _lcBestTrade = _cCombineableBestTrade.combine([this, &isSorted](const std::vector<BestTrade_c>& s0, const std::vector<BestTrade_c>& s1)
  {
    //qDebug() << "Sizes are " << s0.size() << ", " << s1.size();

    std::vector<BestTrade_c>
      lcBestTrade(s0.size() + s1.size());

    auto
      nByteCount = sizeof(BestTrade_c) * s0.size();

    // Bypass ctors.
    if (!s0.empty())
    {
      memcpy(lcBestTrade.data(), s0.data(), nByteCount);
    }
    
    if (!s1.empty())
    {
      memcpy(&lcBestTrade[s0.size()], s1.data(), sizeof(BestTrade_c) * s1.size());
    }
    

    if (lcBestTrade.size() > 50000)
    {
      qDebug() << "Performing parallel sort";
      concurrency::parallel_sort(lcBestTrade.begin(), lcBestTrade.end(), std::greater<BestTrade_c>());
    }
    else
    {
      std::sort(lcBestTrade.begin(), lcBestTrade.end(), std::greater<BestTrade_c>());
    }

    isSorted = true;

    if (_cQueryRestriction._nMaxResults > 0 &&
        lcBestTrade.size() > _cQueryRestriction._nMaxResults)
    {
      lcBestTrade.resize(_cQueryRestriction._nMaxResults);
    }

    return lcBestTrade;
  });

  if (!isSorted)
  {
    std::sort(_lcBestTrade.begin(), _lcBestTrade.end(), std::greater<BestTrade_c>());
    _lcBestTrade.resize(std::min(_cQueryRestriction._nMaxResults, static_cast<int>(_lcBestTrade.size())));
  }
}

void
evian::SymmetricTradeQueryImpl_c::ComputeTrade(const evian::Station_c& cStation0, const evian::Station_c& cStation1, const QueryRestriction_c& cQueryRestriction, const TradeDatabaseStorage_c& /*cTradeDatabase*/)
{
  SymmetricTrade_c
    cSymmetricTrade(BestTrade_c::FindBestTradeBetweenStationsFast(cStation0, cStation1, cQueryRestriction),
                    BestTrade_c::FindBestTradeBetweenStationsFast(cStation1, cStation0, cQueryRestriction));

  if (cSymmetricTrade.Profit() >= cQueryRestriction._rMinProfit)
  { 
    _cCombineableSymmetricTrade.local().push_back(cSymmetricTrade);
  }
}

void
evian::SymmetricTradeQueryImpl_c::Combine()
{
  bool
    isSorted = false;

  _lcSymmetricTrade = _cCombineableSymmetricTrade.combine([this, &isSorted](const std::vector<SymmetricTrade_c>& s0, const std::vector<SymmetricTrade_c>& s1)
  {
    //qDebug() << "Sizes are " << s0.size() << ", " << s1.size();

    std::vector<SymmetricTrade_c>
      lcSymmetricTrade(s0.size() + s1.size());

    auto
      nByteCount = sizeof(SymmetricTrade_c) * s0.size();

    // Bypass ctors.
    memcpy(lcSymmetricTrade.data(), s0.data(), nByteCount);
    memcpy(&lcSymmetricTrade[s0.size()], s1.data(), sizeof(SymmetricTrade_c) * s1.size());

    if (lcSymmetricTrade.size() > 50000)
    {
      qDebug() << "Performing parallel sort";
      concurrency::parallel_sort(lcSymmetricTrade.begin(), lcSymmetricTrade.end(), std::greater<SymmetricTrade_c>());
    }
    else
    {
      std::sort(lcSymmetricTrade.begin(), lcSymmetricTrade.end(), std::greater<SymmetricTrade_c>());
    }
    
    isSorted = true; // No mutex needed, will be true in the end.

    if (_cQueryRestriction._nMaxResults > 0 &&
      lcSymmetricTrade.size() > _cQueryRestriction._nMaxResults)
    {
      lcSymmetricTrade.resize(_cQueryRestriction._nMaxResults);
    }

    
    return lcSymmetricTrade;
  });


  if (!isSorted)
  {
    std::sort(_lcSymmetricTrade.begin(), _lcSymmetricTrade.end(), std::greater<SymmetricTrade_c>());
    _lcSymmetricTrade.resize(std::min(_cQueryRestriction._nMaxResults, static_cast<int>(_lcSymmetricTrade.size())));
  }
}

void
evian::CommodityMinMaxAnalysisQuery_c::operator()(const TradeDatabaseStorage_c& cTradeDatabase)
{
  QList<std::reference_wrapper<const System_c>>
    lcSystem;

  if (_nSystemID != -1 && _cQueryRestriction._rMaxDistance > 0) // Valid system and range
  {
    auto
      cPos = cTradeDatabase.GetSystem(_nSystemID)._cPosition;

    for (const auto& cSystem : cTradeDatabase._lcSystem)
    {
      if (Dist(cPos, cSystem._cPosition) <= _cQueryRestriction._rMaxDistance)
      {
        lcSystem.append(cSystem);
      }
    }
  }
  else if (_cQueryRestriction._rMaxDistance == 0)
  {
    for (const auto& cSystem : cTradeDatabase._lcSystem)
    {
      lcSystem.append(cSystem);
    }
  }
  else
  {
    EVIAN_ASSERT(_nSystemID == -1);
    qDebug() << "No suitable system to build table from";
    return;
  }
  
  Listing_c
    cListing;

  cListing._nCommodityID = _nCommodityID;

  for (const System_c& cSystem : lcSystem)
  {
    for (const auto pcStation: cSystem._lpcStation)
    { // This will check if the listing is disabled.
      if (_cQueryRestriction.IsStationExcluded(*pcStation))
      {
        continue;
      }

      if (pcStation->_paSellPrice[_nCommodityID] != Station_c::SELL_DEFAULT)
      {
        auto 
          it = std::lower_bound(pcStation->_lcListing.begin(), pcStation->_lcListing.end(), cListing);

        if (it != pcStation->_lcListing.end())
        {
          if (cListing._nCommodityID == it->_nCommodityID)
          {
            _lcListingMaxSell.append(*it);
          }
          else
          {
            EVIAN_ASSERT(false && "If a commodity is enabled in _paSellPrice it must also be in _lcListing");
          }
        }
        else
        {
          EVIAN_ASSERT(false && "If a commodity is enabled in _paSellPrice it must also be in _lcListing");
        }
      }

      if (pcStation->_paBuyPrice[_nCommodityID] != Station_c::BUY_DEFAULT)
      {
        auto 
          it = std::lower_bound(pcStation->_lcListing.begin(), pcStation->_lcListing.end(), cListing);

        if (it != pcStation->_lcListing.end())
        {
          if (cListing._nCommodityID == it->_nCommodityID)
          {
            _lcListingMinBuy.append(*it);
          }
          else
          {
            EVIAN_ASSERT(false && "If a commodity is enabled in _paBuyPrice it must also be in _lcListing");
          }
        }
        else
        {
          EVIAN_ASSERT(false && "If a commodity is enabled in _paBuyPrice it must also be in _lcListing");
        }
      }
    }
  }

  std::sort(_lcListingMinBuy.begin(), _lcListingMinBuy.end(), [](const Listing_c& cListing0, const Listing_c& cListing1)
  {
    if (cListing0._nBuyPrice == 0 || cListing0._nSupply == 0)
    {
      return false;
    }

    if (cListing1._nBuyPrice == 0 || cListing1._nSupply == 0)
    {
      return true;
    }

    return cListing0._nBuyPrice < cListing1._nBuyPrice;
  });

  std::sort(_lcListingMaxSell.begin(), _lcListingMaxSell.end(), [](const Listing_c& cListing0, const Listing_c& cListing1)
  {
    return cListing0._nSellPrice > cListing1._nSellPrice;
  });

}

int
evian::CommodityMinMaxAnalysisQuery_c::GetSystemID() const
{
  return _nSystemID;
}

void
evian::SymmetricSmugglingQuery_c::ComputeTrade(const evian::Station_c& cStation0, const evian::Station_c& cStation1, const QueryRestriction_c& cQueryRestriction, const TradeDatabaseStorage_c& cTradeDatabase)
{
  auto
    lcBestTrade = BestTrade_c::FindSmugglingRouteBetweenStations(cStation0, cStation1, cTradeDatabase, cQueryRestriction);

  auto
    cBestTradeReturn = BestTrade_c::FindBestTradeBetweenStationsFast(cStation1, cStation0, cQueryRestriction);

  for (const auto& cBestTrade : lcBestTrade)
  {
    _cCombineableSymmetricTrade.local().push_back(SymmetricTrade_c(cBestTrade, cBestTradeReturn));
  }
}

void
evian::SymmetricSmugglingQuery_c::Combine()
{
  _lcSymmetricTrade = _cCombineableSymmetricTrade.combine([this](const std::vector<SymmetricTrade_c>& s0, const std::vector<SymmetricTrade_c>& s1)
  {
    std::vector<SymmetricTrade_c>
      lcSymmetricTrade(s0.size() + s1.size());

    auto
      nByteCount = sizeof(SymmetricTrade_c) * s0.size();

    // Bypass ctors.
    memcpy(lcSymmetricTrade.data(), s0.data(), nByteCount);
    memcpy(&lcSymmetricTrade[s0.size()], s1.data(), sizeof(SymmetricTrade_c) * s1.size());

    return lcSymmetricTrade;
  });

  std::sort(_lcSymmetricTrade.begin(), _lcSymmetricTrade.end(), std::greater<SymmetricTrade_c>());
}
