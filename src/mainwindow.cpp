//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "stdafx.h"
#include "mainwindow.hpp"
#include "ui_mainwindow.h"
#include "evian.hpp"
#include "ImportEDDB.hpp"
#include "DatabaseQuery.hpp"
#include "evianqueryqt.hpp"
#include "eviandatabaseexplorerqt.hpp"
#include "eviansettingsqt.hpp"
#include "ednetlogparserqt.hpp"
#include "eddn.hpp"
#include "aboutdialog.h"
#include "querywidget.hpp"
#include "tradedetailwidget.hpp"
#include "evianqt.hpp"
#include "evianeddnticker.hpp"

MainWindow::MainWindow(EvianApplication * pcEvianApplication, QWidget *parent) :
  _pcEvianApplication(pcEvianApplication),
  QMainWindow(parent),
  ui(new Ui::MainWindow),
  _pcEvianSettingsQt(0),
  _pcAboutDialog(0),
  _pcEvianEDDNTicker(0),
  _pcQThreadEDDNSubscriber(0),
  _pcQThreadDatabaseEngine(0),
  _pcEDDNSubscriber(0)
{
  evian::InitAndSaveSettingsHelper(key_concurrency_mode, evian::Concurrency_Automatic);
  evian::InitAndSaveSettingsHelper(key_fd_log_directory, EDNetLogParserQT::FindEDFile("Logs"));
  auto
    sAppconfigXMLFile = evian::InitAndSaveSettingsHelper(key_appconfigxmlfile, EDNetLogParserQT::FindEDFile("appconfig.xml")).toString();
  
  if (evian::InitAndSaveSettingsHelper(key_auto_update_appconfigxml, false).toBool())
  {
    eviansettingsqt::ConsiderEnableVerboseLogging(sAppconfigXMLFile);
  }

  ui->setupUi(this);

  QGraphicsDropShadowEffect * dse = new QGraphicsDropShadowEffect();
  dse->setBlurRadius(16);
  dse->setOffset(0);
  dse->setColor(QColor(255, 255, 255));
  ui->labelTestIllicit->setGraphicsEffect(dse);
  setCentralWidget(NULL);
  ui->menuBar->hide();

  connect(this,              &MainWindow::StatusMessageUpdated,
          this->statusBar(), &QStatusBar::showMessage);

  //setWindowFlags(Qt::CustomizeWindowHint | Qt::FramelessWindowHint);

  _pcEDNetLogParserQT = new EDNetLogParserQT(this);

  InitCurrentSystem();

  connect(_pcEDNetLogParserQT, &EDNetLogParserQT::CurrentSystemChanged,
    this, &MainWindow::SetCurrentSystem);

  ui->statusBar->addPermanentWidget(_pcQLabelCurrentSystem);

  InitDatabaseEngine();
  InitEDDNSubscriber();
  InitUpdater();
  CreateEvianEDDNTicker();

  connect(_pcTradeDatabaseEngine, &evian::TradeDatabaseEngine_c::Message,
          ui->statusBar,          &QStatusBar::showMessage);

  connect(this, &MainWindow::DatabaseStorageCreated,
          _pcTradeDatabaseEngine, &evian::TradeDatabaseEngine_c::ResetDatabaseStorage);
  
  connect(_pcTradeDatabaseEngine, &evian::TradeDatabaseEngine_c::DatabaseReady,
          [this]()
          {
            ShowStatusMessage(QString("Database ready"));
          });

  connect(_pcTradeDatabaseEngine, &evian::TradeDatabaseEngine_c::DatabaseAboutToBeReset,
    this, &MainWindow::DeleteDatabaseExplorerQtChildren, Qt::BlockingQueuedConnection);
          
  connect(_pcTradeDatabaseEngine, &evian::TradeDatabaseEngine_c::DatabaseReady,
          this, &MainWindow::StartEDDNSubscriber);

  //ImportDatabaseFromEDDB();
  ImportDatabase();
  
  // Bring up a default query, so the user has a nice button to push.
  //on_actionNew_Query_triggered();
  //on_actionQuery_2_triggered();
  //setDockOptions(QMainWindow::ForceTabbedDocks);
  setDockNestingEnabled(true);

  connect(_pcTradeDatabaseEngine, &evian::TradeDatabaseEngine_c::DatabaseReady,
         this, &MainWindow::on_actionQuery_2_triggered);
}

void 
MainWindow::paintEvent(QPaintEvent *pe)
{
  QMainWindow::paintEvent(pe);
}

void
MainWindow::ImportDatabase()
{
  QSettings
    cQSettings;

  QString
    sDatabaseFilename = cQSettings.value(key_database_file).toString(),
    sListingsFilename = cQSettings.value(key_database_listings_file).toString();

  if (!QFile::exists(sDatabaseFilename))
  {
    ShowStatusMessage(QString("Could not load database from %1").arg(sDatabaseFilename));
    return;
  }

  if (!QFile::exists(sListingsFilename))
  {
    ShowStatusMessage(QString("Could not load database from %1").arg(sListingsFilename));
    return;
  }

  ShowStatusMessage(QString("Loading database from %1 and %2").arg(sDatabaseFilename).arg(sListingsFilename));
  
  typedef QSharedPointer<evian::TradeDatabaseStorage_c> result_type;

  QFuture<result_type>
    cFuture = QtConcurrent::run(&evian::TradeDatabaseEngine_c::Deserialize, sDatabaseFilename, sListingsFilename);

  auto
    pcFutureWatcher = new QFutureWatcher<result_type>(this);

  connect(pcFutureWatcher, &QFutureWatcher<result_type>::finished,
          [this, pcFutureWatcher]()
          {
            emit DatabaseStorageCreated(pcFutureWatcher->result());
          });

  connect(pcFutureWatcher, &QFutureWatcher<result_type>::finished,
          pcFutureWatcher, &QObject::deleteLater);

  pcFutureWatcher->setFuture(cFuture);
}

void
MainWindow::ImportDatabaseFromEDDB()
{
  auto
    pcImportEDDBFutureHelper = new ImportEDDBFutureHelper_c(this);

  connect(pcImportEDDBFutureHelper, &ImportEDDBFutureHelper_c::DatabaseStorageCreated,
          _pcTradeDatabaseEngine,   &evian::TradeDatabaseEngine_c::ResetDatabaseStorage);
  
  connect(pcImportEDDBFutureHelper, &ImportEDDBFutureHelper_c::DatabaseStatusUpdated,
          _pcEvianSettingsQt,       &eviansettingsqt::DatabaseUpdated);

  pcImportEDDBFutureHelper->Start();
}

void MainWindow::InitDatabaseEngine()
{
  EVIAN_ASSERT(_pcQThreadDatabaseEngine == 0);

  _pcQThreadDatabaseEngine = new QThread();
  _pcTradeDatabaseEngine = new evian::TradeDatabaseEngine_c();
  _pcQThreadDatabaseEngine->setObjectName(QStringLiteral("Evian Database Engine Thread"));
  _pcTradeDatabaseEngine->moveToThread(_pcQThreadDatabaseEngine);

  connect(_pcQThreadDatabaseEngine, &QThread::started,
          _pcTradeDatabaseEngine, &evian::TradeDatabaseEngine_c::Process);
  
  connect(_pcTradeDatabaseEngine, &evian::TradeDatabaseEngine_c::Finished,
          _pcTradeDatabaseEngine, &evian::TradeDatabaseEngine_c::deleteLater);

  connect(_pcTradeDatabaseEngine, &evian::TradeDatabaseEngine_c::destroyed,
          _pcQThreadDatabaseEngine, &QThread::quit);

  connect(_pcQThreadDatabaseEngine, &QThread::finished,
          _pcQThreadDatabaseEngine, &QThread::deleteLater);
  
  _pcQThreadDatabaseEngine->start();

  ShowStatusMessage(QString("Database engine started"));
}

void
MainWindow::InitCurrentSystem()
{
  _pcQLabelCurrentSystem = new QLabel(this);
  _pcQLabelCurrentSystem->setFrameStyle(QFrame::Panel | QFrame::Sunken);
  _pcQLabelCurrentSystem->setAlignment(Qt::AlignBottom | Qt::AlignRight);

  SetCurrentSystem(_pcEDNetLogParserQT->GetCurrentSystem());
}

void
MainWindow::InitEDDNSubscriber()
{
  _pcQThreadEDDNSubscriber = new QThread();
  _pcQThreadEDDNSubscriber->setObjectName(QStringLiteral("Evian EDDN Subscriber Thread"));

  _pcEDDNSubscriber = new eddn::Subscriber(this, 0);

  _pcEDDNSubscriber->moveToThread(_pcQThreadEDDNSubscriber);

  connect(_pcQThreadEDDNSubscriber, &QThread::started,
          _pcEDDNSubscriber, &eddn::Subscriber::Process);

  connect(_pcEDDNSubscriber, &eddn::Subscriber::Finished,
          _pcEDDNSubscriber, &eddn::Subscriber::deleteLater);
  
  connect(_pcEDDNSubscriber, &eddn::Subscriber::destroyed,
          _pcQThreadEDDNSubscriber, &QThread::quit);

  connect(_pcQThreadEDDNSubscriber, &QThread::finished,
          _pcQThreadEDDNSubscriber, &QThread::deleteLater);

  connect(_pcEDDNSubscriber, &eddn::Subscriber::EDDNMessage,
          _pcTradeDatabaseEngine, &evian::TradeDatabaseEngine_c::UpdateListingEDDN);
}

void
MainWindow::StartEDDNSubscriber()
{
  EVIAN_ASSERT(_pcQThreadEDDNSubscriber && !_pcQThreadEDDNSubscriber->isRunning());
  
  ShowStatusMessage(QString("Starting EDDN Subscriber"));
  _pcQThreadEDDNSubscriber->start();

  disconnect(_pcTradeDatabaseEngine, &evian::TradeDatabaseEngine_c::DatabaseReady,
             this, &MainWindow::StartEDDNSubscriber);  
}

void
MainWindow::TerminateEDDNSubscriber()
{
  if (_pcEDDNSubscriber != 0)
  {
    qDebug() << "Terminating EDDN subscriber";
    _pcEDDNSubscriber->Terminate();
  }
}

void
MainWindow::TerminateDatabaseEngine()
{
  qDebug() << "Terminating trade engine";
  _pcTradeDatabaseEngine->Terminate(); // Will triger quit() in the thread.
}

void MainWindow::InitUpdater()
{
#ifdef EVIAN_HAVE_WINSPARKLE
    win_sparkle_set_appcast_url("https://bitbucket.org/johanseland/eviantool/downloads/appcast.xml");
    win_sparkle_init();
#endif
}

void MainWindow::TerminateUpdater()
{
#ifdef EVIAN_HAVE_WINSPARKLE
  win_sparkle_cleanup();
#endif
}

MainWindow::~MainWindow()
{
  qDebug() << __FUNCTION__;
  _pcEvianApplication->SetMainWindow(0); // This should avoid notify to try to access the lock.
  delete ui;
}

QString
MainWindow::LandingPadToString(evian::Station_c::LandingPadSize eLandingpadSize)
{
  switch (eLandingpadSize)
  {
    case evian::Station_c::LANDINGPAD_SMALL:
      return QString("Small");
    case evian::Station_c::LANDINGPAD_MEDIUM:
      return QString("Medium");
    case evian::Station_c::LANDINGPAD_LARGE:
      return QString("Large");
    case evian::Station_c::LANDINGPAD_UNKNOWN:
    default:
      return QString("Unknown");
  }
}

QString 
MainWindow::GetCurrentSystem() const
{
  return _pcQLabelCurrentSystem->text();
}

void MainWindow::DeleteDatabaseExplorerQtChildren()
{
  auto
    lpcDatabaseExplorer = findChildren<EvianDatabaseExplorerQt*>();

  for (auto pcDatabaseExplorer : lpcDatabaseExplorer)
  {
    pcDatabaseExplorer->deleteLater();
  }
}

void
MainWindow::ShowStatusMessage(QString cQString)
{
  emit StatusMessageUpdated(cQString, 0);
}

void
MainWindow::ShowStatusMessage(QString cQString, Message_Class)
{
  ShowStatusMessage(cQString);
}

QList<QDockWidget*>
MainWindow::GetDockWidgetsForArea(Qt::DockWidgetArea eDockWidgetArea)
{
  auto
    lpcQDockWidget = findChildren<QDockWidget*>(),
    lpcQDockWidgetsInArea = QList<QDockWidget*>();

  for (auto pcQDockWidget : lpcQDockWidget)
  {
    if (dockWidgetArea(pcQDockWidget) == eDockWidgetArea)
    {
      lpcQDockWidgetsInArea.append(pcQDockWidget);
    }
  }

  return lpcQDockWidgetsInArea;
}

void
MainWindow::SetCurrentSystem(QString cCurrentSystem)
{
  if (cCurrentSystem.isEmpty())
  {
    _pcQLabelCurrentSystem->setText("Unknown");
  }
  else
  {
    if (_pcQLabelCurrentSystem->text() != cCurrentSystem)
    {
      _pcQLabelCurrentSystem->setText(cCurrentSystem);
      emit CurrentSystemChanged(cCurrentSystem);
    }
  }
}

void
MainWindow::on_actionQuit_triggered()
{
  auto
    lpcDatabaseExplorers = findChildren<EvianDatabaseExplorerQt*>();

  for (auto pcDatabaseExplorer : lpcDatabaseExplorers)
  {
    pcDatabaseExplorer->deleteLater();
  }

  QApplication::quit(); // Will eventually invoke QuitApplication().
}

ExecuteQuery::ExecuteQuery(QObject * parent, QSharedPointer<evian::DatabaseQuery_c> pcDatabaseQuery, evian::TradeDatabaseEngine_c & cTradeDatabase) :
  QThread(parent),
  _pcDatabaseQuery(pcDatabaseQuery),
  _cTradeDatabaseEngine(cTradeDatabase),
  _nElapsedTime(-1),
  _nResult(-1)
{
}


ExecuteQuery::~ExecuteQuery()
{
}

void
ExecuteQuery::run()
{
  if (!_cTradeDatabaseEngine.IsDatabaseReady())
  {
    emit ElapsedTimeMessage("Database is not ready", 0);
  }
  else
  {
    evian::EvianReadLocker_c
      cEvianReadLocker(__FILE__, __LINE__, _cTradeDatabaseEngine.GetEvianReadWriteLock());

    _nElapsedTime = GetTickCount64();

    _pcDatabaseQuery->_eQueryStatus = evian::DatabaseQuery_c::QUERY_PROCESSING;
    _pcDatabaseQuery->operator()(_cTradeDatabaseEngine.GetTradeDatabase());
    _pcDatabaseQuery->Combine();
    _pcDatabaseQuery->_eQueryStatus = evian::DatabaseQuery_c::QUERY_COMPLETED;

    _nElapsedTime = GetTickCount64() - _nElapsedTime;

    QString
      cElapsedTimeMessage = "Query completed in " + QString::number(_nElapsedTime) + " ms. " + QString::number(_pcDatabaseQuery->GetNumberOfResults()) + " results returned.";

    emit ElapsedTimeMessage(cElapsedTimeMessage, 5000);
  }

  emit QueryCompleted();
}

void
MainWindow::on_actionNew_Query_triggered()
{
  auto
    pEvianQueryQT = new EvianQueryQT(this);

  auto 
    iPos = ui->tabWidget->addTab(pEvianQueryQT, pEvianQueryQT->windowTitle());
  ui->tabWidget->setCurrentIndex(iPos);
  //addDockWidget(Qt::RightDockWidgetArea, pEvianQueryQT);
}

void
MainWindow::on_actionDatabase_triggered()
{
  auto 
    pEvianDatabaseExplorerQt = new EvianDatabaseExplorerQt(this);

  auto
    pcQDockWidget = DecorateAsDockWidget("Database query", pEvianDatabaseExplorerQt);
  
  AddToTopArea(pcQDockWidget);
}

void
MainWindow::on_actionSettings_triggered()
{
  if (!_pcEvianSettingsQt)
  {
    CreateEvianSettings();
  }

  if (_pcEvianSettingsQt->isVisible())
  {
    _pcEvianSettingsQt->hide();
  }
  else
  {
    _pcEvianSettingsQt->move(QCursor::pos());

    _pcEvianSettingsQt->show();

    _pcEvianSettingsQt->raise();
    _pcEvianSettingsQt->activateWindow();
  }
}

void MainWindow::CreateEvianSettings()
{
  _pcEvianSettingsQt = new eviansettingsqt(this, this);

  connect(_pcEvianSettingsQt, &eviansettingsqt::EDDBDatabaseDownloadComplete,
          this, &MainWindow::RebuildEDDBDatabase);

  connect(_pcEvianSettingsQt, &eviansettingsqt::ImportEDDBFromDisk,
          this, &MainWindow::RebuildEDDBDatabase);

  connect(this, &MainWindow::DatabaseStatusUpdated,
          _pcEvianSettingsQt, &eviansettingsqt::EDDBDatabaseStatusUpdated);

  connect(_pcEvianSettingsQt, &eviansettingsqt::FilterOlderThanUpdated,
          _pcTradeDatabaseEngine, &evian::TradeDatabaseEngine_c::UpdateEarliestCommodityTime);

  connect(_pcTradeDatabaseEngine, &evian::TradeDatabaseEngine_c::DatabaseReady,
          _pcEvianSettingsQt, &eviansettingsqt::DatabaseUpdated);

  auto
    pcQDropShadowEffect = new QGraphicsDropShadowEffect(_pcEvianSettingsQt);

  pcQDropShadowEffect->setBlurRadius(9.0);
  pcQDropShadowEffect->setColor(QColor(0, 0, 0, 160));
  pcQDropShadowEffect->setOffset(4.0);
  _pcEvianSettingsQt->setGraphicsEffect(pcQDropShadowEffect);

  QFontMetrics
    cQFontMetrics(_pcEvianSettingsQt->GetWidgetFont());

  QSettings
    cQSettings;

  auto
    sDatabaseDirectory = cQSettings.value(key_datadirectory).toString();

  int
    nWidth = cQFontMetrics.width(sDatabaseDirectory);

  _pcEvianSettingsQt->resize(2*nWidth, 1.5*nWidth);
}

void
MainWindow::CreateEvianEDDNTicker()
{
  _pcEvianEDDNTicker = new EvianEDDNTicker_c(*_pcTradeDatabaseEngine, this);

  connect(_pcTradeDatabaseEngine, &evian::TradeDatabaseEngine_c::EDDNV1CommodityMessagesAdded,
          _pcEvianEDDNTicker,     &EvianEDDNTicker_c::EDDNMessagesAdded);

  addDockWidget(Qt::BottomDockWidgetArea, _pcEvianEDDNTicker);
}

void
MainWindow::RebuildEDDBDatabase()
{
  qDebug() << "Rebuild database from signal";
  ImportDatabaseFromEDDB(); // Uses the values stored in QSettings.
}

void
MainWindow::QuitApplication()
{
  qDebug() << __FUNCTION__;

  QSettings
    cQSettings;

  if (cQSettings.value(key_database_save_on_exit, true).toBool())
  {
    qDebug() << "Saving database";

    evian::EvianReadLocker_c
      cEvianReadLock(__FILE__, __LINE__, _pcTradeDatabaseEngine->GetEvianReadWriteLock());

    auto
      sFilename         = cQSettings.value(key_database_file).toString(),
      sFilenameListings = cQSettings.value(key_database_listings_file).toString();

    _pcTradeDatabaseEngine->Serialize(sFilename, sFilenameListings, evian::TradeDatabaseEngine_c::SerializationMode_Binary);
  }

  cQSettings.sync();

  TerminateEDDNSubscriber();
  TerminateDatabaseEngine();
  _pcEDNetLogParserQT->SetWatching(false);
}

QVariant
InitAndSaveSettingsHelper(const QString& key, const QVariant& defaultValue)
{
  QSettings
    cQSettings;

  auto
    cValue = cQSettings.value(key, defaultValue);

  if (!cQSettings.contains(key))
  {
    cQSettings.setValue(key, cValue);
  }

  return cValue;
}

StationsTableModel_c::StationsTableModel_c(MainWindow *pMainWindow, QObject * parent /*= 0*/) :
  QAbstractTableModel(parent),
  _pcMainWindow(pMainWindow)
{

}

int
StationsTableModel_c::rowCount(const QModelIndex & /*parent*/) const 
{
  return static_cast<int>(_pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase()._lcStation.size());
}

int
StationsTableModel_c::columnCount(const QModelIndex & /*parent*/) const 
{
  return StationsField_Last;
}

QVariant
StationsTableModel_c::data(const QModelIndex &index, int role /* = Qt::DisplayRole */) const 
{
  int
    nColumn = index.column(),
    nRow    = index.row();

  using evian::Station_c;

  const auto&
    cStation = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase()._lcStation[nRow];

  if (role == Qt::DisplayRole)
  {
    switch (nColumn)
    {
    case Field_DistanceToStar:
      return cStation._nDistanceToStar;
    case Field_MaxLandingPadSize:
      return _pcMainWindow->LandingPadToString(cStation._eMaxLandingPadSize);
    case Field_Name:
      return cStation._cName;
    case Field_StarportType:
      return cStation._cStarportType;
    case Field_System:
      return _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetSystemName(cStation._nSystemID);
    case Field_Updated:
      return FormatDurationSinceNow(QDateTime::fromTime_t(cStation._nUpdated));
    }
  }

  return QVariant();
}

QVariant
StationsTableModel_c::headerData(int section, Qt::Orientation orientation, int role /* = Qt::DisplayRole */) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal) 
    {
      switch (section)
      {
      case Field_Name:
        return QString("Station Name");
      case Field_DistanceToStar:
        return QString("Distance to Star");
      case Field_MaxLandingPadSize:
        return QString("Max Landing Pad Size");
      case Field_StarportType:
        return QString("Startport type");
      case Field_System:
        return QString("System");
      case Field_Updated:
        return QString("Last Updated");
      }
    }
  }

  return QVariant();
}

ListingTableModel_c::ListingTableModel_c(MainWindow * pMainWindow, QObject * parent) :
  QAbstractTableModel(parent),
  _pcMainWindow(pMainWindow),
  _nStationID(-1)
{}

int
ListingTableModel_c::rowCount(const QModelIndex & /*parent*/) const 
{
  if (_nStationID < 0)
  {
    return 0;
  }

  return static_cast<int>(_pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetStation(_nStationID)._lcListing.size());
}

int
ListingTableModel_c::columnCount(const QModelIndex & /*parent*/) const 
{
  return ListingField_Last;
}

QVariant
ListingTableModel_c::data(const QModelIndex &index, int role /* = Qt::DisplayRole */) const 
{
  if (_nStationID < 0)
  {
    return QVariant();
  }

  int
    nColumn = index.column(),
    nRow    = index.row();

  const auto &
    cStation = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetStation(_nStationID);

  const auto &
    lcListing = cStation._lcListing;

  EVIAN_ASSERT(nRow < lcListing.size());

  const auto &
    cListing = lcListing[nRow];

  QLocale
    cQLocale(QLocale::English);

  if (role == Qt::DisplayRole)
  {
    switch (nColumn)
    {
    case Field_Name:
      return _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetCommodityName(cListing._nCommodityID);
    case Field_BuyPrice:
      return cQLocale.toString(cListing._nBuyPrice) + " Cr";
    case Field_SellPrice:
      return cQLocale.toString(cListing._nSellPrice) + " Cr";
    case Field_Demand:
      return cQLocale.toString(cListing._nDemand);
    case Field_Supply:
      return cQLocale.toString(cListing._nSupply);
    case Field_UpdateCount:
      return cListing._nUpdateCount;
    case Field_Updated:
      return FormatDurationSinceNow(QDateTime::fromTime_t(cListing._nUpdated));
    case Field_ListingStatus:
      return cListing._isEnabled ? QString("Enabled") : QString("Disabled");
    }
  }

  if (role == Qt::CheckStateRole)
  {
    if (nColumn == Field_ListingStatus)
    {
      return cListing._isEnabled ? Qt::Checked : Qt::Unchecked;
    }
  }

  if (role == Qt::TextAlignmentRole)
  {
    const auto 
      nColumn = index.column();

    bool
      bIsNumericColumn = 
      nColumn == Field_BuyPrice    ||
      nColumn == Field_SellPrice   ||
      nColumn == Field_Demand      ||
      nColumn == Field_Supply      ||
      nColumn == Field_UpdateCount;

    if (bIsNumericColumn)
    {
      return Qt::AlignCenter + Qt::AlignVCenter;
    }
    else
    {
      return Qt::AlignLeft + Qt::AlignVCenter;
    }
  }

  return QVariant();
}

QVariant
ListingTableModel_c::headerData(int section, Qt::Orientation orientation, int role /* = Qt::DisplayRole */) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal) 
    {
      switch (section)
      {
      case Field_Name:
        return QString("Commodity");
      case Field_BuyPrice:
        return QString("Buy Price");
      case Field_SellPrice:
        return QString("Sell Price");
      case Field_Demand:
        return QString("Demand");
      case Field_Supply:
        return QString("Supply");
      case Field_Updated:
        return QString("Last updated");
      case Field_UpdateCount:
        return QString("Updated Count");
      case Field_ListingStatus:
        return QString("Status");
      }
    }
  }
  return QVariant();
}

Qt::ItemFlags
ListingTableModel_c::flags(const QModelIndex & index) const 
{
  int
    nColumn = index.column();

  auto
    eItemFlags = QAbstractTableModel::flags(index);

  switch (nColumn)
  {
    case Field_BuyPrice:
    case Field_SellPrice:
      eItemFlags = eItemFlags | Qt::ItemIsEditable;
      break;
    case Field_ListingStatus:
      eItemFlags = eItemFlags | Qt::ItemIsUserCheckable;
     break;
  }
  
  return eItemFlags; 
}

bool
ListingTableModel_c::setData(const QModelIndex & index, const QVariant & value, const int role /* = Qt::EditRole*/)
{
  int
    nColumn = index.column(),
    nRow    = index.row();
  
  auto &
    cStation = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetStation(_nStationID);

  auto &
    lcListing = cStation._lcListing;
  
  auto 
    cListing = lcListing[nRow]; // Notice copy!

  if (role == Qt::EditRole)
  {
    switch (nColumn)
    {
    case Field_BuyPrice:
      cListing._nBuyPrice = value.toInt();
      emit ListingUpdated(cListing, nRow);
      return true;
    case Field_SellPrice:
      cListing._nSellPrice = value.toInt();
      emit ListingUpdated(cListing, nRow);
      return true;
    }
  }

  if (role == Qt::CheckStateRole)
  {
    switch (nColumn)
    {
      case Field_ListingStatus:
        cListing._isEnabled = value.toBool();
        emit ListingUpdated(cListing, nRow);
        return true;
    }
  }

  return false;
}

void ListingTableModel_c::SetStationID(int nStationID)
{
  if (_nStationID == nStationID)
  {
    return;
  }

  beginResetModel();
  _nStationID = nStationID;
  endResetModel();
}

void ListingTableModel_c::UpdateListing(evian::Listing_c cListing, int iRowHint)
{
  if (cListing._nStationID != _nStationID)
  {
    return;
  }

  QModelIndex
    cTopLeft = createIndex(iRowHint, 0),
    cBottomRight = createIndex(iRowHint, ListingField_Last-1);

  emit dataChanged(cTopLeft, cBottomRight);
}

void ListingTableModel_c::AddListing(evian::Listing_c cListing, int iRowHint)
{
  if (cListing._nStationID != _nStationID)
  {
    return;
  }
  beginInsertRows(QModelIndex(), iRowHint, iRowHint);
  endInsertRows();
}

EvianApplication::EvianApplication(int &argc, char* argv[])
  : QApplication(argc, argv),
    _pcMainWindow(0)
{

}

bool EvianApplication::notify(QObject * pcQObject, QEvent * pcQEvent)
{

//return QApplication::notify(pcQObject, pcQEvent);

#if 1
  if (_pcMainWindow == 0)
  {
    return QApplication::notify(pcQObject, pcQEvent);
  } 
  else 
  {
   // Can be called recursively, but we take it in the top level only.
    if (_pcMainWindow->thread() == QThread::currentThread() && // Autolock the GUI thread, other threads must do their own locking.
        !evian::TradeDatabaseEngine_c::GetEvianReadWriteLock().isReadLockedByCurrentThread())
    {
      evian::EvianReadLocker_c
        cEvianReadLocker(__FILE__, __LINE__, evian::TradeDatabaseEngine_c::GetEvianReadWriteLock());

      return QApplication::notify(pcQObject, pcQEvent);
    }

    return QApplication::notify(pcQObject, pcQEvent); 
  }
#endif
}

void EvianApplication::SetMainWindow(MainWindow * pcMainWindow)
{
  if (_pcMainWindow != 0 && pcMainWindow == 0)
  {
    disconnect(this, SIGNAL(aboutToQuit()),
      _pcMainWindow, SLOT(QuitApplication()));
    
    _pcMainWindow = pcMainWindow;
  } 
  else if (_pcMainWindow == 0 && pcMainWindow != 0)
  {
    _pcMainWindow = pcMainWindow;

    connect(this, SIGNAL(aboutToQuit()),
      _pcMainWindow, SLOT(QuitApplication()));
  }
}

ImportEDDBFutureHelper_c::ImportEDDBFutureHelper_c(QObject * pcQObject /*= 0*/) :
  QObject(pcQObject)
{
}

ImportEDDBFutureHelper_c::~ImportEDDBFutureHelper_c()
{
  qDebug() << __FUNCTION__;
}

void
ImportEDDBFutureHelper_c::Start()
{
  QSettings
    cQSettings;

  evian::ImportEDDB_c::EDDBV4Context_c
    cEDDBV4Context;

  cEDDBV4Context._sCommodities = cQSettings.value(key_commoditiesfile).toString();
  cEDDBV4Context._sListings    = cQSettings.value(key_listingsfile).toString();
  cEDDBV4Context._sStations    = cQSettings.value(key_stationsfile).toString();
  cEDDBV4Context._sSystems     = cQSettings.value(key_systemsfile).toString();

  if (QFile::exists(cEDDBV4Context._sCommodities) &&
      QFile::exists(cEDDBV4Context._sListings)    &&
      QFile::exists(cEDDBV4Context._sStations)    &&
      QFile::exists(cEDDBV4Context._sSystems))
  {
    connect(&_cQFutureWatcher, SIGNAL(finished()), this, SLOT(FutureFinished()));
    connect(&_cQFutureWatcher, SIGNAL(canceled()), this, SLOT(FutureCanceled()));

    _cQFuture = QtConcurrent::run(evian::ImportEDDB_c::CreateTradeDatabaseFromEDDBV4, cEDDBV4Context);
    _cQFutureWatcher.setFuture(_cQFuture);
  }
  else
  {
    qWarning() << "Could not load database as database files does not exist";
    deleteLater();
  }
}

void
ImportEDDBFutureHelper_c::FutureFinished()
{
  if (_cQFuture.result()._eResultStatus == evian::ImportEDDB_c::CreateTradeDatabaseResult_c::Result_Failure)
  {
    qDebug() << "Could not import EDDB. DatabaseStorageCreated not triggered.";
    qDebug() << _cQFuture.result()._sErrorMessage;
    QMessageBox::warning(dynamic_cast<QWidget*>(parent()), QString("Error importing from EDDB"), _cQFuture.result()._sErrorMessage);
  }
  else
  {
    emit DatabaseStorageCreated(_cQFuture.result()._pcTradeDatabaseStorage);
    emit DatabaseStatusUpdated();
    qDebug() << __FILE__ << "Database rebuilt";
  }
  deleteLater();
}

void ImportEDDBFutureHelper_c::FutureCanceled()
{
  qDebug() << "Could not import EDDB. DatabaseStorageCreated not triggered.";
  QMessageBox::warning(dynamic_cast<QWidget*>(parent()), QString("Error importing from EDDB"), "Could not parse JSON");
  deleteLater();
}

void
MainWindow::on_tabWidget_tabCloseRequested(int index)
{
    auto
      pcQWidget = ui->tabWidget->widget(index);
    ui->tabWidget->removeTab(index);
    pcQWidget->deleteLater();
}

void
MainWindow::on_actionAbout_triggered()
{
  if (!_pcAboutDialog)
  {
    _pcAboutDialog = new AboutDialog_c(this);
    
    auto
      pcQDropShadowEffect = new QGraphicsDropShadowEffect(_pcAboutDialog);
    pcQDropShadowEffect->setBlurRadius(9.0);
    pcQDropShadowEffect->setColor(QColor(0, 0, 0, 160));
    pcQDropShadowEffect->setOffset(4.0);
    _pcAboutDialog->setGraphicsEffect(pcQDropShadowEffect);
  }

  if (_pcAboutDialog->isVisible())
  {
    _pcAboutDialog->hide();
  }
  else
  {
    _pcAboutDialog->move(QCursor::pos());
    _pcAboutDialog->show();
    _pcAboutDialog->raise();
    _pcAboutDialog->activateWindow();
  }
}

QDockWidget*
MainWindow::DecorateAsDockWidget(QString cTitle, QWidget* pcQWidget)
{
  auto 
    pcQDockWidget = new QDockWidget(cTitle, this);

  pcQDockWidget->resize(640,480);

  auto
    pcQWidgetDockContents = new QWidget();

  pcQWidgetDockContents->setObjectName(QStringLiteral("dockWidgetContents"));

  auto
    pcQVBoxLayout = new QVBoxLayout(pcQWidgetDockContents);

  pcQVBoxLayout->setObjectName(QStringLiteral("verticalLayout"));
  pcQWidget->setParent(pcQWidgetDockContents);

  pcQVBoxLayout->addWidget(pcQWidget);

  pcQDockWidget->setWidget(pcQWidgetDockContents);

  connect(pcQWidget,     &QWidget::windowTitleChanged,
          pcQDockWidget, &QWidget::setWindowTitle);

  connect(pcQWidget,     &QWidget::destroyed,
          pcQDockWidget, &QWidget::deleteLater);

  return pcQDockWidget;
}

void
MainWindow::AddToTopArea(QDockWidget* pcQDockWidget)
{
  auto
    lpcQDockWidget = GetDockWidgetsForArea(Qt::TopDockWidgetArea);

  addDockWidget(Qt::TopDockWidgetArea, pcQDockWidget);
  
  if (!lpcQDockWidget.empty())
  {
    pcQDockWidget->show();
    tabifyDockWidget(lpcQDockWidget[0], pcQDockWidget);
  }
}

void
MainWindow::on_actionQuery_2_triggered()
{
  auto
    pcQDockWidget = DecorateAsDockWidget("Trade Query", new QueryWidget_c(this, 0));
  AddToTopArea(pcQDockWidget);
}
