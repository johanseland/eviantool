//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef EDDN_HPP
#define EDDN_HPP

#include <QObject>

class MainWindow;

namespace evian
{
  struct Listing_c;
}

namespace eddn {

namespace key 
{
  static const char
    schemaRef[]        = "$schemaRef",
    header[]           = "header",
    message[]          = "message",
    uploaderID[]       = "uploaderID",
    softwareName[]     = "softwareName",
    softwareVersion[]  = "softwareVersion",
    gatewayTimestamp[] = "gatewayTimestamp",

    systemName[]       = "systemName",
    stationName[]      = "stationName",
    itemName[]         = "itemName",
    buyPrice[]         = "buyPrice",
    stationStock[]     = "stationStock",
    supplyLevel[]      = "supplyLevel",
    sellPrice[]        = "sellPrice",
    demand[]           = "demand",
    demandLevel[]      = "demandLevel",
    timestamp[]        = "timestamp";
}

struct SchemaVersion
{
  enum Type {
    Commodity_V1,
    Commodity_V2,
    Invalid
  };

  static const char SchemaV1[]; // "http://schemas.elite-markets.net/eddn/commodity/1";
  static const char SchemaV2[]; // "http://schemas.elite-markets.net/eddn/commodity/2"

  static SchemaVersion::Type
    GetSchemaVersion(const QJsonObject& cQJsonObject);
};

struct EDDNMessage_c 
{
  virtual ~EDDNMessage_c() {};
  virtual SchemaVersion::Type GetSchemaVersion() const = 0;
};

struct CommodityV1_c : public EDDNMessage_c
{
  struct Header_c 
  {
      QString       _cUploaderID,
                    _cSoftwareName,
                    _cSoftwareVersion;
      QDateTime     _cQDateTimeTimestamp;

      static const QStringList GetRequiredKeys();
      static Header_c ReadFromJSON(const QJsonObject& cQJsonObject);
      static void WriteToJSON(const Header_c& cEDDNHeaderV1,  QJsonObject& cQJsonObject);
  };

  struct Message_c
  {
    enum SupplyLevel { SupplyLevel_Low, SupplyLevel_Medium, SupplyLevel_High, SupplyLevel_Invalid };

    QString       _cSystemName,
                  _cStationName,
                  _cItemName;
  
    QDateTime     _cQDateTimeTimestamp;
  
    int           _nBuyPrice,
                  _nStationStock,
                  _nSellPrice,
                  _nDemand;
  
    SupplyLevel   _eSupplyLevel,
                  _eDemandLevel;

    static const QStringList GetRequiredKeys();
    static Message_c ReadFromJSON(const QJsonObject& cQJsonObject);
    static void WriteToJSON(const Message_c& cEDDNHeaderV1,  QJsonObject& cQJsonObject);
  
    static SupplyLevel SupplyLevelFromString(const QString& cQString);
    static QString SupplyLevelToString(SupplyLevel eSupplyLevel);
  };

  Header_c      _cHeader;
  Message_c     _cMessage;

  SchemaVersion::Type GetSchemaVersion() const override;
};

struct CommodityV2_c : public EDDNMessage_c
{
  typedef CommodityV1_c::Header_c Header_c;

  struct Message_c 
  {
    struct Properties_c
    {
      QString   _cSystemName,
                _cStationName;

      QDateTime     
                _cQDateTimeTimestamp;

    };

    struct Item_c
    {
      QString   _cName;

      int       _nBuyPrice,
                _nSupply,
                _nSellPrice,
                _nDemand;
      
      CommodityV1_c::Message_c::SupplyLevel
                _eSupplyLevel,
                _eDemandLevel;
    };

    static const QStringList GetRequiredKeys();
    static Message_c ReadFromJSON(const QJsonObject& cQJsonObject);

    Properties_c
                _cProperties;
    QList<Item_c>
                _lcItem;
  };

  Header_c      _cHeader;
  Message_c     _cMessage;

  static CommodityV2_c ReadFromJSON(const QJsonObject& cQJsonObject);
  SchemaVersion::Type GetSchemaVersion() const override;
};

struct ZMQResources;

/// Subscriber is a worker class intended to be used with a QThread to subscribe to
/// the EDDN-feed and push messages about updates to the rest of the application.
class Subscriber : public QObject
{
  Q_OBJECT
public:
  explicit Subscriber(MainWindow* pcMainWindow, QObject *parent = 0);
  ~Subscriber();

signals:
  void Finished();
  void Error(QString);
  void EDDNMessage(QJsonDocument);
  void EDDNConnectionReady();

public slots:
  void Terminate();
  void Process();

private:
  void InjectEDDNTestdata();
  void InitializeZeroMQ();
  bool          _bTerminate;
  MainWindow *  _pcMainWindow;
  QString       _cEDDNRelay;
  QByteArray    _cQByteArray;

  std::shared_ptr<ZMQResources>
                _pcZMQResources;
};

/// Monitor is a worker class intended to be used with a QThread to monitor the status of the
/// ZMQ-connection used by Subscriber.
///
/// Once Process() has been stared this class will not call QCoreApplication::processEvents() to
/// process incoming signals, so any signals connections must be direct and not queued.
class Monitor_c : public QObject
{
  Q_OBJECT
public:
  explicit Monitor_c(std::shared_ptr<ZMQResources> pcZQMResources, QObject *parent = 0);
  ~Monitor_c();

signals:
  void Finished();
  void Error(QString);

public slots:
  //void Terminate(); ///< Terminates the thread. Must use direct slot-connection.
  void Process();

private:
  std::shared_ptr<ZMQResources>
                _pcZMQResources;
};
}
#endif // EDDN_HPP
