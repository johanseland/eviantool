//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "stdafx.h"
#include "mainwindow.hpp"
#include "splashscreen.hpp"
#include "evianqt.hpp"

int main(int argc, char *argv[])
{
    qsrand(static_cast<int>(QDateTime::currentMSecsSinceEpoch()));
    EvianApplication cEvianApplication(argc, argv);

    QCoreApplication::setApplicationVersion(EVIAN_APP_VERSION);
    QCoreApplication::setOrganizationName("Seland Consulting");
    QCoreApplication::setOrganizationDomain("johanseland.com");
    QCoreApplication::setApplicationName("EvianTool");
    QSettings::setDefaultFormat(QSettings::IniFormat);
    QFontDatabase::addApplicationFont(QString(":/assets/fonts/EUROCAPS.TTF"));
    QFontDatabase::addApplicationFont(QString(":/assets/fonts/ROCKETSCRIPT.TTF"));
    QFontDatabase::addApplicationFont(QString(":/assets/fonts/Sintoy-Bold.otf"));
    QFontDatabase::addApplicationFont(QString(":/assets/fonts/Sintoy-Regular.otf"));
    
    qRegisterMetaType<evian::Listing_c>("EvianListing_c");
    qRegisterMetaType<QSharedPointer<evian::TradeDatabaseStorage_c>>("QSharedPointer<evian::TradeDatabaseStorage_c");

    QSettings
      cQSettings;

    auto
      cApplicationDataDirectory = cQSettings.value(key_datadirectory, QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)).toString();
    cQSettings.setValue(key_datadirectory, cApplicationDataDirectory);
    evian::InitAndSaveSettingsHelper(key_database_file, cApplicationDataDirectory + "/evian.dat");
    evian::InitAndSaveSettingsHelper(key_database_listings_file, cApplicationDataDirectory + "/listings.dat");
    evian::InitAndSaveSettingsHelper(key_logfile,       cApplicationDataDirectory + "/cmdrevian.log");
    evian::InitAndSaveSettingsHelper(key_database_data_validation_mode, QJsonDocument::Validate);

    qSetMessagePattern("[%{time h:mm:ss.zzz} %{function}:%{line}] %{type} - %{message}\n");
    qInstallMessageHandler(evian::QtMessageHandler);

    auto
      sLogFile = cQSettings.value(key_logfile).toString();

    QDir
      cQDir;

    bool
      isDataDirectoryAvilable = cQDir.mkpath(cApplicationDataDirectory);

    EVIAN_ASSERT(isDataDirectoryAvilable);

    if (QFile::exists(sLogFile))
    {
      QFile::remove(sLogFile);
    }

    qInfo() << "CMDR Evian Version " << QCoreApplication::applicationVersion() << " starting up.";
    
    //QApplication::setStyle(QStyleFactory::create("Fusion"));

    auto
      cQPalette = QPalette(QColor(255, 85, 0), QColor(50, 50, 50));

    QApplication::setPalette(cQPalette);

    MainWindow 
      cMainWindow(&cEvianApplication);

    cMainWindow.setWindowIcon(QIcon(":/assets/factions/Empire.svg"));

    QRect rec = QApplication::desktop()->screenGeometry();
    auto
      height = rec.height(),
      width = rec.width();

    eviangui::Splashscreen_c
      cSplashscreen;
    
    cSplashscreen.resize(width / 2, height / 2);

    cSplashscreen.show();

    QTimer
      cQTimer;

    cMainWindow.resize(width * 0.75, height * 0.75);

    //QPropertyAnimation *animation = new QPropertyAnimation(&cSplashscreen, "windowOpacity");
    //
    //animation->setDuration(2000);
    //animation->setStartValue(1.0);
    //animation->setEndValue(0.0);
    //animation->start();

    QObject::connect(&cQTimer, &QTimer::timeout, &cSplashscreen, &QSplashScreen::close);
    QObject::connect(&cQTimer, &QTimer::timeout, &cMainWindow, &QWidget::show);
    cQTimer.start(1250);
    //cSplashscreen.showMessage("Loading assets");
    cEvianApplication.SetMainWindow(&cMainWindow);
    //w.show();

    return cEvianApplication.exec();
}
