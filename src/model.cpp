//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "stdafx.h"
#include "model.hpp"
#include "evian.hpp"
#include "mainwindow.hpp"
#include "DatabaseQuery.hpp"
#include "tradedetailwidget.hpp"

EVIAN_BEGIN_NAMESPACE(model)

QIcon
GetFactionIcon(const QString& sFaction)
{
  QString
    sAllegianceIconPath = QString(":/assets/factions/") + sFaction + ".svg";

  bool
    isAllegianceIcon = QFile::exists(sAllegianceIconPath);

  if (isAllegianceIcon)
  {
    return QIcon(sAllegianceIconPath);
  }

  return QIcon();
}

QIcon
GetFactionIcon(const evian::System_c& cSystem)
{
  return GetFactionIcon(cSystem._sAllegiance);
}

QIcon
GetFactionIcon(const evian::Station_c& cStation)
{
  return GetFactionIcon(cStation._sAllegiance);
}


QIcon
GetPowerIcon(const evian::System_c& cSystem)
{
  QString
    sPowerControlFactionIconPath = QString(":/assets/factions/") + cSystem._sPowerControlFaction + ".svg";

  bool
    isPowerControlFactionIcon = QFile::exists(sPowerControlFactionIconPath);

  if (isPowerControlFactionIcon)
  {
    return QIcon(sPowerControlFactionIconPath);
  }

  return QIcon();
}

QIcon
GetIconForPowerControlFaction(QString sPowerControlFaction)
{
  QString
    sPowerControlFactionIconPath = QString(":/assets/factions/") + sPowerControlFaction + ".svg";

  bool
    isPowerControlFactionIcon = QFile::exists(sPowerControlFactionIconPath);
  
  if (isPowerControlFactionIcon)
  {
    return QIcon(sPowerControlFactionIconPath);
  }

  return QIcon();
}

QIcon
GetIconForSystem(const evian::System_c& cSystem)
{
  QString
    sAllegianceIconPath          = QString(":/assets/factions/") + cSystem._sAllegiance + ".svg",
    sPowerControlFactionIconPath = QString(":/assets/factions/") + cSystem._sPowerControlFaction + ".svg";

  bool
    isAllegianceIcon          = QFile::exists(sAllegianceIconPath),
    isPowerControlFactionIcon = QFile::exists(sPowerControlFactionIconPath);

  if (isPowerControlFactionIcon)
  {
    return QIcon(sPowerControlFactionIconPath);
  }
  else if (isAllegianceIcon)
  {
    return QIcon(sAllegianceIconPath);
  }

  return QIcon();
}

QString
GetIconPathForStation(const evian::Station_c& cStation)
{
  QString
    sStationIcon;

  // SM icons are prettier when small.
  if (cStation._cStarportType.contains("Planetary Outpost"))
  {
    sStationIcon = QString(":/assets/stations/planetaryoutpost2.png");
  }
  else if (cStation._cStarportType.contains("Outpost"))
  {
    sStationIcon = QString(":/assets/stations/Outpost_sm.svg");
  }
  else if (cStation._cStarportType.contains("Ocellus"))
  {
    sStationIcon = QString(":/assets/stations/Ocellus_sm.svg");
  }
  else if (cStation._cStarportType.contains("Orbis"))
  {
    sStationIcon = QString(":/assets/stations/Orbis_sm.svg");
  }
  else if (cStation._cStarportType.contains("Coriolis"))
  {
    sStationIcon = QString(":/assets/stations/Coriolis_sm.svg");
  }
  else if (cStation._cStarportType.contains("Starport"))
  {
    sStationIcon = QString(":/assets/stations/Coriolis_sm.svg");
  }

  return sStationIcon;
}

QIcon
GetIconForStation(const evian::Station_c& cStation)
{
  auto
    sStationIcon = GetIconPathForStation(cStation);

  if (QFile::exists(sStationIcon))
  {
    return QIcon(sStationIcon);
  }

  return QIcon();
}


SymmetricTradeModel_c::SymmetricTradeModel_c(MainWindow * parent, QSharedPointer<evian::SymmetricTradeQuery_c> pcDatabaseQuery) :
  QAbstractTableModel(parent),
  _pcDatabaseQuery(pcDatabaseQuery),
  _pcMainWindow(parent)
{}

int SymmetricTradeModel_c::rowCount(const QModelIndex & /*parent*/) const
{
  return static_cast<int>(_pcDatabaseQuery->_lcSymmetricTrade.size());

  if (_pcDatabaseQuery->_eQueryStatus == evian::DatabaseQuery_c::QUERY_COMPLETED)
  {
    return static_cast<int>(_pcDatabaseQuery->_lcSymmetricTrade.size());
  }
  return 0;
}

inline int
SymmetricTradeModel_c::columnCount(const QModelIndex & /*parent*/) const
{
  return SymmetricTradeModel::Column_Last;
}

QVariant
SymmetricTradeModel_c::data(const QModelIndex & index, int role) const
{
  using namespace SymmetricTradeModel;

  QLocale
    cQLocale(QLocale::English);

  auto &
    cSymmetricTrade = _pcDatabaseQuery->_lcSymmetricTrade[index.row()];

  const evian::TradeDatabaseStorage_c&
    cTradeDatabase = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase();

  const auto
    &cStation0 = cTradeDatabase.GetStation(cSymmetricTrade._cBestTrade0._nFromStationID),
    &cStation1 = cTradeDatabase.GetStation(cSymmetricTrade._cBestTrade0._nToStationID);

  const auto
    &cSystem0 = cTradeDatabase.GetSystem(cSymmetricTrade._cBestTrade0._nFromSystemID),
    &cSystem1 = cTradeDatabase.GetSystem(cSymmetricTrade._cBestTrade0._nToSystemID);

  if (role == Qt::DisplayRole)
  {
    switch(index.column())
    { 
    case Column_TotalProfit:
      return cQLocale.toString(cSymmetricTrade._cBestTrade0._nProfit + cSymmetricTrade._cBestTrade1._nProfit) + " Cr";
    case Column_System1:
      return cSystem0._cName;
    case Column_Station1:
      return cStation0._cName;
    case Column_Distance1:
      return QString::number(cStation0._nDistanceToStar) + " ls";
    case Column_Commodity1:
      return cTradeDatabase.GetCommodityName(cSymmetricTrade._cBestTrade0._nCommodityID);
    case Column_Profit1:
      return QString("%1 Cr").arg(cQLocale.toString(cSymmetricTrade._cBestTrade0._nProfit));
    case Column_System2:
      return cSystem1._cName;
    case Column_Station2:
      return cStation1._cName;
    case Column_Distance2:
      return QString("%1 ls").arg(cStation1._nDistanceToStar, 5);
    case Column_Commodity2:
      return cTradeDatabase.GetCommodityName(cSymmetricTrade._cBestTrade1._nCommodityID);
    case Column_Profit2:
      return QString("%1 Cr").arg(cQLocale.toString(cSymmetricTrade._cBestTrade1._nProfit));
    case Column_Distance:
      return QString::number( Dist(cSystem0._cPosition, cSystem1._cPosition), 'f', 2) + " LY";
    }
  }

  if (role == SortRole) // In order to sort formatted numbers by value and not by string.
  {
    switch (index.column())
    {
    case Column_TotalProfit:
      return cSymmetricTrade._cBestTrade0._nProfit + cSymmetricTrade._cBestTrade1._nProfit;
    case Column_System1:
      return cSystem0._cName;
    case Column_Station1:
      return cStation0._cName;
    case Column_Distance1:
      return cStation0._nDistanceToStar;
    case Column_Commodity1:
      return cTradeDatabase.GetCommodityName(cSymmetricTrade._cBestTrade0._nCommodityID);
    case Column_Profit1:
      return cSymmetricTrade._cBestTrade1._nProfit;
    case Column_System2:
      return cSystem1._cName;
    case Column_Station2:
      return cStation1._cName;
    case Column_Distance2:
      return cStation1._nDistanceToStar;
    case Column_Commodity2:
      return cTradeDatabase.GetCommodityName(cSymmetricTrade._cBestTrade1._nCommodityID);
    case Column_Profit2:
      return cSymmetricTrade._cBestTrade1._nProfit;
    case Column_Distance:
      return Dist(cSystem0._cPosition, cSystem1._cPosition);
    }
  }


  if (role == Qt::TextAlignmentRole)
  {
    const auto 
      nColumn = index.column();

    bool
      bIsNumericColumn = 
        nColumn == Column_TotalProfit ||
        nColumn == Column_Profit1     ||
        nColumn == Column_Profit2     ||
        nColumn == Column_Distance    ||
        nColumn == Column_Distance1   ||
        nColumn == Column_Distance2;

    if (bIsNumericColumn)
    {
      return Qt::AlignCenter + Qt::AlignVCenter;
    }
    else
    {
      return Qt::AlignLeft + Qt::AlignVCenter;
    }
  }

  if (role == Qt::DecorationRole)
  {
    switch (index.column())
    {
    case Column_System1:
      return GetIconForSystem(cSystem0);
    case Column_System2:
      return GetIconForSystem(cSystem1);
    case Column_Station1:
      return GetIconForStation(cStation0);
    case Column_Station2:
      return GetIconForStation(cStation1);
    default:
      return QVariant();
    }
  }

  if (role == Qt::ToolTipRole)
  {
    QString
      s = FormatSystemTooltip(cSystem0) + "\n";
    
    s += FormatStationTooltip(cStation0) + "\n";
    s += FormatSystemTooltip(cSystem1) + "\n";
    s += FormatStationTooltip(cStation1);

    return s;
  }

  return QVariant();
}

QVariant
SymmetricTradeModel_c::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal) {
      return SymmetricTradeModel::ColumnName[section];
    }
  }
  return QVariant();
}

void
SymmetricTradeModel_c::sort(int nColumn, Qt::SortOrder /*order*/) const
{
  using namespace evian;
  using namespace SymmetricTradeModel;
  
  switch (nColumn)
  {
  case Column_TotalProfit:
    std::sort(_pcDatabaseQuery->_lcSymmetricTrade.begin(), _pcDatabaseQuery->_lcSymmetricTrade.end(),
      [](const SymmetricTrade_c& s0, const SymmetricTrade_c& s1)
    {
      return s0.Profit() < s1.Profit();
    });
    break;
  case Column_Profit1:
    std::sort(_pcDatabaseQuery->_lcSymmetricTrade.begin(), _pcDatabaseQuery->_lcSymmetricTrade.end(),
      [](const SymmetricTrade_c& s0, const SymmetricTrade_c& s1)
    {
      return s0._cBestTrade0._nProfit < s1._cBestTrade0._nProfit;
    });
    break;
  case Column_Profit2:
    std::sort(_pcDatabaseQuery->_lcSymmetricTrade.begin(), _pcDatabaseQuery->_lcSymmetricTrade.end(),
      [](const SymmetricTrade_c& s0, const SymmetricTrade_c& s1)
    {
      return s0._cBestTrade1._nProfit < s1._cBestTrade1._nProfit;
    });
    break;
  }
}

void SymmetricTradeModel_c::ResultsReady()
{
  //emit dataChanged()
  beginResetModel();
  endResetModel();
}



SystemTableModel_c::SystemTableModel_c(SystemTable_e eSystemTable, MainWindow * pMainWindow, QObject * parent) :
  QAbstractTableModel(parent),
  _pcMainWindow(pMainWindow),
  _eSystemTable(eSystemTable)
{
}

int
SystemTableModel_c::rowCount(const QModelIndex & /*parent*/) const
{
  switch (_eSystemTable)
  {
  case Table_All_Systems:
    return static_cast<int>(_pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase()._lcSystem.size());
  case Table_Systems_with_Stations:
    return static_cast<int>(_pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase()._lcSystemWithStation.size());
  case Table_Faction:
    if (_pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase()._cFactionToSystemMap.contains(_sFaction))
    {
      return _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase()._cFactionToSystemMap[_sFaction].size();
    }
    qDebug() << "Unknown faction " << _sFaction;
    return 0;
  default:
    qDebug() << "Unknown _eSystemTable " << _eSystemTable;
    return 0;
  }
}

int
SystemTableModel_c::columnCount(const QModelIndex & /* parent*/) const
{
  return SystemField_Last;
}

QVariant
SystemTableModel_c::data(const QModelIndex & index, const int role) const
{
  int
    nColumn = index.column(),
    nRow    = index.row();

  const evian::System_c&
    cSystem = [nRow, this]()
    {
      switch (_eSystemTable)
      {
        case Table_All_Systems:
          return _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase()._lcSystem[nRow];
        case Table_Systems_with_Stations:
          return _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase()._lcSystemWithStation[nRow].get();
        default:
        case Table_Faction:
          return _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase()._cFactionToSystemMap[_sFaction][nRow].get();
      }
    }();

  auto
    cQLocale = QLocale(QLocale::English);

  if (role == Qt::DisplayRole)
  {
    switch (nColumn)
    {
    case Field_Name:
      return cSystem._cName;
    case Field_Position:
      return '(' + QString::number(cSystem._cPosition.x, 'f', 2) + ", "
        + QString::number(cSystem._cPosition.y, 'f', 2) + ", "
        + QString::number(cSystem._cPosition.z, 'f', 2) + ")";
    case Field_NeedPermit:
      return QString(cSystem._bNeedPermit ? "true" : "false");
    case Field_NumStations:
      return cSystem._lpcStation.size();
    case Field_Updated:
      return FormatDurationSinceNow(QDateTime::fromTime_t(cSystem._nUpdated));
    case Field_PowerControlFaction:
      return FormatPowerControlFaction(cSystem);
    case Field_Government:
      return cSystem._sGovernment;
    case Field_Allegiance:
      return cSystem._sAllegiance;
    case Field_MinorFaction:
      return cSystem._sMinorFaction;
    case Field_EconomicState:
      return cSystem._sEconomicState;
    case Field_Population:
      return cQLocale.toString(cSystem._nPopulation);
    case SystemField_Last:
      qDebug() << "Unhandled columnn: " << nColumn;
    }
  }

  if (role == Qt::TextAlignmentRole)
  {
    switch (nColumn)
    {
      case Field_Allegiance:
      case Field_Name:
      case Field_PowerControlFaction:
        return Qt::AlignLeft + Qt::AlignVCenter;
      default:
        return Qt::AlignHCenter + Qt::AlignVCenter;
    }
  }

  if (role == Qt::DecorationRole)
  {
    switch (nColumn)
    {
    case Field_Allegiance:
      return GetFactionIcon(cSystem);
    case Field_PowerControlFaction:
      return GetPowerIcon(cSystem);
    default:
      return QVariant();
    }
  }

  if (role == Qt::ToolTipRole)
  {
    return FormatSystemTooltip(cSystem);
  }

  return QVariant();
}

QString
SystemTableModel_c::FormatPowerControlFaction(const evian::System_c& cSystem) const
{
  if (!cSystem._sPowerControlFaction.isEmpty())
  {
    return cSystem._sPowerControlFaction;
  }

  QString
    sExploitedBy;

  if (!cSystem._lcExploitedBy.isEmpty())
  {
    sExploitedBy += QString("%1 (Exploited)").arg(cSystem._lcExploitedBy[0]._sPowerControlFaction);
  }

  if (cSystem._lcExploitedBy.size() > 1)
  {
    sExploitedBy += QString(" +%1").arg(cSystem._lcExploitedBy.size()-1);
  }
  //for (const auto& cExploitedBy : cSystem._lcExploitedBy)
  //{
  //  //sExploitedBy += QString("Exploited by %1 (%2, %3 LY)").arg(cExploitedBy._sPowerControlFaction).arg(cExploitedBy._sControlSystem).arg(cExploitedBy._rDistToControlSystem);
  //  if (!sExploitedBy.isEmpty())
  //  {
  //    sExploitedBy += ", "; 
  //  }
  //  sExploitedBy += QString("%1 (Exploited)").arg(cExploitedBy._sPowerControlFaction);
  //}

  return sExploitedBy;
}

QVariant
SystemTableModel_c::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal) {
      switch (section)
      {
      case Field_Name:
        return QString("System Name");
      case Field_Position:
        return QString("Position");
      case Field_NeedPermit:
        return QString("Permit");
      case Field_NumStations:
        return QString("#Stations");
      case Field_Updated:
        return QString("Last updated");
      case Field_PowerControlFaction:
        return QString("Powerplay Faction");
      case Field_Allegiance:
        return QString("Allegiance");
      case Field_Government:
        return QString("Government");
      case Field_MinorFaction:
        return QString("Faction");
      case Field_EconomicState:
        return QString("Ec. State");
      case Field_Population:
        return QString("Population");
      }
    }
  }
  return QVariant();
}

void model::SystemTableModel_c::SetFaction(const QString& sFaction)
{
  if (sFaction != _sFaction)
  {
    beginResetModel();
    _sFaction = sFaction;
    endResetModel();
  }
}

model::SystemTableModel_c * CreateSystemWithStationModel(MainWindow * pcMainWindow, QObject * pcQObject /*= 0*/)
{
  return new model::SystemTableModel_c(model::SystemTableModel_c::Table_Systems_with_Stations, pcMainWindow, pcQObject);
}


void PriceListSystemUpdatedCallback(const QString & cQString, QComboBox * pcComboBoxStation, MainWindow * pcMainWindow)
{
  pcComboBoxStation->clear();

  const auto &
    cSystem = pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetSystem(cQString);

  for (const auto& pcStation : cSystem._lpcStation)
  {
    QString 
      cDetailedStationName = pcStation->_cName + " (" + QString::number(pcStation->_nDistanceToStar) + " ls, ";

    cDetailedStationName += "Pad Size: " + MainWindow::LandingPadToString(pcStation->_eMaxLandingPadSize) + ")";

    if (pcStation->_lcListing.empty())
    {
      cDetailedStationName += " [NO DATA]";
    }

    pcComboBoxStation->addItem(cDetailedStationName, pcStation->_nID);
  }

  if (!cSystem._lpcStation.empty())
  {
    pcComboBoxStation->setCurrentIndex(0);
  }
}

void SetupSystemToStationComboboxes(QComboBox * pcComboBoxSystem, QComboBox * pcComboBoxStation, MainWindow * pcMainWindow, QObject * pcQObject /*= 0 */)
{
  auto
    pcSystemWithStationsModel = model::CreateSystemWithStationModel(pcMainWindow, pcQObject);

  auto
    pcQCompleterSystem = new QCompleter(pcSystemWithStationsModel);

  pcQCompleterSystem->setCaseSensitivity(Qt::CaseInsensitive);
  pcQCompleterSystem->setPopup(new QListView);
  pcQCompleterSystem->setCompletionMode(QCompleter::PopupCompletion);

  pcComboBoxSystem->setModel(pcSystemWithStationsModel);
  pcComboBoxSystem->setCompleter(pcQCompleterSystem);

  /*QObject::connect(pcComboBoxSystem, static_cast<void(QComboBox::*)(const QString &)>(&QComboBox::activated),
                    [=](const QString& cSystem) 
                    {
                      PriceListSystemUpdatedCallback(cSystem, pcComboBoxStation, pcMainWindow);
                    });
  */
  QObject::connect(pcComboBoxSystem, static_cast<void(QComboBox::*)(const QString &)>(&QComboBox::currentIndexChanged),
    [=](const QString& cSystem) 
  {
    PriceListSystemUpdatedCallback(cSystem, pcComboBoxStation, pcMainWindow);
  });

}

model::EDDNV1TableModel_c::EDDNV1TableModel_c(const evian::TradeDatabaseEngine_c& cTradeDatabaseEngine, QObject * parent) :
  QAbstractTableModel(parent),
  _cTradeDatabaseEngine(cTradeDatabaseEngine),
  _nRows(static_cast<int>(cTradeDatabaseEngine.GetTradeDatabase()._lpcEDDNMessage.size()))
{
}

int model::EDDNV1TableModel_c::rowCount(const QModelIndex & /*parent*/) const 
{
  return _nRows;//static_cast<int>(_cTradeDatabaseEngine.GetTradeDatabase()._lcEDDNCommodityV1.size());
}

int model::EDDNV1TableModel_c::columnCount(const QModelIndex & /*parent*/) const 
{
  return column_last;
}

QVariant 
EDDNV1TableModel_c::data(const QModelIndex &index, int role /* = Qt::DisplayRole */) const 
{
  int
    nColumn = index.column(),
    nRow    = index.row();
  EVIAN_ASSERT(nRow < _nRows);

  const eddn::CommodityV1_c
    cMessage;// = _cTradeDatabaseEngine.GetTradeDatabase()._lpcEDDNMessage[nRow];
  
  if (role == Qt::DisplayRole)
  {
    switch (nColumn)
    {
    case uploaderID:
      return cMessage._cHeader._cUploaderID;
    case softwareName:
      return cMessage._cHeader._cSoftwareName;
    case softwareVersion:
      return cMessage._cHeader._cSoftwareVersion;
    case gatewayTimestamp:
      return cMessage._cHeader._cQDateTimeTimestamp;
    case systemName:
      return cMessage._cMessage._cSystemName;
    case stationName:
      return cMessage._cMessage._cStationName;
    case itemName:
      return cMessage._cMessage._cItemName;
    case buyPrice:
      return cMessage._cMessage._nBuyPrice;
    case stationStock:
      return cMessage._cMessage._nStationStock;
    case supplyLevel:
      return eddn::CommodityV1_c::Message_c::SupplyLevelToString(cMessage._cMessage._eSupplyLevel);
    case sellPrice:
      return cMessage._cMessage._nSellPrice;
    case demand:
      return cMessage._cMessage._nDemand;
    case demandLevel:
      return eddn::CommodityV1_c::Message_c::SupplyLevelToString(cMessage._cMessage._eDemandLevel);
    case timestamp:
      return cMessage._cMessage._cQDateTimeTimestamp;
    default:
      qDebug() << "Unhandled column: " << nColumn;
    }
  }
  return QVariant();
}

void model::EDDNV1TableModel_c::MessagesAdded(int iRowHint, int nMessages)
{
  //qDebug() << __FUNCTION__ << " iRowHint " << iRowHint << " _nRows " << _nRows << " nMessages " << nMessages;
  beginInsertRows(QModelIndex(), iRowHint, iRowHint + nMessages -1);
  _nRows += nMessages;
  endInsertRows();
}

QVariant 
model::EDDNV1TableModel_c::headerData(int iColumn, Qt::Orientation /*orientation*/, int role /* = Qt::DisplayRole */) const
{
  if (role == Qt::DisplayRole)
  {
    switch (iColumn)
    {
    case uploaderID:
      return QString("Uploader ID");
    case softwareName:
      return QString("Software Name");
    case softwareVersion:
      return QString("Software Version");
    case gatewayTimestamp:
      return QString("Gateway Timestamp");
    case systemName:
      return QString("System Name");
    case stationName:
      return QString("Station Name");
    case itemName:
      return QString("Item Name");
    case buyPrice:
      return QString("Buy Price");
    case stationStock:
      return QString("Station Stock");
    case supplyLevel:
      return QString("SupplyLevel");
    case sellPrice:
      return QString("Sell Price");
    case demand:
      return QString("Demand");
    case demandLevel:
      return QString("DemandLevel");
    case timestamp:
      return QString("Timestamp");
    default:
      qDebug() << "Unhandled column: " << iColumn;
      return QVariant();
    }
  } 
  else
  {
    return QVariant();
  }
}


model::DirectionalTradeModel_c::DirectionalTradeModel_c(MainWindow * parent, QSharedPointer<evian::DirectionalTradeQuery_c> pcDatabaseQuery) :
  QAbstractTableModel(parent),
  _pcMainWindow(parent),
  _pcDatabaseQuery(pcDatabaseQuery)
{

}

int DirectionalTradeModel_c::rowCount(const QModelIndex &/*parent*/) const
{
  return static_cast<int>(_pcDatabaseQuery->_lcBestTrade.size());
}

void model::DirectionalTradeModel_c::ResultsReady()
{
  beginResetModel();
  endResetModel();
}

QVariant
model::DirectionalTradeModel_c::headerData(int section, Qt::Orientation orientation, int role /* = Qt::DisplayRole */) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal) {
      return DirectionalTradeModel::ColumnName[section];
    }
  }
  return QVariant();
}

QVariant
model::DirectionalTradeModel_c::data(const QModelIndex &index, int role /* = Qt::DisplayRole */) const
{
  using namespace DirectionalTradeModel;

  QLocale
    cQLocale(QLocale::English);

  auto &
    cTrade = _pcDatabaseQuery->_lcBestTrade[index.row()];

  const evian::TradeDatabaseStorage_c&
    cTradeDatabase = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase();

  const auto
    &cStation0 = cTradeDatabase.GetStation(cTrade._nFromStationID),
    &cStation1 = cTradeDatabase.GetStation(cTrade._nToStationID);

  const auto
    &cSystem0 = cTradeDatabase.GetSystem(cTrade._nFromSystemID),
    &cSystem1 = cTradeDatabase.GetSystem(cTrade._nToSystemID);

  if (role == Qt::DisplayRole)
  {
    switch(index.column())
    { 
    case Column_SystemBegin:
      return cSystem0._cName;
    case Column_StationBegin:
      return cStation0._cName;
    case Column_SystemEnd:
      return cSystem1._cName;
    case Column_StationEnd:
      return cStation1._cName;
    case Column_Commodity:
      return cTradeDatabase.GetCommodityName(cTrade._nCommodityID);
    case Column_DistanceBegin:
      return cQLocale.toString(cStation0._nDistanceToStar) + " ls";
    case Column_DistanceEnd:
      return cQLocale.toString(cStation1._nDistanceToStar) + " ls";
    case Column_Profit:
      return QString("%1 Cr").arg(cQLocale.toString(cTrade._nProfit));
    case Column_Distance:
       return QString::number( Dist(cSystem0._cPosition, cSystem1._cPosition), 'f', 2) + " LY";
    default:
      qDebug() << __LINE__ << "Unknown column " << index.column();
    }
  }
  else if (role == SortRole)
  {
    switch (index.column())
    {
    case Column_SystemBegin:
      return cSystem0._cName;
    case Column_StationBegin:
      return cStation0._cName;
    case Column_SystemEnd:
      return cSystem1._cName;
    case Column_StationEnd:
      return cStation1._cName;
    case Column_Commodity:
      return cTradeDatabase.GetCommodityName(cTrade._nCommodityID);
    case Column_DistanceBegin:
      return cStation0._nDistanceToStar;
    case Column_DistanceEnd:
      return cStation1._nDistanceToStar;
    case Column_Profit:
      return cTrade._nProfit;
    case Column_Distance:
      return Dist(cSystem0._cPosition, cSystem1._cPosition);
    default:
      qDebug() << __LINE__ << "Unknown column " << index.column();
    }
  }

  if (role == Qt::TextAlignmentRole)
  {
    auto 
      nColumn = index.column();

    bool
      bIsNumericColumn = 
      nColumn == Column_Profit ||
      nColumn == Column_Distance    ||
      nColumn == Column_DistanceBegin   ||
      nColumn == Column_DistanceEnd;

    if (bIsNumericColumn)
    {
      return Qt::AlignCenter + Qt::AlignVCenter;
    }
    else
    {
      return Qt::AlignLeft + Qt::AlignVCenter;
    }
  }

  if (role == Qt::DecorationRole)
  {
    switch (index.column())
    {
    case Column_SystemBegin:
      return GetIconForSystem(cSystem0);
    case Column_SystemEnd:
      return GetIconForSystem(cSystem1);
    case Column_StationBegin:
      return GetIconForStation(cStation0);
    case Column_StationEnd:
      return GetIconForStation(cStation1);
    default:
      return QVariant();
    }
  }

  if (role == Qt::ToolTipRole)
  {
    QString
      s = FormatSystemTooltip(cSystem0) + "\n";

    s += FormatStationTooltip(cStation0) + "\n";
    s += FormatSystemTooltip(cSystem1) + "\n";
    s += FormatStationTooltip(cStation1);

    return s;
  }

  return QVariant();
}

int
model::DirectionalTradeModel_c::columnCount(const QModelIndex &/*parent  = QModelIndex() */) const
{
  return DirectionalTradeModel::Column_Last;
}

model::CommodityAnalysisModel_c::CommodityAnalysisModel_c(MainWindow * parent) :
  QAbstractTableModel(parent),
  _pcMainWindow(parent)
{
}

void model::CommodityAnalysisModel_c::SetDatabaseQuery(QSharedPointer<evian::CommodityPriceAnalysisQuery_c> pcCommodityPriceAnalysisQuery)
{
  _pcDatabaseQuery = pcCommodityPriceAnalysisQuery;
}

int
CommodityAnalysisModel_c::rowCount(const QModelIndex &/*parent*/) const
{
  if (_pcDatabaseQuery.isNull())
  {
    return 0;
  }

  return static_cast<int>(_pcDatabaseQuery->_lcCommodityPriceAnalysisBuy.size());
}

QVariant
model::CommodityAnalysisModel_c::data(const QModelIndex &index, int role /* = Qt::DisplayRole */) const
{
  using namespace CommodityAnalysisModel;

  const evian::TradeDatabaseStorage_c&
    cTradeDatabase = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase();

  auto
    nCommodityID = _pcDatabaseQuery->_lcCommodityPriceAnalysisBuy[index.row()]._nCommodityID;

  if (nCommodityID == -1)
  {
    return QVariant();
  }

  if (role == Qt::DisplayRole)
  {
    switch(index.column())
    {
      case Column_Commodity:
        return cTradeDatabase.GetCommodityName(nCommodityID);
      case Column_BuyMax:
        return _pcDatabaseQuery->_lcCommodityPriceAnalysisBuy[nCommodityID]._nPriceMax;
      case Column_BuyMin:
        return _pcDatabaseQuery->_lcCommodityPriceAnalysisBuy[nCommodityID]._nPriceMin;
      case Column_BuyAverage:
        return _pcDatabaseQuery->_lcCommodityPriceAnalysisBuy[nCommodityID]._nPriceAverage;
      case Column_BuyMedian:
        return _pcDatabaseQuery->_lcCommodityPriceAnalysisBuy[nCommodityID]._nPriceMedian;
      case Column_BuyNSamples:
        return _pcDatabaseQuery->_lcCommodityPriceAnalysisBuy[nCommodityID]._nSamples;
      case Column_BuySigma:
        return _pcDatabaseQuery->_lcCommodityPriceAnalysisBuy[nCommodityID]._rSigma;
      case Column_SellMax:
        return _pcDatabaseQuery->_lcCommodityPriceAnalysisSell[nCommodityID]._nPriceMax;
      case Column_SellMin:
        return _pcDatabaseQuery->_lcCommodityPriceAnalysisSell[nCommodityID]._nPriceMin;
      case Column_SellAverage:
        return _pcDatabaseQuery->_lcCommodityPriceAnalysisSell[nCommodityID]._nPriceAverage;
      case Column_SellMedian:
        return _pcDatabaseQuery->_lcCommodityPriceAnalysisSell[nCommodityID]._nPriceMedian;
      case Column_SellNSamples:
        return _pcDatabaseQuery->_lcCommodityPriceAnalysisSell[nCommodityID]._nSamples;
      case Column_SellSigma:
        return _pcDatabaseQuery->_lcCommodityPriceAnalysisSell[nCommodityID]._rSigma;
      default:
        qDebug() << "Unknown column " << index.column();
    }
  }
  
  return QVariant();
}

void model::CommodityAnalysisModel_c::ResultsReady()
{
  beginResetModel();
  endResetModel();
}

QVariant
model::CommodityAnalysisModel_c::headerData(int section, Qt::Orientation orientation, int role /* = Qt::DisplayRole */) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal) 
    {
      QString
        cQString = CommodityAnalysisModel::ColumnName[section];
      return cQString;
     }
   }

   return QVariant();
}

int
model::CommodityAnalysisModel_c::columnCount(const QModelIndex &/*parent  = QModelIndex() */) const
{
  return CommodityAnalysisModel::Column_Last;
}


EDDNItemModel_c::EDDNItemModel_c(const evian::TradeDatabaseEngine_c& cTradeDatabaseEngine, QObject * parent) :
  QStandardItemModel(parent),
  _cTradeDatabaseEngine(cTradeDatabaseEngine)
{
  const auto&
    lpcEDDNMessage = _cTradeDatabaseEngine.GetTradeDatabase()._lpcEDDNMessage;

  for (const auto& pcEDDNMessage : lpcEDDNMessage)
  {
    AppendEDDNMessage(pcEDDNMessage);
  }

  setHorizontalHeaderLabels(QStringList() << "Location" << "Timestamp" << "Buy Price" << "Sell Price" << "Demand" << "Supply");
  setColumnCount(6);
}

void model::EDDNItemModel_c::AppendEDDNMessage(QSharedPointer<eddn::EDDNMessage_c> pcEDDNMessage)
{
  if (pcEDDNMessage->GetSchemaVersion() == eddn::SchemaVersion::Commodity_V1)
  {
    auto 
      pcCommodityV1 = pcEDDNMessage.dynamicCast<eddn::CommodityV1_c>();
    EVIAN_ASSERT(!pcCommodityV1.isNull());

    auto
      pcStandardItemMessage = new QStandardItem(QString("%1/%2").arg(pcCommodityV1->_cMessage._cStationName).arg(pcCommodityV1->_cMessage._cSystemName)),
      pcStandardItemHeader  = new QStandardItem(QLatin1Literal("Header")),
      pcStandardItemListing = new QStandardItem(QLatin1Literal("Message"));

    pcStandardItemHeader->appendRow(new QStandardItem(pcCommodityV1->_cHeader._cSoftwareName));
    pcStandardItemHeader->appendRow(new QStandardItem(pcCommodityV1->_cHeader._cSoftwareVersion));
    pcStandardItemHeader->appendRow(new QStandardItem(pcCommodityV1->_cHeader._cUploaderID));
    pcStandardItemHeader->appendRow(new QStandardItem(pcCommodityV1->_cHeader._cQDateTimeTimestamp.toString()));

    pcStandardItemListing->appendRow(QList<QStandardItem*>() << new QStandardItem(pcCommodityV1->_cMessage._cItemName)
                                                             << new QStandardItem(QString::number(pcCommodityV1->_cMessage._nBuyPrice))
                                                             << new QStandardItem(QString::number(pcCommodityV1->_cMessage._nSellPrice))
                                                             << new QStandardItem(QString::number(pcCommodityV1->_cMessage._nDemand))
                                                             << new QStandardItem(QString::number(pcCommodityV1->_cMessage._nStationStock)));
    pcStandardItemMessage->appendRow(pcStandardItemHeader);
    pcStandardItemMessage->appendRow(pcStandardItemListing);
    this->appendRow(pcStandardItemMessage);
  }
  else if (pcEDDNMessage->GetSchemaVersion() == eddn::SchemaVersion::Commodity_V2)
  {
    auto 
      pcCommodityV2 = pcEDDNMessage.dynamicCast<eddn::CommodityV2_c>();
    EVIAN_ASSERT(!pcCommodityV2.isNull());

    auto
      pcStandardItemMessage     = new QStandardItem(QString("%1/%2").arg(pcCommodityV2->_cMessage._cProperties._cStationName).arg(pcCommodityV2->_cMessage._cProperties._cSystemName)),
      pcStandardItemHeader      = new QStandardItem(QLatin1Literal("Header")),
      pcStandardItemListingList = new QStandardItem(QLatin1Literal("Message")),
      pcStandardItemTimeStamp   = new QStandardItem(pcCommodityV2->_cHeader._cQDateTimeTimestamp.toString());

    pcStandardItemHeader->appendRow(new QStandardItem(pcCommodityV2->_cHeader._cSoftwareName));
    pcStandardItemHeader->appendRow(new QStandardItem(pcCommodityV2->_cHeader._cSoftwareVersion));
    pcStandardItemHeader->appendRow(new QStandardItem(pcCommodityV2->_cHeader._cUploaderID));
    pcStandardItemHeader->appendRow(new QStandardItem(pcCommodityV2->_cHeader._cQDateTimeTimestamp.toString()));

    QLocale
      cQLocale(QLocale::English);

    for (const auto& cItem : pcCommodityV2->_cMessage._lcItem)
    {
      QList<QStandardItem*> 
        lpcQStandardItem;

      lpcQStandardItem << new QStandardItem(cItem._cName)
             << new QStandardItem("")
             << new QStandardItem(cQLocale.toString(cItem._nBuyPrice))
             << new QStandardItem(cQLocale.toString(cItem._nSellPrice))
             << new QStandardItem(cQLocale.toString(cItem._nDemand))
             << new QStandardItem(cQLocale.toString(cItem._nSupply));

      for (int i = 2; i < lpcQStandardItem.size()-1; ++i)
      {
        lpcQStandardItem[i]->setData(Qt::AlignCenter, Qt::TextAlignmentRole);
      }

      pcStandardItemListingList->appendRow(lpcQStandardItem);
    }
    
    pcStandardItemMessage->appendRow(pcStandardItemListingList);
    pcStandardItemMessage->appendRow(pcStandardItemHeader);

    this->appendRow(QList<QStandardItem*>() << pcStandardItemMessage << pcStandardItemTimeStamp);
  }
  else
  {
    EVIAN_ASSERT(false);
  }
}

void
model::EDDNItemModel_c::MessagesAdded(int iRowHint, int nMessages)
{
  const auto&
    lpcEDDNMessage = _cTradeDatabaseEngine.GetTradeDatabase()._lpcEDDNMessage;
  
  for (auto i = iRowHint; i < iRowHint + nMessages; ++i)
  {
    AppendEDDNMessage(lpcEDDNMessage[i]);
  }
}


CommodityMaxSellPriceModel_c::CommodityMaxSellPriceModel_c(MainWindow * pcMainWindow, QObject * parent) :
  QAbstractTableModel(parent),
  _pcMainWindow(pcMainWindow),
  _nRow(25)
{
}

void
CommodityMaxSellPriceModel_c::SetListingList(QList<std::reference_wrapper<const evian::Listing_c>>& lcListing)
{
  _lcListing = lcListing;
}

void
CommodityMaxSellPriceModel_c::SetRowCount(int nRow)
{
  _nRow = nRow;
}

void
model::CommodityMaxSellPriceModel_c::SetDatabaseQuery(QSharedPointer<evian::CommodityMinMaxAnalysisQuery_c> pcDatabaseQuery)
{
  _pcDatabaseQuery = pcDatabaseQuery;
}

int
CommodityMaxSellPriceModel_c::rowCount(const QModelIndex & /*parent*/) const 
{
  return std::min(_nRow, _lcListing.size());
}

int
CommodityMaxSellPriceModel_c::columnCount(const QModelIndex & /*parent*/) const 
{
  return CommodityMaxSellPriceModel::Column_Last;
}

QVariant
model::CommodityMaxSellPriceModel_c::data(const QModelIndex &index, int role /* = Qt::DisplayRole */) const 
{
  using namespace CommodityMaxSellPriceModel;
  
  const evian::Listing_c&
    cListing = _lcListing[index.row()];

  const auto&
    cStation = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetStation(cListing._nStationID);

  const auto&
    cSystem = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetSystem(cStation._nSystemID);

  const auto
    cCommodityAnalysis = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase()._lcCommodityPriceAnalysisSell[cListing._nCommodityID];

  bool
    isPermitNeeded = cSystem._bNeedPermit;

  QLocale
    cQLocale(QLocale::English);

  QString
    sDistFromCurrent("-");
  if (_pcDatabaseQuery->GetSystemID() != -1)
  {
    const auto&
      cCurrentSystem = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetSystem(_pcDatabaseQuery->GetSystemID());
  
    double
      dist = Dist(cCurrentSystem._cPosition, cSystem._cPosition);

    sDistFromCurrent = QString("%1 LY").arg(dist, 4, 'f', 1);
  }

  if (role == Qt::DisplayRole)
  {
    switch (index.column())
    {
      case Column_Station:
        return cStation._cName;
      case Column_System:
        return cSystem._cName;
      case Column_SellPrice:
        return QString("%1 Cr").arg(cListing._nSellPrice);
      case Column_Demand:
        return cQLocale.toString(cListing._nDemand);
      case Column_Compare:
        return QString("%1 %").arg(((static_cast<double>(cListing._nSellPrice) / cCommodityAnalysis._nPriceAverage) * 100) - 100, 5, 'f', 1);
      case Column_Pad:
        return MainWindow::LandingPadToString(cStation._eMaxLandingPadSize);
      case Column_TimeSinceUpdate:
        return FormatDurationSinceNow(QDateTime::fromTime_t(cListing._nUpdated));
      case Column_IsPermitNeeded:
        return isPermitNeeded ? QString("Yes") : QString("No");
      case Column_Dist:
        return sDistFromCurrent;
      default:
        return QVariant();
    }
  }

  if (role == Qt::TextAlignmentRole)
  {
    auto 
      nColumn = index.column();

    switch (nColumn)
    {
      case Column_Station:
      case Column_System:
        return Qt::AlignLeft + Qt::AlignVCenter;
      case Column_Pad:
      case Column_TimeSinceUpdate:
      case Column_Dist:
      case Column_IsPermitNeeded:
      case Column_SellPrice:
      case Column_Demand:
      case Column_Compare:
        return Qt::AlignHCenter + Qt::AlignVCenter;
      default:
        return QVariant();
    }
  }

  if (role == Qt::DecorationRole)
  {
    switch (index.column())
    {
    case Column_Station:
      return GetIconForStation(cStation);
    case Column_System:
      return GetFactionIcon(cSystem);
    default:
      return QVariant();
    }
  }

  if (role == Qt::ToolTipRole)
  {
    return QString("I am a tooltip");
  }

  return QVariant();
}

QVariant
CommodityMaxSellPriceModel_c::headerData(int section, Qt::Orientation orientation, int role /* = Qt::DisplayRole */) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal) {
      return CommodityMaxSellPriceModel::ColumnName[section];
    }
  }
  return QVariant();
}

void
CommodityMaxSellPriceModel_c::ResultsReady()
{
  beginResetModel();
  _lcListing = _pcDatabaseQuery->_lcListingMaxSell;
  endResetModel();
}

CommodityMinBuyPriceModel_c::CommodityMinBuyPriceModel_c(MainWindow * pcMainWindow, QObject * parent) :
  QAbstractTableModel(parent),
  _pcMainWindow(pcMainWindow),
  _nRow(25)
{
}

void 
model::CommodityMinBuyPriceModel_c::ResultsReady()
{
  beginResetModel();
  _lcListing = _pcDatabaseQuery->_lcListingMinBuy;
  endResetModel();
}

QVariant
CommodityMinBuyPriceModel_c::headerData(int section, Qt::Orientation orientation, int role /* = Qt::DisplayRole */) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal) {
      return CommodityMinBuyPriceModel::ColumnName[section];
    }
  }
  return QVariant();
}

QVariant
CommodityMinBuyPriceModel_c::data(const QModelIndex &index, int role /* = Qt::DisplayRole */) const 
{
  using namespace CommodityMinBuyPriceModel;

  const evian::Listing_c&
    cListing = _lcListing[index.row()];

  const QString
    sCommodity = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetCommodityName(cListing._nCommodityID);

  const auto&
    cStation = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetStation(cListing._nStationID);

  const auto&
    cSystem = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetSystem(cStation._nSystemID);

  const auto
    cCommodityAnalysis = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase()._lcCommodityPriceAnalysisBuy[cListing._nCommodityID];

  QString
    sDistFromCurrent("-");

  if (_pcDatabaseQuery->GetSystemID() != -1)
  {
    const auto&
      cCurrentSystem = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetSystem(_pcDatabaseQuery->GetSystemID());

    double
      dist = Dist(cCurrentSystem._cPosition, cSystem._cPosition);

    sDistFromCurrent = QString("%1 LY").arg(dist, 4, 'f', 1);
  }

  QLocale
    cQLocale(QLocale::English);
  
  bool
    isIllicit = false,
    isPermitNeeded = cSystem._bNeedPermit;

  if (std::find(cStation._lcProhibitedCommodities.begin(), cStation._lcProhibitedCommodities.end(),sCommodity) != cStation._lcProhibitedCommodities.end())
  {
    isIllicit = true;
  } 

  if (role == Qt::DisplayRole)
  {
    switch (index.column())
    {
    case Column_Station:
      return cStation._cName;
    case Column_System:
      return cSystem._cName;
    case Column_BuyPrice:
      return QString("%1 Cr").arg(cListing._nBuyPrice);
    case Column_Supply:
      return cQLocale.toString(cListing._nSupply);
    case Column_Compare:
      return QString("%1 %").arg(((static_cast<double>(cListing._nBuyPrice) / cCommodityAnalysis._nPriceAverage) * 100) - 100, 5, 'f', 1);
    case Column_Pad:
      return MainWindow::LandingPadToString(cStation._eMaxLandingPadSize);
    case Column_TimeSinceUpdate:
      return FormatDurationSinceNow(QDateTime::fromTime_t(cListing._nUpdated));
    case Column_Dist:
      return sDistFromCurrent;
    case Column_IsPermitNeeded:
      return isPermitNeeded ? QString("Yes") : QString("No");
    case Column_Illicit:
      return isIllicit ? QString("Yes") : QString("No");
    default:
      return QVariant();
    }
  }

  if (role == Qt::TextAlignmentRole)
  {
    auto 
      nColumn = index.column();

    switch (nColumn)
    {
    case Column_Pad:
    case Column_TimeSinceUpdate:
    case Column_Dist:
    case Column_Illicit:
    case Column_IsPermitNeeded:
      return Qt::AlignHCenter + Qt::AlignVCenter;
    case Column_BuyPrice:
    case Column_Supply:
    case Column_Compare:
      return Qt::AlignRight + Qt::AlignVCenter;
    default:
      return QVariant();
    }
  }

  if (role == Qt::DecorationRole)
  {
    switch (index.column())
    {
    case Column_Station:
      return GetIconForStation(cStation);
    case Column_System:
      return GetPowerIcon(cSystem);
    default:
      return QVariant();
    }
  }

  if (role == Qt::ForegroundRole)
  {
    if (index.column() == Column_Illicit && isIllicit)
    {
      return QBrush(QColor(255, 113, 0));
    }
  }

  return QVariant();
}

int model::CommodityMinBuyPriceModel_c::columnCount(const QModelIndex & /*parent*/) const 
{
  return CommodityMinBuyPriceModel::Column_Last;
}

int model::CommodityMinBuyPriceModel_c::rowCount(const QModelIndex & /*parent*/) const 
{
  return std::min(_nRow, _lcListing.size());
}

void model::CommodityMinBuyPriceModel_c::SetDatabaseQuery(QSharedPointer<evian::CommodityMinMaxAnalysisQuery_c> pcDatabaseQuery)
{
  _pcDatabaseQuery = pcDatabaseQuery;
}

void model::CommodityMinBuyPriceModel_c::SetRowCount(int nRow)
{
  _nRow = nRow;
}

void model::CommodityMinBuyPriceModel_c::SetListingList(QList<std::reference_wrapper<const evian::Listing_c>>& lcListing)
{
  _lcListing = lcListing;
}

QString
FormatStationTooltip(const evian::Station_c& cStation)
{
  QString
    s = QString("%1 Dist: %2 ls ").arg(cStation._cName).arg(cStation._nDistanceToStar);
  
  if (cStation._isBlackMarket)
  {
    s += "Blackmarket ";
  }

  if (cStation._isRefuel)
  {
    s += "Refuel ";
  }

  if (cStation._isRepair)
  {
    s += "Repair ";
  }

  if (cStation._isShipyard)
  {
    s += "Shipyard ";
  }

  return s;
}

QString
FormatSystemTooltip(const evian::System_c& cSystem)
{
  QString
    s = QString("%1 ").arg(cSystem._cName);

  if (!cSystem._sAllegiance.isEmpty())
  {
    s += cSystem._sAllegiance + " ";
  }

  if (!cSystem._sPowerControlFaction.isEmpty())
  {
    s += cSystem._sPowerControlFaction + " ";
  }

  for (const auto& cExploitedBy : cSystem._lcExploitedBy)
  {
    s += QString("Exploited by: %1 ").arg(cExploitedBy._sPowerControlFaction);
  }

  if (cSystem._bNeedPermit)
  {
    s += "PERMIT REQUIRED ";
  }

  return s;
}


EDDNHotspotModel_c::EDDNHotspotModel_c(const evian::TradeDatabaseEngine_c& cTradeDatabaseEngine, QObject * parent) :
  QStandardItemModel(parent),
  _cTradeDatabaseEngine(cTradeDatabaseEngine),
  _nEDDNMessages(0),
  _qDateTimeMessageFirst(QDate(3300, 1, 1)),
  _qDateTimeMessageLast(QDate(2014, 12, 16))
{
  const auto&
    lpcEDDNMessage = _cTradeDatabaseEngine.GetTradeDatabase()._lpcEDDNMessage;

  for (const auto& pcEDDNMessage : lpcEDDNMessage)
  {
    AppendEDDNMessage(pcEDDNMessage);
  }

  setHorizontalHeaderLabels(QStringList() << "Location" << "# Updates" << "Last timestamp" << "Last CMDR");
}



void
EDDNHotspotModel_c::MessagesAdded(int iRowHint, int nMessages)
{
  const auto&
    lpcEDDNMessage = _cTradeDatabaseEngine.GetTradeDatabase()._lpcEDDNMessage;
  
  for (auto i = iRowHint; i < iRowHint + nMessages; ++i)
  {
    AppendEDDNMessage(lpcEDDNMessage[i]);
  }
}

void
EDDNHotspotModel_c::AppendEDDNMessage(QSharedPointer<eddn::EDDNMessage_c> pcEDDNMessage)
{
  if (pcEDDNMessage->GetSchemaVersion() == eddn::SchemaVersion::Commodity_V2)
  {
    _nEDDNMessages++;

    auto 
      pcCommodityV2 = pcEDDNMessage.dynamicCast<eddn::CommodityV2_c>();
    EVIAN_ASSERT(!pcCommodityV2.isNull());

    auto
      sLocation = QString("%1/%2").arg(pcCommodityV2->_cMessage._cProperties._cStationName).arg(pcCommodityV2->_cMessage._cProperties._cSystemName);

    _qDateTimeMessageFirst = std::min(_qDateTimeMessageFirst, pcCommodityV2->_cHeader._cQDateTimeTimestamp);
    _qDateTimeMessageLast  = std::max(_qDateTimeMessageLast , pcCommodityV2->_cHeader._cQDateTimeTimestamp);

    auto
      lItems = findItems(sLocation, 0);

    QStandardItem
      *pqStandardItemLocation      = 0,
      *pqStandardItemNumUpdates    = 0,
      *pqStandardItemLastTimestamp = 0,
      *pqStandardItemLastCMDR      = 0;

    if (lItems.size() > 0)
    {
      EVIAN_ASSERT(lItems.size() == 1);

      pqStandardItemLocation = lItems[0];

      auto
        nIndex = indexFromItem(pqStandardItemLocation);

      pqStandardItemNumUpdates    = item(nIndex.row(), 1);
      pqStandardItemLastTimestamp = item(nIndex.row(), 2);
      pqStandardItemLastCMDR      = item(nIndex.row(), 3);
    }
    else
    {
      pqStandardItemLocation      = new QStandardItem(sLocation);
      pqStandardItemNumUpdates    = new QStandardItem();
      pqStandardItemLastTimestamp = new QStandardItem();
      pqStandardItemLastCMDR      = new QStandardItem();
      appendRow(QList<QStandardItem*>() << pqStandardItemLocation << pqStandardItemNumUpdates << pqStandardItemLastTimestamp << pqStandardItemLastCMDR);
    }

    auto
      nUpdates = pqStandardItemNumUpdates->data(Qt::DisplayRole).toInt() + 1;

    pqStandardItemNumUpdates->setData(nUpdates, Qt::DisplayRole);

    pqStandardItemLastTimestamp->setText(pcCommodityV2->_cHeader._cQDateTimeTimestamp.toString());
    pqStandardItemLastCMDR->setText(QString("CMDR %1").arg(pcCommodityV2->_cHeader._cUploaderID));
  }
}

EVIAN_END_NAMESPACE(model)