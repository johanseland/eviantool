//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef EVIANDATABASEEXPLORERQT_HPP
#define EVIANDATABASEEXPLORERQT_HPP

#include <QDockWidget>
#include "evian.hpp"
#include "querywidget.hpp"

namespace Ui {
class EvianDatabaseExplorerQt;
}

namespace model {
  class CommodityMaxSellPriceModel_c;
  class CommodityMinBuyPriceModel_c;
}

class MainWindow;
class StationsTableModel_c;

class EvianDatabaseExplorerQt : public QWidget
{
  Q_OBJECT
  enum class WidgetMode_e { Table, Tree };

public:
  explicit EvianDatabaseExplorerQt(MainWindow *parent = 0);
  ~EvianDatabaseExplorerQt();

  void static   SetHeaderParameters(QTableView* pcQTableView, int columnCount);
  void static   SetHeaderParameters(QTreeView* pcQTreView, int columnCount);

private slots:
  void          ConsiderUpdateFilterRegexp(const QString & cQString);
  void          UpdateActiveCommodity();
  void          PriceListStationUpdatedLineEdit();

  void          on_comboBox_activated(const QString & arg1);
  void          on_toolButtonCurrent_clicked();
  void          on_pushButton_clicked();
  void          on_lineEditStationSelector_editingFinished();
  void          on_spinBoxCommodityRange_editingFinished();
  void          on_lineEditCommoditySystem_editingFinished();
  void          on_comboBoxCommodityBuyOrSell_activated(const QString &arg1);
  void          on_comboBoxCommodityMinLandingPad_currentIndexChanged(int index);
  void          on_comboBoxSystemSubsetSelector_currentIndexChanged(const QString &arg1);

private:
  void          PriceListStationUpdated(int nStationID);
  void          CrateProhibitedCommoditiesTags(const  evian::Station_c &cStation);
  

  void          InitCompleter();
  QString       DeduceActiveCommoditySystem();
  
  static void   CreateSystemTags(const evian::System_c& cSystem, QGroupBox * pcQGroupBox);
  static void   CreateStationTags(const  evian::Station_c &cStation, QGroupBox * pcQGroupBox);
  static void   CreateTag(QString sDisplay, QString sTagName, QGroupBox* pcQGroupBox);
  static void   CreateTag(QIcon cQIcon, QString sDisplay, QString sTagName, QGroupBox* pcQGroupBox);
  void          PopulateSystemSubsetSelector();
  Ui::EvianDatabaseExplorerQt *
                ui;
  MainWindow *
                _pcMainWindow;

  QAbstractItemModel 
                *_pcAbstractItemModel;

  model::CommodityMaxSellPriceModel_c
                *_pcCommodityMaxSellPriceModel;

  model::CommodityMinBuyPriceModel_c
                *_pcCommodityMinBuyPriceModel;

  QSortFilterProxyModel
                *_pcQSortFilterProxyModel;

  WidgetMode_e  _eWidgetMode;
  QStringList   _lcQStringSystemsAndStations;
  QCompleter    *_pcQCompleter;

  CompleterListCache_c
                _cCompleterListCache;
};

class AcceleratedFilterProxyModel : public QSortFilterProxyModel
{
  Q_OBJECT
public:
  AcceleratedFilterProxyModel(MainWindow * pcMainWindow, QObject * parent) :
    QSortFilterProxyModel(parent),
    _pcMainWindow(pcMainWindow)
  {}

  bool filterAcceptsRow(int sourceRow, const QModelIndex & sourceParent) const override;
  void SetFilterHash(const evian::EvianFilterHash& cEvianFilterHash);
  void SetFilterString(const QString&);
  QString GetFilterString() const;

private:
  MainWindow 
                *_pcMainWindow;

  QString       _cQStringFilter;

  evian::EvianFilterHash
                _cEvianFilterHash;

  std::vector<evian::Station_c>
                _lcStation;

  std::vector<evian::System_c>
                _lcSystem;
};

#endif // EVIANDATABASEEXPLORERQT_HPP
