//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#pragma once

#include <string>
#include <cstdlib>
#include <map>
#include <algorithm>
#include <iostream>
#include <cassert>
#include <bitset>
#include <QString>
#include <QMetaType>
#include "evianutil.hpp"

#define EVIAN_BEGIN_NAMESPACE(x) namespace x {
#define EVIAN_END_NAMESPACE(x) } 


namespace {
	static const char
  key_commoditiesurl[]                  = "evian/eddb/v4/commodities/url",
  key_stationsurl[]                     = "evian/eddb/v4/stations/url",
  key_systemsurl[]                      = "evian/eddb/v4/systems/url",
  key_stationsfile[]                    = "evian/eddb/v4/stations/file",
  key_listingsurl[]                     = "evian/eddb/v4/listings/url",
  key_modulesurl[]                      = "evian/eddb/v4/modules/url",
  key_systemsfile[]                     = "evian/eddb/v4/systems/file",
  key_commoditiesfile[]                 = "evian/eddb/v4/commodities/file",
  key_listingsfile[]                    = "evian/eddb/v4/listings/file",
  key_modulesfile[]                     = "evian/eddb/v4/modules/file",
  key_datadirectory[]                   = "evian/datadirectory",
  key_eddb_last_update[]                = "evian/eddb/lastupdate",
  key_fd_log_directory[]                = "evian/fdlogdirectory",
  key_database_file[]                   = "evian/database/deserialized_file",
  key_database_listings_file[]          = "evian/database/listings_file",
  key_database_data_validation_mode[]   = "evian/database/data_validation_mode",
  key_concurrency_mode[]                = "evian/concurrency_mode",
  key_synchronize_db_on_startup[]       = "evian/default_synchronize_on_startup",
  key_edlog_max_logfiles[]              = "evian/edlog/max_logfiles",
  key_dbexplorer_min_filter_length[]	  = "evian/dbexplorer/min_filter_length",
  key_eddn_relay[]                      = "evian/eddn/relay",
  key_eddn_timeout_ms[]                 = "evian/eddn/timeout_ms",
  key_eddn_reconnect_s[]                = "evian/eddn/reconnect_s",
  key_database_sleeptime_ms[]           = "evian/database/sleeptime_ms",
  key_database_filter_older_than_s[]    = "evian/database/filter_older_than_s",
  key_database_save_on_exit[]           = "evian/database/save_on_exit",
  key_logfile[]                         = "evian/logfile",
  key_appconfigxmlfile[]                = "evian/appconfigxml/file",
  key_auto_update_appconfigxml[]        = "evian/appconfigxml/autoupdate",
  key_eddn_on_dynamodb_url[]            = "evian/eddn_on_dynamodb_url",
  key_eddn_on_dynamodb_last_update[]    = "evian/eddn_on_dynamodb/last_update",
  key_query_system_distance[]           = "evian/query/system_distance_ly",
  key_query_star_distance[]             = "evian/query/star_distance_ls",
  key_query_min_landing_pad[]           = "evian/query/min_landing_pad",
  key_query_max_landing_pad[]           = "evian/query/max_landing_pad",
  key_query_min_supply[]                = "evian/query/min_supply",
  key_query_min_profit[]                = "evian/query/min_profit",
  key_query_ignore_planetary_outposts[] = "evian/query/ignore_planetary_outposts";
    }


template<class T>
struct Vec3 {
  union
  {
    struct
    {
      T x,
        y ,
        z;
    };
    T data[3];
  };

  Vec3() :
    x(0),
    y(0),
    z(0)
  {
  }

  Vec3(T cT) :
    x(cT),
    y(cT),
    z(cT)
  {}

  Vec3(T x, T y, T z) :
    x(x),
    y(y),
    z(z)
  {}

  Vec3<int> Floor() const
  {
    return Vec3<int>(static_cast<int>(floor(x)),
                     static_cast<int>(floor(y)),
                     static_cast<int>(floor(z)));
  }

  Vec3<int> Ceil() const
  {
    return Vec3<int>(static_cast<int>(ceil(x)),
                     static_cast<int>(ceil(y)),
                     static_cast<int>(ceil(z)));
  }
  
  bool operator==(const Vec3<T>& lhs) const
  {
    return (x == lhs.x &&
            y == lhs.y &&
            z == lhs.z);
  }
};

template<class T>
Vec3<T> MinElement(const Vec3<T>& v0, const Vec3<T>& v1)
{
  using std::min;
  return Vec3<T>(min(v0.x, v1.x), min(v0.y, v1.y), min(v0.z, v1.z));
}

template<class T>
Vec3<T> MaxElement(const Vec3<T>& v0, const Vec3<T>& v1)
{
  using std::max;
  return Vec3<T>(max(v0.x, v1.x), max(v0.y, v1.y), max(v0.z, v1.z));
}

template<class T>
double Dist(const Vec3<T>& v0, const Vec3<T>& v1)
{
  auto
    cDiff = v0 - v1;

  return sqrt(cDiff.x * cDiff.x + cDiff.y * cDiff.y + cDiff.z * cDiff.z);
}

template<>
double Dist(const Vec3<float>& v0, const Vec3<float>& v1);

template<class T>
Vec3<T> operator-(const Vec3<T>& v0, const Vec3<T>& v1) {
  return Vec3<T>(v0.x - v1.x, 
                 v0.y - v1.y,
                 v0.z - v1.z);
}

template<class T>
Vec3<T> operator+(const Vec3<T>& v0, const Vec3<T>& v1) {
  return Vec3<T>(v0.x + v1.x,
                 v0.y + v1.y,
                 v0.z + v1.z);
}

template<class T, class U>
Vec3<T> operator/(const Vec3<T>& v, const Vec3<U>& u) {
  return Vec3<T>(v.x / u.x,
                 v.y / u.y,
                 v.z / u.z);
}


template<class T, class U>
Vec3<T> operator/(const Vec3<T>& v, const U u) {
  return Vec3<T>(v.x / u, v.y / u, v.z / u);  
}

template<class T, class U>
Vec3<T> operator*(const Vec3<T>& v, const U u) {
  return Vec3<T>(v.x * u, v.y * u, v.z * u);
}

template<class T>
std::wstring 
ToString(const Vec3<T>& cVec)
{
  std::wstringstream
    ss;
  ss << "(" << cVec.x << ", " << cVec.y << "," << cVec.z << ")";

  return ss.str();
}

typedef Vec3<float> Vec3f;
typedef Vec3<double> Vec3d;
typedef Vec3<int32_t> Int3;
typedef Vec3<int64_t> Int64_3;

struct BoundingBox_c
{
  Vec3f         _cMin,
                _cMax;
  
  BoundingBox_c() :
    _cMin(std::numeric_limits<float>::max()),
    _cMax(std::numeric_limits<float>::min())
  {}
  
  BoundingBox_c(const Vec3f& cMin, const Vec3f& cMax) :
    _cMin(cMin),
    _cMax(cMax)
  {
  }


  inline void Expand(const Vec3f& v)
  {
    _cMin = MinElement(_cMin, v);
    _cMax = MaxElement(_cMax, v);
  }
    
};

std::bitset<37>
ComputeStringHash(const QString& cQString);

namespace evian {

typedef std::bitset<37> EvianFilterHash;

struct System_c;
struct Station_c;
class DatabaseQuery_c;
class TradeDatabaseStorage_c;

template <typename Iterator, typename Value>
Iterator find_sorted(Iterator begin, Iterator end, Value value)
{
  // skip all smaller values
  while (begin != end && *begin < value)
    ++begin;
  // hit ?
  if (begin != end && *begin == value)
    return begin;
  else
    return end;
}


enum ConcurrencyMode {
  Concurrency_Automatic,
  Concurrency_ForceMultiThreading,
  Concurrency_ForceSingleThreading
};

struct DataBaseItem_c {
  int64_t       _nID;

  DataBaseItem_c() :
    _nID(-1)
    {
    }
};

inline bool operator<(const DataBaseItem_c& c1, const DataBaseItem_c& c2)
{
  return c1._nID < c2._nID;
}


struct CommodityCatergory_c : public DataBaseItem_c
{
  QString         _cName;

  static void WriteToJson(const CommodityCatergory_c& cCommodity, QJsonObject& cQJsonObject);
  static CommodityCatergory_c ReadFromJson(const QJsonObject& cQJsonObject);

};


struct Commodity_c : public DataBaseItem_c
{
  int64_t        _nCategoryID;
  int            _nAveragePrice;
  QString        _cName;

  Commodity_c() :
    _nCategoryID(-1),
    _nAveragePrice(-1)
    {
    }

  static void WriteToJson(const Commodity_c& cCommodity, QJsonObject& cQJsonObject);
  static Commodity_c ReadFromJson(const QJsonObject& cQJsonObject);
};

// {"id":1,"name":"1 G. Caeli","x":80.90625,"y":-83.53125,"z":-30.8125,"faction":"","population":null,"government":null,"allegiance":"Empire",
//   "state":null,"security":null,"primary_economy":null,"needs_permit":0,"updated_at":1424899435}

inline bool operator<(const CommodityCatergory_c& c1, const CommodityCatergory_c& c2)
{
  return c1._nID < c2._nID;
}

struct System_c : public DataBaseItem_c
{
  struct ExploitedBy_c
  {
    QString _sPowerControlFaction;
    QString _sControlSystem;
    int     _nControlSystemID;
    float   _rDistToControlSystem;

    ExploitedBy_c() :
      _nControlSystemID(-1),
      _rDistToControlSystem(0)
      {}
  };

  Vec3f         _cPosition;
  int64_t       _nUpdated,
                _nPopulation;

  QString       _cName,
                _sPowerControlFaction, /// Zemina Torval etc.
                _sAllegiance,          /// ie Empire, Federation, Independent
                _sMinorFaction,
                _sGovernment,
                _sEconomicState,              /// Boom, Civil War etc.
                _sSecurity,
                _sPrimaryEconomy,
                _sSimbad_ref,
                _sPowerState;

  bool          _bNeedPermit;

  EvianFilterHash 
                _cNameHash; /// Used to speed up filtering.

  std::vector<Station_c*> // Filled by SetupBacklinks()
                _lpcStation;
  
  QList<ExploitedBy_c> // Filled by CreateExploitedBy;
                _lcExploitedBy;

  System_c() :
    _nUpdated(-1),
    _nPopulation(-1),
    _bNeedPermit(false)
    {

    }

  static void WriteToJson(const System_c& cSystem, QJsonObject& cQJsonObject);
  static System_c ReadFromJson(const QJsonObject& cQJsonObject);
};

inline bool operator==(const evian::System_c& cSystem0, const System_c& cSystem1)
{
  return cSystem0._nID == cSystem1._nID;
}

// {"id":1, "name" : "Bain Colony", "system_id" : 18370, "max_landing_pad_size" : "L", "distance_to_star" : 16253, "faction" : "", "government" : null, 
// "allegiance" : null, "state" : null, "type" : "Unknown Starport", "has_blackmarket" : 0, "has_commodities" : 1, "has_refuel" : null, "has_repair" : null, 
// "has_rearm" : null, "has_outfitting" : null, "has_shipyard" : null, "import_commodities" : [], "export_commodities" : [], 
// "prohibited_commodities" : [], "economies" : [], "updated_at" : 1429408824}

struct Listing_c : public DataBaseItem_c
{
  int           _nCommodityID,
                _nBuyPrice,
                _nSellPrice,
                _nStationID,
                _nSupply,
                _nDemand,
                _nUpdateCount;

  int64_t       _nUpdated;
  bool          _isEnabled;

  bool operator<(const Listing_c& cListing) const;
  
  static const Listing_c DummyListing;

  bool IsSameStationAndCommodiy(const Listing_c& cListing) const;
  bool IsValid() const;

  Listing_c() :
    _nCommodityID(-1),
    _nBuyPrice(0),
    _nSellPrice (0),
    _nStationID(-1),
    _nSupply(0),
    _nDemand(0),
    _nUpdateCount(0),
    _nUpdated(-1),
    _isEnabled(true)
    {
    }

  static void WriteToJson(const Listing_c& cListing, QJsonObject& cQJsonObject);
};

Q_DECLARE_METATYPE(evian::Listing_c);

struct Station_c : public DataBaseItem_c
{
  enum LandingPadSize { LANDINGPAD_SMALL, LANDINGPAD_MEDIUM, LANDINGPAD_LARGE, LANDINGPAD_UNKNOWN };

  int           _nSystemID,
                _nDistanceToStar;

  int64_t       _nUpdated,
                _nShipyardUpdated;

  QString       _cName,
                _cStarportType,
                _sFaction,
                _sGovernment,
                _sAllegiance,
                _sEconomicState;

  LandingPadSize
                _eMaxLandingPadSize;

  EvianFilterHash
                _cNameHash;

  std::vector<QString>
                _lcProhibitedCommodities,
                _lcEconomies,
                _lcShips;
  
  std::vector<int>
                _lnModules;

  bool          _isBlackMarket,
                _isRefuel,
                _isRepair,
                _isRearm,
                _isOutfitting,
                _isShipyard,
                _isPlanetaryOutpost;
                
  std::vector<Listing_c>
                _lcListing;
  
  static const int
                MAX_COMMODITIES = 384,
                BLOCK_ALIGNMENT = 32,
                SELL_DEFAULT = -1000000, // Large, but far away from overflow.
                BUY_DEFAULT = 1000000,
                SUPPLY_DEFAULT = 0;
  int 
    * __restrict _paBuyPrice,
    * __restrict _paSellPrice,
    * __restrict _paSupply;

  static LandingPadSize 
                LandingPadSizeFromString(const std::string string);
  static bool   IsPlanetaryOutpostFromString(const QString&);

  System_c*
    _pcSystem;
  
  Station_c();
  ~Station_c();
 
  void          CreateScratchArrays();
  void          DisableListing(const Listing_c& cListing);
  void          EnableListing(const Listing_c& cListing);

  int           GetSupplyForCommodity(int nID) const;
  const Listing_c& 
                GetListingForCommodity(int nID) const;

  std::string   PrettyPrintListings(const TradeDatabaseStorage_c& cTradeDatabase) const;
  static void   WriteToJson(const Station_c& cStation, QJsonObject& cQJsonObject);
  static Station_c 
                ReadFromJson(const QJsonObject& cQJsonObject);
  
  static QJsonArray
                WriteToJsonAsArray(const Station_c& cStation);
  static void   ReadListingsJson(Station_c& cStation, QJsonArray& qJsonArray);
  static void   WriteListingsJson(const Station_c& cStation, QJsonObject& qJsonObject);
  static void   DeserializeListings(std::vector<Station_c>& lcStation, const char* paKey, const QJsonObject& qJsonObjectParent);
};

template<class CellElement>
class GridCell_c
{
public:
  std::vector<std::reference_wrapper<const CellElement>>
                _lCellElement;
};

template<class CellElement>
class Grid_c
{
  BoundingBox_c _cBoundingBox;
  float         _rCellSpacing;
  Int3          _cGridDimension;
  
  static GridCell_c<CellElement>
                _cGridCellEmpty;
public:
  std::map<int, GridCell_c<CellElement>>
    _lcGridCell;

  Grid_c() :
    _rCellSpacing(15.0f)
    {

    }
  void SetGridDimension(const Int3& nNumberOfCells)
  {
    EVIAN_ASSERT(nNumberOfCells.x > 0 && nNumberOfCells.y > 0 && nNumberOfCells.z > 0);
    _cGridDimension = nNumberOfCells;
  }

  void SetCellSpacing(float rCellSpacing)
  {
    EVIAN_ASSERT(rCellSpacing > 0.0f);
    _rCellSpacing = rCellSpacing;
    ComputeGridDimension();
  }

  Int3 IJKFromIndex(int nIndex) const
  {
    Int3 
      nInt3(nIndex % _cGridDimension.x,
            (nIndex / _cGridDimension.x) % _cGridDimension.y,
            ((nIndex / _cGridDimension.x) / _cGridDimension.y) % _cGridDimension.z);
    
    return nInt3;
  }

  int IndexFromIJK(int nI, int nJ, int nK) const
  {
    return IndexFromIJK(Int3(nI, nJ, nK));
  }

  bool IsInsideGridIJK(Int3 cIJK) const {
    return cIJK.x < _cGridDimension.x && cIJK.y < _cGridDimension.y && cIJK.z < _cGridDimension.z &&
           cIJK.x >= 0 && cIJK.y >= 0 && cIJK.z >= 0; 
  }

  int IndexFromIJK(const Int3& cIJK) const
  {
    EVIAN_ASSERT(cIJK.x < _cGridDimension.x && cIJK.y < _cGridDimension.y && cIJK.z < _cGridDimension.z);
    EVIAN_ASSERT(cIJK.x >= 0 && cIJK.y >= 0 && cIJK.z >= 0);

    int 
      nIndex = cIJK.x + cIJK.y * _cGridDimension.x + cIJK.z * _cGridDimension.x * _cGridDimension.y;

    return nIndex;
  }

  int IndexFromPosition(const Vec3f& cPos) const
  {
    auto
      nGridIndex = IndexFromIJK(IJKFromPosition(cPos));

    return nGridIndex;
  }

  Int3 IJKFromPosition(const Vec3f& cPos) const
  {
    auto
      cTranslatedPosition = cPos - _cBoundingBox._cMin;

    auto
      cIJK = (cTranslatedPosition / _rCellSpacing).Floor();

    return cIJK;
  }

  void SetBoundingBox(const BoundingBox_c& cBoundingBox)
  {
    _cBoundingBox = cBoundingBox;
    ComputeGridDimension();
  }

  Int3 ComputeGridDimension() {
    auto
      cExtents = (_cBoundingBox._cMax - _cBoundingBox._cMin) / _rCellSpacing;
   
   _cGridDimension = cExtents.Ceil();

    return _cGridDimension;
  }

  void AddToGrid(const CellElement& cCellElement)
  {
    auto 
      nGridIndex = IndexFromPosition(cCellElement._cPosition);

    _lcGridCell[nGridIndex]._lCellElement.push_back(std::cref(cCellElement));
  }

  const GridCell_c<CellElement>& GetGridCell(const Int3& cIJK) const
  {
    bool
      isOutsideGrid = (cIJK.x < 0 ||
                       cIJK.y < 0 ||
                       cIJK.z < 0 ||
                       cIJK.x >= _cGridDimension.x ||
                       cIJK.y >= _cGridDimension.y ||
                       cIJK.z >= _cGridDimension.z);
    if (isOutsideGrid)
    {
      return _cGridCellEmpty;
    }

    auto
      nIndex = IndexFromIJK(cIJK);

    auto it = _lcGridCell.find(nIndex);

    if (it != _lcGridCell.end())
    {
      return it->second;
    }
    else
    {
      return _cGridCellEmpty;
    }
  }
};

template<class CellElement> GridCell_c<CellElement>
  Grid_c<CellElement>::_cGridCellEmpty;

class EvianReadWriteLock_c 
{
public:
  EvianReadWriteLock_c(QString cName) :
    _cName(cName),
    _pcQThreadWrite(0)
  {}

  void LockForRead() 
  {
    EVIAN_ASSERT(!isReadLockedByCurrentThread());

    _cQReadWriteLock.lockForRead();

    QThread*
      pcQThread = QThread::currentThread();

    QMutexLocker
      cQMutexLocker(&_cQMutex);

    _lpcQThreadRead.append(pcQThread);
  }

  void LockForWrite()
  {
    EVIAN_ASSERT(!isReadLockedByCurrentThread() && "Thread is already ready locked by current thread"); // isReadLockedByCurrentThread has mutexes.

    _cQReadWriteLock.lockForWrite();

    QThread*
      pcQThread = QThread::currentThread();

    QMutexLocker
      cQMutexLocker(&_cQMutex); // Unlock() must have finished for us to have the writelock.

    EVIAN_ASSERT(_pcQThreadWrite == 0);
    _pcQThreadWrite = pcQThread;
  }

  bool TryLockForWrite()
  {
    EVIAN_ASSERT(!isReadLockedByCurrentThread() && "Thread is already ready locked by current thread"); // isReadLockedByCurrentThread has mutexes.
    
    bool
      isLocked = _cQReadWriteLock.tryLockForWrite();

    if (isLocked)
    {
      QThread*
        pcQThread = QThread::currentThread();

      QMutexLocker
        cQMutexLocker(&_cQMutex); // Unlock() must have finished.

      EVIAN_ASSERT(_pcQThreadWrite == 0);
      _pcQThreadWrite = pcQThread;
    }

    return isLocked;
  }

  void Unlock()
  {
    QThread*
      pcQThread = QThread::currentThread();

    {
      QMutexLocker
        cQMutexLocker(&_cQMutex);

      EVIAN_ASSERT(pcQThread == _pcQThreadWrite || _lpcQThreadRead.indexOf(pcQThread) != -1 && "Only lockers can unlock");

      if (pcQThread == _pcQThreadWrite)
      {
        _pcQThreadWrite = 0;
      }
      else
      {
        int
          i = _lpcQThreadRead.indexOf(pcQThread);
        _lpcQThreadRead.removeAt(i);
      }
    }
    _cQReadWriteLock.unlock();
  }

  bool IsWriteLocked() const
  {
    // We do not need a mutex to compare two pointer values.
    return _pcQThreadWrite != 0;
  }

  bool IsWriteLockedByCurrentThread() const
  {

    QThread*
      pcQThread = QThread::currentThread();
    
    // We do not need a mutex to compare two pointer values.
    return pcQThread == _pcQThreadWrite;
  }


  bool isReadLockedByCurrentThread() const
  {

    QThread*
      pcQThread = QThread::currentThread();
    
    QMutexLocker
      cQMutexLocker(&_cQMutex);

    return _lpcQThreadRead.indexOf(pcQThread) != -1;
  }

private:
  QReadWriteLock
                _cQReadWriteLock;

  mutable QMutex        
                _cQMutex; // Protects internal datastructures;
  
  QList<QThread*> 
                _lpcQThreadRead;
  QThread*
                _pcQThreadWrite;

  QString       _cName;
};

class EvianReadLocker_c 
{
private:
  QString       _cFileName;
  int           _iLineNum;

  EvianReadWriteLock_c&
                _cEvianReadWriteLock;
public:
  EvianReadLocker_c(QString cFileName, int iLineNum, EvianReadWriteLock_c& cEvianReadWriteLock) :
    _cFileName(cFileName),
    _iLineNum(iLineNum),
    _cEvianReadWriteLock(cEvianReadWriteLock)
  {
    cEvianReadWriteLock.LockForRead();
  }

  ~EvianReadLocker_c()
  {
    _cEvianReadWriteLock.Unlock();
  }
};

} // namespace evian