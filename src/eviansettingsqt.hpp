//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef EVIANSETTINGSQT_HPP
#define EVIANSETTINGSQT_HPP

#include <QDockWidget>
#include <QNetworkAccessManager>
#include <QList>
#include <QNetworkReply>

namespace Ui {
class eviansettingsqt;
}

namespace evian {
  class EDDNOnDynamoDB_c;
}

class MainWindow;

class eviansettingsqt : public QWidget
{
    Q_OBJECT
public:
  explicit eviansettingsqt(MainWindow* pcMainWindow, QWidget *parent = 0);
  ~eviansettingsqt();

  void          UpdateEDDBDatabaseStatusFromSettings();
  void          UpdateDynamoDBStatusFromSettings();
  void          SerializeDatabase();
  QFont         GetWidgetFont();
  static void   ConsiderEnableVerboseLogging(QString sAppconfigFileName);
protected:
  void          mousePressEvent(QMouseEvent * pcQMouseEvent);
  void          mouseMoveEvent(QMouseEvent * pcQMouseEvent);

private:
  Ui::eviansettingsqt *ui;
  
  QNetworkAccessManager
              _cQNetworkAccessManager;

  QMap<QNetworkReply*, QString>
              _mpcQNetworkReply;

  QMap<QNetworkReply*, qint64>
              _mpcQNetworkReplyBytesAlreadyRecieved;

  bool        _isNetworkError;

  void          DownloadURL(QString cURL, QString cFilename);
  void          SetupFilterOlderThan();
  void          SetupEDDNOnDynamoDB();
  void          SetAppconfigXMLStatus();

  QString       DatbaseRebuiltString();
  qint64        _nBytesDownloaded;

  MainWindow *  _pcMainWindow;

  evian::EDDNOnDynamoDB_c
                *_pcEDDNOnDynamoDB;
              
  static bool   IsVerboseLoggingEnabled();
  static bool   IsVerboseLoggingEnabled(QFile& qFile);
private slots:
  void          on_toolButton_clicked();
  void          on_toolButton_2_clicked();
  void          on_toolButton_3_clicked();
  void          on_toolButton_4_clicked();
  void          on_lineEditDatabaseDirectory_editingFinished();
  void          on_lineEditEDDBStationsURL_editingFinished();
  void          on_lineEditEDDBSystemsURL_editingFinished();
  void          on_lineEditEDDBCommoditiesURL_editingFinished();
  void          on_lineEditEDDBCommoditiesFile_editingFinished();
  void          on_lineEditEDDBSystemsFile_editingFinished();
  void          on_lineEditEDDBStationsFile_editingFinished();
  void          on_pushButton_clicked();
  void          on_pushButtonUpdateDatabase_clicked();

  void          DownloadFinishedQNetworkReply();
  void          DownloadFinished(QNetworkReply*);
  void          DownloadError(QNetworkReply::NetworkError);
  void          DownloadProgress(qint64, qint64);

  void          on_comboBoxMultiThreadingMode_activated(const QString &arg1);
  void          on_toolButton_5_clicked();
  void          on_lineEditEDLogDirectory_editingFinished();
  void          on_spinBoxMaxEDLogfiles_editingFinished();
  void          on_pushButtonSave_clicked();
  void          on_pushButtonLoad_clicked();
  void          on_pushButtonEDDBReimport_clicked();
  void          on_comboBoxFilterOlderThan_currentIndexChanged(int index);
  void          on_checkBoxSaveDatabaseOnExit_clicked(bool checked);
  void          on_pushButtonUpdateDynamoDB_clicked();
  void          on_lineEditDynamoDBURL_editingFinished();
  void          on_checkBoxSynchronizeDBOnStartup_clicked(bool checked);
  void          on_lineEditEDDBListingsFile_editingFinished();
  void          on_lineEditEDDBModulesFile_editingFinished();
  void          on_lineEditEDDBListingsURL_editingFinished();
  void          on_lineEditEDDBModulesURL_editingFinished();
  void          on_toolButton_6_clicked();
  void          on_toolButton_10_clicked();
  void          on_pushButtonUpdateAppconfigXML_clicked();
  void          on_comboBoxDataValidationMode_activated(int index);
  void          on_lineEditAppconfigXMLLocation_editingFinished();
  void          on_checkBoxAppconfigXMLAutomaticUpdate_clicked(bool checked);
  void          on_lineEditSerializationListingsFilename_editingFinished();

public slots:
  void          EDDBDatabaseStatusUpdated(QString);
  void          DatabaseUpdated();

signals:
  void          EDDBDatabaseDownloadComplete();
  void          ImportEDDBFromDisk();
  void          FilterOlderThanUpdated(int);

private:
  QPoint        _cQPointOldPos;
};

#endif // EVIANSETTINGSQT_HPP
