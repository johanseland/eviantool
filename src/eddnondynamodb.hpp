#pragma once

#include <QObject>
#include "evian.hpp"

class QNetworkAccessManager;

EVIAN_BEGIN_NAMESPACE(evian)

class EDDNOnDynamoDB_c : public QObject
{
  Q_OBJECT
public:
  explicit EDDNOnDynamoDB_c(QNetworkAccessManager* pcQNetworkAccessManager, QObject *parent = 0);

signals:
  void EDDNMessage(QJsonDocument);
  void UIMessage(QString);

public slots:
  void DownloadDate(QDateTime, qint64 iFromOffset);

private slots:
  void QNetworkReplyFinished();
  void QNetworkReplyError(QNetworkReply::NetworkError eNetworkError);
  void RetryDownloadURL(QString sUrl);
  void DownloadURL(QString sUrl);

private:
  QNetworkAccessManager*
                _pcQNetworkAccessManager;
  qint64        _nDownloadInterval;
  int           _nDownloadErrors;
  const int     _nMaxDownloadErrors,
                _nMinDownloadInterval;
};

EVIAN_END_NAMESPACE(evian)
