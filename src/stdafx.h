//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#define NOMINMAX
#define WINDOWS_LEAN_AND_MEAN
//#define _ITERATOR_DEBUG_LEVEL 0
#define _SECURE_SCL 0
#define _SECURE_SCL_THROWS 0

#include "targetver.h"


#include <atomic>
#include <algorithm>
#include <array>
#include <cassert>
#include <cstdio>
#include <iomanip>
#include <iostream>
#include <map>
#include <sstream>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <sys/stat.h>
#include <tchar.h>
#include <thread>
#include <time.h>
#include <type_traits>
#include <unordered_map>
#include <vector>


#if defined(EVIAN_HAVE_ZMQ)
#include <zmq.h>
#include <3rdparty\zmq.hpp>
#include <3rdparty\zlib.h>
#endif

#include <windows.h>
#include <ppl.h>

#if defined(EVIAN_HAVE_QT)
#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include <QtNetwork>
#include <QtConcurrent>
#include <QtAlgorithms>
#include <QtXml>
#endif

#ifdef EVIAN_HAVE_WINSPARKLE
#include <winsparkle.h>
#endif

//#include "advisor-annotate.h"