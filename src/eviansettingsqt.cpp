//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "stdafx.h"
#include "evian.hpp"
#include  "eviansettingsqt.hpp"
#include "mainwindow.hpp"
#include "ednetlogparserqt.hpp"
#include "tradedetailwidget.hpp"
#include "evianqt.hpp"
#include "ui_eviansettingsqt.h"
#include "eddnondynamodb.hpp"

eviansettingsqt::eviansettingsqt(MainWindow * pcMainWindow, QWidget *parent) :
  QWidget(parent),
  _pcMainWindow(pcMainWindow),
  ui(new Ui::eviansettingsqt),
  _pcEDDNOnDynamoDB(new evian::EDDNOnDynamoDB_c(&_cQNetworkAccessManager, this))
{
  setWindowFlags(Qt::Window | Qt::FramelessWindowHint | Qt::Dialog);
  setAttribute(Qt::WA_TranslucentBackground);
  ui->setupUi(this);

  dynamic_cast<QBoxLayout*>(ui->widgetSizeGrip->layout())->addWidget(new QSizeGrip(this), 0, Qt::AlignBottom | Qt::AlignRight);

  ui->progressBarDatabaseUpdateStatus->setHidden(true);

  QSettings
    qSettings;

  ui->lineEditEDDBCommoditiesURL->setText(qSettings.value(key_commoditiesurl, "https://eddb.io/archive/v4/commodities.json").toString());
  ui->lineEditEDDBStationsURL->setText(qSettings.value(key_stationsurl, "https://eddb.io/archive/v4/stations.json").toString());
  ui->lineEditEDDBSystemsURL->setText(qSettings.value(key_systemsurl, "https://eddb.io/archive/v4/systems.json").toString());
  ui->lineEditEDDBListingsURL->setText(qSettings.value(key_listingsurl, "https://eddb.io/archive/v4/listings.csv").toString());
  ui->lineEditEDDBModulesURL->setText(qSettings.value(key_modulesurl, "https://eddb.io/archive/v4/modules.json").toString());
  ui->comboBoxDataValidationMode->setCurrentIndex(qSettings.value(key_database_data_validation_mode, 0).toInt());
  ui->checkBoxAppconfigXMLAutomaticUpdate->setChecked(qSettings.value(key_auto_update_appconfigxml).toBool());

  auto
    sDatabaseDirectory = qSettings.value(key_datadirectory).toString();

  ui->lineEditDatabaseDirectory->setText(sDatabaseDirectory);

  ui->lineEditEDDBStationsFile->setText(qSettings.value(key_stationsfile, sDatabaseDirectory + "/stations.json").toString());
  ui->lineEditEDDBSystemsFile->setText(qSettings.value(key_systemsfile, sDatabaseDirectory + "/systems.json").toString());
  ui->lineEditEDDBCommoditiesFile->setText(qSettings.value(key_commoditiesfile, sDatabaseDirectory + "/commodities.json").toString());
  ui->lineEditEDDBListingsFile->setText(qSettings.value(key_listingsfile, sDatabaseDirectory + "/listings.csv").toString());
  ui->lineEditEDDBModulesFile->setText(qSettings.value(key_modulesfile, sDatabaseDirectory + "/modules.json").toString());

  ui->checkBoxSaveDatabaseOnExit->setChecked(qSettings.value(key_database_save_on_exit, true).toBool());
  ui->checkBoxSynchronizeDBOnStartup->setChecked(qSettings.value(key_synchronize_db_on_startup, true).toBool());

  // Make sure the default values are available later.
  qSettings.setValue(key_commoditiesfile, ui->lineEditEDDBCommoditiesFile->text());
  qSettings.setValue(key_commoditiesurl, ui->lineEditEDDBCommoditiesURL->text());

  qSettings.setValue(key_systemsfile, ui->lineEditEDDBSystemsFile->text());
  qSettings.setValue(key_systemsurl, ui->lineEditEDDBSystemsURL->text());

  qSettings.setValue(key_stationsfile, ui->lineEditEDDBStationsFile->text());
  qSettings.setValue(key_stationsurl, ui->lineEditEDDBStationsURL->text());

  qSettings.setValue(key_listingsfile, ui->lineEditEDDBListingsFile->text());
  qSettings.setValue(key_listingsurl, ui->lineEditEDDBListingsURL->text());

  qSettings.setValue(key_modulesfile, ui->lineEditEDDBModulesFile->text());
  qSettings.setValue(key_modulesurl, ui->lineEditEDDBModulesURL->text());

  qSettings.setValue(key_datadirectory, ui->lineEditDatabaseDirectory->text());

  evian::InitAndSaveSettingsHelper(key_edlog_max_logfiles, 0);
  ui->lineEditSerializationFilename->setText(qSettings.value(key_database_file).toString());
  ui->lineEditSerializationListingsFilename->setText(qSettings.value(key_database_listings_file).toString());

  evian::InitAndSaveSettingsHelper(key_appconfigxmlfile, EDNetLogParserQT::FindEDFile("appconfig.xml"));
  ui->lineEditAppconfigXMLLocation->setText(qSettings.value(key_appconfigxmlfile).toString());
  SetAppconfigXMLStatus();

  SetupFilterOlderThan();
  UpdateEDDBDatabaseStatusFromSettings();
  UpdateDynamoDBStatusFromSettings();

  ui->lineEditEDLogDirectory->setText(qSettings.value(key_fd_log_directory).toString());
  evian::AddDropShadowEffect(ui->labelAskUserNotToDownloadNeedlessly);

  SetupEDDNOnDynamoDB();
}

void
eviansettingsqt::UpdateEDDBDatabaseStatusFromSettings()
{
  QSettings
    cQSettings;

  QDateTime
    cQDateTime = cQSettings.value(key_eddb_last_update).toDateTime();

  if (cQDateTime.isNull())
  {
    EDDBDatabaseStatusUpdated("Unknown");
  }
  else
  {
    QString
      cMessage = QString("Database rebuilt: ") + cQDateTime.toString(Qt::ISODate);

    EDDBDatabaseStatusUpdated(cMessage);
  }
}

void
eviansettingsqt::UpdateDynamoDBStatusFromSettings()
{
  QSettings
    qSettings;

  QDateTime
    qDateTime = qSettings.value(key_eddn_on_dynamodb_last_update, QDateTime::fromMSecsSinceEpoch(0)).toDateTime();

  ui->labelDynamoDBStatus->setText(QString("Last updated: %1").arg(qDateTime.toString(Qt::ISODate)));
  ui->comboBoxDynamoDBDaysToFetch->setItemData(0, qDateTime);
}

eviansettingsqt::~eviansettingsqt()
{
  delete ui;
}

void
eviansettingsqt::mousePressEvent(QMouseEvent * pcQMouseEvent)
{
  _cQPointOldPos = pcQMouseEvent->globalPos();
}

void
eviansettingsqt::mouseMoveEvent(QMouseEvent * pcQMouseEvent)
{
  const QPoint
    delta = pcQMouseEvent->globalPos() - _cQPointOldPos;

  move(x() + delta.x(), y() + delta.y());
  _cQPointOldPos = pcQMouseEvent->globalPos();
}

void
eviansettingsqt::DownloadURL(QString cURL, QString cFilename)
{
  qDebug() << "Downloading " << cURL << " saving to " << cFilename << endl;

  QUrl
    cQUrl = cURL.toLocal8Bit();

  QNetworkRequest
    cQNetworkRequest(cQUrl);

  cQNetworkRequest.setHeader(QNetworkRequest::UserAgentHeader, evian::EvianUserAgentString());
  cQNetworkRequest.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
  cQNetworkRequest.setAttribute(QNetworkRequest::HttpPipeliningAllowedAttribute, true);
  cQNetworkRequest.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QNetworkRequest::AlwaysNetwork);

  for (const auto& cQByteArray : cQNetworkRequest.rawHeaderList())
  {
    qDebug() << cQByteArray << ": " << cQNetworkRequest.rawHeader(cQByteArray);
  }

  QNetworkReply
    *pcQNetworkReply = _cQNetworkAccessManager.get(cQNetworkRequest);

  qDebug() << "BufferSize: " << pcQNetworkReply->readBufferSize();

  _mpcQNetworkReply[pcQNetworkReply] = cFilename;
  _mpcQNetworkReplyBytesAlreadyRecieved[pcQNetworkReply] = 0;

  connect(pcQNetworkReply, &QNetworkReply::downloadProgress,
    this, &eviansettingsqt::DownloadProgress);

  connect(pcQNetworkReply, SIGNAL(finished()),
    this, SLOT(DownloadFinishedQNetworkReply()));

  connect(pcQNetworkReply, static_cast<void(QNetworkReply::*)(QNetworkReply::NetworkError)>(&QNetworkReply::error),
    this, &eviansettingsqt::DownloadError);
}

void
eviansettingsqt::SetupFilterOlderThan()
{
  qint64
    nSecsInHour = 3600;

  QList<qint64>
    _lnFilterOlderThanSecsAgo;

  _lnFilterOlderThanSecsAgo.append(1 * nSecsInHour);
  _lnFilterOlderThanSecsAgo.append(6 * nSecsInHour);
  _lnFilterOlderThanSecsAgo.append(12 * nSecsInHour);
  _lnFilterOlderThanSecsAgo.append(24 * nSecsInHour);
  _lnFilterOlderThanSecsAgo.append(48 * nSecsInHour);
  _lnFilterOlderThanSecsAgo.append(7 * 24 * nSecsInHour);
  _lnFilterOlderThanSecsAgo.append(14 * 24 * nSecsInHour);
  _lnFilterOlderThanSecsAgo.append(30 * 24 * nSecsInHour);
  _lnFilterOlderThanSecsAgo.append(60 * 24 * nSecsInHour);

  QDateTime
    cQDateTime = QDateTime::currentDateTime();

  ui->comboBoxFilterOlderThan->blockSignals(true);

  for (int iSecsAgo = 0; iSecsAgo < _lnFilterOlderThanSecsAgo.size(); ++iSecsAgo)
  {
    auto
      nSecsAgo = _lnFilterOlderThanSecsAgo[iSecsAgo];
    auto
      cQString = FormatDurationSinceNow(cQDateTime.addSecs(-nSecsAgo));
    ui->comboBoxFilterOlderThan->addItem(cQString, nSecsAgo);
  }

  int
    nCurrentValue = evian::InitAndSaveSettingsHelper(key_database_filter_older_than_s, 14 * 24 * nSecsInHour).toInt();

  auto
    nIndex = ui->comboBoxFilterOlderThan->findData(nCurrentValue);

  if (nIndex != -1)
  {
    ui->comboBoxFilterOlderThan->setCurrentIndex(nIndex);
  }
  else
  {
    ui->comboBoxFilterOlderThan->setCurrentIndex(6); // 14 days.
  }

  ui->comboBoxFilterOlderThan->blockSignals(false);
}

void
eviansettingsqt::SetupEDDNOnDynamoDB()
{
  QSettings
    cQSettings;

  ui->lineEditDynamoDBURL->setText(cQSettings.value(key_eddn_on_dynamodb_url, "https://43h3di62h7.execute-api.eu-west-1.amazonaws.com/beta/commodities/").toString());

  connect(_pcEDDNOnDynamoDB, &evian::EDDNOnDynamoDB_c::EDDNMessage,
    _pcMainWindow->_pcTradeDatabaseEngine, &evian::TradeDatabaseEngine_c::HandleEDDNMessage);

  connect(_pcEDDNOnDynamoDB, &evian::EDDNOnDynamoDB_c::UIMessage,
    ui->labelDynamoDBStatus, &QLabel::setText);
  
  // If changing the order here, it must also be updated in UpdateDynamoDBStatusFromSettings().
  ui->comboBoxDynamoDBDaysToFetch->addItem("Since last update", cQSettings.value(key_eddn_on_dynamodb_last_update).toDateTime());
  ui->comboBoxDynamoDBDaysToFetch->addItem("Today", QDateTime::currentDateTimeUtc());
  ui->comboBoxDynamoDBDaysToFetch->addItem("Yesterday", QDateTime::currentDateTimeUtc().addDays(-1));
  ui->comboBoxDynamoDBDaysToFetch->addItem("Two days ago", QDateTime::currentDateTimeUtc().addDays(-2));
  ui->comboBoxDynamoDBDaysToFetch->addItem("Last week", QDateTime::currentDateTimeUtc().addDays(-7));
}

void
eviansettingsqt::SetAppconfigXMLStatus()
{
  QSettings
    qSettings;

  auto
    sAppconfigXMLFile = qSettings.value(key_appconfigxmlfile).toString();

  if (!QFile::exists(sAppconfigXMLFile))
  {
    ui->labelAppconfigXMLStatus->setText("Unable to locate AppConfig.xml");
  }
  else
  {
    ui->labelAppconfigXMLStatus->setText(IsVerboseLoggingEnabled() ? "Verbose logging enabled" : "Verbose logging disabled");
  }
}

void
eviansettingsqt::on_toolButton_clicked()
{
  auto
    fileName = QFileDialog::getOpenFileName(this, "Select Commodities File");/*, QStandardPaths::writableLocation(QStandardPaths::DesktopLocation),
                                            "JSON (*.json)");*/
  ui->lineEditEDDBCommoditiesFile->setText(fileName);
}

void
eviansettingsqt::on_toolButton_2_clicked()
{
  auto
    fileName = QFileDialog::getOpenFileName(this, "Select Stations File");/*, QStandardPaths::writableLocation(QStandardPaths::DesktopLocation),
                                            "JSON (*.json)");*/
  ui->lineEditEDDBStationsFile->setText(fileName);
  ui->lineEditEDDBStationsFile->setFocus();
}

void eviansettingsqt::on_toolButton_3_clicked()
{
  auto
    fileName = QFileDialog::getOpenFileName(this, "Select Systems File");/*, QStandardPaths::writableLocation(QStandardPaths::DesktopLocation),
                                            "JSON (*.json)");*/
  ui->lineEditEDDBSystemsFile->setText(fileName);
}

void eviansettingsqt::on_toolButton_4_clicked()
{
  QSettings
    cQSettings;

  QString
    cDefaultDir = cQSettings.value(key_datadirectory).toString(); // Should have been made by main().

  auto
    dirName = QFileDialog::getExistingDirectory(this, "Select Evian datadirectory", cDefaultDir);

  ui->lineEditDatabaseDirectory->setText(dirName);

}

void eviansettingsqt::on_toolButton_6_clicked()
{
  auto
    fileName = QFileDialog::getOpenFileName(this, "Select Listings File");/*, QStandardPaths::writableLocation(QStandardPaths::DesktopLocation),
                                            "CSV (*.csv)");*/
  ui->lineEditEDDBListingsFile->setText(fileName);
}

void eviansettingsqt::on_toolButton_10_clicked()
{
  auto
    fileName = QFileDialog::getOpenFileName(this, "Select Modules File");/*, QStandardPaths::writableLocation(QStandardPaths::DesktopLocation),
                                            "JSON (*.json)");*/
  ui->lineEditEDDBModulesFile->setText(fileName);
}

void eviansettingsqt::on_lineEditDatabaseDirectory_editingFinished()
{
  QSettings
    cQSettings;

  cQSettings.setValue(key_datadirectory, ui->lineEditDatabaseDirectory->text());
}

void eviansettingsqt::on_lineEditEDDBStationsURL_editingFinished()
{
  QSettings
    cQSettings;

  cQSettings.setValue(key_stationsurl, ui->lineEditEDDBStationsURL->text());

}

void eviansettingsqt::on_lineEditEDDBSystemsURL_editingFinished()
{
  QSettings
    cQSettings;

  cQSettings.setValue(key_systemsurl, ui->lineEditEDDBSystemsURL->text());
}


void eviansettingsqt::on_lineEditEDDBCommoditiesURL_editingFinished()
{
  QSettings
    cQSettings;

  cQSettings.setValue(key_commoditiesurl, ui->lineEditEDDBCommoditiesURL->text());
}

void eviansettingsqt::on_lineEditEDDBCommoditiesFile_editingFinished()
{
  QSettings
    cQSettings;

  cQSettings.setValue(key_commoditiesfile, ui->lineEditEDDBCommoditiesFile->text());
}

void eviansettingsqt::on_lineEditEDDBSystemsFile_editingFinished()
{
  QSettings
    cQSettings;

  cQSettings.setValue(key_systemsfile, ui->lineEditEDDBSystemsFile->text());
}

void eviansettingsqt::on_lineEditEDDBStationsFile_editingFinished()
{
  QSettings
    cQSettings;

  cQSettings.setValue(key_stationsfile, ui->lineEditEDDBStationsFile->text());
}

void eviansettingsqt::on_lineEditEDDBListingsFile_editingFinished()
{
  QSettings
    cQSettings;

  cQSettings.setValue(key_listingsfile, ui->lineEditEDDBListingsFile->text());
}


void eviansettingsqt::on_lineEditEDDBModulesFile_editingFinished()
{
  QSettings
    cQSettings;

  cQSettings.setValue(key_modulesfile, ui->lineEditEDDBModulesFile->text());
}

void eviansettingsqt::on_lineEditEDDBListingsURL_editingFinished()
{
  QSettings
    cQSettings;

  cQSettings.setValue(key_listingsurl, ui->lineEditEDDBListingsURL->text());

}


void eviansettingsqt::on_lineEditEDDBModulesURL_editingFinished()
{
  QSettings
    cQSettings;

  cQSettings.setValue(key_modulesurl, ui->lineEditEDDBModulesURL->text());
}




void eviansettingsqt::on_pushButton_clicked()
{
  this->hide();
}

void eviansettingsqt::on_pushButtonUpdateDatabase_clicked()
{
  QSettings
    cQSettings;

  ui->labelDatabaseStatus->setText("Downloading");

  _nBytesDownloaded = 0.0;
  _isNetworkError = false;

  ui->progressBarDatabaseUpdateStatus->setVisible(true);
  ui->progressBarDatabaseUpdateStatus->setValue(0);

  DownloadURL(cQSettings.value(key_commoditiesurl).toString(),
    cQSettings.value(key_commoditiesfile).toString());

  DownloadURL(cQSettings.value(key_systemsurl).toString(),
    cQSettings.value(key_systemsfile).toString());

  DownloadURL(cQSettings.value(key_stationsurl).toString(),
    cQSettings.value(key_stationsfile).toString());

  DownloadURL(cQSettings.value(key_listingsurl).toString(),
    cQSettings.value(key_listingsfile).toString());

  EVIAN_ASSERT(_mpcQNetworkReply.size() == 4);
}

void
eviansettingsqt::DownloadFinished(QNetworkReply* pcQNetworkReply)
{
  if (pcQNetworkReply->error() != QNetworkReply::NoError)
  {
    QMessageBox::critical(this, "Download error", QString("Could not download ") + pcQNetworkReply->url().toString());
  }
  else
  {
    QString
      cFilename = _mpcQNetworkReply[pcQNetworkReply];

    auto
      iHTTPStatusCode = pcQNetworkReply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    auto
      cByteArrayPhraseAttribue = pcQNetworkReply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toByteArray();
    auto
      cQUrl = pcQNetworkReply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl();

    qDebug() << cFilename << " status: " << iHTTPStatusCode << ", PhraseAttribute: " << cByteArrayPhraseAttribue << " RedirectURL: " << cQUrl.toString();

    for (const auto& cQPair : pcQNetworkReply->rawHeaderPairs())
    {
      qDebug() << cQPair.first << ": " << cQPair.second;
    }

    QFile
      cQFile(cFilename);

    if (!cQFile.open(QIODevice::WriteOnly))
    {
      QMessageBox::critical(this, "File save error", QString("Could not write to ") + cFilename);
    }
    else
    {
      qDebug() << "Writing " << cFilename;
      cQFile.write(pcQNetworkReply->readAll());
      cQFile.close();
    }
  }

  _mpcQNetworkReply.remove(pcQNetworkReply);

  if (_mpcQNetworkReply.isEmpty())
  {
    EVIAN_ASSERT(_isNetworkError == false);
    emit EDDBDatabaseDownloadComplete(); // Will trigger signal to update status.

    _mpcQNetworkReplyBytesAlreadyRecieved.clear();
    ui->progressBarDatabaseUpdateStatus->setVisible(false);
  }

  pcQNetworkReply->deleteLater();
}

void
eviansettingsqt::DownloadFinishedQNetworkReply()
{
  auto
    pcQNetworkReply = dynamic_cast<QNetworkReply*>(QObject::sender());

  if (pcQNetworkReply)
  {
    DownloadFinished(pcQNetworkReply);
  }
}

void
eviansettingsqt::DownloadError(QNetworkReply::NetworkError eNetworkError)
{
  qDebug() << "Network error: " << eNetworkError;

  auto
    pcQNetworkReply = dynamic_cast<QNetworkReply*>(QObject::sender());

  if (pcQNetworkReply)
  {
    qDebug() << "Network error (errorString): " << pcQNetworkReply->errorString();
  }

  _isNetworkError = true;
}

void
eviansettingsqt::DownloadProgress(qint64 bytesRecieved, qint64 /*bytesTotal*/)
{
  double
    kb = 1024,
    mb = 1024 * kb;

  auto
    pcQNetworkReply = dynamic_cast<QNetworkReply*>(sender());

  auto
    nBytesSinceLastUpdate = bytesRecieved - _mpcQNetworkReplyBytesAlreadyRecieved[pcQNetworkReply];

  _mpcQNetworkReplyBytesAlreadyRecieved[pcQNetworkReply] = bytesRecieved;

  _nBytesDownloaded += nBytesSinceLastUpdate;;

  double
    nMBDownloaded = _nBytesDownloaded / mb;

  //ui->progressBarDatabaseUpdateStatus->setValue(static_cast<int>(nNewValue));
  ui->labelDatabaseStatus->setText(QString("Downloading (") + QString::number(nMBDownloaded) + " MB received)");
}

bool
eviansettingsqt::IsVerboseLoggingEnabled()
{
  QSettings
    qSettings;

  auto
    sAppconfigXML = qSettings.value(key_appconfigxmlfile).toString();

  QFile
    qFile(sAppconfigXML);

  if (!QFile::exists(sAppconfigXML) || !qFile.open(QIODevice::ReadOnly))
  {
    return false;
  }

  return IsVerboseLoggingEnabled(qFile);
}

bool
eviansettingsqt::IsVerboseLoggingEnabled(QFile& qFile)
{
  QDomDocument
    qDomDocument;

  qDomDocument.setContent(&qFile);

  auto
    qDomElement = qDomDocument.documentElement();

  auto
    qDomNodeList = qDomElement.elementsByTagName("Network");

  if (qDomNodeList.size() != 1)
  {
    qDebug() << qFile.fileName() << " contains " << qDomNodeList.size() << " elements with the Network tag.";
  }

  for (auto i = 0; i < qDomNodeList.size(); ++i)
  {
    auto
      qDomNode = qDomNodeList.item(i);

    auto
      qDomElement = qDomNode.toElement();

    if (qDomElement.attribute("VerboseLogging", "0") == "1")
      return true;
  }

  return false;
}

void
CreateBackupFile(QFile& qFile)
{
  QString
    sBackupFileName = qFile.fileName() + ".CMDREvian.bak";

  if (QFile::exists(sBackupFileName))
  {
    bool
      isRemove = QFile::remove(sBackupFileName);

    if (!isRemove)
    {
      qWarning() << "Failed to remove " << sBackupFileName;
    }
  }

  bool
    isCopy = qFile.copy(sBackupFileName);

  if (!isCopy)
  {
    qWarning() << "Failed to copy " << sBackupFileName;
  }
}

void
eviansettingsqt::ConsiderEnableVerboseLogging(QString sAppConfigXMLLocation)
{
  QFile
    qFile(sAppConfigXMLLocation);

  if (!qFile.open(QIODevice::ReadOnly))
  {
    qWarning() << "Could not open " << sAppConfigXMLLocation << " for modification.";
    return;
  }

  if (IsVerboseLoggingEnabled(qFile))
  {
    return;
  }

  CreateBackupFile(qFile);

  QDomDocument
    qDomDocument(sAppConfigXMLLocation);

  qDomDocument.setContent(&qFile);
  qFile.close();

  auto
    qDomElement = qDomDocument.documentElement();

  auto
    qDomNodeList = qDomElement.elementsByTagName("Network");

  if (qDomNodeList.size() != 1)
  {
    qDebug() << qFile.fileName() << " contains " << qDomNodeList.size() << " elements with the Network tag.";
  }

  for (auto i = 0; i < qDomNodeList.size(); ++i)
  {
    auto
      qDomNode = qDomNodeList.item(i);

    auto
      qDomElement = qDomNode.toElement();

    qDomElement.setAttribute("VerboseLogging", "1");

    bool
      isOpen = qFile.open(QFile::WriteOnly);

    if (!isOpen)
    {
      qWarning() << "Failed to open " << sAppConfigXMLLocation;
      return;
    }

    auto
      qByteArray = qDomDocument.toByteArray();

    auto
      nBytesWritten = qFile.write(qByteArray);

    if (nBytesWritten == -1)
    {
      qWarning() << "Failed to write to " << sAppConfigXMLLocation;
      return;
    }
  }
}

void eviansettingsqt::EDDBDatabaseStatusUpdated(QString cQString)
{
  ui->labelDatabaseStatus->setText(cQString);
}

void eviansettingsqt::DatabaseUpdated()
{
  ui->pushButtonEDDBReimport->setChecked(false);
  UpdateEDDBDatabaseStatusFromSettings();
  UpdateDynamoDBStatusFromSettings();
}

void eviansettingsqt::on_comboBoxMultiThreadingMode_activated(const QString &arg1)
{
  QSettings
    cQSettings;

  if (arg1 == "Preferred")
  {
    cQSettings.setValue(key_concurrency_mode, evian::Concurrency_Automatic);
  }
  else if (arg1 == "Force Multithreading")
  {
    cQSettings.setValue(key_concurrency_mode, evian::Concurrency_ForceMultiThreading);
  }
  else if (arg1 == "Force Singlethreading")
  {
    cQSettings.setValue(key_concurrency_mode, evian::Concurrency_ForceSingleThreading);
  }
  else
  {
    qDebug() << "Unknown multithreadingmode: " << arg1;
  }
}

void eviansettingsqt::on_toolButton_5_clicked()
{
  auto
    fileName = QFileDialog::getExistingDirectory(this, "Select Elite:Dangerous Log Directory", QStandardPaths::writableLocation(QStandardPaths::DesktopLocation));

  ui->lineEditEDLogDirectory->setText(fileName);
}

void eviansettingsqt::on_lineEditEDLogDirectory_editingFinished()
{
  QSettings
    cQSettings;

  cQSettings.setValue(key_fd_log_directory, ui->lineEditEDLogDirectory->text());
  _pcMainWindow->_pcEDNetLogParserQT->SetLogDirectory(ui->lineEditEDLogDirectory->text());
}

void eviansettingsqt::on_spinBoxMaxEDLogfiles_editingFinished()
{
  QSettings
    cQSettings;

  cQSettings.setValue(key_edlog_max_logfiles, ui->spinBoxMaxEDLogfiles->value());
  _pcMainWindow->_pcEDNetLogParserQT->SetMaxEDLogfiles(ui->spinBoxMaxEDLogfiles->value());
}

void
eviansettingsqt::on_pushButtonSave_clicked()
{
  SerializeDatabase();
}

namespace {
  std::tuple<bool, qint64>
    LockAndSerializeDatabase(MainWindow * pcMainWindow, QString sFilename, QString sListingsFilename, evian::TradeDatabaseEngine_c::SerializationMode eSerializationMode)
  {
    evian::EvianReadLocker_c
      cEvianReadLock(__FILE__, __LINE__, pcMainWindow->_pcTradeDatabaseEngine->GetEvianReadWriteLock());

    QElapsedTimer
      cQElapsedTimer;

    cQElapsedTimer.start();

    bool
      isSuccessful = pcMainWindow->_pcTradeDatabaseEngine->Serialize(sFilename, sListingsFilename, eSerializationMode);

    return std::make_tuple(isSuccessful, cQElapsedTimer.elapsed());
  }
}
void
eviansettingsqt::SerializeDatabase()
{
  auto
    eSerializationMode = static_cast<evian::TradeDatabaseEngine_c::SerializationMode>(ui->comboBoxSerializationMode->currentIndex());

  typedef std::tuple<bool, qint64>
    Result_Tuple;

  QSettings
    qSettings;
  
  auto
    sDatabaseFilename = qSettings.value(key_database_file).toString(),
    sListingsFilename = qSettings.value(key_database_listings_file).toString();

    auto
    pcQFutureWatcher = new QFutureWatcher<Result_Tuple>(this);

  ui->pushButtonSave->setChecked(true);

  auto cQFuture =
    QtConcurrent::run(LockAndSerializeDatabase, _pcMainWindow, sDatabaseFilename, sListingsFilename, eSerializationMode);

  connect(pcQFutureWatcher, &QFutureWatcher<Result_Tuple>::finished,
    ui->pushButtonSave, &QPushButton::toggle);

  connect(pcQFutureWatcher, &QFutureWatcher<Result_Tuple>::finished,
    [=]()
  {
    auto
      result = pcQFutureWatcher->future().result();

    bool
      isSuccessful = std::get<0>(result);

    auto
      nElapsed = std::get<1>(result);

    if (isSuccessful)
    {
      QString
        cQString = QString("Saved %1 (%2 ms)").arg(ui->lineEditSerializationFilename->text()).arg(nElapsed);

      _pcMainWindow->ShowStatusMessage(cQString);
    }
    else
    {
      QString
        cQString = QString("Save of %1 failed! (%2 ms)").arg(ui->lineEditSerializationFilename->text()).arg(nElapsed);

      _pcMainWindow->statusBar()->showMessage(cQString);
    }
  });

  connect(pcQFutureWatcher, &QFutureWatcher<Result_Tuple>::finished,
    pcQFutureWatcher, &QFutureWatcher<Result_Tuple>::deleteLater);

  pcQFutureWatcher->setFuture(cQFuture);
}

QFont
eviansettingsqt::GetWidgetFont()
{
  QFont
    cQFont = ui->label_7->property("font").value<QFont>();

  return cQFont;
}

void
eviansettingsqt::on_pushButtonLoad_clicked()
{
  QSettings
    qSettings;

  auto
    sFilenameDatebase = qSettings.value(key_database_file).toString(),
    sFilenameListings = qSettings.value(key_database_listings_file).toString();

  _pcMainWindow->_pcTradeDatabaseEngine->Deserialize(sFilenameDatebase, sFilenameListings);
}

void
eviansettingsqt::on_pushButtonEDDBReimport_clicked()
{
  ui->pushButtonEDDBReimport->setChecked(true);
  emit ImportEDDBFromDisk();
}

void
eviansettingsqt::on_comboBoxFilterOlderThan_currentIndexChanged(int /*index*/)
{
  auto
    nValue = ui->comboBoxFilterOlderThan->currentData().toInt();

  EVIAN_ASSERT(nValue > 0 && "Must be a positivie number");

  QSettings
    cQSettings;

  cQSettings.setValue(key_database_filter_older_than_s, nValue);

  emit FilterOlderThanUpdated(nValue);
}

void
eviansettingsqt::on_checkBoxSaveDatabaseOnExit_clicked(bool isChecked)
{
  QSettings
    cQSettings;

  cQSettings.setValue(key_database_save_on_exit, isChecked);
}

void
eviansettingsqt::on_pushButtonUpdateDynamoDB_clicked()
{
  auto
    qDateTime = ui->comboBoxDynamoDBDaysToFetch->itemData(ui->comboBoxDynamoDBDaysToFetch->currentIndex()).toDateTime();

  qint64
    nOffset = 0;

  if (ui->comboBoxDynamoDBDaysToFetch->currentIndex() == 0) // 0 == since last update
  {
    nOffset = qDateTime.toMSecsSinceEpoch() * 1000; // To microseconds;
  }

  _pcEDDNOnDynamoDB->DownloadDate(qDateTime, nOffset);
}

void
eviansettingsqt::on_lineEditDynamoDBURL_editingFinished()
{
  QSettings
    cQSettings;

  cQSettings.setValue(key_eddn_on_dynamodb_url, ui->lineEditDynamoDBURL->text());
}

void
eviansettingsqt::on_checkBoxSynchronizeDBOnStartup_clicked(bool checked)
{
  QSettings
    cQSettings;

  cQSettings.setValue(key_synchronize_db_on_startup, checked);
}

void
eviansettingsqt::on_pushButtonUpdateAppconfigXML_clicked()
{
  QSettings
    qSettings;

  auto
    qFilename = qSettings.value(key_appconfigxmlfile).toString();

  ConsiderEnableVerboseLogging(qFilename);
  SetAppconfigXMLStatus();
}

void
eviansettingsqt::on_comboBoxDataValidationMode_activated(int index)
{
  QSettings
    qSettings;

  qSettings.setValue(key_database_data_validation_mode, index);
}

void
eviansettingsqt::on_lineEditAppconfigXMLLocation_editingFinished()
{
  QSettings
    qSettings;

  qSettings.setValue(key_appconfigxmlfile, ui->lineEditAppconfigXMLLocation->text());
}

void eviansettingsqt::on_checkBoxAppconfigXMLAutomaticUpdate_clicked(bool checked)
{
  QSettings
    qSettings;

  qSettings.setValue(key_auto_update_appconfigxml, checked);
}

void eviansettingsqt::on_lineEditSerializationListingsFilename_editingFinished()
{
  QSettings
    qSettings;

  qSettings.setValue(key_database_listings_file, ui->lineEditSerializationListingsFilename->text());
}
