//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#include "stdafx.h"
#include "querywidget.hpp"
#include "ui_querywidget.h"
#include "mainwindow.hpp"
#include "evianqueryqt.hpp"
#include "model.hpp"
#include "DatabaseQuery.hpp"
#include "eviandatabaseexplorerqt.hpp"
#include "tradedetailwidget.hpp"
#include "flowlayout.hpp"

QueryWidget_c::QueryWidget_c(MainWindow *pcMainWindow, QWidget *parent) :
  QWidget(parent),
  ui(new Ui::QueryWidget_c),
  _pcMainWindow(pcMainWindow),
  _pcProxyModel(new QSortFilterProxyModel(this)),
  _eQueryType(Query_Loop),
  _cRestrictionDatabaseView(_pcMainWindow, this),
  _cQueryRestrictionDatabase(_pcMainWindow->_pcTradeDatabaseEngine, this)
{
  ui->setupUi(this);
  EnableBackgroundImage();
  ui->groupBoxAdvancedOptions->hide();
  //ui->groupBoxRestrictions->hide();
  ui->lineEditStartLocation->setPlaceholderText(QueryWidget::ANY_LOCATION);
  ui->lineEditEndLocation->setPlaceholderText(QueryWidget::ANY_LOCATION);
  ui->progressBar->hide();

  InitCompleters();
  InitTableViewResults();
  SetDefaultValuesFromSettings();

  connect(ui->verticalSliderHopLoop, &QSlider::valueChanged,
          [this](int sliderValue) 
          {
            EVIAN_ASSERT(sliderValue == 0 || sliderValue == 1);
            SetQueryType(static_cast<Query_Type>(sliderValue));
          });

  connect(ui->tableViewResults, &QTableView::doubleClicked,
          this, &QueryWidget_c::TradeDetailsRequested);

  connect(&_cQueryRestrictionDatabase, &evian::QueryExclusionDatabase_c::CommodityExcluded,
          &_cRestrictionDatabaseView,  &RestrictionDatabaseView_c::AddExludedCommodity);

  connect(&_cQueryRestrictionDatabase, &evian::QueryExclusionDatabase_c::StationExcluded,
          &_cRestrictionDatabaseView,  &RestrictionDatabaseView_c::AddExcludedStation);
  
  connect(&_cQueryRestrictionDatabase, &evian::QueryExclusionDatabase_c::SystemExcluded,
          &_cRestrictionDatabaseView,  &RestrictionDatabaseView_c::AddExcludedSystem);

  connect(&_cQueryRestrictionDatabase, &evian::QueryExclusionDatabase_c::FactionExcluded,
          &_cRestrictionDatabaseView,  &RestrictionDatabaseView_c::AddExcludedFaction);

  connect(&_cRestrictionDatabaseView,  &RestrictionDatabaseView_c::IncludeCommodity,
          &_cQueryRestrictionDatabase, &evian::QueryExclusionDatabase_c::AddIncludedCommodity);
  
  connect(&_cRestrictionDatabaseView,  &RestrictionDatabaseView_c::IncludeStation,
          &_cQueryRestrictionDatabase, &evian::QueryExclusionDatabase_c::AddIncludedStation);

  connect(&_cRestrictionDatabaseView,  &RestrictionDatabaseView_c::IncludeSystem,
          &_cQueryRestrictionDatabase, &evian::QueryExclusionDatabase_c::AddIncludedSystem);

  connect(&_cRestrictionDatabaseView,  &RestrictionDatabaseView_c::IncludeFaction,
          &_cQueryRestrictionDatabase, &evian::QueryExclusionDatabase_c::AddIncludedFaction);

  connect(ui->groupBoxRestrictions, &QWidget::customContextMenuRequested,
          this,                     &QueryWidget_c::RestrictionContextMenuRequested);

  delete ui->groupBoxRestrictions->layout();

  auto
    pcFlowLayout = new FlowLayout(ui->groupBoxRestrictions);

  ui->groupBoxRestrictions->setLayout(pcFlowLayout);

  _cRestrictionDatabaseView.SetLayout(ui->groupBoxRestrictions->layout());
}

QueryWidget_c::~QueryWidget_c()
{
  delete ui;
}

QueryWidget_c::Query_Type QueryWidget_c::GetQueryType() const
{
  return _eQueryType;
}

void
QueryWidget_c::SetQueryType(Query_Type eQueryType)
{
  if (_eQueryType != eQueryType)
  {
    _eQueryType = eQueryType;
    emit QueryTypeChanged();
  }
}

void
QueryWidget_c::SetWindowAndTabTitle(const QString& cQString)
{
  setWindowTitle(cQString);
}

QString
QueryWidget_c::DeduceSystemNameBegin() const
{
  return DeduceSystemName(ui->lineEditStartLocation->text());
}

QString
QueryWidget_c::DeduceSystemNameEnd() const
{
  return DeduceSystemName(ui->lineEditEndLocation->text());
}

QString 
QueryWidget_c::DeduceSystemName(const QString& cQString) const
{
  auto
    lcQStringSystemStation = cQString.split(QueryWidget::SPLIT);

  QString
    cSystemName;

  if (lcQStringSystemStation.size() == 1)
  {
    cSystemName = lcQStringSystemStation[0].trimmed();
  }
  else
  {
    cSystemName = lcQStringSystemStation[1].trimmed();
  }

  return cSystemName;
}

QString
CompleterListCache_c::DeduceLocationFromCompleter(QLineEdit * pcQLineEdit)
{
  auto
    cQString = pcQLineEdit->text();

  auto
    pcQCompleter = pcQLineEdit->completer();

  if (pcQCompleter != NULL)
  {
    auto
      pcModel = pcQCompleter->completionModel();

    // Ie searching for 'sol' with wrong capitalization
    auto
      lcQModelIndex = pcModel->match(pcModel->index(0,0), 0, cQString, -1, Qt::MatchFixedString);

    if (lcQModelIndex.size() == 1)
    {
      cQString = lcQModelIndex.first().data().toString();
      pcQLineEdit->setText(cQString);
    }
    else if (lcQModelIndex.isEmpty()) // Ie. Entering 'empire' while only match is 'Faction Empire'
    {
      lcQModelIndex = pcModel->match(pcModel->index(0,0), 0, cQString, -1, Qt::MatchContains);
      if (lcQModelIndex.size() == 1)
      {
        cQString = lcQModelIndex.first().data().toString();
        pcQLineEdit->setText(cQString);
      }
    }
  }

  return cQString;
}

QString
QueryWidget_c::FormatLocation(const QString& cQString) const
{
  using namespace QueryWidget;

  const auto&
    cFactionToSystemMap = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase()._cFactionToSystemMap;

  if (IsAnyLocation(cQString))
  {
    return QLatin1Literal("Any");
  }
  else if (cFactionToSystemMap.contains(RemoveFactionPrefix(cQString)))
  {
    return RemoveFactionPrefix(cQString);
  }
  else
  {
    return cQString;
  }
}

QString
QueryWidget_c::FormatSystemDistance() const
{
  if (ui->doubleSpinBoxMaxLY->value() == 0.0)
  {
    return QString("(Unlimited)");
  }
  else
  {
    return QString("(%1 LY)").arg(QString::number(ui->doubleSpinBoxMaxLY->value()));
  }
}

void QueryWidget_c::Message(QString cQString) const
{
  _pcMainWindow->statusBar()->showMessage(cQString);
}

void
QueryWidget_c::InitCompleters()
{
  _lcQStringSystemsAndStations.clear();

  auto
    &cTradeDatabase = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase();

  _lcQStringSystemsAndStations << QueryWidget::ANY_LOCATION;

  for(const auto& cSystem: cTradeDatabase._lcSystemWithStation)
  {
    _lcQStringSystemsAndStations << cSystem.get()._cName;

    for (const auto cStation : cSystem.get()._lpcStation)
    {
      _lcQStringSystemsAndStations << QString("%1 / %2").arg(cStation->_cName).arg(cSystem.get()._cName);
    }
  }

  QHashIterator<QString, QList<std::reference_wrapper<const evian::System_c>>>
    it(cTradeDatabase._cFactionToSystemMap);

  while (it.hasNext())
  {
    it.next();
    _lcQStringSystemsAndStations << QString("%1%2").arg(QueryWidget::FACTION_PREFIX).arg(it.key());
  }

  _lcQStringSystemsAndStations.sort(Qt::CaseInsensitive);

  _cCompleterListCache.SetQStringList(_lcQStringSystemsAndStations);
}

void
CompleterListCache_c::SetupQCompleter(QCompleter * pcQCompleter)
{
  pcQCompleter->setModelSorting(QCompleter::CaseInsensitivelySortedModel);
  pcQCompleter->setCompletionMode(QCompleter::PopupCompletion);
  pcQCompleter->setCaseSensitivity(Qt::CaseInsensitive);
  pcQCompleter->setFilterMode(Qt::MatchContains);
}

void QueryWidget_c::ExecuteSymmetricAnyToAnyQuery()
{
  QSettings
    cQSettings;

  evian::ConcurrencyMode
    eConcurrencyMode = static_cast<evian::ConcurrencyMode>(cQSettings.value(key_concurrency_mode).toInt());

  float
    rMaxDistance = ui->doubleSpinBoxMaxLY->value() != 0 ? ui->doubleSpinBoxMaxLY->value() : std::numeric_limits<float>::max();

  bool 
    bUseConcurrencyRuntime = false;

  switch (eConcurrencyMode)
  {
  case evian::Concurrency_Automatic:
    bUseConcurrencyRuntime = rMaxDistance > 30;
    break;
  case evian::Concurrency_ForceMultiThreading:
    bUseConcurrencyRuntime = true;
    break;
  case evian::Concurrency_ForceSingleThreading:
    bUseConcurrencyRuntime = false;
    break;
  }

  auto
    pcBestSymmetricTrade = QSharedPointer<evian::BestSymmetricTrade_c>(new evian::BestSymmetricTrade_c(30, bUseConcurrencyRuntime));

  _pcMainWindow->_lcDatabaseQuery.append(pcBestSymmetricTrade);
  
  pcBestSymmetricTrade->_cQueryRestriction = CreateQueryRestriction();

  CreateSymmetricModelAndStartQuery(pcBestSymmetricTrade);
}

void
QueryWidget_c::ExecuteSymmetricQueryGroupToGroup(const QList<std::reference_wrapper<const evian::System_c>>& lcSystemFrom, const QList<std::reference_wrapper<const evian::System_c>>& lcSystemTo)
{
  typedef evian::GroupToGroupQuery_c<evian::SymmetricTradeQueryImpl_c> Query_c;
  typedef evian::GroupToGroupQuery_c<evian::SymmetricSmugglingQuery_c> SmugglingQuery_c;

  QSharedPointer<evian::SymmetricTradeQuery_c>
    pcQuery;
  
  if (ui->checkBoxSmuggling->isChecked())
  {
    pcQuery.reset(new SmugglingQuery_c(lcSystemFrom, lcSystemTo));
  }
  else
  {
    pcQuery.reset(new Query_c(lcSystemFrom, lcSystemTo));
  }

  _pcMainWindow->_lcDatabaseQuery.append(pcQuery);

  pcQuery->SetQueryRestriction(CreateQueryRestriction());

  if (lcSystemFrom.size() == 1 && lcSystemTo.size() == 1)
  {
    pcQuery->_cQueryRestriction._rMaxDistance = std::numeric_limits<float>::max();
    pcQuery->_cQueryRestriction._rMaxDistanceToStar = std::numeric_limits<float>::max();
    _pcMainWindow->ShowStatusMessage("Ignoring Max Distance since only two systems are part of query");
  }
  
  CreateSymmetricModelAndStartQuery(pcQuery);
}

void
QueryWidget_c::ExecuteDirectionalQueryGroupToGroup(const QList<std::reference_wrapper<const evian::System_c>>& lcSystemFrom, const QList<std::reference_wrapper<const evian::System_c>>& lcSystemTo)
{
  typedef evian::GroupToGroupQuery_c<evian::DirectionalTradeQueryImpl_c> Query_c;
  typedef evian::GroupToGroupQuery_c<evian::DirectionalSmugglingQuery_c> SmugglingQuery_c;

  QSharedPointer<evian::DirectionalTradeQuery_c>
    pcQuery;

  if (ui->checkBoxSmuggling->isChecked())
  {
    pcQuery.reset(new SmugglingQuery_c(lcSystemFrom, lcSystemTo)); 
  }
  else
  {
    pcQuery.reset(new Query_c(lcSystemFrom, lcSystemTo));
  }
  _pcMainWindow->_lcDatabaseQuery.append(pcQuery);

  pcQuery->SetQueryRestriction(CreateQueryRestriction());
  
  if (lcSystemFrom.size() == 1 && lcSystemTo.size() == 1)
  {
    pcQuery->_cQueryRestriction._rMaxDistance = std::numeric_limits<float>::max();
    _pcMainWindow->ShowStatusMessage("Ignoring Max Distance since only two systems are part of query");
  }

  CreateDirectionalModelAndStartQuery(pcQuery);
}

void
QueryWidget_c::CreateSymmetricModelAndStartQuery(QSharedPointer<evian::SymmetricTradeQuery_c> pcSymmetricTradeQuery)
{
  auto
    pcSymmetricTradeModel = new model::SymmetricTradeModel_c(_pcMainWindow, pcSymmetricTradeQuery);

  disconnect(ui->tableViewResults, &QTableView::customContextMenuRequested, 0, 0);

  connect(ui->tableViewResults, &QTableView::customContextMenuRequested,
          this,                 &QueryWidget_c::SymmetricTradeModelContextMenuRequested);

  _pcProxyModel->setDynamicSortFilter(true);
  _pcProxyModel->setSourceModel(pcSymmetricTradeModel);
  _pcProxyModel->setSortRole(model::SymmetricTradeModel_c::SortRole);

  ui->tableViewResults->setModel(_pcProxyModel);

  EvianDatabaseExplorerQt::SetHeaderParameters(ui->tableViewResults, _pcProxyModel->columnCount());

  _pcMainWindow->_lpcQAbstractTableModel.append(pcSymmetricTradeModel);

  auto
    pcQuery = new ExecuteQuery(this, pcSymmetricTradeQuery, *_pcMainWindow->_pcTradeDatabaseEngine);

  connect(pcQuery, &ExecuteQuery::QueryCompleted, pcSymmetricTradeModel, &model::SymmetricTradeModel_c::ResultsReady);
  connect(pcQuery, &ExecuteQuery::QueryCompleted, ui->progressBar, &QProgressBar::hide);
  connect(pcQuery, &ExecuteQuery::QueryCompleted, ui->pushButtonExecuteQuery, &QPushButton::toggle);
  connect(pcQuery, &ExecuteQuery::ElapsedTimeMessage, _pcMainWindow->statusBar(), &QStatusBar::showMessage);

  //QString
  //cNewWindowTitle = ui->comboBoxQueryType->currentText() + " (" + QString::number(ui->doubleSpinBoxMaxLY->value()) + " LY, " + QString::number(ui->doubleSpinBoxMaxLS->value()) + " ls)";

  ui->progressBar->show();

  //setWindowTitle(cNewWindowTitle);

  pcQuery->start();
}

void
QueryWidget_c::CreateDirectionalModelAndStartQuery(QSharedPointer<evian::DirectionalTradeQuery_c> pcDirectionalTradeQuery)
{
  auto
    pcDirectionalTradeModel = new model::DirectionalTradeModel_c(_pcMainWindow, pcDirectionalTradeQuery);

  disconnect(ui->tableViewResults, &QTableView::customContextMenuRequested, 0, 0);

  _pcProxyModel->setDynamicSortFilter(true);
  _pcProxyModel->setSourceModel(pcDirectionalTradeModel);
  _pcProxyModel->setSortRole(model::DirectionalTradeModel_c::SortRole);

  ui->tableViewResults->setModel(_pcProxyModel);
  EvianDatabaseExplorerQt::SetHeaderParameters(ui->tableViewResults, _pcProxyModel->columnCount());

  _pcMainWindow->_lpcQAbstractTableModel.append(pcDirectionalTradeModel);

  auto
    pcQuery = new ExecuteQuery(this, pcDirectionalTradeQuery, *_pcMainWindow->_pcTradeDatabaseEngine);

  connect(ui->tableViewResults, &QTableView::customContextMenuRequested,
          this,                 &QueryWidget_c::DirectionalTradeModelContextMenuRequested);
  connect(pcQuery, &ExecuteQuery::QueryCompleted, pcDirectionalTradeModel, &model::DirectionalTradeModel_c::ResultsReady);
  connect(pcQuery, &ExecuteQuery::QueryCompleted, ui->progressBar, &QProgressBar::hide);
  connect(pcQuery, &ExecuteQuery::QueryCompleted, ui->pushButtonExecuteQuery, &QPushButton::toggle);
  connect(pcQuery, &ExecuteQuery::ElapsedTimeMessage, _pcMainWindow->statusBar(), &QStatusBar::showMessage);

  //QString
  //cNewWindowTitle = ui->comboBoxQueryType->currentText() + " (" + QString::number(ui->doubleSpinBoxMaxLY->value()) + " LY, " + QString::number(ui->doubleSpinBoxMaxLS->value()) + " ls)";

  ui->progressBar->show();

  //setWindowTitle(cNewWindowTitle);

  pcQuery->start();
}

evian::QueryRestriction_c
QueryWidget_c::CreateQueryRestriction() const
{
  evian::QueryRestriction_c
    cQueryRestriction;

  float
    rMaxDistance = ui->doubleSpinBoxMaxLY->value() != 0 ? ui->doubleSpinBoxMaxLY->value() : std::numeric_limits<float>::max();

  cQueryRestriction._nMinSupply                      = ui->spinBoxMinimumSupply->value();
  cQueryRestriction._rMaxDistanceToStar              = ui->doubleSpinBoxMaxLS->value() != 0 ? ui->doubleSpinBoxMaxLS->value() : std::numeric_limits<float>::max();
  cQueryRestriction._rMaxDistance                    = rMaxDistance;
  cQueryRestriction._isIntraSystemTradingIgnored     = ui->checkBoxSkipIntrasystemTrading->isChecked();
  cQueryRestriction._isIgnoreStationsWithoutDistance = ui->checkBoxIgnoreStationsWithoutDistance->isChecked();
  cQueryRestriction._isIgnorePlanetaryOutposts       = ui->checkBoxIgnorePlanetaryOutposts->isChecked();
  cQueryRestriction._rMinProfit                      = ui->spinBoxMinProfit->value();
  cQueryRestriction._nMaxResults                     = ui->spinBoxMaxResults->value();
  cQueryRestriction._isCommodityExcluded             = _cQueryRestrictionDatabase._lisCommodityExcluded;
  cQueryRestriction._lnExcludedStations              = _cQueryRestrictionDatabase._lnExcludedStations;
  cQueryRestriction._lnExcludedSystems               = _cQueryRestrictionDatabase._lnExcludedSystems;
  cQueryRestriction._lsExcludedFactions              = _cQueryRestrictionDatabase._lsExcludedFactions;
  cQueryRestriction._eMinLandingPadSize              = static_cast<evian::Station_c::LandingPadSize>(ui->comboBoxMinLandingPad->currentIndex());
  cQueryRestriction._eMaxLandingPadSize              = static_cast<evian::Station_c::LandingPadSize>(ui->comboBoxMaxLandingPad->currentIndex());

  if (!IsAnyLocation(ui->lineEditEndLocation->text()))
  {
    auto
      cQStringList = ui->lineEditEndLocation->text().split(QueryWidget::SPLIT);

      if (cQStringList.size() == 2)
      {
        AddOtherStationsInSystemToIgnoreList(cQueryRestriction, cQStringList[0].trimmed(), cQStringList[1].trimmed());
      }
  }

  if (!IsAnyLocation(ui->lineEditStartLocation->text()))
  {
    auto
      cQStringList = ui->lineEditStartLocation->text().split(QueryWidget::SPLIT);

    if (cQStringList.size() == 2)
    {
      AddOtherStationsInSystemToIgnoreList(cQueryRestriction, cQStringList[0].trimmed(), cQStringList[1].trimmed());
    }
  }

  return cQueryRestriction;
}


void
QueryWidget_c::on_pushButtonExecuteQuery_clicked()
{
  ui->pushButtonExecuteQuery->setChecked(true);

  DisableBackgroundImage();
  switch (_eQueryType)
  {
    case Query_Loop:
      SetupAndPerformSymmetricQuery();
      break;
    case Query_Hop:
      SetupAndPerformDirectionalQuery();
      break;
    default:
      qDebug() << "Unhandled Query_Type";
      ui->pushButtonExecuteQuery->setChecked(false);
  }
}

void
QueryWidget_c::SetupAndPerformDirectionalQuery()
{
  auto
    lcSystemFrom = GetFromGroup(),
    lcSystemTo   = GetToGroup();

  SetWindowAndTabTitle(FormatLocation(ui->lineEditStartLocation->text()) + " -> " + FormatLocation(ui->lineEditEndLocation->text()) + FormatSystemDistance());

  ExecuteDirectionalQueryGroupToGroup(lcSystemFrom, lcSystemTo);
}


void
QueryWidget_c::AddOtherStationsInSystemToIgnoreList(evian::QueryRestriction_c& cQueryRestriction, QString cStationName, QString cSystemName) const
{
  // Now add the others stations in the system as excluded stations in the query-restriction.
  const auto
    &cSystem = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetSystem(cSystemName);

  for (const auto pcStation : cSystem._lpcStation)
  {
    if (pcStation->_cName != cStationName)
    {
      cQueryRestriction._lnExcludedStations.push_back(pcStation->_nID);
    }
  }

  std::sort(cQueryRestriction._lnExcludedStations.begin(), cQueryRestriction._lnExcludedStations.end());
}

bool QueryWidget_c::IsAnyLocation(const QString& cQString) const
{
  return cQString == QueryWidget::ANY_LOCATION || cQString.isEmpty();
}

QList<std::reference_wrapper<const evian::System_c>>
QueryWidget_c::GetFromGroup()
{
  auto 
    cQString = CompleterListCache_c::DeduceLocationFromCompleter(ui->lineEditStartLocation);

  return GetSystemGroup(cQString);
}

QList<std::reference_wrapper<const evian::System_c>>
QueryWidget_c::GetToGroup()
{
  auto 
    cQString = CompleterListCache_c::DeduceLocationFromCompleter(ui->lineEditEndLocation);

  return GetSystemGroup(cQString);
}

QList<std::reference_wrapper<const evian::System_c>> QueryWidget_c::GetSystemGroup(const QString& cQString)
{
  QList<std::reference_wrapper<const evian::System_c>>
    lcSystem;

  const auto&
    cFactionToSystemMap = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase()._cFactionToSystemMap;

  using namespace QueryWidget;

  if (IsAnyLocation(cQString))
  {
    lcSystem = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase()._lcSystemWithStation;
  } 
  else if (cFactionToSystemMap.contains(RemoveFactionPrefix(cQString)))
  {
    lcSystem = cFactionToSystemMap.value(RemoveFactionPrefix(cQString));
  }
  else
  {
    auto
      cSystemName = DeduceSystemName(cQString);
    
    const auto&
      cSystem = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetSystem(cSystemName);

    lcSystem.append(cSystem);
  }

  return lcSystem;
}

void
QueryWidget_c::SetDefaultValuesFromSettings()
{
  QSettings
    cQSettings;

  ui->doubleSpinBoxMaxLS->setValue(cQSettings.value(key_query_star_distance, "1000").toDouble());
  ui->doubleSpinBoxMaxLY->setValue(cQSettings.value(key_query_system_distance, "10").toDouble());
  ui->comboBoxMaxLandingPad->setCurrentIndex(cQSettings.value(key_query_max_landing_pad, 2).toInt());
  ui->comboBoxMinLandingPad->setCurrentIndex(cQSettings.value(key_query_min_landing_pad, 0).toInt());
  ui->spinBoxMinimumSupply->setValue(cQSettings.value(key_query_min_supply, "100").toInt());
  ui->spinBoxMinProfit->setValue(cQSettings.value(key_query_min_profit, "100").toInt());
  ui->checkBoxIgnorePlanetaryOutposts->setChecked(cQSettings.value(key_query_ignore_planetary_outposts, false).toBool());
}

void
QueryWidget_c::EnableBackgroundImage()
{
  QDir
    cQDir(":/assets/ships/");

  int
    nImage = cQDir.entryList().size(),
    iImage = qrand() % nImage;
  
  auto
    //sUrl = QString("background-image: url(:/assets/ships/%1)").arg(cQDir.entryList()[iImage]);
    sUrl = QString("border-image: url(:/assets/ships/%1) 0 0 0 0 stretch stretch;").arg(cQDir.entryList()[iImage]);
  //qDebug() << sUrl;

  ui->tableViewResults->setStyleSheet(sUrl);
}

void
QueryWidget_c::DisableBackgroundImage()
{
  ui->tableViewResults->setStyleSheet("");
}

void
CompleterListCache_c::ConsiderModifyCompleter(const QString & cQString, QCompleter * pcQCompleter, QLineEdit* pcQLineEdit, CompleterListCache_c * pcCompleterListCache)
{
  const int
    nKey = 3;

  if (cQString.length() < nKey)
  {
    qDebug() << "Short key";
    if (pcQCompleter != NULL)
    {
      qDebug() << "Deleting completer";
      pcQLineEdit->setCompleter(0);
      pcQCompleter->deleteLater();
      pcQCompleter = NULL;
    }
  }
  else if (cQString.length() == nKey)
  {
    qDebug() << "Long key";
    auto
      cQStringList = pcCompleterListCache->GetStringList(cQString.toLower());

    pcQCompleter = new QCompleter(cQStringList, pcQLineEdit->parent());
    SetupQCompleter(pcQCompleter);
    pcQLineEdit->setCompleter(pcQCompleter);
  }
}

void
QueryWidget_c::SetupAndPerformSymmetricQuery()
{
  if (IsAnyLocation(ui->lineEditEndLocation->text()) && IsAnyLocation(ui->lineEditStartLocation->text()))
  {
    SetWindowAndTabTitle("Any <-> Any " + FormatSystemDistance());
    ExecuteSymmetricAnyToAnyQuery();
  }
  else
  {
    auto
      lcSystemFrom = GetFromGroup(),
      lcSystemTo   = GetToGroup();
   
    SetWindowAndTabTitle(FormatLocation(ui->lineEditStartLocation->text()) + " <-> " + FormatLocation(ui->lineEditEndLocation->text()) + FormatSystemDistance());
    ExecuteSymmetricQueryGroupToGroup(lcSystemFrom, lcSystemTo);
  }
}

void
QueryWidget_c::InitTableViewResults()
{
  ui->tableViewResults->setContextMenuPolicy(Qt::CustomContextMenu);
  ui->tableViewResults->horizontalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);
  
  connect(ui->tableViewResults->horizontalHeader(), &QWidget::customContextMenuRequested,
          this,                                     &QueryWidget_c::HeaderContextMenuRequested);

  //connect(ui->tableViewResults, &QTableView::customContextMenuRequested,
  //        this,                 &QueryWidget_c::SymmetricTradeModelContextMenuRequested);

  connect(this,                        &QueryWidget_c::ExcludeCommodity,
          &_cQueryRestrictionDatabase, &evian::QueryExclusionDatabase_c::AddExcludedCommodity);
  connect(this,                        &QueryWidget_c::ExcludeSystem,
          &_cQueryRestrictionDatabase, &evian::QueryExclusionDatabase_c::AddExcludedSystem);
  connect(this,                        &QueryWidget_c::ExcludeStation,
          &_cQueryRestrictionDatabase, &evian::QueryExclusionDatabase_c::AddExcludedStation);
  connect(this,                        &QueryWidget_c::ExcludeFaction,
          &_cQueryRestrictionDatabase, &evian::QueryExclusionDatabase_c::AddExcludedFaction);
  connect(this,                        &QueryWidget_c::ClearAllRestrictions,
          &_cQueryRestrictionDatabase, &evian::QueryExclusionDatabase_c::ClearAllExclusions);

  connect(this,                        &QueryWidget_c::ClearAllRestrictions,
          &_cRestrictionDatabaseView,  &RestrictionDatabaseView_c::ClearAllExclusions);


}

void
QueryWidget_c::HeaderContextMenuRequested(QPoint pos)
{
  QHeaderView*
    pcQHeaderView = ui->tableViewResults->horizontalHeader();

  qDebug() << pcQHeaderView->count();
  qDebug() << "ui->tableViewResults->width(): " << ui->tableViewResults->width();
  auto 
    nColumn = pcQHeaderView->logicalIndexAt(pos);

  auto
    pcQMenu = new QMenu(this);

  auto
    cQString = _pcProxyModel->sourceModel()->headerData(nColumn, Qt::Horizontal).toString();

  auto
    pcQActionHideSelected = new QAction(QString("Hide column ") + cQString, pcQMenu),
    pcQActionResizeVisible = new QAction(QString("Resize visible columns"), pcQMenu),
    pcQActionShowAll = new QAction(QString("Show all headers"), pcQMenu);

  connect(pcQActionHideSelected, &QAction::triggered,
          [this,nColumn]
          {
            ui->tableViewResults->horizontalHeader()->hideSection(nColumn);
          });
  
  connect(pcQActionResizeVisible, &QAction::triggered,
          [pcQHeaderView]
          {
            auto
              nVisibleColumns = 0;
            for (auto i = 0; i < pcQHeaderView->count(); ++i)
            {
              if (!pcQHeaderView->isSectionHidden(i))
              {
                nVisibleColumns++;
              }
            }
            qDebug() << "pcQHeaderView->width(): " << pcQHeaderView->width();
            //pcQHeaderView->setDefaultSectionSize(pcQHeaderView->width() / nVisibleColumns);
            auto
              nDefaultWidth = pcQHeaderView->width() / nVisibleColumns;
            for (auto i = 0; i < pcQHeaderView->count(); ++i)
            {
              if (!pcQHeaderView->isSectionHidden(i))
              {
                pcQHeaderView->resizeSection(i, nDefaultWidth);
              }
            }
          });

  connect(pcQActionShowAll, &QAction::triggered,
          [this] 
          {
            for (auto i = 0; i < ui->tableViewResults->horizontalHeader()->count(); ++i)
            {
              ui->tableViewResults->horizontalHeader()->setSectionHidden(i, false);
            }
          });

  pcQMenu->addAction(pcQActionHideSelected);
  pcQMenu->addAction(pcQActionResizeVisible);
  pcQMenu->addAction(pcQActionShowAll);

  pcQMenu->popup(ui->tableViewResults->horizontalHeader()->viewport()->mapToGlobal(pos));

  connect(pcQMenu, &QMenu::aboutToHide,
          pcQMenu, &QMenu::deleteLater);
}

void
QueryWidget_c::DirectionalTradeModelContextMenuRequested(QPoint pos)
{
  QModelIndex 
    index  = _pcProxyModel->mapToSource(ui->tableViewResults->indexAt(pos));

  auto
    nRow = index.row();

  qDebug() << ui->tableViewResults->indexAt(pos);
  qDebug() << index;

  using namespace model::DirectionalTradeModel;

  auto
    pSourceModel = _pcProxyModel->sourceModel();

  if (!pSourceModel)
  {
    return; // Source model not set yet.
  }

  EVIAN_ASSERT(dynamic_cast<model::DirectionalTradeModel_c*>(pSourceModel) != NULL);

  QString
    cCommodity  = _pcProxyModel->sourceModel()->data(index).toString(),
    cSystem1    = _pcProxyModel->sourceModel()->data(index.sibling(nRow, Column_SystemBegin)).toString();

  auto
    pcQMenu = new QMenu(this);

  QList<Column>
    leColumn;

  leColumn << Column_SystemBegin << Column_StationBegin << Column_SystemEnd << Column_StationEnd << Column_Commodity;

  QMap<Column, QAction*>
    cQMap;

  for (auto eColumn: leColumn)
  {
    QString 
      s = pSourceModel->data(index.sibling(nRow, eColumn)).toString();

    auto
      pcQAction = new QAction( QString("Exclude %1").arg(s), pcQMenu);

    cQMap[eColumn] = pcQAction;
    switch (eColumn) 
    {
    case Column_SystemBegin:
    case Column_SystemEnd:
      connect(pcQAction, &QAction::triggered,
        [s, this]() 
      {
        emit ExcludeSystem(s);
      });
      break;
    case Column_StationBegin:
    case Column_StationEnd:
      pcQAction->setText(QString("Exclude %2/%1").arg(pSourceModel->data(index.sibling(nRow, eColumn+11)).toString()).arg(s));

      connect(pcQAction, &QAction::triggered,
        [s, this, eColumn, pSourceModel, index, nRow]() 
      {
        QString
          cSystem = pSourceModel->data(index.sibling(nRow, eColumn+1)).toString();
        emit ExcludeStation(cSystem, s);
      });
      break;
    case Column_Commodity:
      connect(pcQAction, &QAction::triggered,
        [s, this]() 
      {
        emit ExcludeCommodity(s);
      });
      break;
    default:
      EVIAN_ASSERT("Unhandled column in action create list");
    }
  }

  auto
    pcActionClearAll = CreateClearAllAction(pcQMenu);

  for (auto pcAction: cQMap.values())
  {
    pcQMenu->addAction(pcAction);
  }
  pcQMenu->addSeparator();

  pcQMenu->addAction(pcActionClearAll);
  pcQMenu->popup(ui->tableViewResults->viewport()->mapToGlobal(pos));

  connect(pcQMenu, &QMenu::aboutToHide,
    pcQMenu, &QMenu::deleteLater);
}

QAction*
QueryWidget_c::CreateClearAllAction(QMenu* pcQMenu)
{
  auto
    pcActionClearAll = new QAction("Clear all restrictions", pcQMenu);

  connect(pcActionClearAll, &QAction::triggered,
    this,             &QueryWidget_c::ClearAllRestrictions);

  return pcActionClearAll;
}

void
QueryWidget_c::RestrictionContextMenuRequested(QPoint pos)
{
  auto
    pcQMenu = new QMenu("Add Restrictions", this),
    pcQMenuFactions = new QMenu("Factions", pcQMenu),
    pcQMenuCommodities = new QMenu("Commodities", pcQMenu);

  const auto&
    cFactionToSystemMap = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase()._cFactionToSystemMap;

  QHashIterator<QString, QList<std::reference_wrapper<const evian::System_c>>>
    it(cFactionToSystemMap);

  while (it.hasNext())
  {
    it.next();

    auto
      sFaction = it.key();

    auto
      pcQAction = new QAction(QString("Exclude %1").arg(sFaction), pcQMenuFactions);

    connect(pcQAction, &QAction::triggered,
      [this, sFaction]() 
    {
      emit ExcludeFaction(sFaction);
    });
    
    pcQMenuFactions->addAction(pcQAction);
  }

  pcQMenu->addMenu(pcQMenuFactions);

  const auto&
    lcCommodity = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase()._lcCommodity;

  for (const auto& cCommodity : lcCommodity)
  {
    auto
      sCommodity = cCommodity._cName;

    auto pcQAction = new QAction(QString("Exclude %1").arg(sCommodity), pcQMenuCommodities);

    connect(pcQAction, &QAction::triggered,
      [this, sCommodity]() 
    {
      emit ExcludeCommodity(sCommodity);
    });

    pcQMenuCommodities->addAction(pcQAction);
  }

  pcQMenu->addMenu(pcQMenuCommodities);

  pcQMenu->addAction(CreateClearAllAction(pcQMenu));

  pcQMenu->popup(ui->groupBoxRestrictions->mapToGlobal(pos));

  connect(pcQMenu, &QMenu::aboutToHide,
    pcQMenu, &QMenu::deleteLater);
}

void 
QueryWidget_c::SymmetricTradeModelContextMenuRequested(QPoint pos)
{
  QModelIndex 
    index  = _pcProxyModel->mapToSource(ui->tableViewResults->indexAt(pos));

  auto
    nRow = index.row();

  qDebug() << ui->tableViewResults->indexAt(pos);
  qDebug() << index;

  using namespace model::SymmetricTradeModel;

  auto
    pSourceModel = _pcProxyModel->sourceModel();

  if (!pSourceModel)
  {
    return; // Source model not set yet.
  }

  QString
    cCommodity  = _pcProxyModel->sourceModel()->data(index).toString(),
    cSystem1    = _pcProxyModel->sourceModel()->data(index.sibling(nRow, Column_System1)).toString();

  auto
    pcQMenu = new QMenu(this);

  QList<Column>
    leColumn;

  leColumn << Column_System1 << Column_Station1 << Column_System2 << Column_Station2 << Column_Commodity1 << Column_Commodity2;

  QMap<Column, QAction*>
    cQMap;

  for (auto eColumn: leColumn)
  {
    QString 
      s = pSourceModel->data(index.sibling(nRow, eColumn)).toString();

    auto
      pcQAction = new QAction( QString("Exclude %1").arg(s), pcQMenu);

    cQMap[eColumn] = pcQAction;
    switch (eColumn) 
    {
    case Column_System1:
    case Column_System2:
      connect(pcQAction, &QAction::triggered,
        [s, this]() 
      {
        emit ExcludeSystem(s);
      });
      break;
    case Column_Station1:
    case Column_Station2:
      pcQAction->setText(QString("Exclude %2/%1").arg(pSourceModel->data(index.sibling(nRow, eColumn+1)).toString()).arg(s));

      connect(pcQAction, &QAction::triggered,
        [s, this, eColumn, pSourceModel, index, nRow]() 
      {
        QString
          cSystem = pSourceModel->data(index.sibling(nRow, eColumn+1)).toString();
        emit ExcludeStation(cSystem, s);
      });
      break;
    case Column_Commodity1:
    case Column_Commodity2:
      connect(pcQAction, &QAction::triggered,
        [s, this]() 
      {
        emit ExcludeCommodity(s);
      });
      break;
    default:
      EVIAN_ASSERT("Unhandled column in action create list");
    }
  }

  auto
    pcActionClearAll = CreateClearAllAction(pcQMenu);

  for (auto pcAction: cQMap.values())
  {
    pcQMenu->addAction(pcAction);
  }
  pcQMenu->addSeparator();

  pcQMenu->addAction(pcActionClearAll);
  pcQMenu->popup(ui->tableViewResults->viewport()->mapToGlobal(pos));

  connect(pcQMenu, &QMenu::aboutToHide,
    pcQMenu, &QMenu::deleteLater);
}

void
QueryWidget_c::TradeDetailsRequested(QModelIndex cQModelIndexProxy)
{
  auto
    pcSymmetricTradeModel = dynamic_cast<model::SymmetricTradeModel_c*>(_pcProxyModel->sourceModel());

  auto
    pcDirectionalTradeModel = dynamic_cast<model::DirectionalTradeModel_c*>(_pcProxyModel->sourceModel());
  
  if (pcSymmetricTradeModel || pcDirectionalTradeModel)
  {
    QModelIndex 
      index  = _pcProxyModel->mapToSource(cQModelIndexProxy);

    auto // Let them be a child of mainwindow, so they do not disappear when the query-widget is closed.
      pcTradeDetailsWidget = new TradeDetailWidget_c(_pcMainWindow, _pcMainWindow); 

    connect(_pcMainWindow->_pcTradeDatabaseEngine, &evian::TradeDatabaseEngine_c::ListingUpdated,
            pcTradeDetailsWidget, &TradeDetailWidget_c::UpdateListing);
    
    connect(_pcMainWindow, &MainWindow::CurrentSystemChanged,
            pcTradeDetailsWidget, &TradeDetailWidget_c::UpdateCurrentSystem);
    
    if (pcSymmetricTradeModel)
    {
      pcTradeDetailsWidget->SetSymmetricTrade(pcSymmetricTradeModel->_pcDatabaseQuery->_lcSymmetricTrade[index.row()]);
    }
    else
    {
      pcTradeDetailsWidget->SetDirectionalTrade(pcDirectionalTradeModel->_pcDatabaseQuery->_lcBestTrade[index.row()]);
    }
    
    pcTradeDetailsWidget->show();
  }
  else
  {
    qDebug() << "Ignoring TradeDetailsRequested due to invalid cast on sourceModel.";
  }
}

void
QueryWidget_c::on_pushButtonSwapLocations_clicked()
{
  auto 
    cQStringEndLocation = ui->lineEditEndLocation->text(),
    cQStringStartLocation = ui->lineEditStartLocation->text();

  ui->lineEditEndLocation->setText(cQStringStartLocation);
  ui->lineEditStartLocation->setText(cQStringEndLocation);
}

void
QueryWidget_c::on_checkBoxAdvancedSettings_clicked(bool checked)
{
  // We need to hide widgetMainOptions to avoid ugly resize-flickering when hiding groupBoxAdvancedOptions.
  qDebug() << ui->groupBoxAdvancedOptions->size();

  ui->widgetMainOptions->hide();
  ui->groupBoxAdvancedOptions->setVisible(checked);
  ui->widgetMainOptions->show();
}

void
QueryWidget_c::SliderMoved(int i)
{
  qDebug() << __FUNCTION__ << " " << i;
}

RestrictionDatabaseView_c::RestrictionDatabaseView_c(MainWindow * pcMainWindow, QWidget * parent) :
  QObject(parent),
  _pcMainWindow(pcMainWindow),
  _pcQLayout(0),
  _pcQWidgetParent(parent)
{
}

void RestrictionDatabaseView_c::SetLayout(QLayout * pcQLayout)
{
  _pcQLayout = pcQLayout;
}

void
RestrictionDatabaseView_c::AddExludedCommodity(QString cCommodityName)
{
  EVIAN_ASSERT(_pcQLayout != 0);

  QIcon cQIcon(":/assets/icons/boxes6.svg");

  auto
    pcQPushButton = new QPushButton(cQIcon, cCommodityName, _pcQWidgetParent);

  pcQPushButton->setProperty("evian_commodity", true);
  pcQPushButton->setProperty("evian_tag", true);

  connect(pcQPushButton, &QPushButton::clicked,
    [cCommodityName, this]()
  {
    emit IncludeCommodity(cCommodityName);
  });

  SetupClickedConnections(pcQPushButton);
  _pcQLayout->addWidget(pcQPushButton);
}

void
RestrictionDatabaseView_c::AddExcludedSystem(QString cSystemName)
{
  EVIAN_ASSERT(_pcQLayout != 0);

  QIcon cQIcon(":/assets/icons/saturn7.svg");

  auto
    pcQPushButton = new QPushButton(cQIcon, cSystemName, _pcQWidgetParent);

  pcQPushButton->setProperty("evian_system", true);
  pcQPushButton->setProperty("evian_tag", true);
  
  connect(pcQPushButton, &QPushButton::clicked,
    [cSystemName, this]()
  {
    emit IncludeSystem(cSystemName);
  });
  
  SetupClickedConnections(pcQPushButton);

  _pcQLayout->addWidget(pcQPushButton);
}

void
RestrictionDatabaseView_c::AddExcludedStation(QString cSystemName, QString cStationName)
{
  EVIAN_ASSERT(_pcQLayout != 0);

  QString
    cQStringLabel = QString("%1 (%2)").arg(cStationName).arg(cSystemName);

  const auto&
    cStation = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetStation(cStationName, cSystemName);

  QIcon 
    cQIcon = model::GetIconForStation(cStation);

  auto
    pcQPushButton = new QPushButton(cQIcon, cQStringLabel, _pcQWidgetParent);

  pcQPushButton->setProperty("evian_station", true);
  pcQPushButton->setProperty("evian_tag", true);

  connect(pcQPushButton, &QPushButton::clicked,
    [cSystemName, cStationName, this]()
    {
      emit IncludeStation(cSystemName, cStationName);
    });

  SetupClickedConnections(pcQPushButton);
  
  _pcQLayout->addWidget(pcQPushButton);
}

void
RestrictionDatabaseView_c::AddExcludedFaction(QString sFaction)
{

  QIcon
    cQIcon(":/assets/factions/" + sFaction + ".svg");

  auto
    pcQPushButton = new QPushButton(cQIcon, sFaction, _pcQWidgetParent);

  pcQPushButton->setProperty("evian_faction", true);
  pcQPushButton->setProperty("evian_tag", true);

  connect(pcQPushButton, &QPushButton::clicked,
    [sFaction, this]()
    {
      emit IncludeFaction(sFaction);
    });
  SetupClickedConnections(pcQPushButton);

  _pcQLayout->addWidget(pcQPushButton);
}


void
RestrictionDatabaseView_c::ClearAllExclusions()
{
  //for (auto pcQWidget : _pcQLayout->children())
  while (_pcQLayout->count() > 0)
  {
    auto
      pcQWidget = _pcQLayout->itemAt(0)->widget();

    _pcQLayout->removeItem(_pcQLayout->itemAt(0));

    if (!pcQWidget)
    {
      continue;
    }

    auto
      pcQPushButton = dynamic_cast<QPushButton*>(pcQWidget);
    
    if (pcQPushButton)
    {
      pcQPushButton->hide();
    }

    pcQWidget->deleteLater();
  }
}

void
RestrictionDatabaseView_c::SetupClickedConnections(QPushButton * pcQPushButton)
{
  connect(pcQPushButton, &QPushButton::clicked,
          pcQPushButton, &QPushButton::hide);
  connect(pcQPushButton, &QPushButton::clicked,
          pcQPushButton, &QPushButton::deleteLater);
}

void
QueryWidget_c::on_pushButtonSetCurrentSystem_clicked()
{
    auto
        nCurrentSystemID = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetSystemID(_pcMainWindow->GetCurrentSystem());

      if (nCurrentSystemID != -1) // Compute distance from current system
      {
        const auto
          &cCurrentSystem = _pcMainWindow->_pcTradeDatabaseEngine->GetTradeDatabase().GetSystem(nCurrentSystemID);
        ui->lineEditStartLocation->setText(cCurrentSystem._cName);
      }
}

void
QueryWidget_c::on_lineEditStartLocation_textEdited(const QString &cQString)
{
  CompleterListCache_c::ConsiderModifyCompleter(cQString, ui->lineEditStartLocation->completer(), ui->lineEditStartLocation, &_cCompleterListCache);
}

void CompleterListCache_c::SetQStringList(const QStringList& cQStringList)
{
  _cQStringList = cQStringList;
}

QStringList
CompleterListCache_c::GetStringList(const QString& sKey)
{
  EVIAN_ASSERT(sKey.toLower() == sKey);

  auto
    it = _cQHash.find(sKey);

  if (it != _cQHash.end())
  {
    return *it;
  }
  
  for(const auto& cQString : _cQStringList)
  {
    if (cQString.toLower().contains(sKey))
    {
      _cQHash[sKey].append(cQString);
    }
  }

  return _cQHash[sKey];
}

void
QueryWidget_c::on_lineEditEndLocation_textEdited(const QString &arg1)
{
  CompleterListCache_c::ConsiderModifyCompleter(arg1, ui->lineEditEndLocation->completer(), ui->lineEditEndLocation, &_cCompleterListCache);
}

QString
QueryWidget::RemoveFactionPrefix(const QString& cQString)
{
  return QString(cQString).remove(QueryWidget::FACTION_PREFIX);
}

void
QueryWidget_c::on_doubleSpinBoxMaxLY_valueChanged(double arg1)
{
  QSettings
    cQSettings;

  cQSettings.setValue(key_query_system_distance, arg1);
}

void
QueryWidget_c::on_doubleSpinBoxMaxLS_valueChanged(double arg1)
{
  QSettings
    cQSettings;

  cQSettings.setValue(key_query_star_distance, arg1);
}

void
QueryWidget_c::on_comboBoxMinLandingPad_activated(int index)
{
  QSettings
    cQSettings;

  cQSettings.setValue(key_query_min_landing_pad, index);
}

void
QueryWidget_c::on_comboBoxMaxLandingPad_activated(int index)
{
  QSettings
    cQSettings;

  cQSettings.setValue(key_query_max_landing_pad, index);
}

void QueryWidget_c::on_spinBoxMinProfit_valueChanged(int arg1)
{
  QSettings
    qSettings;

  qSettings.setValue(key_query_min_profit, arg1);
}

void QueryWidget_c::on_spinBoxMinimumSupply_valueChanged(int arg1)
{
  QSettings
    qSettings;

  qSettings.setValue(key_query_min_supply, arg1);
}

void QueryWidget_c::on_checkBoxIgnorePlanetaryOutposts_toggled(bool checked)
{
  QSettings
    qSettings;

  qSettings.setValue(key_query_ignore_planetary_outposts, checked);
}
