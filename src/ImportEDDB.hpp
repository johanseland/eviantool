//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#pragma once
#include <string>
#include <QSharedPointer>
#include "evian.hpp"

namespace evian {

class TradeDatabaseStorage_c;

class ImportEDDB_c
{
public:
  ImportEDDB_c(TradeDatabaseStorage_c& cTradeDataBase);

  void ParseCommodities(std::string FileName);
  void ParseSystems(std::string FileName);
  void ParseStations(std::string FileName);
  
  struct EDDBV4Context_c
  {
    QString     _sSystems,
                _sStations,
                _sCommodities,
                _sModules,
                _sListings;
  };

  struct CreateTradeDatabaseResult_c
  {
    enum ResultStatus_e { Result_Success, Result_Failure };
    
    ResultStatus_e
                _eResultStatus;
    
    QString     _sErrorMessage;

    QSharedPointer<evian::TradeDatabaseStorage_c>
                _pcTradeDatabaseStorage;
  };
  //static QSharedPointer<evian::TradeDatabaseStorage_c> CreateTradeDatabaseFromEDDB(QString cCommoditiesFilename, QString cStationFilename, QString cSystemFilename);

  static CreateTradeDatabaseResult_c 
                CreateTradeDatabaseFromEDDB(QString cCommoditiesFilename, QString cStationFilename, QString cSystemFilename);
  static CreateTradeDatabaseResult_c 
                CreateTradeDatabaseFromEDDBV4(EDDBV4Context_c EDDBV4Context_c);
  void          ParseSystemsV4(QString sSystems);

private:
  std::vector<Station_c>&
    _lcStation;
  std::vector<System_c>&
    _lcSystem;
  std::vector<CommodityCatergory_c>&
    _lcCommodityCategory;
  std::vector<Commodity_c>&
    _lcCommodity;
};
}