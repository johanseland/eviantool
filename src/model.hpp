//The MIT License (MIT)
//
// Copyright (c) 2015 Johan Seland
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef MODEL_HPP
#define MODEL_HPP

#include "evian.hpp"
#include "database.hpp"
#include <QIcon>

class MainWindow;

namespace evian
{
  class SymmetricTradeQuery_c;
  class DirectionalTradeQuery_c;
  class CommodityPriceAnalysisQuery_c;
}

EVIAN_BEGIN_NAMESPACE(model)

QIcon   GetFactionIcon(const QString& sFaction);
QIcon   GetFactionIcon(const evian::Station_c& cStation);
QIcon   GetFactionIcon(const evian::System_c& cSystem);
QIcon   GetPowerIcon(const evian::System_c& cSystem);
QIcon   GetIconForSystem(const evian::System_c& cSystem);
QString GetIconPathForStation(const evian::Station_c& cStation);
QIcon   GetIconForStation(const evian::Station_c& cStation);
QString FormatStationTooltip(const evian::Station_c& cStation);
QString FormatSystemTooltip(const evian::System_c& cSystem);
QString FormatCommodityTooltip(const evian::Commodity_c& cCommodity, const evian::TradeDatabaseEngine_c& cTradeDatabaseEngine);

// Remember that the context-menu in evianqueryqt is dependent on the relative ordering of (System1, Station1) and (System2, Station2)
#define SYMMETRIC_TRADE_MODEL_KEYS  \
  X(TotalProfit, "Total Profit")    \
  X(Distance,    "Distance")        \
  X(Station1,    "Station")         \
  X(System1,     "System 1")        \
  X(Distance1,   "Star Dist")       \
  X(Commodity1,  "Commodity")       \
  X(Profit1,     "Profit")          \
  X(Station2,    "Station")         \
  X(System2,     "System 2")        \
  X(Distance2,   "Star Dist")       \
  X(Commodity2,  "Commodity")       \
  X(Profit2,     "Profit")

namespace SymmetricTradeModel
{
#define X(A, B) Column_##A,
  enum Column { SYMMETRIC_TRADE_MODEL_KEYS Column_Last };
#undef X

#define X(A, B) B,
static const char* ColumnName[] = { SYMMETRIC_TRADE_MODEL_KEYS };
#undef X
}
 
class SymmetricTradeModel_c : public QAbstractTableModel
{
  Q_OBJECT
public:
  SymmetricTradeModel_c(MainWindow * parent, QSharedPointer<evian::SymmetricTradeQuery_c> pcDatabaseQuery);
  
  int rowCount(const QModelIndex &parent) const;
  
  int columnCount(const QModelIndex &parent /* = QModelIndex() */) const;
  
  QVariant data(const QModelIndex &index, int role /* = Qt::DisplayRole */) const;
  QVariant headerData(int section, Qt::Orientation orientation, int role /* = Qt::DisplayRole */) const;
  void sort(int column, Qt::SortOrder order /* = Qt::AscendingOrder */) const;
  
  QSharedPointer<evian::SymmetricTradeQuery_c>
    _pcDatabaseQuery;
  
  MainWindow *  _pcMainWindow;

	enum Role
	{
		SortRole = Qt::UserRole
	};

	public slots:
		void ResultsReady();
};

#define DIRECTIONAL_TRADE_MODEL_KEYS      \
  X(Profit,        "Profit")              \
  X(Distance,      "Distance")            \
  X(StationBegin,  "Start Station")       \
  X(SystemBegin,   "Start System")        \
  X(DistanceBegin, "Star Dist")           \
  X(Commodity,     "Commodity")           \
  X(StationEnd,    "Destination Station") \
  X(SystemEnd,     "Destination System")  \
  X(DistanceEnd,   "Star Dist")

namespace DirectionalTradeModel
{
#define X(A, B) Column_##A,
  enum Column { DIRECTIONAL_TRADE_MODEL_KEYS Column_Last };
#undef X

#define X(A, B) B,
  static const char* ColumnName[] = { DIRECTIONAL_TRADE_MODEL_KEYS };
#undef X
}

class DirectionalTradeModel_c : public QAbstractTableModel
{
  Q_OBJECT
public:
  DirectionalTradeModel_c(MainWindow * parent, QSharedPointer<evian::DirectionalTradeQuery_c> pcDatabaseQuery);

  int rowCount(const QModelIndex &parent) const;

  int columnCount(const QModelIndex &parent /* = QModelIndex() */) const;

  QVariant data(const QModelIndex &index, int role /* = Qt::DisplayRole */) const;
  QVariant headerData(int section, Qt::Orientation orientation, int role /* = Qt::DisplayRole */) const;

  QSharedPointer<evian::DirectionalTradeQuery_c>
                _pcDatabaseQuery;

  enum Role
  {
    SortRole = Qt::UserRole
  };
public slots:
  void ResultsReady();

private:
  MainWindow *  _pcMainWindow;
};

class SystemTableModel_c : public QAbstractTableModel
{
  Q_OBJECT
public:

  enum SystemTable_e
  {
    Table_All_Systems, Table_Systems_with_Stations, Table_Faction
  };

  SystemTableModel_c(SystemTable_e eSystemTable, MainWindow * pMainWindow, QObject * parent);
  int rowCount(const QModelIndex & parent) const override;
  int columnCount(const QModelIndex & parent) const override;
  QVariant data(const QModelIndex &index, int role /* = Qt::DisplayRole */) const override;
  QVariant headerData(int section, Qt::Orientation orientation, int role /* = Qt::DisplayRole */) const;

  void SetFaction(const QString& sFaction);

  enum SystemField
  {
    Field_Name, Field_Allegiance, Field_PowerControlFaction, Field_MinorFaction, Field_Government, Field_Population, Field_NeedPermit, Field_EconomicState, Field_NumStations, Field_Position, Field_Updated, SystemField_Last
  };

private:
  MainWindow *  _pcMainWindow;
  SystemTable_e _eSystemTable;
  QString       _sFaction; /// Used as key if _eSystemTable == Table_Faction;

  QString FormatPowerControlFaction(const evian::System_c& cSystem) const;
};

class EDDNItemModel_c : public QStandardItemModel
{
  Q_OBJECT
public:
  EDDNItemModel_c(const evian::TradeDatabaseEngine_c& cTradeDatabaseEngine, QObject * parent);

public slots:
  void          MessagesAdded(int iRowHint, int nMessages);

private:
  const evian::TradeDatabaseEngine_c&
                _cTradeDatabaseEngine;

  void          AppendEDDNMessage(QSharedPointer<eddn::EDDNMessage_c> pcEDDNMessage);
};

class EDDNHotspotModel_c : public QStandardItemModel
{
  Q_OBJECT
public:
  EDDNHotspotModel_c(const evian::TradeDatabaseEngine_c& cTradeDatabaseEngine, QObject * parent);

public slots:
  void          MessagesAdded(int iRowHint, int nMessages);

private:
  const evian::TradeDatabaseEngine_c&
                _cTradeDatabaseEngine;

  void          AppendEDDNMessage(QSharedPointer<eddn::EDDNMessage_c> pcEDDNMessage);

  qint64        _nEDDNMessages;
  QDateTime     _qDateTimeMessageFirst,
                _qDateTimeMessageLast;
};

class EDDNV1TableModel_c : public QAbstractTableModel
{
  Q_OBJECT
public:
  EDDNV1TableModel_c(const evian::TradeDatabaseEngine_c& cTradeDatabaseEngine, QObject * parent);

  int rowCount(const QModelIndex & parent) const override;
  int columnCount(const QModelIndex & parent) const override;
  QVariant data(const QModelIndex &index, int role /* = Qt::DisplayRole */) const override;
  QVariant headerData(int section, Qt::Orientation orientation, int role /* = Qt::DisplayRole */) const;

  enum {
    uploaderID,      
    softwareName,    
    softwareVersion, 
    gatewayTimestamp,

    systemName,      
    stationName,     
    itemName,        
    buyPrice,        
    stationStock,    
    supplyLevel,     
    sellPrice,       
    demand,          
    demandLevel,     
    timestamp,
    column_last       
  };

public slots:
  void MessagesAdded(int iRowHint, int nMessages);

private:
  const evian::TradeDatabaseEngine_c&
                _cTradeDatabaseEngine;
  int           _nRows;
};


/// Create and set up a SystemWithStationTabelModel with the appropriate connections to the database.
SystemTableModel_c * CreateSystemWithStationModel(MainWindow * pcMainWindow, QObject * pcQObject = 0);

void SetupSystemToStationComboboxes(QComboBox * pcComboBoxSystem, QComboBox * pcComboBoxStation, MainWindow * pcMainWindow, QObject * pcQObject = 0);

#define COMMODITY_ANALYSIS_MODEL_KEYS  \
X(Commodity,    "Commodity Name")      \
X(BuyMax,       "Max Buy Price")       \
X(BuyMin,       "Min Buy Price")       \
X(BuyAverage,   "Average Buy Price")   \
X(BuyMedian,    "Median Buy Price")    \
X(BuyNSamples,  "#Samples Buy")        \
X(BuySigma,     "Sigma Buy")           \
X(SellMax,      "Max Sell Price")      \
X(SellMin,      "Min Sell Price")      \
X(SellAverage,  "Average Sell Price")  \
X(SellMedian,   "Median Sell Price")   \
X(SellNSamples, "#Samples Sell")       \
X(SellSigma,    "Sigma Sell")

namespace CommodityAnalysisModel
{
#define X(A, B) Column_##A,
enum Column { COMMODITY_ANALYSIS_MODEL_KEYS Column_Last};
#undef X

#define X(A, B) B,
static const char* ColumnName[] = { COMMODITY_ANALYSIS_MODEL_KEYS };
#undef X
}

class CommodityAnalysisModel_c : public QAbstractTableModel
{
  Q_OBJECT
public:
  CommodityAnalysisModel_c(MainWindow * parent);

  void SetDatabaseQuery(QSharedPointer<evian::CommodityPriceAnalysisQuery_c> pcCommodityPriceAnalysisQuery);
  int rowCount(const QModelIndex &parent) const;
  int columnCount(const QModelIndex &parent /* = QModelIndex() */) const;

  QVariant data(const QModelIndex &index, int role /* = Qt::DisplayRole */) const;
  QVariant headerData(int section, Qt::Orientation orientation, int role /* = Qt::DisplayRole */) const;
  //void sort(int column, Qt::SortOrder order /* = Qt::AscendingOrder */) const;

  enum Role
  {
    SortRole = Qt::UserRole
  };

public slots:
    void ResultsReady();

private:
  QSharedPointer<evian::CommodityPriceAnalysisQuery_c>
                _pcDatabaseQuery;

  MainWindow *  _pcMainWindow;
};

#define COMMODITY_MAX_SELL_PRICE_MODEL_KEYS \
  X(Station,           "Station")     \
  X(System,            "System")      \
  X(SellPrice,         "Sell")        \
  X(Compare,           "Vs Avg")      \
  X(Demand,            "Demand")      \
  X(Pad,               "Pad")         \
  X(IsPermitNeeded,    "Need Permit") \
  X(Dist,              "Distance")    \
  X(TimeSinceUpdate,   "Updated")

namespace CommodityMaxSellPriceModel
{
#define X(A,B) Column_##A,
enum Column { COMMODITY_MAX_SELL_PRICE_MODEL_KEYS Column_Last};
#undef X

#define X(A, B) B,
static const char* ColumnName[] = { COMMODITY_MAX_SELL_PRICE_MODEL_KEYS };
#undef X
}

class CommodityMaxSellPriceModel_c : public QAbstractTableModel
{
  Q_OBJECT
public:
  CommodityMaxSellPriceModel_c(MainWindow * pcMainWindow, QObject * parent);
  void SetListingList(QList<std::reference_wrapper<const evian::Listing_c>>& lcListing);
  void SetRowCount(int nRow);
  void SetDatabaseQuery(QSharedPointer<evian::CommodityMinMaxAnalysisQuery_c> pcDatabaseQuery);
  int rowCount(const QModelIndex & parent) const override;
  int columnCount(const QModelIndex & parent) const override;
  QVariant data(const QModelIndex &index, int role /* = Qt::DisplayRole */) const override;
  QVariant headerData(int section, Qt::Orientation orientation, int role /* = Qt::DisplayRole */) const;

public slots:
  void ResultsReady();

private:
  QSharedPointer<evian::CommodityMinMaxAnalysisQuery_c>
                _pcDatabaseQuery;

  MainWindow    *_pcMainWindow;
  int           _nRow;

  QList<std::reference_wrapper<const evian::Listing_c>>
                _lcListing;
};

#define COMMODITY_MIN_BUY_PRICE_MODEL_KEYS \
  X(Station,           "Station")     \
  X(System,            "System")      \
  X(BuyPrice,          "Buy")         \
  X(Compare,           "Vs Avg")      \
  X(Supply,            "Supply")      \
  X(Illicit,           "Illicit")     \
  X(IsPermitNeeded,    "Need Permit") \
  X(Pad,               "Pad")         \
  X(Dist,              "Distance")    \
  X(TimeSinceUpdate,   "Updated")

namespace CommodityMinBuyPriceModel
{
#define X(A,B) Column_##A,
  enum Column { COMMODITY_MIN_BUY_PRICE_MODEL_KEYS Column_Last};
#undef X

#define X(A, B) B,
  static const char* ColumnName[] = { COMMODITY_MIN_BUY_PRICE_MODEL_KEYS };
#undef X
}

class CommodityMinBuyPriceModel_c : public QAbstractTableModel
{
  Q_OBJECT
public:
  CommodityMinBuyPriceModel_c(MainWindow * pcMainWindow, QObject * parent);
  void SetListingList(QList<std::reference_wrapper<const evian::Listing_c>>& lcListing);
  void SetRowCount(int nRow);
  void SetDatabaseQuery(QSharedPointer<evian::CommodityMinMaxAnalysisQuery_c> pcDatabaseQuery);
  int rowCount(const QModelIndex & parent) const override;
  int columnCount(const QModelIndex & parent) const override;
  QVariant data(const QModelIndex &index, int role /* = Qt::DisplayRole */) const override;
  QVariant headerData(int section, Qt::Orientation orientation, int role /* = Qt::DisplayRole */) const;

public slots:
  void ResultsReady();

private:
  QSharedPointer<evian::CommodityMinMaxAnalysisQuery_c>
                _pcDatabaseQuery;

  MainWindow    *_pcMainWindow;
  int           _nRow;

  QList<std::reference_wrapper<const evian::Listing_c>>
    _lcListing;
};

EVIAN_END_NAMESPACE(model)

#endif // MODEL_HPP
